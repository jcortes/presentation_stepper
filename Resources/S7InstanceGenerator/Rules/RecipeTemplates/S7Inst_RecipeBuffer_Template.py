# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Communication Objects.
from S7Inst_Generic_Template import S7Inst_Generic_Template


class RecipeBuffer_Template(S7Inst_Generic_Template):

    def process(self, *params):
        self.thePlugin.writeInUABLog("processInstances in Jython for Recipes.")
        xml_config = params[1]

        recipe_info = '''// DB_RECIPES_INTERFACE
DATA_BLOCK DB_RECIPES_INTERFACE
TITLE = DB_RECIPES_INTERFACE

AUTHOR: 'ICE/SIC'
NAME: 'Recipes'
FAMILY: 'UNICOS'
STRUCT   
	ActivationTimeout : INT := %(ActivationTimeout)s;
	RecipeHeader:              STRUCT     // Recipe Header     
			RecipeIDLow:       INT;
			RecipeIDHigh:      INT;
			NbManReg:          INT;
			NbofDPAR:          INT;
			DBofDPAR:          INT;
			NbofWPAR:          INT;
			DBofWPAR:          INT;
			NbofAPAR:          INT;
			DBofAPAR:          INT;
			NbofAA:            INT;
			DBofAA:            INT;
			NbofPID:           INT;
			DBofPID:           INT;  
			SentValue:         INT;
			CRC:               INT;
			Void01:            INT;
			Void02:            INT;
			Void03:            INT;
			Void04:            INT;
			Void05:            INT;
							   END_STRUCT;
     
	RecipeStatus:              STRUCT  // Recipe Status
			RecipeIDLow:       INT;
			RecipeIDHigh:      INT;
			NbManReg:          INT;
			NbofDPAR:          INT;
			DBofDPAR:          INT;
			NbofWPAR:          INT;
			DBofWPAR:          INT;
			NbofAPAR:          INT;
			DBofAPAR:          INT;
			NbofAA:            INT;
			DBofAA:            INT;
			NbofPID:           INT;
			DBofPID:           INT;  
			RecipeStatus:      INT;
			CRCStatus:         INT;
			ErrorDetail:       INT;
			Void01:            INT;
			Void02:            INT;
			Void03:            INT;
			Void04:            INT;      
							   END_STRUCT;    
    ManRegAddr:   ARRAY[1..%(buffer_size)s] OF INT;   // ManReg01 Addresses
    ManRegVal:    ARRAY[1..%(buffer_size)s] OF INT;   // ManReg01 Values
    RequestAddr:  ARRAY[1..%(buffer_size)s] OF INT;   // Request Addresses
    RequestVal:   ARRAY[1..%(buffer_size)s] OF REAL;   // Request Values
END_STRUCT;
BEGIN
END_DATA_BLOCK

// DB_RECIPES_INTERFACE_old
DATA_BLOCK DB_RECIPES_INTERFACE_old
TITLE = DB_RECIPES_INTERFACE_old

AUTHOR: 'ICE/SIC'
NAME: 'Recipes'
FAMILY: 'UNICOS'
STRUCT   
	ActivationTimeout : INT := %(ActivationTimeout)s;
	RecipeHeader:              STRUCT     // Recipe Header     
			RecipeIDLow:       INT;
			RecipeIDHigh:      INT;
			NbManReg:          INT;
			NbofDPAR:          INT;
			DBofDPAR:          INT;
			NbofWPAR:          INT;
			DBofWPAR:          INT;
			NbofAPAR:          INT;
			DBofAPAR:          INT;
			NbofAA:            INT;
			DBofAA:            INT;
			NbofPID:           INT;
			DBofPID:           INT;  
			SentValue:         INT;
			CRC:               INT;
			Void01:            INT;
			Void02:            INT;
			Void03:            INT;
			Void04:            INT;
			Void05:            INT;
							   END_STRUCT;
     
	RecipeStatus:              STRUCT  // Recipe Status
			RecipeIDLow:       INT;
			RecipeIDHigh:      INT;
			NbManReg:          INT;
			NbofDPAR:          INT;
			DBofDPAR:          INT;
			NbofWPAR:          INT;
			DBofWPAR:          INT;
			NbofAPAR:          INT;
			DBofAPAR:          INT;
			NbofAA:            INT;
			DBofAA:            INT;
			NbofPID:           INT;
			DBofPID:           INT;  
			RecipeStatus:      INT;
			CRCStatus:         INT;
			ErrorDetail:       INT;
			Void01:            INT;
			Void02:            INT;
			Void03:            INT;
			Void04:            INT;      
							   END_STRUCT;
END_STRUCT;
BEGIN
END_DATA_BLOCK

'''
        # General Steps for the Recipes file:
        # 1. Create the DB_RECIPES with the required buffers

        params = {
            'ActivationTimeout': xml_config.getPLCParameter("RecipeParameters:ActivationTimeout"),
            'buffer_size': self.get_recipe_buffer_size(xml_config)
        }
        self.thePlugin.writeInstanceInfo("Recipes.SCL", recipe_info % params)
