# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
from time import strftime
import os

from research.ch.cern.unicos.templateshandling import IUnicosTemplate  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin  # REQUIRED
from java.lang import System
from research.ch.cern.unicos.utilities import AbsolutePathBuilder


class Example_PostProcess_Template(IUnicosTemplate):
    thePlugin = 0
    theUnicosProject = 0

    def initialize(self):
        # Get a reference to the plug-in
        self.thePlugin = APlugin.getPluginInterface()
        # Get a reference to the specs file.
        self.theUnicosProject = self.thePlugin.getUnicosProject()
        self.thePlugin.writeInUABLog("initialize Example_PostProcess_Template.")

    def check(self):
        self.thePlugin.writeInUABLog("check Example_PostProcess_Template.")

    def begin(self):
        self.thePlugin.writeInUABLog("begin in Jython for Example_PostProcess_Template.")

    def process(self):
        self.thePlugin.writeInUABLog("Starting processing Example_PostProcess_Template.")

        current_time = strftime("%Y-%m-%d %H:%M:%S")  # '2007-03-03 22:14:39'
        app_name = self.thePlugin.getApplicationName()
        plugin_id = self.thePlugin.getId()
        username = System.getProperty("user.name")

        self.thePlugin.writeDebugInUABLog("[%s] user %s run post process for application %s" %
                                          (current_time, username, app_name))

        output_file_path = AbsolutePathBuilder.getTechnicalPathParameter(plugin_id + ":OutputParameters:OutputFile")
        self.thePlugin.writeDebugInUABLog(
            self.thePlugin.getId() + ":OutputParameters:OutputFile path = " + output_file_path)

        config = self.thePlugin.getXMLConfig()
        params = config.getTechnicalParametersMap(self.thePlugin.getId() + ":OutputParameters")
        self.thePlugin.writeDebugInUABLog(self.thePlugin.getId() + ":OutputParameters map")
        for key in params.keySet():
            value = params.get(key)
            self.thePlugin.writeDebugInUABLog("%s = %s" % (key, value))

        # Get the absolute path of the specs
        spec_path = self.theUnicosProject.getSpecsPath()
        # Get the relative path of the specs
        spec_name = os.path.basename(spec_path)
        self.thePlugin.writeDebugInUABLog("Processing spec file " + spec_name)

        # Example of how to loop through all the spec sheets counting # of objects
        device_types_list = self.theUnicosProject.getAllDeviceTypes()
        for device_type in device_types_list:
            instances = device_type.getAllDeviceTypeInstances()
            self.thePlugin.writeDebugInUABLog("%s contains %s objects" %
                                              (device_type.getDeviceTypeName(), len(instances)))

        self.generate_data()

    def generate_data(self):
        # Write a text file in the output folder
        plain_text_data = "This is an example of text file."
        self.thePlugin.writeFile("example.txt", plain_text_data)
        # Write an xml file in the output folder (and verify that the content is well formed)
        xml_data = "<root><text>This is an example of xml file.</text></root>"
        self.thePlugin.writeXmlFile("example.xml", xml_data)

    def end(self):
        self.thePlugin.writeInUABLog("end in Jython for Example_PostProcess_Template.")

    def shutdown(self):
        self.thePlugin.writeInUABLog("shutdown in Jython for Example_PostProcess_Template.")
