# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Communication Objects.
from S7Inst_Generic_Template import S7Inst_Generic_Template


class CompilationBaseline_Template(S7Inst_Generic_Template):

    def process(self, unicos_project, xml_config, generate_global_files, *_):
        """
        General Steps for the Baseline compilation file:
        1. Launch the global SCL files from the Baseline
        2. Launch the DeviceType SCL files from the Baseline

        :param unicos_project:
        :param xml_config:
        :param generate_global_files: comes from "Global files scope" dropdown on Wizard.
                true = All types. false = Selected types.
        """
        global_scl = '''
(*Launch of the SCL files********************************************************)
//Data types and functions
%(recipes)s
CPC_BASE_Unicos;
CPC_TSPP_Unicos;

(*FBs**************************)
%(device_types)s
'''
        params = {
            'recipes': "",
            'device_types': ""
        }

        self.thePlugin.writeInUABLog("processInstances in Jython for CompilationBaseline.")

        if xml_config.getPLCParameter("RecipeParameters:GenerateBuffers").strip().lower() == "true":
            params['recipes'] = '''Recipes;'''

        types_to_process = self.get_types_to_process(self.thePlugin, unicos_project, xml_config, generate_global_files)
        for device_type in types_to_process:
            device_type_name = device_type.getDeviceTypeName()
            representation_name = self.thePlugin.getTargetDeviceInformationParam("RepresentationName", device_type_name)
            params['device_types'] += '''CPC_FB_%s;\n''' % representation_name

        self.thePlugin.writeInstanceInfo("1_Compilation_Baseline.INP", global_scl % params)
