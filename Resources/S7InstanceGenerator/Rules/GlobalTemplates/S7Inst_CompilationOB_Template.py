# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Communication Objects.
from S7Inst_Generic_Template import S7Inst_Generic_Template


class CompilationOB_Template(S7Inst_Generic_Template):

    def process(self, unicos_project, xml_config, generate_global_files, *_):
        """
        General Steps for the Compilation file:
        1. Starting OB (100,102)
        2. Diagnostic OB (82)
        3. Errors and Hot restart: 81, 83, 84, 101 (only S7-400)
        4. Diagnostic functions : FC_Diag1&2
        5. OB1(main cycle)
        6. Call the Recipes mechanism and provide the right values form WinCCOA
        7. OB32 (UNICOS live counter)
        8. OB35 (sampling PIDs)
        9. All other errors treatment are included in the baseline

        :param unicos_project:
        :param xml_config:
        :param generate_global_files: comes from "Global files scope" dropdown on Wizard.
                true = All types. false = Selected types.
        """
        params = {
            'OB100': "",
            'S7_400_DB': "",
            'DB_DIAGNOSTIC': "",
            'OB82': "",
            'OB1_1': "", 'OB1_2': "", 'OB1_3': "", 'OB1_4': "",
            'DB_RECIPES': "",
            'OB1_COUNTER': "", 'OB32': "",
            'OB86': "",
            'OB35': "",
            'FI': ""
        }
        plc_declaration = xml_config.getPLCDeclarations()[0]
        plc_type = str(plc_declaration.getPLCType().getValue())

        types_to_process = self.get_types_to_process(self.thePlugin, unicos_project, xml_config, generate_global_files)
        type_names_to_process = [device_type.getDeviceTypeName() for device_type in types_to_process]

        diagnostic = self.thePlugin.getPluginParameter("GeneralData:DiagnosticChannel")
        params['OB100'] = '''(*RESTART (WARM RESTART)*********************************************)
''' + self.get_OB10x(100, 'Controller' in type_names_to_process, diagnostic)

        if plc_type.startswith("S7-400"):
            params['S7_400_DB'] = self.get_s7_400_db('Controller' in type_names_to_process, diagnostic)

        FIdeviceVector = self.thePlugin.get_instances_FI("DigitalAlarm")
        fast_interlocks_present = not FIdeviceVector.isEmpty() # Fast interlock presence detected by any fast interlock digital alarm
        if fast_interlocks_present:
            params['FI'] = self.get_FI_block(unicos_project)

        # order of devices is important and hard-coded in this file
        if "DigitalInput" in type_names_to_process:
            params['OB1_1'] += '''    FC_DI();    // Digital inputs\n'''
        if "AnalogInput" in type_names_to_process:
            params['OB1_1'] += '''    FC_AI();    // Analog inputs\n'''
        if "AnalogInputReal" in type_names_to_process:
            params['OB1_1'] += '''    FC_AIR();    // Analog inputs Real\n'''
        if "Encoder" in type_names_to_process:
            params['OB1_1'] += '''    FC_ENC();    // Encoders\n'''
        if "Controller" in type_names_to_process:
            params['OB1_2'] += '''    (*Controller functions called in FC_PCO_LOGIC(). Activate only if no PID Logic declared*)\n'''
            params['OB1_2'] += '''    // FC_CONTROLLER(group:=0);// All Controllers running (option=0)\n'''
        if "ProcessControlObject" in type_names_to_process:
            params['OB1_2'] += '''    (*Process Logic Control *)\n'''
            params['OB1_2'] += '''    FC_PCO_LOGIC();       // Process Control object logic\n'''

        for device_type, instance_amount in types_to_process.iteritems():
            device_type_name = device_type.getDeviceTypeName()
            family_name = device_type.getObjectType()
            representation_name = self.thePlugin.getTargetDeviceInformationParam("RepresentationName", device_type_name)
            if family_name == "InterfaceObjectFamily":
                params['OB1_1'] += '''    FC_%s();    // %s objects\n''' % (representation_name, device_type_name)
            elif family_name == "FieldObjectFamily":
                limit_size = int(self.thePlugin.getTargetDeviceInformationParam("LimitSize", device_type_name))
                optimized = self.thePlugin.getTargetDeviceInformationParam("Optimized", device_type_name).lower()
                if optimized == 'true' and device_type_name != 'Analog':  # TODO: check if this ever executed?
                    params['OB1_3'] += '''    FB_%(repr)s_All.DB_%(repr)s_All();    // %(type)s objects\n''' % \
                        {'repr': representation_name, 'type': device_type_name}
                    if instance_amount > limit_size:
                        params['OB1_3'] += '''    FB_%(repr)s_All2.DB_%(repr)s_All2();    // %(type)s objects\n''' % \
                            {'repr': representation_name, 'type': device_type_name}
                elif device_type_name != 'Analog':
                    params['OB1_3'] += '''    FC_%(repr)s();    // %(type)s objects\n''' % \
                        {'repr': representation_name, 'type': device_type_name}
                    if instance_amount > limit_size:  # TODO FC_2() ?
                        params['OB1_3'] += '''    FC_%(repr)s2();    // %(type)s objects\n''' % \
                            {'repr': representation_name, 'type': device_type_name}
                else:   # Analog needs special treatment
                    numbersToBeGenerated = []

                    instance_list = device_type.getAllDeviceTypeInstances()
                    air_limit_size = int(self.thePlugin.getTargetDeviceInformationParam("LimitSize", "AnalogInputReal"))
                    instance_split = {1: []}
                    for instance in instance_list:
                        pos = self.get_instance_pos(unicos_project, instance, air_limit_size)
                        if pos not in instance_split:
                            instance_split[pos] = []
                        instance_split[pos].append(instance)
                    positions = instance_split.keys()
                    positions.sort()
                    for pos in positions:
                        optimized_blocks_amount = len(instance_split[pos]) / (limit_size + 1)
                        start_idx = 1 if pos == 1 else pos * 10
                        for block_number in range(optimized_blocks_amount + 1):
                            numbersToBeGenerated.append(start_idx + block_number)

                    # print:
                    for number in numbersToBeGenerated:
                        block_number = number
                        if block_number == 1:
                            block_number = ""
                        params['OB1_3'] += '''    FC_%(repr)s%(number)s();    // %(type)s objects\n''' % \
                            {'repr': representation_name, 'type': device_type_name, 'number': block_number}

        if "DigitalAlarm" in type_names_to_process:
            params['OB1_3'] += '''    FC_DA();   // DIGITAL ALARM objects\n'''
        if "AnalogAlarm" in type_names_to_process:
            params['OB1_3'] += '''    FC_AA();   // ANALOG ALARM objects\n'''
        if "DigitalOutput" in type_names_to_process:
            params['OB1_4'] += '''    FC_DO();    //Digital Outputs\n'''
        if "AnalogOutput" in type_names_to_process:
            params['OB1_4'] += '''    FC_AO();    //Analog Outputs\n'''
        if "AnalogOutputReal" in type_names_to_process:
            params['OB1_4'] += '''    FC_AOR();    //Analog Outputs REAL\n'''

        params['DB_RECIPES'] = self.get_recipe_db(xml_config)

        if plc_type.startswith("S7-300"):
            params['OB1_COUNTER'] = '''
// 1 second counter to simulate OB32 in a S7-300
UNICOS_Counter1:=UNICOS_Counter1+OB1_PREV_CYCLE;
IF UNICOS_Counter1 > 1000 THEN
    UNICOS_Counter1:= UNICOS_Counter1 -1000;
    UNICOS_LiveCounter:=UNICOS_LiveCounter+1;
END_IF;
'''
        else:
            params['OB32'] = '''
// UNICOS Live Counter 
ORGANIZATION_BLOCK OB32
VAR_TEMP
  // Counter 1 second
  Info: ARRAY[0..19] OF BYTE;
  // Temporary Variables
END_VAR
  UNICOS_LiveCounter:=UNICOS_LiveCounter+1;
  
END_ORGANIZATION_BLOCK
'''

        if diagnostic == 'true':
            params['DB_DIAGNOSTIC'] = self.get_diagnostic_db(unicos_project, types_to_process)

            params['OB82'] = '''
    DB_IO_DIAGNOSTIC.IOFlag         := OB82_IO_FLAG;
    DB_IO_DIAGNOSTIC.MDL_ADDR       := OB82_MDL_ADDR;
    DB_IO_DIAGNOSTIC.FB_diag1_Exec  := TRUE;
    FB_diag1.DB_IO_DIAGNOSTIC ();
'''
            params['OB86'] = '''
    DB_IO_DIAGNOSTIC.OB86_EV_CLASS   := OB86_EV_CLASS ;         //16#38/39 Event class 3
    DB_IO_DIAGNOSTIC.OB86_FLT_ID     := OB86_FLT_ID ;           //16#C1/C4/C5, Fault identifcation code
    DB_IO_DIAGNOSTIC.OB86_RACKS_FLTD := OB86_RACKS_FLTD;        //Racks in fault
    
    //Profibus DP Diagnostics
    IF DB_IO_DIAGNOSTIC.OB86_EV_CLASS = B#16#39 AND DB_IO_DIAGNOSTIC.OB86_FLT_ID = B#16#C4 THEN
        DB_IO_DIAGNOSTIC.OB86_DP_MasterSystem    := BYTE_TO_INT (OB86_RACKS_FLTDb[2]);
        DB_IO_DIAGNOSTIC.OB86_DP_Slave           := BYTE_TO_INT (OB86_RACKS_FLTDb[3]);
        DB_IO_DIAGNOSTIC.FC_Diag2_Exec           := TRUE;
        DB_IO_DIAGNOSTIC.OB86_DP_Slave_Error     := TRUE;
        FC_Diag2();
    END_IF;
    
    IF DB_IO_DIAGNOSTIC.OB86_EV_CLASS = B#16#38 AND DB_IO_DIAGNOSTIC.OB86_FLT_ID = B#16#C4 THEN
        DB_IO_DIAGNOSTIC.FC_Diag2_Exec         := TRUE;
        DB_IO_DIAGNOSTIC.OB86_DP_Slave_Error   := FALSE;
        FC_Diag2();
    END_IF;
    
    //Profinet IO Diagnostics
    IF DB_IO_DIAGNOSTIC.OB86_EV_CLASS = B#16#39 AND DB_IO_DIAGNOSTIC.OB86_FLT_ID = B#16#CB THEN
        Temp1 := SHR (IN:=OB86_RACKS_FLTDw[1], N:=11);                                         //shift right 11 positions to isolate bits 11 to 14
        DB_IO_DIAGNOSTIC.OB86_PN_IOSystem      := WORD_TO_INT (Temp1 AND W#16#F);                   //bits 11 to 14 of OB86_RACKS_FLTD
        DB_IO_DIAGNOSTIC.OB86_PN_Station       := WORD_TO_INT (OB86_RACKS_FLTDw[1] AND W#16#7FF);   //bits  0 to 10 of OB86_RACKS_FLTD  
        DB_IO_DIAGNOSTIC.FC_Diag2_Exec         := TRUE;
        DB_IO_DIAGNOSTIC.OB86_PN_Station_Error := TRUE;
        FC_Diag2();
    END_IF;
    
    IF DB_IO_DIAGNOSTIC.OB86_EV_CLASS = B#16#38 AND DB_IO_DIAGNOSTIC.OB86_FLT_ID = B#16#CB THEN
        DB_IO_DIAGNOSTIC.FC_Diag2_Exec         := TRUE;
        DB_IO_DIAGNOSTIC.OB86_PN_Station_Error := FALSE;
        FC_Diag2();
    END_IF;
'''

        if 'Controller' not in type_names_to_process:
            params['OB35'] = '''// Scheduling:  No controller defintion '''
        else:
            params['OB35'] = '''// PID sampling time management
ORGANIZATION_BLOCK OB35
VAR_TEMP
  OB35_EV_CLASS : BYTE ;    //Bits 0-3 = 1 (Coming event), Bits 4-7 = 1 (Event class 1)
  OB35_STRT_INF : BYTE ;    //16#36 (OB 35 has started)
  OB35_PRIORITY : BYTE ;    //11 (Priority of 1 is lowest)
  OB35_OB_NUMBR : BYTE ;    //35 (Organization block 35, OB35)
  OB35_RESERVED_1 : BYTE ;    //Reserved for system
  OB35_RESERVED_2 : BYTE ;    //Reserved for system
  OB35_PHASE_OFFSET : WORD ;    //Phase offset (msec)
  OB35_RESERVED_3 : INT ;    //Reserved for system
  OB35_EXC_FREQ : INT ;    //Frequency of execution (msec)
  OB35_DATE_TIME : DATE_AND_TIME ;    //Date and time OB35 started
  I:INT;
END_VAR
    "PID_EXEC_CYCLE" := DINT_TO_TIME(INT_TO_DINT(OB35_EXC_FREQ)); //getting OB35 sampling time
   
    LP_SCHED(TM_BASE :=PID_EXEC_CYCLE  
           ,COM_RST:= false
           ,DB_NBR :=  DB_SCHED      // IN: BLOCK_DB
           );
   
    // Reset bit activated DB_SCHED
    FOR I:=1 TO %s DO
              IF  DB_SCHED.LOOP_DAT[I].enable THEN
                    FC_CONTROLLER(group:=I);
                    DB_SCHED.LOOP_DAT[I].enable:=false;
              END_IF;
    END_FOR;
END_ORGANIZATION_BLOCK''' % self.thePlugin.getGroupMaxNb()

        output = self.process_template(".scl", params)
        self.thePlugin.writeInstanceInfo("4_Compilation_OB.scl", output)

    def get_FI_block(self, unicos_project):
        fc_block = '''%(FI_FC)s

(*%(OB_Num)s**************************************************************************)
ORGANIZATION_BLOCK %(OB_Num)s 
TITLE = '%(Type_interrupt)s Interrupt'
//
// %(Call_Type)s
//
AUTHOR: 'ICE/PLC'
NAME: '%(OB_Num)s'
FAMILY: 'UNICOS'
VAR_TEMP
    (*UNUSED VARIABLE*)
    UNUSED_VARIABLE : ARRAY [0..20] OF BYTE;
%(CI_Vars)s
END_VAR
BEGIN 
    (*Update inputs*)
    FB_FI_PIU.DB_FI_PIU();   // Peripheral input update
%(CI_start)s
    (*Getting inputs*)
%(FI_1)s
%(FI_2)s
%(FI_3)s
    (*UNICOS objects calculating*)
%(FI_4)s
    (*Setting Outputs*)
%(FI_5)s
    (*Update outputs*)
    FI_POU();   // Peripheral output update
     
    //Calling TS_Event_Manager for TSPP communication with the SCADA
    FC_Event_FI();
%(CI_end)s    
END_ORGANIZATION_BLOCK
'''

        params = {
            'FI_FC': "",
            'Type_interrupt': "",
            'OB_Num': "",
            'Call_Type': "",
            'FI_1': "",
            'FI_2': "",
            'FI_3': "",
            'FI_4': "",
            'FI_5': "",
            'CI_Vars': "",
            'CI_start': "",
            'CI_end': ""
        }

        DI_FI_instance_list = self.thePlugin.get_instances_FI("DigitalInput")
        fast_interlocks_DI_present = len(DI_FI_instance_list) != 0
        ONOFF_FI_instance_list = self.thePlugin.get_instances_FI("OnOff")
        fast_interlocks_OnOff_present = len(ONOFF_FI_instance_list) != 0
        DA_FI_instance_list = self.thePlugin.get_instances_FI("DigitalAlarm")
        fast_interlocks_DA_present = len(DA_FI_instance_list) != 0
        DO_FI_instance_list = self.thePlugin.get_instances_FI("DigitalOutput")
        fast_interlocks_DO_present = len(DO_FI_instance_list) != 0

        if fast_interlocks_DI_present:
            params['FI_1'] += '''    FC_DI_FI();    // Digital inputs\n'''
        if fast_interlocks_OnOff_present:
            params['FI_3'] += '''    (*Process Logic Control*)
    FC_FI_LOGIC();  // Process Control object logic\n'''
            params['FI_4'] += '''    FC_ONOFF_FI();    // OnOff objects\n'''
        if fast_interlocks_DA_present:
            params['FI_2'] += '''    FC_FI_LOGIC();
    FC_DA_FI();   // DIGITAL ALARM objects\n'''
        if fast_interlocks_DO_present:
            params['FI_5'] += '''    FC_DO_FI();    //Digital Outputs\n'''

        interrupt_type = DA_FI_instance_list.iterator().next().getAttributeData("LogicDeviceDefinitions:Fast Interlock Type")
        if interrupt_type == "Hardware Interrupt":
            params["OB_Num"] = "OB40"
            params["Type_interrupt"] = "Hardware"
            params["Call_Type"] = "Called after a hardware interrupt trigger"
        elif interrupt_type == "Cyclic Interrupt":
            params["OB_Num"] = "OB34"
            params["Type_interrupt"] = "Cyclic"
            params["Call_Type"] = "Called every user-defined cyclic time"
            params["CI_Vars"] = '''    Dis_error : INT;
    En_error : INT;'''
            params["CI_start"] = '''    IF (DB_FI_PIU.Inp_change) THEN
    // Disable interrupt OB 34 until EN_IRT is called
    Dis_error := DIS_IRT(MODE := B#16#02, OB_NR := 34);

    //Calling TS_Event_Manager for TSPP communication with the SCADA
    FC_Event_FI();
'''
            params["CI_end"] = '''    // Reenable interrupt OB 34
    En_error := EN_IRT(MODE := B#16#02, OB_NR := 34);
    END_IF;'''

        params['FI_FC'] = self.get_FI_FC(DI_FI_instance_list, DO_FI_instance_list, interrupt_type)
        return fc_block % params

    def get_FI_FC(self, DI_instance, DO_instance, interrupt_type):
        fc_block = '''
FUNCTION_BLOCK FB_FI_PIU
//
// Peripheral Input Update
//
VAR
%(CI_Vars)s
END_VAR
VAR_TEMP
    PIB_FI: BYTE;
END_VAR

BEGIN
%(piu)s    
END_FUNCTION_BLOCK

DATA_BLOCK DB_FI_PIU FB_FI_PIU // Data block for the peripheral input update function
BEGIN
END_DATA_BLOCK

FUNCTION FI_POU : VOID
//
// Peripheral Output Update
//
BEGIN 
%(pou)s    
END_FUNCTION
'''

        params = {
            'piu': [],
            'pou': [],
            'CI_Vars': []
        }

        if interrupt_type == "Cyclic Interrupt":
            params['CI_Vars'].append('''    First_cycle : BOOL := TRUE;''')
            params['piu'].append("    Inp_change := FALSE;")

        remaining_instances = list(DI_instance)
        current_instances = []
        input_byte = None
        bits = []
        while len(remaining_instances) > 0:
            for instance in remaining_instances:
                instance_byte, instance_bit = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1").strip().lower().replace("i", "").replace("db", "").split(".")
                if input_byte is None:
                    input_byte = instance_byte
                    bits.append(instance_bit)
                    current_instances.append(instance)
                elif instance_byte == input_byte:
                    bits.append(instance_bit)
                    current_instances.append(instance)
            params['piu'].append(self.get_FI_FC_PIU(input_byte, bits, interrupt_type))
            if interrupt_type == "Cyclic Interrupt":
                params['CI_Vars'].append('    LC_IB%s : BYTE;' % input_byte)
            for instance in current_instances:
                remaining_instances.remove(instance)
            input_byte = None
            bits = []
            current_instances = []

        if interrupt_type == "Cyclic Interrupt":
            params['CI_Vars'].append('''END_VAR
VAR_OUTPUT
    Inp_change : BOOL;''')
            params['piu'].append('''    IF (First_cycle) THEN
        First_cycle := FALSE;
    END_IF;''')

        params['CI_Vars'] = "\n".join(params['CI_Vars'])
        params['piu'] = "\n".join(params['piu'])

        output_bytes = []
        for instance in DO_instance:
            instance_byte, instance_bit = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1").strip().lower().replace("q", "").replace("db", "").split(".")
            if instance_byte not in output_bytes:
                output_bytes.append(instance_byte)
        for output_byte in output_bytes:
            params['pou'].append('''    PQB [%s] := QB [%s];''' % (output_byte, output_byte))
        params['pou'] = "\n".join(params['pou'])
        return fc_block % params

    def get_FI_FC_PIU(self, input_byte, bits, interrupt_type):
        if interrupt_type == "Hardware Interrupt":
            block = '''    PIB_FI := PIB [%(input_byte)s];  //%(bits)s
    IB [%(input_byte)s] := (IB[%(input_byte)s] AND 2#%(zero_bits)s) OR (PIB_FI AND 2#%(one_bits)s);
'''
        elif interrupt_type == "Cyclic Interrupt":
            block = '''    PIB_FI := PIB [%(input_byte)s];  //%(bits)s
    PIB_FI := PIB_FI AND 2#%(one_bits)s;
    IF (First_cycle) THEN
        LC_IB%(input_byte)s := PIB_FI;
    END_IF;
    IF (PIB_FI <> LC_IB%(input_byte)s) THEN
        Inp_change := TRUE;
        LC_IB%(input_byte)s := PIB_FI;
        IB [%(input_byte)s] := (IB [%(input_byte)s] AND 2#%(zero_bits)s) OR PIB_FI;
    END_IF;
'''

        params = {
            'input_byte': input_byte,
            'bits': [],
            'zero_bits': [],
            'one_bits': []
        }

        for current_bit in bits:
            params['bits'].append('%s.%s' % (input_byte, current_bit))
        for idx in range(8):
            if str(idx) in bits:
                params['zero_bits'].append('0')
                params['one_bits'].append('1')
            else:
                params['zero_bits'].append('1')
                params['one_bits'].append('0')
        params['bits'] = ", ".join(params['bits'])
        params['zero_bits'] = "".join(reversed(params['zero_bits']))
        params['one_bits'] = "".join(reversed(params['one_bits']))
        return block % params


    def get_s7_400_db(self, controller_exists_in_generation, diagnostic):
        """ For S7-400 PLC create:
                OB81 (Power Supply Error detection)
                OB83 (Insert/Remove Module Interrupt detection),
                OB84 (CPU Hardware Fault detection)
                OB101 (Hot Restart).
        """
        s7_400_db = '''
// Power Supply Fault
ORGANIZATION_BLOCK OB81
VAR_TEMP
  // Reserved
  Info: ARRAY[0..19] OF BYTE;
  // Temporary Variables
END_VAR
    ;
END_ORGANIZATION_BLOCK

// I/O Point fault 2
ORGANIZATION_BLOCK OB83
VAR_TEMP
  // Reserved
  Info: ARRAY[0..19] OF BYTE;
  // Temporary Variables
END_VAR
    ;
END_ORGANIZATION_BLOCK

// CPU fault
ORGANIZATION_BLOCK OB84
VAR_TEMP
  // Reserved
  Info: ARRAY[0..19] OF BYTE;
  // Temporary Variables
END_VAR;
END_ORGANIZATION_BLOCK


(*HOT RESTART*********************************************)
''' + self.get_OB10x(101, controller_exists_in_generation, diagnostic) + '''

(*COLD RESTART**********************************************)
''' + self.get_OB10x(102, controller_exists_in_generation, diagnostic) + '\n'
        return s7_400_db

    def get_OB10x(self, OB_number, controller_exists_in_generation, diagnostic):
        OB10x_block = '''ORGANIZATION_BLOCK OB%(OB_number)s 
TITLE = '%(OB_type)s restart'
//
// Called during %(OB_type)s restart
//
AUTHOR: 'UNICOS'
NAME: 'OB%(OB_number)s'
FAMILY: 'EXEC'
VAR_TEMP
    OB%(OB_number)s_EV_CLASS  : BYTE;
    OB%(OB_number)s_STRUP     : BYTE;
    OB%(OB_number)s_PRIORITY  : BYTE;
    OB%(OB_number)s_OB_NUMBR  : BYTE;
    OB%(OB_number)s_RESERVED_1: BYTE;
    OB%(OB_number)s_RESERVED_2: BYTE;
    OB%(OB_number)s_STOP      : WORD;
    OB%(OB_number)s_STRT_INFO : DWORD;
    OB%(OB_number)s_DATE_TIME   : DATE_AND_TIME;

END_VAR
BEGIN 
//Time Smoothing (alarms)
    UNICOS_TimeSmooth := 10;

//Init of First_Cycle variable    
    First_Cycle := TRUE;

//Init of Live counter marker form Live counter datablock    
    UNICOS_LiveCounter := DB_WINCCOA.UNICOS_LiveCounter;    
    
    %(FC_CONTROLLER)s

    %(OB10x_DIAGNOSTIC)s

END_ORGANIZATION_BLOCK'''

        params = {
            'OB_number' : OB_number,
            'OB_type' : '',
            'OB10x_DIAGNOSTIC' : '',
            'FC_CONTROLLER' : ''
        }

        if OB_number == 100:
            params['OB_type'] = 'warm'
        if OB_number == 101:
            params['OB_type'] = 'hot'
        if OB_number == 102:
            params['OB_type'] = 'cold'

        if controller_exists_in_generation:
            params['FC_CONTROLLER'] = '''FC_CONTROLLER(group:=0);// All Controllers running (option=0)'''

        if diagnostic == 'true':
            params['OB10x_DIAGNOSTIC'] = '''FC_Diag3(); // Call diagnostics for all the objects in the project'''

        return OB10x_block % params

    def get_recipe_db(self, xml_config):
        recipe_db = '''
    // Calling the Recipes Mechanism and passing the information from WinCCOA (DB_RECIPES_INTERFACE) to the CPC_DB_RECIPES
    CPC_FB_RECIPES.CPC_DB_RECIPES(
        Header := DB_RECIPES_INTERFACE.RecipeHeader      //Header from DB_RECIPES_INTERFACE to CPC_DB_RECIPES
        ,DBnum := %(DBnum)s                  //DB number of Recipe buffers
        ,HeaderBase := %(HeaderBase)s                   //Header buffer base address
        ,StatusBase := %(StatusBase)s                   //Recipe Status buffer base address
        ,ManRegAddrBase := %(ManRegAddrBase)s           //Manual Requests addresses buffer base address
        ,ManRegValBase := %(ManRegValBase)s             //Manual Reuqests values buffer base address
        ,ReqAddrBase := %(ReqAddrBase)s                 //Request Address buffer base address
        ,ReqValBase := %(ReqValBase)s                   //Request Values buffer base address
        ,BuffersBase := %(ManRegAddrBase)s                 //Buffers base address
        ,BuffersEnd := %(BuffersEnd)s                   //Buffers last address
        ,Timeout := T#%(Timeout)ss                         //Recipe transfer timeout
    );
    
    DB_RECIPES_INTERFACE.RecipeStatus := CPC_DB_RECIPES.Status; //Status from CPC_DB_RECIPES to DB_RECIPES_INTERFACE
    '''
        generate_buffers = xml_config.getPLCParameter("RecipeParameters:GenerateBuffers").strip().lower()
        if generate_buffers != "true":
            return ""

        header_buffer_size = int(xml_config.getPLCParameter("RecipeParameters:HeaderBufferSize").strip())
        buffer_size_calculated = self.get_recipe_buffer_size(xml_config)
        params = {
            'DBnum': self.thePlugin.getAddress("DB_RECIPES_INTERFACE"),
            'HeaderBase': '2',
            'StatusBase': header_buffer_size * 2 + 2,
            'ManRegAddrBase': header_buffer_size * 4 + 2,
            'ManRegValBase': header_buffer_size * 4 + buffer_size_calculated * 2 + 2,
            'ReqAddrBase': header_buffer_size * 4 + buffer_size_calculated * 4 + 2,
            'ReqValBase': header_buffer_size * 4 + buffer_size_calculated * 6 + 2,
            'BuffersEnd': header_buffer_size * 4 + buffer_size_calculated * 10 + 2,
            'Timeout': xml_config.getPLCParameter("RecipeParameters:ActivationTimeout")
        }

        return recipe_db % params

    def get_diagnostic_db(self, unicos_project, types_to_process):
        diagnostic_db = '''(* Diagnostic information from the OB82 and RECORD retreived from SFB54 *)

FUNCTION_BLOCK FB_diag1
//
// DIAGNOSTIC FUNCTION FOR I/O CARDS
//
AUTHOR: UNICOS
NAME: Diagnose
FAMILY: DIAG
VAR_TEMP
    PLC_ADDR        : REAL;
    T               : INT;  //type of card (DI=1 AI=2 DO=3 AO=4)
    chIdx           : INT;  //Channel index
    objIdx          : INT;  //CPC object index
    grChIdx         : INT;  //Channel group index
    Nr_Of_Channels  : INT;
    grCh            : INT;  //Number of 8-channel groups
    ungrCh          : INT;  //Number of channels outside 8-channel groups
    currentGrCh     : INT;  //Current number of channels in the group
    Mask            : BYTE;
    Channel_Error   : BOOL;
    Break           : BOOL;
    Card_Error      : BOOL;

    BASE_FLAG       : BYTE; //Base direction (input/output) of the module
    BASE_ADDR       : WORD; //Base address of the module
    START_ADDR      : INT;  //Start address of the channel specific information
    NUMBYTES        : INT;
    NUMBITS         : INT;
END_VAR

VAR
    RDSYSST_Data            : ARRAY[0..799] OF BYTE; //50 x 16 bytes = 800 bytes
    RDREC_Data              : ARRAY[0..409] OF BYTE; //Up to 65 channels

    //RALRM variables
    RALRM                   : RALRM;
    RALRM_TINFO_Data        : ARRAY[0..31] OF BYTE;
    RALRM_AINFO_Data        : ARRAY[0..40] OF BYTE;
    
    //RD_LGADR variables
    RDLGADR_ERROR           : INT;
    RDLGADR_PEADDR          : ARRAY[0..39] OF WORD;
    RDLGADR_PECOUNT         : INT;
    RDLGADR_PAADDR          : ARRAY[0..39] OF WORD;
    RDLGADR_PACOUNT         : INT;

    IOFlag                  : BYTE;                     // = B#16#54 for an Input and B#16#55 for an Output
    MDL_ADDR                : WORD;                     //Address of the module
        
    OB86_EV_CLASS           : BYTE;                     //16#38/39 Event class 3
    OB86_FLT_ID             : BYTE;                     //16#C1/C4/C5, Fault identifcation code
    OB86_RACKS_FLTD         : DWORD;                    //Rack in fault
    OB86_DP_MasterSystem    : INT;                      //DP Master System number
    OB86_PN_IOSystem        : INT;                      //PN IO System number
    OB86_DP_Slave           : INT;                      //DP Slave number
    OB86_PN_Station         : INT;                      //PN IO Station number
    OB86_DP_Slave_Error     : BOOL;                     //DP slave error
    OB86_PN_Station_Error   : BOOL;                     //PN IO Station error 
    FB_Diag1_Exec           : BOOL;                     //Execution of FB_Diag1 in progress
    FC_Diag2_Exec           : BOOL;                     //Execution of FC_Diag2 in progress
END_VAR

CONST
    numDI                   := %(numDI)s;
    numDI_FI                := %(numDI_FI)s;
    numAI                   := %(numAI)s;
    numDO                   := %(numDO)s;
    numDO_FI                := %(numDO_FI)s;
    numAO                   := %(numAO)s;
END_CONST

BEGIN
    //Call SFB54 (RALRM) to obtain the record.
    RALRM(MODE     := 1,
          MLEN     := 39,
          TINFO    := RALRM_TINFO_Data,
          AINFO    := RALRM_AINFO_Data);

    IF ((B#16#FF AND RALRM_TINFO_Data[20]) = B#16#00) OR ((B#16#0F AND RALRM_TINFO_Data[22]) = B#16#03) THEN //CENTRAL IO TREATMENT
    //Type of card
        IF (B#16#7F AND RALRM_AINFO_Data[8]) = B#16#70                                                      THEN T:= 1; END_IF; //DI
        IF ((B#16#7F AND RALRM_AINFO_Data[8]) = B#16#71) OR ((B#16#7F AND RALRM_AINFO_Data[8]) = B#16#7B)   THEN T:= 2; END_IF; //AI. Code 7B from ET200S AI modules
        IF (B#16#7F AND RALRM_AINFO_Data[8]) = B#16#72                                                      THEN T:= 3; END_IF; //DO
        IF ((B#16#7F AND RALRM_AINFO_Data[8]) = B#16#73) OR ((B#16#7F AND RALRM_AINFO_Data[8]) = B#16#7C)   THEN T:= 4; END_IF; //AO. Code 7C from ET200S AO modules

    //Number of channels in this module
        Nr_Of_Channels := BYTE_TO_INT(RALRM_AINFO_Data[10]);

    //Detection of module error
        Card_Error :=  ((B#16#F0 AND RALRM_AINFO_Data[4]) <> 0);

    //Number of bytes containing information o chanmnel in the RECORD
        grCh          := Nr_Of_Channels DIV 8;  //Number of groups of 8 channels in the module (e.g if 12 AI module then grCh = 1)
        ungrCh         := Nr_Of_Channels MOD 8;  //Number of channels outside groups of 8 (e.g 12 AI module ungrCh = 4)
        IF ungrCh = 0 THEN grCh := grCh - 1 ; END_IF;

    //Scan of all channel information in the RECORD BYTE 11 onwards
    FOR grChIdx := 0 TO grCh DO
        IF grChIdx = grCh AND ungrCh > 0 THEN currentGrCh := ungrCh-1; ELSE currentGrCh := 7; END_IF;
        FOR chIdx := 0 TO currentGrCh DO
            //Init
            Break := 0;
            Mask := B#16#01;
            Mask := ROL(IN:=Mask, N:=chIdx);

           //Calculation OF PLC Address according to the type of channel
            CASE T OF
                1 : PLC_ADDR := INT_TO_REAL(WORD_TO_INT(MDL_ADDR)) + grChIdx + INT_TO_REAL(chIdx)/10 ;  //DI
                2 : PLC_ADDR := INT_TO_REAL(WORD_TO_INT(MDL_ADDR)) + 16*grChIdx + chIdx*2;              //AI
                3 : PLC_ADDR := INT_TO_REAL(WORD_TO_INT(MDL_ADDR)) + grChIdx + INT_TO_REAL(chIdx)/10 ;  //DO
                4 : PLC_ADDR := INT_TO_REAL(WORD_TO_INT(MDL_ADDR)) + 16*grChIdx + chIdx*2;              //AO

            END_CASE;

            //IO Error detection for every channel
            IF ((RALRM_AINFO_Data[11+grChIdx] AND Mask) = Mask) OR Card_Error THEN
                Channel_Error := 1 ;       //IO error
            ELSE
                Channel_Error := 0 ;       //No IO error
            END_IF;

            //Init
            objIdx:= 1;
            CASE T OF
                0 : ;
%(DIAGNOSTIC_CASE1)s
%(DIAGNOSTIC_CASE2)s
%(DIAGNOSTIC_CASE3)s
%(DIAGNOSTIC_CASE4)s
            END_CASE;
        END_FOR;
    END_FOR;

    ELSE
        IF (B#16#0F AND RALRM_TINFO_Data[22]) = B#16#02 THEN                                                 //PROFIBUS TREATMENT
            START_ADDR := 14;
        ELSIF (B#16#0F AND RALRM_TINFO_Data[22]) = B#16#08 THEN                                              //PROFINET TREATMENT
            START_ADDR := 28;
        ELSE                                                                                                 //NOT IMPLEMENTED
            ;
        END_IF;
        
        //Type of card (Non defined data type for digital modules and Word data type for analog modules)
        IF ((B#16#FF AND RALRM_AINFO_Data[START_ADDR + 3]) = B#16#00) AND ((B#16#E0 AND RALRM_AINFO_Data[START_ADDR + 2]) = B#16#20)         THEN T:= 1; END_IF; //DI
        IF ((B#16#FF AND RALRM_AINFO_Data[START_ADDR + 3]) = B#16#05) AND ((B#16#E0 AND RALRM_AINFO_Data[START_ADDR + 2]) = B#16#20)         THEN T:= 2; END_IF; //AI
        IF ((B#16#FF AND RALRM_AINFO_Data[START_ADDR + 3]) = B#16#00) AND ((B#16#E0 AND RALRM_AINFO_Data[START_ADDR + 2]) = B#16#40)         THEN T:= 3; END_IF; //DO
        IF ((B#16#FF AND RALRM_AINFO_Data[START_ADDR + 3]) = B#16#05) AND ((B#16#E0 AND RALRM_AINFO_Data[START_ADDR + 2]) = B#16#40)         THEN T:= 4; END_IF; //AO
    
        //Channel error
        IF (B#16#18 AND RALRM_AINFO_Data[START_ADDR + 2]) = B#16#10 THEN //Channel recover data
            Channel_Error := FALSE;
        ELSIF (B#16#18 AND RALRM_AINFO_Data[START_ADDR + 2]) = B#16#08 THEN //Channel error data
            Channel_Error := TRUE;
        END_IF;

        IF (RALRM_AINFO_Data[START_ADDR] = B#16#80) AND (RALRM_AINFO_Data[START_ADDR + 1] = B#16#00) THEN //Module error
            ;
%(PERIPHERY_MODULE_DIAGNOSTIC_CASE1)s
%(PERIPHERY_MODULE_DIAGNOSTIC_CASE2)s
%(PERIPHERY_MODULE_DIAGNOSTIC_CASE3)s
%(PERIPHERY_MODULE_DIAGNOSTIC_CASE4)s
        ELSE //Channel error
        
            //Init
            Break := 0;
            objIdx:= 1;
            
            //Calculation OF PLC Address according to the type of channel
            IF (T = 1) OR (T = 3) THEN
                NUMBITS := BYTE_TO_INT(RALRM_AINFO_Data[START_ADDR + 1]) MOD 8;
                NUMBYTES := BYTE_TO_INT(RALRM_AINFO_Data[START_ADDR + 1]) DIV 8;
            END_IF;
            
            CASE T OF
                0 : ;
%(PERIPHERY_DIAGNOSTIC_CASE1)s
%(PERIPHERY_DIAGNOSTIC_CASE2)s
%(PERIPHERY_DIAGNOSTIC_CASE3)s
%(PERIPHERY_DIAGNOSTIC_CASE4)s
            END_CASE;
        END_IF;
    END_IF;

END_FUNCTION_BLOCK

DATA_BLOCK DB_IO_DIAGNOSTIC FB_Diag1
BEGIN
END_DATA_BLOCK


(* Diagnostic of Profibus slaves *)
FUNCTION FC_Diag2 : VOID
//
// Diagnostic of profibus slaves
//
AUTHOR: UNICOS
NAME: Diagnose
FAMILY: DIAG

VAR_TEMP
    //RDSYSST variables
    LOG_ADDR        : WORD; //Logical address of the module
    RDSYSST_REQ     : BOOL;
    RDSYSST_ERROR   : INT;
    RDSYSST_BUSY    : BOOL;
    RDSYSST_HEADER          : STRUCT
        LENTHDR             : WORD;
        N_DR                : WORD;
    END_STRUCT;
    
    //RDREC variables
    RDREC_TRIG      : BOOL;
    RDREC_INDEX     : INT;
    NumErrCh        : INT;
    START_ADDR      : INT;
    
    //Loop variables
    objIdx          : INT;  //CPC object index
    mdlIdx          : INT;  //Module index
    chIdx           : INT;  //Channel index
    
    //Others
    BASE_FLAG       : BYTE; //Base direction (input/output) of the module
    BASE_ADDR       : WORD; //Base address of the module
    Numbytes        : INT;
    Numbits         : INT;
    Card_Error      : BOOL;

END_VAR

CONST
    numDI                   := %(numDI)s;
    numDI_FI                := %(numDI_FI)s;
    numAI                   := %(numAI)s;
    numDO                   := %(numDO)s;
    numDO_FI                := %(numDO_FI)s;
    numAO                   := %(numAO)s;
END_CONST

BEGIN
    DB_IO_DIAGNOSTIC.FC_Diag2_Exec := FALSE;

    //READ SYSTEM
    RDSYSST_REQ := TRUE;
    REPEAT
    RDSYSST_ERROR := RDSYSST(REQ        := RDSYSST_REQ,
                             SZL_ID     := W#16#0D91,
                             INDEX      := DWORD_TO_WORD(DB_IO_DIAGNOSTIC.OB86_RACKS_FLTD AND DW#16#0000FFFF),
                             BUSY       => RDSYSST_BUSY,
                             SZL_HEADER => RDSYSST_HEADER,
                             DR         => DB_IO_DIAGNOSTIC.RDSYSST_Data);
              
        RDSYSST_REQ := FALSE;
    UNTIL RDSYSST_BUSY = FALSE
    END_REPEAT;

    FOR mdlIdx := 1 TO WORD_TO_INT(RDSYSST_HEADER.N_DR)-1 DO //Loop around modules except station
        //Call SFB52 (RDREC) constantly until the RECORD is retreived.
        RDREC_TRIG := TRUE;
        IF (DB_IO_DIAGNOSTIC.RDSYSST_Data[0] AND B#16#80) <> 0 THEN //PROFINET
            RDREC_INDEX := WORD_TO_INT(W#16#800A);
        ELSE                                                        //PROFIBUS
            RDREC_INDEX := 1;
        END_IF;
        LOG_ADDR := SHL(IN:=BYTE_TO_WORD(DB_IO_DIAGNOSTIC.RDSYSST_Data[4 + WORD_TO_INT(RDSYSST_HEADER.LENTHDR)*mdlIdx]), N:=8) OR
                    BYTE_TO_WORD(DB_IO_DIAGNOSTIC.RDSYSST_Data[5 + WORD_TO_INT(RDSYSST_HEADER.LENTHDR)*mdlIdx]);
        REPEAT
            RDREC.RDREC_Diag(REQ     := RDREC_TRIG,
                              ID      := WORD_TO_DWORD(LOG_ADDR),
                              INDEX   := RDREC_INDEX,
                              MLEN    := 410,
                              RECORD  := DB_IO_DIAGNOSTIC.RDREC_Data);
                  
            RDREC_TRIG := FALSE;
        UNTIL RDREC_Diag.Busy = FALSE
        END_REPEAT;
    
%(FCDIAG2_DIAGNOSTIC_CASE1)s
%(FCDIAG2_DIAGNOSTIC_CASE2)s
%(FCDIAG2_DIAGNOSTIC_CASE3)s
%(FCDIAG2_DIAGNOSTIC_CASE4)s

    END_FOR;

%(FC_Diag2)s
END_FUNCTION

(* Diagnostics for all objects during initialization *)
FUNCTION FC_Diag3 : VOID
//
// Diagnostics for all objects
//
AUTHOR: UNICOS
NAME: Diagnose
FAMILY: DIAG

VAR_TEMP
    objIdx          : INT;  //CPC object index
    chIdx           : INT;  //Channel index
    RDREC_TRIG      : BOOL;
    RDREC_INDEX     : INT;
    NumErrCh        : INT;
    BASE_ADDR       : WORD; //Base address of the module
    START_ADDR      : INT;
    Numbytes        : INT;
    Numbits         : INT;
    Card_error      : BOOL;
END_VAR

CONST
    numDI                   := %(numDI)s;
    numDI_FI                := %(numDI_FI)s;
    numAI                   := %(numAI)s;
    numDO                   := %(numDO)s;
    numDO_FI                := %(numDO_FI)s;
    numAO                   := %(numAO)s;
END_CONST

BEGIN
%(FCDIAG3_DIAGNOSTIC_CASE1)s
%(FCDIAG3_DIAGNOSTIC_CASE2)s
%(FCDIAG3_DIAGNOSTIC_CASE3)s
%(FCDIAG3_DIAGNOSTIC_CASE4)s
END_FUNCTION'''
        params = {
            'numDI': '0',
            'numDI_FI': '0',
            'numAI': '0',
            'numDO': '0',
            'numDO_FI': '0',
            'numAO': '0',
            'DIAGNOSTIC_CASE1': '',
            'DIAGNOSTIC_CASE2': '',
            'DIAGNOSTIC_CASE3': '',
            'DIAGNOSTIC_CASE4': '',
            'PERIPHERY_MODULE_DIAGNOSTIC_CASE1': '',
            'PERIPHERY_MODULE_DIAGNOSTIC_CASE2': '',
            'PERIPHERY_MODULE_DIAGNOSTIC_CASE3': '',
            'PERIPHERY_MODULE_DIAGNOSTIC_CASE4': '',
            'PERIPHERY_DIAGNOSTIC_CASE1': '',
            'PERIPHERY_DIAGNOSTIC_CASE2': '',
            'PERIPHERY_DIAGNOSTIC_CASE3': '',
            'PERIPHERY_DIAGNOSTIC_CASE4': '',
            'FCDIAG2_DIAGNOSTIC_CASE1': '',
            'FCDIAG2_DIAGNOSTIC_CASE2': '',
            'FCDIAG2_DIAGNOSTIC_CASE3': '',
            'FCDIAG2_DIAGNOSTIC_CASE4': '',
            'FCDIAG3_DIAGNOSTIC_CASE1': '',
            'FCDIAG3_DIAGNOSTIC_CASE2': '',
            'FCDIAG3_DIAGNOSTIC_CASE3': '',
            'FCDIAG3_DIAGNOSTIC_CASE4': '',
            'FC_Diag2': ''
        }

        RDSYSST_RDREC_Diag = '''            IF NOT RDREC_Diag.ERROR THEN //Record adquired
                %(numbits)s
                Numbytes := REAL_TO_INT(TRUNC(%(objname)s_ERROR.IOERROR[objIdx].ADDR) - WORD_TO_INT(DB_IO_DIAGNOSTIC.RDLGADR_%(paddr)s[0]));
                IF RDREC_INDEX = 1 AND (BYTE_TO_INT(DB_IO_DIAGNOSTIC.RDREC_Data[6]) > 0) THEN //CENTRAL / PROFIBUS
                    //Detection of module error
                    Card_error :=  ((B#16#F0 AND DB_IO_DIAGNOSTIC.RDREC_Data[0]) <> 0);
                    %(objname)s_ERROR.IOERROR[objIdx].Err := (%(addrcalc)s <> 0) OR Card_error;
                ELSE
                    IF RDREC_INDEX = 1 THEN                                                   //PROFIBUS S7
                        START_ADDR := 10;
                        IF ((RDREC_Diag.LEN - 10) > 0) AND ((DB_IO_DIAGNOSTIC.RDREC_Data[START_ADDR] = B#16#80) AND (DB_IO_DIAGNOSTIC.RDREC_Data[START_ADDR + 1] = B#16#00)) THEN //Error for all channels in the module
                            %(objname)s_ERROR.IOERROR[objIdx].Err := TRUE;
                            NumErrCh := 0;
                        ELSE
                            NumErrCh := (RDREC_Diag.LEN - 10) / 6;
                        END_IF;
                    ELSE                                                                      //PROFINET
                        START_ADDR := 20;
                        IF RDREC_Diag.LEN = 0 THEN
                            NumErrCh := 0;
                        ELSE
                            IF (DB_IO_DIAGNOSTIC.RDREC_Data[START_ADDR] = B#16#80) AND (DB_IO_DIAGNOSTIC.RDREC_Data[START_ADDR + 1] = B#16#00) THEN //Error for all channels in the module
                                %(objname)s_ERROR.IOERROR[objIdx].Err := TRUE;
                                NumErrCh := 0;
                            ELSE
                                NumErrCh := (RDREC_Diag.LEN - 20) / 6;
                            END_IF;
                        END_IF;
                    END_IF;
                    IF NumErrCh > 0 THEN
                        FOR chIdx := 0 TO NumErrCh - 1 DO
                            IF BYTE_TO_INT(DB_IO_DIAGNOSTIC.RDREC_Data[START_ADDR + 1 + 6*chIdx]) = %(addrcalc2)s THEN
                                %(objname)s_ERROR.IOERROR[objIdx].Err := TRUE;
                            END_IF;
                        END_FOR;
                    END_IF;
                END_IF;
            ELSE                         //Record not adquired
                IF (RDREC_Diag.STATUS AND DW#16#00FFFF00) <> DW#16#0080A000 THEN
                    //Record not adquired for reasons different than not known record in module
                    %(objname)s_ERROR.IOERROR[objIdx].Err := TRUE;
                END_IF;
            END_IF;'''

        commonparams = {
            'DIAGNOSTIC_CASE' : '''WHILE (objIdx<num%(objname)s+1 AND break = 0) DO  //%(objname)s
                        IF %(objname)s_ERROR.IOERROR[objIdx].ADDR = PLC_ADDR THEN %(objname)s_ERROR.IOERROR[objIdx].Err := Channel_Error; break := 1; END_IF;
                        objIdx := objIdx+1;
                    END_WHILE;''',
            'PERIPHERY_MODULE_DIAGNOSTIC_CASE' : '''            FOR objIdx := 1 TO num%(objname)s DO
                RDLGADR_ERROR := RD_LGADR(IOID := b#16#00,
                                            LADDR := %(laddrop)sINT_TO_WORD(REAL_TO_INT(TRUNC(%(objname)s_ERROR.IOERROR[objIdx].ADDR))),
                                            PEADDR => RDLGADR_PEADDR,
                                            PECOUNT => RDLGADR_PECOUNT,
                                            PAADDR => RDLGADR_PAADDR,
                                            PACOUNT => RDLGADR_PACOUNT);

                IF (RDLGADR_PECOUNT > 0 AND RDLGADR_PACOUNT > 0) THEN //Module has inputs and outputs
                    IF WORD_TO_INT(RDLGADR_PEADDR[0]) <= WORD_TO_INT(RDLGADR_PAADDR[0]) THEN //Module referenced by input address
                        BASE_FLAG := B#16#54;
                        BASE_ADDR := RDLGADR_PEADDR[0];
                    ELSE                                                                     //Module referenced by output address
                        BASE_FLAG := B#16#55;
                        BASE_ADDR := RDLGADR_PAADDR[0];
                    END_IF;
                ELSE                                                  //Module only has %(objtype)ss
                    BASE_FLAG := B#16#%(objflag)s;
                    BASE_ADDR := RDLGADR_%(paddr)s[0];
                END_IF;
                                        
                IF (RDLGADR_ERROR = 0) AND (BASE_ADDR = MDL_ADDR) AND (BASE_FLAG = IOFlag) THEN //%(objtype)s belongs to current module
                    %(objname)s_ERROR.IOERROR[objIdx].Err := Channel_Error;
                END_IF;
            END_FOR;''',
            'PERIPHERY_DIAGNOSTIC_CASE' : '''PLC_ADDR := INT_TO_REAL(WORD_TO_INT(MDL_ADDR)) + %(addrcalc)s;   //%(objname)s
                    WHILE (objIdx<num%(objname)s+1 AND break = 0) DO
                        IF %(objname)s_ERROR.IOERROR[objIdx].ADDR = PLC_ADDR THEN %(objname)s_ERROR.IOERROR[objIdx].Err := Channel_Error; break := 1; END_IF;
                        objIdx := objIdx+1;
                    END_WHILE;''',
            'FCDIAG2_DIAGNOSTIC_CASE' : '''        FOR objIdx:=1 TO num%(objname)s DO //%(objname)s
            DB_IO_DIAGNOSTIC.RDLGADR_ERROR := RD_LGADR(IOID := b#16#00,
                                        LADDR := %(laddrop)sINT_TO_WORD(REAL_TO_INT(TRUNC(%(objname)s_ERROR.IOERROR[objIdx].ADDR))),
                                        PEADDR => DB_IO_DIAGNOSTIC.RDLGADR_PEADDR,
                                        PECOUNT => DB_IO_DIAGNOSTIC.RDLGADR_PECOUNT,
                                        PAADDR => DB_IO_DIAGNOSTIC.RDLGADR_PAADDR,
                                        PACOUNT => DB_IO_DIAGNOSTIC.RDLGADR_PACOUNT);

            IF (DB_IO_DIAGNOSTIC.RDLGADR_PECOUNT > 0 AND DB_IO_DIAGNOSTIC.RDLGADR_PACOUNT > 0) THEN //Module has inputs and outputs
                IF WORD_TO_INT(DB_IO_DIAGNOSTIC.RDLGADR_PEADDR[0]) <= WORD_TO_INT(DB_IO_DIAGNOSTIC.RDLGADR_PAADDR[0]) THEN //Module referenced by input address
                    BASE_FLAG := B#16#B4;
                    BASE_ADDR := DB_IO_DIAGNOSTIC.RDLGADR_PEADDR[0];
                ELSE                                                                                                       //Module referenced by output address
                    BASE_FLAG := B#16#B5;
                    BASE_ADDR := DB_IO_DIAGNOSTIC.RDLGADR_PAADDR[0];
                END_IF;
            ELSE                                                                                    //Module only has %(objtype)ss
                BASE_FLAG := B#16#%(objflag)s;
                BASE_ADDR := DB_IO_DIAGNOSTIC.RDLGADR_%(paddr)s[0];
            END_IF;
                                    
            IF (DB_IO_DIAGNOSTIC.RDLGADR_ERROR = 0) AND (BASE_ADDR = LOG_ADDR) AND (BASE_FLAG = DB_IO_DIAGNOSTIC.RDSYSST_Data[12 + WORD_TO_INT(RDSYSST_HEADER.LENTHDR)*mdlIdx]) THEN //%(objtype)s belongs to current module
                %(objname)s_ERROR.IOERROR[objIdx].Err := FALSE;
                
''' + "\n".join(["    " + line for line in RDSYSST_RDREC_Diag.splitlines()]) + '''
                                        
            END_IF;
        END_FOR;''',
            'FCDIAG3_DIAGNOSTIC_CASE' : '''    //%(objname)s
    FOR objIdx:=1 TO num%(objname)s DO
        %(objname)s_ERROR.IOERROR[objIdx].Err := FALSE;
        
        DB_IO_DIAGNOSTIC.RDLGADR_ERROR := RD_LGADR(IOID := b#16#00,
                                                LADDR := %(laddrop)sINT_TO_WORD(REAL_TO_INT(TRUNC(%(objname)s_ERROR.IOERROR[objIdx].ADDR))),
                                                PEADDR => DB_IO_DIAGNOSTIC.RDLGADR_PEADDR,
                                                PECOUNT => DB_IO_DIAGNOSTIC.RDLGADR_PECOUNT,
                                                PAADDR => DB_IO_DIAGNOSTIC.RDLGADR_PAADDR,
                                                PACOUNT => DB_IO_DIAGNOSTIC.RDLGADR_PACOUNT);
        
        IF DB_IO_DIAGNOSTIC.RDLGADR_ERROR = 0 THEN    //Module of the object detected
            //Call SFB52 (RDREC) constantly until the RECORD is retreived.
            RDREC_TRIG := TRUE;
            RDREC_INDEX := WORD_TO_INT(W#16#800A);
            IF (DB_IO_DIAGNOSTIC.RDLGADR_PECOUNT > 0 AND DB_IO_DIAGNOSTIC.RDLGADR_PACOUNT > 0) THEN //Module has inputs and outputs
                IF WORD_TO_INT(DB_IO_DIAGNOSTIC.RDLGADR_PEADDR[0]) <= WORD_TO_INT(DB_IO_DIAGNOSTIC.RDLGADR_PAADDR[0]) THEN //Module referenced by input address
                    BASE_ADDR := DB_IO_DIAGNOSTIC.RDLGADR_PEADDR[0];
                ELSE                                                                                                       //Module referenced by output address
                    BASE_ADDR := DB_IO_DIAGNOSTIC.RDLGADR_PAADDR[0];
                END_IF;
            ELSE                                                                                    //Module only has %(objtype)ss
                BASE_ADDR := %(laddrop)sDB_IO_DIAGNOSTIC.RDLGADR_%(paddr)s[0];
            END_IF;
            
            REPEAT
                RDREC.RDREC_Diag(REQ     := RDREC_TRIG,
                                 ID      := WORD_TO_DWORD(BASE_ADDR),
                                 INDEX   := RDREC_INDEX,
                                 MLEN    := 410,
                                 RECORD  := DB_IO_DIAGNOSTIC.RDREC_Data);
                      
                RDREC_TRIG := FALSE;
            UNTIL RDREC_Diag.Busy = FALSE
            END_REPEAT;
            IF RDREC_Diag.ERROR AND ((RDREC_Diag.STATUS AND DW#16#00FFFF00) = DW#16#0080B000) THEN //NOT PROFINET
                //Call SFB52 (RDREC) constantly until the RECORD is retreived.
                RDREC_TRIG := TRUE;
                RDREC_INDEX := 1;
                
                REPEAT
                    RDREC.RDREC_Diag(REQ     := RDREC_TRIG,
                                     ID      := WORD_TO_DWORD(BASE_ADDR),
                                     INDEX   := RDREC_INDEX,
                                     MLEN    := 410,
                                     RECORD  := DB_IO_DIAGNOSTIC.RDREC_Data);
                          
                    RDREC_TRIG := FALSE;
                UNTIL RDREC_Diag.Busy = FALSE
                END_REPEAT;
            END_IF;
            
''' + RDSYSST_RDREC_Diag + '''
        END_IF;
    END_FOR;'''
        }

        DI_FI_instance_list = self.thePlugin.get_instances_FI("DigitalInput")
        DI_FI_instance_amount = len(DI_FI_instance_list)
        fast_interlocks_DI_present = DI_FI_instance_amount != 0
        DO_FI_instance_list = self.thePlugin.get_instances_FI("DigitalOutput")
        DO_FI_instance_amount = len(DO_FI_instance_list)
        fast_interlocks_DO_present = DO_FI_instance_amount != 0

        for device_type, instance_amount in types_to_process.iteritems():
            device_type_name = device_type.getDeviceTypeName()
            if device_type_name == "DigitalInput":
                if fast_interlocks_DI_present:
                    instance_amount = instance_amount - DI_FI_instance_amount
                params['numDI'] = instance_amount
                params['DIAGNOSTIC_CASE1'] = '''                1 : ''' + commonparams['DIAGNOSTIC_CASE'] % {'objname' : 'DI'}
                params['PERIPHERY_MODULE_DIAGNOSTIC_CASE1'] = commonparams['PERIPHERY_MODULE_DIAGNOSTIC_CASE'] %\
                    {'objname' : 'DI', 'laddrop' : '', 'objflag' : '54', 'paddr' : 'PEADDR', 'objtype' : 'Input'}
                params['PERIPHERY_DIAGNOSTIC_CASE1'] = '''              1 : ''' + commonparams['PERIPHERY_DIAGNOSTIC_CASE'] %\
                    {'objname' : 'DI', 'addrcalc' : 'NUMBYTES + INT_TO_REAL(NUMBITS)/10'}
                params['FCDIAG2_DIAGNOSTIC_CASE1'] = commonparams['FCDIAG2_DIAGNOSTIC_CASE'] %\
                    {'objname' : 'DI', 'laddrop' : '', 'objflag' : 'B4', 'paddr' : 'PEADDR', 'objtype' : 'Input',
                        'numbits' : 'Numbits := REAL_TO_INT((DI_ERROR.IOERROR[objIdx].ADDR - TRUNC(DI_ERROR.IOERROR[objIdx].ADDR)) * 10);',
                        'addrcalc' : '(DB_IO_DIAGNOSTIC.RDREC_Data[7 + Numbytes] AND SHL(IN := B#16#01, N := Numbits))',
                        'addrcalc2' : '(Numbits*10 + Numbytes*8)'}
                params['FCDIAG3_DIAGNOSTIC_CASE1'] = commonparams['FCDIAG3_DIAGNOSTIC_CASE'] %\
                    {'objname' : 'DI', 'laddrop' : '', 'paddr' : 'PEADDR', 'objtype' : 'Input',
                        'numbits' : 'Numbits := REAL_TO_INT((DI_ERROR.IOERROR[objIdx].ADDR - TRUNC(DI_ERROR.IOERROR[objIdx].ADDR)) * 10);',
                        'addrcalc' : '(DB_IO_DIAGNOSTIC.RDREC_Data[7 + Numbytes] AND SHL(IN := B#16#01, N := Numbits))',
                        'addrcalc2' : '(Numbytes * 8 + Numbits)'}
                if fast_interlocks_DI_present:
                    params['numDI_FI'] = DI_FI_instance_amount
                    params['DIAGNOSTIC_CASE1'] += '''\n                    objIdx := 1;
                    ''' + commonparams['DIAGNOSTIC_CASE'] % {'objname' : 'DI_FI'}
                    params['PERIPHERY_MODULE_DIAGNOSTIC_CASE1'] += '''\n''' + commonparams['PERIPHERY_MODULE_DIAGNOSTIC_CASE'] %\
                        {'objname' : 'DI_FI', 'laddrop' : '', 'objflag' : '54', 'paddr' : 'PEADDR', 'objtype' : 'Input'}
                    params['PERIPHERY_DIAGNOSTIC_CASE1'] += '''\n               objIdx := 1;
                    ''' + commonparams['PERIPHERY_DIAGNOSTIC_CASE'] % {'objname' : 'DI_FI', 'addrcalc' : 'NUMBYTES + INT_TO_REAL(NUMBITS)/10'}
                    params['FCDIAG2_DIAGNOSTIC_CASE1'] += commonparams['FCDIAG2_DIAGNOSTIC_CASE'] %\
                        {'objname' : 'DI_FI', 'laddrop' : '', 'objflag' : 'B4', 'paddr' : 'PEADDR', 'objtype' : 'Input',
                            'numbits' : 'Numbits := REAL_TO_INT((DI_FI_ERROR.IOERROR[objIdx].ADDR - TRUNC(DI_FI_ERROR.IOERROR[objIdx].ADDR)) * 10);',
                            'addrcalc' : '(DB_IO_DIAGNOSTIC.RDREC_Data[7 + Numbytes] AND SHL(IN := B#16#01, N := Numbits))',
                            'addrcalc2' : '(Numbits*10 + Numbytes*8)'}
                    params['FCDIAG3_DIAGNOSTIC_CASE1'] += '\n' + commonparams['FCDIAG3_DIAGNOSTIC_CASE'] %\
                        {'objname' : 'DI_FI', 'laddrop' : '', 'paddr' : 'PEADDR', 'objtype' : 'Input',
                            'numbits' : 'Numbits := REAL_TO_INT((DI_FI_ERROR.IOERROR[objIdx].ADDR - TRUNC(DI_FI_ERROR.IOERROR[objIdx].ADDR)) * 10);',
                            'addrcalc' : '(DB_IO_DIAGNOSTIC.RDREC_Data[7 + Numbytes] AND SHL(IN := B#16#01, N := Numbits))',
                            'addrcalc2' : '(Numbytes * 8 + Numbits)'}

            if device_type_name == "AnalogInput":
                params['numAI'] = instance_amount
                params['DIAGNOSTIC_CASE2'] = '''                2 : ''' + commonparams['DIAGNOSTIC_CASE'] % {'objname' : 'AI'}
                params['PERIPHERY_MODULE_DIAGNOSTIC_CASE2'] = commonparams['PERIPHERY_MODULE_DIAGNOSTIC_CASE'] %\
                    {'objname' : 'AI', 'laddrop' : '', 'objflag' : '54', 'paddr' : 'PEADDR', 'objtype' : 'Input'}
                params['PERIPHERY_DIAGNOSTIC_CASE2'] = '''              2 : ''' + commonparams['PERIPHERY_DIAGNOSTIC_CASE'] %\
                    {'objname' : 'AI', 'addrcalc' : '2*BYTE_TO_INT(RALRM_AINFO_Data[START_ADDR + 1])'} 
                params['FCDIAG2_DIAGNOSTIC_CASE2'] = commonparams['FCDIAG2_DIAGNOSTIC_CASE'] %\
                    {'objname' : 'AI', 'laddrop' : '', 'objflag' : 'B4', 'paddr' : 'PEADDR', 'objtype' : 'Input', 'numbits' : '',
                        'addrcalc' : '(DB_IO_DIAGNOSTIC.RDREC_Data[7] AND SHL(IN := B#16#01, N := Numbytes/2))',
                        'addrcalc2' : '(Numbytes/2)'}
                params['FCDIAG3_DIAGNOSTIC_CASE2'] = commonparams['FCDIAG3_DIAGNOSTIC_CASE'] %\
                    {'objname' : 'AI', 'laddrop' : '', 'paddr' : 'PEADDR', 'objtype' : 'Input',
                        'numbits' : '',
                        'addrcalc' : '(DB_IO_DIAGNOSTIC.RDREC_Data[7] AND SHL(IN := B#16#01, N := Numbytes/2))',
                        'addrcalc2' : '(Numbytes/2)'}

            if device_type_name == "DigitalOutput":
                if fast_interlocks_DO_present:
                    instance_amount = instance_amount - DO_FI_instance_amount
                params['numDO'] = instance_amount
                params['DIAGNOSTIC_CASE3'] = '''                3 : ''' + commonparams['DIAGNOSTIC_CASE'] % {'objname' : 'DO'}
                params['PERIPHERY_MODULE_DIAGNOSTIC_CASE3'] = commonparams['PERIPHERY_MODULE_DIAGNOSTIC_CASE'] %\
                    {'objname' : 'DO', 'laddrop' : 'W#16#8000 OR ', 'objflag' : '55', 'paddr' : 'PAADDR', 'objtype' : 'Output'}
                params['PERIPHERY_DIAGNOSTIC_CASE3'] = '''              3 : ''' + commonparams['PERIPHERY_DIAGNOSTIC_CASE'] %\
                    {'objname' : 'DO', 'addrcalc' : 'NUMBYTES + INT_TO_REAL(NUMBITS)/10'}
                params['FCDIAG2_DIAGNOSTIC_CASE3'] = commonparams['FCDIAG2_DIAGNOSTIC_CASE'] %\
                    {'objname' : 'DO', 'laddrop' : 'W#16#8000 OR ', 'objflag' : 'B5', 'paddr' : 'PAADDR', 'objtype' : 'Output',
                        'numbits' : 'Numbits := REAL_TO_INT((DO_ERROR.IOERROR[objIdx].ADDR - TRUNC(DO_ERROR.IOERROR[objIdx].ADDR)) * 10);',
                        'addrcalc' : '(DB_IO_DIAGNOSTIC.RDREC_Data[7 + Numbytes] AND SHL(IN := B#16#01, N := Numbits))',
                        'addrcalc2' : '(Numbits*10 + Numbytes*8)'}
                params['FCDIAG3_DIAGNOSTIC_CASE3'] = commonparams['FCDIAG3_DIAGNOSTIC_CASE'] %\
                    {'objname' : 'DO', 'laddrop' : 'W#16#8000 OR ', 'paddr' : 'PAADDR', 'objtype' : 'Output',
                        'numbits' : 'Numbits := REAL_TO_INT((DO_ERROR.IOERROR[objIdx].ADDR - TRUNC(DO_ERROR.IOERROR[objIdx].ADDR)) * 10);',
                        'addrcalc' : '(DB_IO_DIAGNOSTIC.RDREC_Data[7 + Numbytes] AND SHL(IN := B#16#01, N := Numbits))',
                        'addrcalc2' : '(Numbytes * 8 + Numbits)'}
                if fast_interlocks_DO_present:
                    params['numDO_FI'] = DO_FI_instance_amount
                    params['DIAGNOSTIC_CASE3'] += '''\n                    objIdx := 1;
                    ''' + commonparams['DIAGNOSTIC_CASE'] % {'objname' : 'DO_FI'}
                    params['PERIPHERY_MODULE_DIAGNOSTIC_CASE3'] += '''\n''' + commonparams['PERIPHERY_MODULE_DIAGNOSTIC_CASE'] %\
                        {'objname' : 'DO_FI', 'laddrop' : 'W#16#8000 OR ', 'objflag' : '55', 'paddr' : 'PAADDR', 'objtype' : 'Output'}
                    params['PERIPHERY_DIAGNOSTIC_CASE3'] += '''\n               objIdx := 1;
                    ''' + commonparams['PERIPHERY_DIAGNOSTIC_CASE'] % {'objname' : 'DO_FI', 'addrcalc' : 'NUMBYTES + INT_TO_REAL(NUMBITS)/10'}
                    params['FCDIAG2_DIAGNOSTIC_CASE3'] += commonparams['FCDIAG2_DIAGNOSTIC_CASE'] %\
                        {'objname' : 'DO_FI', 'laddrop' : 'W#16#8000 OR ', 'objflag' : 'B5', 'paddr' : 'PAADDR', 'objtype' : 'Output',
                            'numbits' : 'Numbits := REAL_TO_INT((DO_FI_ERROR.IOERROR[objIdx].ADDR - TRUNC(DO_FI_ERROR.IOERROR[objIdx].ADDR)) * 10);',
                            'addrcalc' : '(DB_IO_DIAGNOSTIC.RDREC_Data[7 + Numbytes] AND SHL(IN := B#16#01, N := Numbits))',
                            'addrcalc2' : '(Numbits*10 + Numbytes*8)'}
                    params['FCDIAG3_DIAGNOSTIC_CASE3'] += '\n' + commonparams['FCDIAG3_DIAGNOSTIC_CASE'] %\
                        {'objname' : 'DO_FI', 'laddrop' : 'W#16#8000 OR ', 'paddr' : 'PAADDR', 'objtype' : 'Output',
                            'numbits' : 'Numbits := REAL_TO_INT((DO_FI_ERROR.IOERROR[objIdx].ADDR - TRUNC(DO_FI_ERROR.IOERROR[objIdx].ADDR)) * 10);',
                            'addrcalc' : '(DB_IO_DIAGNOSTIC.RDREC_Data[7 + Numbytes] AND SHL(IN := B#16#01, N := Numbits))',
                            'addrcalc2' : '(Numbytes * 8 + Numbits)'}

            if device_type_name == "AnalogOutput":
                params['numAO'] = instance_amount
                params['DIAGNOSTIC_CASE4'] = '''                4 : ''' + commonparams['DIAGNOSTIC_CASE'] % {'objname' : 'AO'}
                params['PERIPHERY_MODULE_DIAGNOSTIC_CASE4'] = commonparams['PERIPHERY_MODULE_DIAGNOSTIC_CASE'] %\
                    {'objname' : 'AO', 'laddrop' : 'W#16#8000 OR ', 'objflag' : '55', 'paddr' : 'PAADDR', 'objtype' : 'Output'}
                params['PERIPHERY_DIAGNOSTIC_CASE4'] = '''              4 : ''' + commonparams['PERIPHERY_DIAGNOSTIC_CASE'] %\
                    {'objname' : 'AO', 'addrcalc' : '2*BYTE_TO_INT(RALRM_AINFO_Data[START_ADDR + 1])'} 
                params['FCDIAG2_DIAGNOSTIC_CASE4'] = commonparams['FCDIAG2_DIAGNOSTIC_CASE'] %\
                    {'objname' : 'AO', 'laddrop' : 'W#16#8000 OR ', 'objflag' : 'B5', 'paddr' : 'PAADDR', 'objtype' : 'Output',
                        'numbits' : '',
                        'addrcalc' : '(DB_IO_DIAGNOSTIC.RDREC_Data[7] AND SHL(IN := B#16#01, N := Numbytes/2))',
                        'addrcalc2' : '(Numbytes/2)'}
                params['FCDIAG3_DIAGNOSTIC_CASE4'] = commonparams['FCDIAG3_DIAGNOSTIC_CASE'] %\
                    {'objname' : 'AO', 'laddrop' : 'W#16#8000 OR ', 'paddr' : 'PAADDR', 'objtype' : 'Output',
                        'numbits' : '',
                        'addrcalc' : '(DB_IO_DIAGNOSTIC.RDREC_Data[7] AND SHL(IN := B#16#01, N := Numbytes/2))',
                        'addrcalc2' : '(Numbytes/2)'}

        DPinstances = unicos_project.findMatchingInstances("DigitalInput,DigitalOutput,AnalogInput,AnalogOutput", "'#FEDeviceIOConfig:FEChannel:InterfaceParam10#'!=''")
        PNinstances = unicos_project.findMatchingInstances("DigitalInput,DigitalOutput,AnalogInput,AnalogOutput", "'#FEDeviceIOConfig:FEChannel:InterfaceParam9#'!=''")
        DPslaves = {}
        PNslaves = {}

        for instance in DPinstances:
            DPslave_id = int(instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam10"))
            if not DPslaves.has_key(DPslave_id):
                DPslaves[DPslave_id] = list()
            DPslaves[DPslave_id].append(instance)

        DPslave_ids = DPslaves.keys()
        DPslave_ids.sort()
        for DPslave_id in DPslave_ids:
            params['FC_Diag2'] += '''    (* I/O Objects mapped to DP Slave number %s *)\n''' % DPslave_id
            params['FC_Diag2'] += '''    IF DB_IO_DIAGNOSTIC.OB86_DP_Slave = %s THEN\n''' % DPslave_id
            for instance in DPslaves[DPslave_id]:
                instance_type = instance.getDeviceTypeName()
                instance_number = instance.getInstanceNumber()
                if instance_type == "DigitalInput":
                    params['FC_Diag2'] += '''        DI_ERROR.IOERROR[''' + str(instance_number) + '''].Err := DB_IO_DIAGNOSTIC.OB86_DP_Slave_Error;\n'''
                if instance_type == "DigitalOutput":
                    params['FC_Diag2'] += '''        DO_ERROR.IOERROR[''' + str(instance_number) + '''].Err := DB_IO_DIAGNOSTIC.OB86_DP_Slave_Error;\n'''
                if instance_type == "AnalogInput":
                    params['FC_Diag2'] += '''        AI_ERROR.IOERROR[''' + str(instance_number) + '''].Err := DB_IO_DIAGNOSTIC.OB86_DP_Slave_Error;\n'''
                if instance_type == "AnalogOutput":
                    params['FC_Diag2'] += '''        AO_ERROR.IOERROR[''' + str(instance_number) + '''].Err := DB_IO_DIAGNOSTIC.OB86_DP_Slave_Error;\n'''
            params['FC_Diag2'] += '''    END_IF;\n'''

        for instance in PNinstances:
            PNslave_id = int(instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam9"))
            if not PNslaves.has_key(PNslave_id):
                PNslaves[PNslave_id] = list()
            PNslaves[PNslave_id].append(instance)

        PNslave_ids = PNslaves.keys()
        PNslave_ids.sort()
        for PNslave_id in PNslave_ids:
            params['FC_Diag2'] += '''    (* I/O Objects mapped to PN Station number %s *)\n''' % PNslave_id
            params['FC_Diag2'] += '''    IF DB_IO_DIAGNOSTIC.OB86_PN_Station = %s THEN\n''' % PNslave_id
            for instance in PNslaves[PNslave_id]:
                instance_type = instance.getDeviceTypeName()
                instance_number = instance.getInstanceNumber()
                if instance_type == "DigitalInput":
                    params['FC_Diag2'] += '''        DI_ERROR.IOERROR[''' + str(instance_number) + '''].Err := DB_IO_DIAGNOSTIC.OB86_PN_Station_Error;\n'''
                if instance_type == "DigitalOutput":
                    params['FC_Diag2'] += '''        DO_ERROR.IOERROR[''' + str(instance_number) + '''].Err := DB_IO_DIAGNOSTIC.OB86_PN_Station_Error;\n'''
                if instance_type == "AnalogInput":
                    params['FC_Diag2'] += '''        AI_ERROR.IOERROR[''' + str(instance_number) + '''].Err := DB_IO_DIAGNOSTIC.OB86_PN_Station_Error;\n'''
                if instance_type == "AnalogOutput":
                    params['FC_Diag2'] += '''        AO_ERROR.IOERROR[''' + str(instance_number) + '''].Err := DB_IO_DIAGNOSTIC.OB86_PN_Station_Error;\n'''
            params['FC_Diag2'] += '''    END_IF;\n'''

        return diagnostic_db % params
