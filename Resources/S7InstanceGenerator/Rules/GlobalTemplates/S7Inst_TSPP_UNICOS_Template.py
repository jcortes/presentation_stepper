# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Communication Objects.
import os
from S7Inst_Generic_Template import S7Inst_Generic_Template


class TSPP_UNICOS_Template(S7Inst_Generic_Template):

    def process(self, unicos_project, xml_config, generate_global_files, *_):
        """ Generate CPC_TSPP_UNICOS.scl """
        self.thePlugin.writeInUABLog("Generate CPC_TSPP_UNICOS.scl")

        FIdeviceVector = self.thePlugin.get_instances_FI("DigitalAlarm")
        fast_interlock_present = not FIdeviceVector.isEmpty() # Fast interlock presence detected by any fast interlock digital alarm
        input_file = open(os.path.join(self.thePlugin.getPluginConfigPath("GeneralData:ConfigFolder"),
                                       "CPC_TSPP_UNICOS.scl"))

        skip = False
        plc_type = xml_config.getPLCDeclarations()[0].getPLCType().getValue()
        plc_name = xml_config.getPLCDeclarations()[0].getName()
        nbStatusTables = self.thePlugin.getSumNbStatusTable()

        for line in input_file:
            if "SEND                : BSEND;" in line:
                rightBSEND = "BSEND"
                bsendComment = " // Dynamic call to the right BSEND depending on the connection interface. In this case, PLCType = " + plc_type
                if plc_type == "S7-300":
                    local_interface = xml_config.getPLCParameter(plc_name + ":SiemensSpecificParameters:PLCS7Connection:LocalInterface")
                    bsendComment += " using " + local_interface
                    if local_interface == "Integrated PN/DP":
                        rightBSEND = "BSEND_S7300PNDP"
                    elif local_interface == "External CP":
                        rightBSEND = "BSEND_S7300"
                line = "SEND                : %s; %s\n" % (rightBSEND, bsendComment)
            if "START Redundancy Code" in line:
                skip = True
            if "ListEventSize := " in line:
                line = "ListEventSize := %s;      // Size of the Event List\n"
                line = line % xml_config.getPLCParameter("SiemensSpecificParameters:GeneralConfiguration:EventBufferSize")
            if "Redundant_PLC :=" in line:
                line = "Redundant_PLC := %s;      // Using a S7-400H PLC in Redundance mode\n"
                redundant_enabled = "false"
                if plc_type == "S7-400H":
                    redundant_enabled = xml_config.getPLCParameter("S7-400HParameters:PlcConfigParameters:RedundantModeEnabled")
                line = line % redundant_enabled.upper()
            if line.startswith("MaxStatusTable"):
                line = "MaxStatusTable      := %s; // Max number of status tables (the length of one \n" % nbStatusTables
            if line.startswith("MaxTableInOneSend"):
                line = "MaxTableInOneSend := %s; // (MAX value S7-400:250  S7-300:100)\n" % min(nbStatusTables, 150)
            if "START Fast Interlock Code" in line:
                if fast_interlock_present:
                    continue
                else:
                    skip = True
            if "END Fast Interlock Code" in line:
            	skip = False
                continue

            if not skip:
                self.thePlugin.writeInstanceInfo("CPC_TSPP_UNICOS.scl", line)
            if "END Redundancy Code" in line:
                skip = False
