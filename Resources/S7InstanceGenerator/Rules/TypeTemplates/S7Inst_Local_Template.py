# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Communication Objects.
from S7Inst_Generic_Template import S7Inst_Generic_Template


class Local_Template(S7Inst_Generic_Template):

    def process(self, *params):
        current_device_type = params[0]
        self.thePlugin.writeInUABLog("processInstances in Jython for %s." % self.device_name)
        instance_list = current_device_type.getAllDeviceTypeInstances()
        spec_version = self.thePlugin.getUnicosProject().getProjectDocumentation().getSpecsVersion()

        params = {"spec_version": spec_version,
                  "INSTANCES_DB": [],
                  "TYPE_ManRequest": [],
                  "TYPE_bin_Status": [],
                  "TYPE_event": [],
                  "InstancesNumber": str(instance_list.size()),
                  "LOCAL_SET": [],
                  "INSTANCES_EXEC": []}

        self.fill_communication_interface(self.device_type_definition.getAttributeFamily(), params)

        for idx, instance in enumerate(instance_list, 1):
            if instance:
                name = instance.getAttributeData("DeviceIdentification:Name")
                params["LOCAL_SET"].append('''    %s   : LOCAL_bin_Status;''' % name)
                params["INSTANCES_DB"].append(self.get_instance_db(instance, name))
                params["INSTANCES_EXEC"].append(self.get_instance_fc(instance, name, idx))
        self.writeDeviceInstances(self.process_template(".scl", params))

    def get_instance_fc(self, instance, name, idx):
        instance_fc = '''// ----------------------------------------------
// ---- Local <%(idx)s>: %(name)s
// ----------------------------------------------
old_status1 := DB_bin_status_LOCAL.%(name)s.StsReg01;
old_status1 := ROR(IN:=old_status1, N:=16);

%(name)s.ManReg01:=DB_LOCAL_ManRequest.LOCAL_Requests[%(idx)s].ManReg01;

%(HFOn_link)s
%(HFOff_link)s
// IOError
%(io_error)s
// IOSimu
%(io_simu)s
// Calls the Baseline function
CPC_FB_LOCAL.%(name)s();

DB_bin_status_LOCAL.%(name)s.stsreg01:= %(name)s.Stsreg01;
DB_Event_LOCAL.LOCAL_evstsreg[%(idx)s].evstsreg01 := old_status1 OR DB_bin_status_LOCAL.%(name)s.stsreg01;
'''
        params = {
            "name": name,
            "idx": idx,
            "HFOn_link": "",
            "HFOff_link": "",
            "io_error": "",
            "io_simu": ""
        }

        feedback_on = self.spec.get_s7db_id(instance, "FEDeviceEnvironmentInputs:Feedback On", "DigitalInput")
        if feedback_on:
            params['HFOn_link'] = '''%s.HFOn := %s.PosSt;''' % (name, feedback_on)

        feedback_off = self.spec.get_s7db_id(instance, "FEDeviceEnvironmentInputs:Feedback Off", "DigitalInput")
        if feedback_off:
            params['HFOff_link'] = '''%s.HFOff := %s.PosSt;''' % (name, feedback_off)

        linked_objects = [feedback_on, feedback_off]
        params['io_error'], params['io_simu'] = self.get_io_error_and_simu(name, linked_objects, exclude_itself=True)

        return instance_fc % params

    def get_parreg_value(self, instance):
        par_reg = ['0'] * 16
        par_reg[14] = self.spec.get_bit_from_attribute(instance, "FEDeviceEnvironmentInputs:Feedback On")
        par_reg[13] = self.spec.get_bit_from_attribute(instance, "FEDeviceEnvironmentInputs:Feedback Off")
        par_reg[9] = self.spec.get_bit_from_attribute(instance, "FEDeviceParameters:ParReg:Full/Empty Animation", on_value='Full/Empty')
        par_reg[5] = self.spec.get_bit_from_attribute(instance, "FEDeviceParameters:ParReg:Position Alarm", on_value='true')
        return "".join(par_reg)

    def get_instance_db(self, instance, name):
        instance_db = '''
DATA_BLOCK  %(name)s  CPC_FB_LOCAL
BEGIN
    PLocal.ParReg :=  2#%(ParReg)s;
END_DATA_BLOCK'''
        params = {"name": name,
                  "ParReg": self.get_parreg_value(instance)}
        return instance_db % params
