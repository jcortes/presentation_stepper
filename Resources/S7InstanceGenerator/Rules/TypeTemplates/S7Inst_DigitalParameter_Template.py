# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Communication Objects.
from S7Inst_Generic_Template import S7Inst_Generic_Template


class DigitalParameter_Template(S7Inst_Generic_Template):

    def process(self, *params):
        current_device_type = params[0]
        self.thePlugin.writeInUABLog("processInstances in Jython for %s." % self.device_name)
        instance_list = current_device_type.getAllDeviceTypeInstances()
        spec_version = self.thePlugin.getUnicosProject().getProjectDocumentation().getSpecsVersion()

        params = {'spec_version': spec_version,
                  'TYPE_ManRequest': [],
                  'TYPE_bin_Status': [],
                  'TYPE_event': [],
                  'DPAR_SET': [],
                  'instance_amount': instance_list.size(),
                  'list_of_variables': [],
                  'static_variables': []}

        self.fill_communication_interface(self.device_type_definition.getAttributeFamily(), params)

        for idx, instance in enumerate(instance_list, 1):
            name = instance.getAttributeData("DeviceIdentification:Name")
            params['list_of_variables'].append('''//  [%s]    %s      ''' % (idx, name))
            params['static_variables'].append('''   %s       : CPC_DB_DPAR;''' % name)
            default_value = instance.getAttributeData("FEDeviceParameters:Default Value").lower()
            if default_value == "false":
                default_value = "W#16#0"
            else:
                default_value = "W#16#1"
            params["DPAR_SET"].append('''DPAR_SET.%s.PosSt := %s;''' % (name, default_value))

        self.writeDeviceInstances(self.process_template(".scl", params))
