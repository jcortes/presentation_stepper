# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Communication Objects.
from S7Inst_Generic_Template import S7Inst_Generic_Template


class AnalogInput_Template(S7Inst_Generic_Template):

    def process(self, *params):
        current_device_type = params[0]
        self.thePlugin.writeInUABLog("processInstances in Jython for %s." % self.device_name)
        instance_list = current_device_type.getAllDeviceTypeInstances()
        spec_version = self.thePlugin.getUnicosProject().getProjectDocumentation().getSpecsVersion()

        limit_size = int(self.thePlugin.getTargetDeviceInformationParam("LimitSize", "AnalogInput"))
        self.thePlugin.writeDebugInUABLog("LimitSize = " + str(limit_size))

        params = {'spec_version': spec_version,
                  'instance_amount': instance_list.size(),
                  'list_of_variables': [],
                  'DB_AI_ALL_S': '',
                  'FOF_AI_all': [],
                  'FOF_amount': 0,
                  'TYPE_ManRequest': [],
                  'TYPE_bin_Status': [],
                  'TYPE_event': [],
                  'TYPE_ana_Status': [],
                  'OPTIMIZED_BLOCK': [],
                  'OPTIMIZED_FB_CALL': [],
                  'ERROR_DB': ''}

        self.fill_communication_interface(self.device_type_definition.getAttributeFamily(), params)

        diagnostic = self.thePlugin.getPluginParameter("GeneralData:DiagnosticChannel")
        if diagnostic == "true":
            params['ERROR_DB'] = self.get_error_db(instance_list)

        block_idx = 0
        filtered_devices_per_optimized_block = {0: 0}
        for idx, instance in enumerate(instance_list, 1):
            name = instance.getAttributeData("DeviceIdentification:Name")
            params['list_of_variables'].append('''//  [%s]    %s''' % (idx, name))
            filter_time = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Filtering Time (s)"))
            if filter_time != '':
                params['FOF_amount'] += 1
                params['FOF_AI_all'].append("FOF[%s].FilterTime :=%s;" % (params['FOF_amount'], filter_time))
                filtered_devices_per_optimized_block[block_idx] += 1
            if idx == limit_size:
                block_idx += 1
                filtered_devices_per_optimized_block[block_idx] = 0

        if params['FOF_amount'] == 0:
            params['FOF_amount'] = 1

        is_large_application = self.spec.is_large_application()
        if is_large_application:
            params['DB_AI_ALL_S'] = self.get_simplified_block(instance_list)

        # split DB based on limit size
        optimized_blocks_amount = (instance_list.size() - 1) / limit_size
        for block_number in range(optimized_blocks_amount + 1):
            block = self.get_optimized_block(block_number, limit_size, instance_list, is_large_application, filtered_devices_per_optimized_block)
            params['OPTIMIZED_BLOCK'].append(block)
            params['OPTIMIZED_FB_CALL'].append('''    FB_AI_all%(block_number)s.DB_AI_all%(block_number)s();''' % {'block_number': AnalogInput_Template.get_block_number(block_number)})

        self.writeDeviceInstances(self.process_template(".scl", params))

    def get_simplified_block(self, instance_list):
        params = {'AI_SET': []}
        for idx, instance in enumerate(instance_list, 1):
            name = instance.getAttributeData("DeviceIdentification:Name")
            params['AI_SET'].append('''            %s       : CPC_DB_AI_S;    // AI number <%s>''' % (name, idx))
        return self.process_template("_simplified.scl", params)

    def get_optimized_block(self, block_number, limit, instance_list, is_large_application, filtered_devices_per_optimized_block):
        begin_index = block_number * limit
        end_index = begin_index + limit

        params = {'block_number': AnalogInput_Template.get_block_number(block_number),
                  'AI_SET': [],
                  'LARGE_APPLICATION': '',
                  'DB_AI_all': [],
                  'FOF_START_IDX': 1,
                  'diagnostic_logic': '',
                  'begin_index': begin_index + 1,
                  'end_index': end_index if end_index < instance_list.size() else instance_list.size()}

        for num in filtered_devices_per_optimized_block:
            if num < block_number:
                params['FOF_START_IDX'] += filtered_devices_per_optimized_block[num]

        for idx, instance in enumerate(instance_list[begin_index:end_index], params['begin_index']):
            name = instance.getAttributeData("DeviceIdentification:Name")
            params['AI_SET'].append('''   %s       : CPC_DB_AI;    // AI number <%s>''' % (name, idx))
            params['DB_AI_all'].append(self.get_instance_assignment(instance, idx))
            params['DB_AI_all'].append(self.get_instance_io_config(instance, name))

        diagnostic = self.thePlugin.getPluginParameter("GeneralData:DiagnosticChannel")
        if diagnostic == "true":
            params['ERROR_DB'] = self.get_error_db(instance_list)
            params['diagnostic_logic'] = '''    

    IF  AI[I].PMaxRaw >  32767 - ((AI[I].PMaxRaw - AI[I].PMinRaw)/10) THEN  //Overflow in the calculation of PMaxRow high limit ( +10% of the Row range)
        IoErrorH := AI[I].HFPos > 32765;                                    // IO Error when end of scale
    ELSE
        IoErrorH := AI[I].HFPos > ((AI[I].PMaxRaw - AI[I].PMinRaw)/10) + AI[I].PMaxRaw; //IO Error when HFPos bigger than MaxRow range + 10% of Row range
    END_IF;
    
    IF  AI[I].PMinRaw <  -32768 + ((AI[I].PMaxRaw - AI[I].PMinRaw)/10) THEN //Overflow in the calculation of PMaxRow low limit ( -10% of the Row range)
        IoErrorL := AI[I].HFPos < -32766;                                   // IO Error when end of scale
    ELSE
        IoErrorL := AI[I].HFPos < AI[I].PMinRaw - ((AI[I].PMaxRaw - AI[I].PMinRaw)/10);  //IO Error when HFPos smaller than MinRow - 10% of Row range
    END_IF;
    
    AI[I].IoError := AI_ERROR.IOERROR[I].Err OR IoErrorH OR IoErrorL;'''

        else:
            params['diagnostic_logic'] = '''    // No diagnostic\n'''
            params['diagnostic_logic'] += '''    // AI[I].IoError := AI_ERROR.IOERROR[I].Err;'''

        if is_large_application:
            params['LARGE_APPLICATION'] = '''
    DB_AI_All_S.DD [(I-1)*SIZE_DB_AI_S_IN_BYTES]:= REAL_TO_DWORD (AI[I].PosSt);
    DB_AI_All_S.DX [((I-1)*SIZE_DB_AI_S_IN_BYTES)+OFFSET_FOMOST_IN_BYTES,BIT_FOMOST]:= AI[I].FoMoSt;
    DB_AI_All_S.DX [((I-1)*SIZE_DB_AI_S_IN_BYTES)+OFFSET_IOERRORW_IN_BYTES,BIT_IOERRORW]:= AI[I].IOErrorW;
    DB_AI_All_S.DX [((I-1)*SIZE_DB_AI_S_IN_BYTES)+OFFSET_IOSIMUW_IN_BYTES,BIT_IOSIMUW]:= AI[I].IOSimuW;'''

        return self.process_template("_optimized.scl", params)

    def get_instance_assignment(self, instance, idx):
        optimized_db_all = '''
// AI number <%(index)s>
AI_SET.%(name)s.PMaxRan := %(PMaxRan)s;
AI_SET.%(name)s.PMinRan := %(PMinRan)s;
AI_SET.%(name)s.PMaxRaw := %(PMaxRaw)s;
AI_SET.%(name)s.PMinRaw := %(PMinRaw)s;
AI_SET.%(name)s.PDb := %(PDb)s;
AI_SET.%(name)s.index := %(index)s;
AI_SET.%(name)s.FOFEn:=%(FOFEn)s;
AI_SET.%(name)s.FEType:=%(FEType)s;'''
        params = {
            'name': instance.getAttributeData("DeviceIdentification:Name"),
            'index': idx,
            'PMaxRan': self.spec.get_attribute_value(instance, "FEDeviceParameters:Range Max"),
            'PMinRan': self.spec.get_attribute_value(instance, "FEDeviceParameters:Range Min"),
            'PMaxRaw': instance.getAttributeData("FEDeviceParameters:Raw Max"),
            'PMinRaw': instance.getAttributeData("FEDeviceParameters:Raw Min"),
            'PDb': self.spec.get_attribute_value(instance, "FEDeviceParameters:Deadband (%)"),
            'FEType': self.spec.get_attribute_value(instance, "FEDeviceIOConfig:FE Encoding Type", "0")
        }
        filter_time = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Filtering Time (s)"))
        if filter_time != "":
            params['FOFEn'] = "TRUE"
        else:
            params['FOFEn'] = "FALSE"
        return optimized_db_all % params

    def get_instance_io_config(self, instance, name):

        fe_config = '''AI_SET.%(name)s.perAddress := %(address)s;
AI_SET.%(name)s.PiwDef := %(piw_def)s;'''

        params = {
            'name': instance.getAttributeData("DeviceIdentification:Name")}

        fe_type = instance.getAttributeData("FEDeviceIOConfig:FE Encoding Type")
        if fe_type == '1':
            address = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1").strip().lower()
            if address.startswith('p'):
                params['piw_def'] = 'true'
                params['address'] = address.replace('piw', '').replace('pib', '')

            else:
                params['piw_def'] = 'false'
                params['address'] = address.replace('iw', '').replace('ib', '')
            return fe_config % params
        else:
            return ''''''

    def get_error_db(self, instance_list):
        error_db = '''(*DB for IoError on Channels with OB82*)
DATA_BLOCK AI_ERROR
TITLE = AI_ERROR
//
// DB with IOError signals of AI
//
AUTHOR: AB_CO_IS
NAME: Error
FAMILY: Error
STRUCT
     IOERROR : ARRAY[1..%(instance_amount)s] OF CPC_IOERROR;
END_STRUCT
BEGIN

%(assignments)s

END_DATA_BLOCK'''
        params = {'instance_amount': instance_list.size()}

        assignments = []
        for idx, instance in enumerate(instance_list, 1):
            interface_param1 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1").strip().lower()
            if interface_param1.startswith('p'):
                interface_param1 = interface_param1.replace('piw', '').replace('pib', '')
            else:
                interface_param1 = interface_param1.replace('iw', '').replace('ib', '')
            if interface_param1:
                assignments.append('''IOERROR[%s].ADDR := %s.0;''' % (str(idx), interface_param1))

        params['assignments'] = "\n".join(assignments)

        return error_db % params
