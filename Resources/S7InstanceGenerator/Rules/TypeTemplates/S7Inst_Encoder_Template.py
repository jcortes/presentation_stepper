# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Communication Objects.
from S7Inst_Generic_Template import S7Inst_Generic_Template


class Encoder_Template(S7Inst_Generic_Template):

    def process(self, *params):
        current_device_type = params[0]
        self.thePlugin.writeInUABLog("processInstances in Jython for %s." % self.device_name)
        instance_list = current_device_type.getAllDeviceTypeInstances()
        spec_version = self.thePlugin.getUnicosProject().getProjectDocumentation().getSpecsVersion()

        params = {'spec_version': spec_version,
                  'instance_amount': instance_list.size(),
                  'TYPE_ana_Status': [],
                  'TYPE_ManRequest': [],
                  'TYPE_bin_Status': [],
                  'TYPE_event': [],
                  'LIST_OF_VARIABLES': [],
                  'STATIC_VARIABLES': [],
                  'ASSIGNMENTS': [],
                  'diagnostic_logic': '',
                  'ERROR_DB': ''}

        self.fill_communication_interface(self.device_type_definition.getAttributeFamily(), params)

        for idx, instance in enumerate(instance_list, 1):
            name = instance.getAttributeData("DeviceIdentification:Name")
            params['LIST_OF_VARIABLES'].append("//  [%s]    %s" % (idx, name))
            params['STATIC_VARIABLES'].append('''   %s       : CPC_DB_ENC;''' % name)
            params['ASSIGNMENTS'].append(self.get_instance_assignment(instance, name, idx))
            params['ASSIGNMENTS'].append(self.get_instance_io_config(instance, name))

        diagnostic = self.thePlugin.getPluginParameter("GeneralData:DiagnosticChannel")
        if diagnostic == "true":
            params['diagnostic_logic'] = '''ENC[I].IoError := ENC_ERROR.IOERROR[I].Err;'''
            params['ERROR_DB'] = self.get_error_db(instance_list)
        else:
            params['diagnostic_logic'] = '''// No diagnostic\n'''
            params['diagnostic_logic'] += '''// ENC[I].IoError := ENC_ERROR.IOERROR[I].Err;'''

        self.writeDeviceInstances(self.process_template(".scl", params))

    def get_instance_assignment(self, instance, name, idx):
        instance_assignment = '''
ENC_SET.%(name)s.PMaxRan := %(PMaxRan)s;
ENC_SET.%(name)s.PMinRan := %(PMinRan)s;
ENC_SET.%(name)s.PMaxRaw := %(PMaxRaw)s;
ENC_SET.%(name)s.PMinRaw := %(PMinRaw)s;
ENC_SET.%(name)s.PDb := %(PDb)s;
ENC_SET.%(name)s.index := %(index)s;
ENC_SET.%(name)s.FEType:= %(FEType)s;
ENC_SET.%(name)s.PEType:= %(PEType)s;
'''
        params = {
            'name': name,
            'index': idx,
            'PMaxRan': self.spec.get_attribute_value(instance, "FEDeviceParameters:Range Max", "0.0"),
            'PMinRan': self.spec.get_attribute_value(instance, "FEDeviceParameters:Range Min", "0.0"),
            'PMaxRaw': instance.getAttributeData("FEDeviceParameters:Raw Max"),
            'PMinRaw': instance.getAttributeData("FEDeviceParameters:Raw Min"),
            'PDb': self.spec.get_attribute_value(instance, "FEDeviceParameters:Deadband (%)", "0.0"),
            'FEType': self.spec.get_attribute_value(instance, "FEDeviceIOConfig:FE Encoding Type", "0"),
            'PEType': ""
        }
        encoder_type = instance.getAttributeData("FEDeviceParameters:Encoder Type")
        if encoder_type == "":
            params['PEType'] = '0'
        elif encoder_type == "Linac":
            params['PEType'] = '1'
        elif encoder_type == "Moore":
            params['PEType'] = '2'

        return instance_assignment % params

    @staticmethod
    def get_instance_io_config(instance, name):
        instance_io = '''ENC_SET.%(name)s.perAddress:= %(per_address)s;
ENC_SET.%(name)s.PiwDef:= %(piw_def)s;'''

        fe_type = instance.getAttributeData("FEDeviceIOConfig:FE Encoding Type")
        if fe_type == "101" or fe_type == "102":
            interface_param1 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1").strip().lower()
            # TODO rewrite for specific prefix in interface_param1
            if interface_param1.startswith("p"):
                interface_param1 = interface_param1[3:]
                piw_def = "true"
            else:
                interface_param1 = interface_param1[2:]
                piw_def = "false"
            params = {
                'name': name,
                'piw_def': piw_def,
                'per_address': interface_param1
            }
            return instance_io % params
        else:
            return ""

    @staticmethod
    def get_error_db(instance_list):
        error_db = '''(*DB for IoError on Channels with OB82*)
DATA_BLOCK ENC_ERROR
TITLE = ENC_ERROR
//
// DB with IOError signals of ENC
//
AUTHOR: AB_CO_IS
NAME: Error
FAMILY: Error
STRUCT
 IOERROR : ARRAY[1..%(instance_amount)s] OF CPC_IOERROR;
END_STRUCT
BEGIN

%(assignments)s

END_DATA_BLOCK'''
        params = {'instance_amount': instance_list.size()}

        assignments = []
        for idx, instance in enumerate(instance_list, 1):
            interface_param1 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1").strip().lower()
            if interface_param1.startswith('p'):
                interface_param1 = interface_param1.replace('piw', '').replace('pib', '')
            else:
                interface_param1 = interface_param1.replace('iw', '').replace('ib', '')
            if interface_param1:
                assignments.append('''IOERROR[%s].ADDR := %s.0;''' % (idx, interface_param1))

        params['assignments'] = "\n".join(assignments)

        return error_db % params
