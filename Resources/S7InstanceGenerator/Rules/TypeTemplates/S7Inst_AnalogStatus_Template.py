# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Communication Objects.
from S7Inst_Generic_Template import S7Inst_Generic_Template


class AnalogStatus_Template(S7Inst_Generic_Template):

    def process(self, *params):
        current_device_type = params[0]
        self.thePlugin.writeInUABLog("processInstances in Jython for %s." % self.device_name)
        instance_list = current_device_type.getAllDeviceTypeInstances()
        spec_version = self.thePlugin.getUnicosProject().getProjectDocumentation().getSpecsVersion()

        limit_size = int(self.thePlugin.getTargetDeviceInformationParam('LimitSize', 'AnalogStatus'))
        self.thePlugin.writeDebugInUABLog('LimitSize = ' + str(limit_size))

        params = {
            'spec_version': spec_version,
            'instance_amount': len(instance_list),
            'list_of_variables': [],
            'TYPE_ana_Status': [],
            'FOF_amount': 0,
            'FOF_AS_all': [],
            'OPTIMIZED_BLOCK': [],
            'OPTIMIZED_FB_CALL': [],
        }

        self.fill_communication_interface(self.device_type_definition.getAttributeFamily(), params)

        filter_index = 0
        block_idx = 0
        filtered_devices_per_optimized_block = {0: 0}

        for idx, instance in enumerate(instance_list, 1):
            name = instance.getAttributeData('DeviceIdentification:Name')
            params['list_of_variables'].append('''// [%s] %s''' % (idx, name))
            filter_time = self.thePlugin.formatNumberPLC(instance.getAttributeData('FEDeviceParameters:Filtering Time (s)'))
            if filter_time != '':
                params['FOF_amount'] += 1
                params['FOF_AS_all'].append('''    FOF[%s].FilterTime := %s;''' % (params['FOF_amount'], filter_time))
                filtered_devices_per_optimized_block[block_idx] += 1
            if idx == limit_size:
                block_idx += 1
                filtered_devices_per_optimized_block[block_idx] = 0

        if params['FOF_amount'] == 0:
            params['FOF_amount'] = 1

        # split DB based on limit size
        optimized_blocks_amount = (instance_list.size() - 1) / limit_size
        for block_number in range(optimized_blocks_amount + 1):
            block = self.get_optimized_block(block_number, limit_size, instance_list, filtered_devices_per_optimized_block)
            params['OPTIMIZED_BLOCK'].append(block)
            params['OPTIMIZED_FB_CALL'].append('''    FB_AS_all%(block_number)s.DB_AS_all%(block_number)s();''' % {'block_number': AnalogStatus_Template.get_block_number(block_number)})

        self.writeDeviceInstances(self.process_template(".scl", params))

    def get_optimized_block(self, block_number, limit, instance_list, filtered_devices_per_optimized_block):
        begin_index = block_number * limit
        end_index = begin_index + limit

        params = {'block_number': AnalogStatus_Template.get_block_number(block_number),
                  'AS_SET': [],
                  'DB_AS_all': [],
                  'FOF_START_IDX': 1,
                  'begin_index': begin_index + 1,
                  'end_index': end_index if end_index < instance_list.size() else instance_list.size()}

        for num in filtered_devices_per_optimized_block:
            if num < block_number:
                params['FOF_START_IDX'] += filtered_devices_per_optimized_block[num]

        for idx, instance in enumerate(instance_list[begin_index:end_index], params['begin_index']):
            name = instance.getAttributeData("DeviceIdentification:Name")
            params['AS_SET'].append('''   %s       : CPC_DB_AS;    // AS number <%s>''' % (name, idx))
            params['DB_AS_all'].append(self.get_instance_assignment(instance, idx))
            params['DB_AS_all'].append(self.get_instance_io_config(instance, name))

        return self.process_template("_optimized.scl", params)

    def get_instance_assignment(self, instance, idx):
        optimized_db_all = '''
AS_SET.%(name)s.index := %(index)s;
AS_SET.%(name)s.FOFEn := %(FOFEn)s;
AS_SET.%(name)s.FEType := %(FEType)s;'''
        params = {
            'name': instance.getAttributeData("DeviceIdentification:Name"),
            'index': idx,
            'FEType': self.spec.get_attribute_value(instance, "FEDeviceIOConfig:FE Encoding Type", "0")
        }

        filter_time = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Filtering Time (s)"))
        if filter_time != "":
            params['FOFEn'] = "TRUE"
        else:
            params['FOFEn'] = "FALSE"

        return optimized_db_all % params

    def get_instance_io_config(self, instance, name):
        fe_config = '''AS_SET.%(name)s.InterfaceParam1 := %(interface_param1)s;
%(interface_param2)s
%(piwdef)s'''

        name = instance.getAttributeData("DeviceIdentification:Name")

        params = {
            'name': name,
            'interface_param1' : '''''',
            'interface_param2' : '''''',
            'piwdef': ''''''}

        fe_type = instance.getAttributeData("FEDeviceIOConfig:FE Encoding Type")
        interface_param1 = instance.getAttributeData('FEDeviceIOConfig:FEChannel:InterfaceParam1').strip().lower()
        if fe_type == '1':
            if interface_param1.startswith('pid'):
                params['piwdef'] = ('''AS_SET.%(name)s.PIWDef := TRUE;''' % {'name': name})
            else:
                params['piwdef'] = ('''AS_SET.%(name)s.PIWDef := FALSE;''' % {'name': name})
            params['interface_param1'] = interface_param1.strip('pid').strip('id')
        elif fe_type == '100':
            if interface_param1.startswith("md"):
                interface_param1 = interface_param1[2:]
            params['interface_param1'] = interface_param1
        elif fe_type == '101':
            if interface_param1.startswith("db"):
                interface_param1 = interface_param1[2:]
            params['interface_param1'] = interface_param1

            interface_param2 = instance.getAttributeData('FEDeviceIOConfig:FEChannel:InterfaceParam2').strip().lower()
            if interface_param2.startswith("dbd"):
                interface_param2 = interface_param2[3:]
            params['interface_param2'] = ('''AS_SET.%(name)s.InterfaceParam2 := %(interface_param2)s;''' % {'name': name, 'interface_param2': interface_param2})
        else:
            return '''\n'''
        return fe_config % params
