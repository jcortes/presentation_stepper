# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Communication Objects.
from S7Inst_Generic_Template import S7Inst_Generic_Template


# noinspection PyClassHasNoInit,PyClassHasNoInit
class AnaDO_Template(S7Inst_Generic_Template):

    def process(self, *params):
        current_device_type = params[0]
        self.thePlugin.writeInUABLog("processInstances in Jython for %s." % self.device_name)
        instance_list = current_device_type.getAllDeviceTypeInstances()
        spec_version = self.thePlugin.getUnicosProject().getProjectDocumentation().getSpecsVersion()

        params = {'spec_version': spec_version,
                  'instance_amount': instance_list.size(),
                  'TYPE_ana_Status': [],
                  'TYPE_ManRequest': [],
                  'TYPE_bin_Status': [],
                  'TYPE_event': [],
                  'DB_AnaDO_ManRequest': [],
                  'DB_bin_status_AnaDO': [],
                  'DB_ana_status_AnaDO': [],
                  'INSTANCE_DB': [],
                  'FC_AnaDO': []}

        self.fill_communication_interface(self.device_type_definition.getAttributeFamily(), params)

        for idx, instance in enumerate(instance_list, 1):
            name = instance.getAttributeData("DeviceIdentification:Name")
            params['DB_bin_status_AnaDO'].append("    %s   : AnaDO_bin_Status;" % name)
            params['DB_ana_status_AnaDO'].append("    %s   : AnaDO_ana_Status;" % name)
            params['INSTANCE_DB'].append(self.get_instance_db(instance))
            params['FC_AnaDO'].append(self.get_instance_fc(instance, name, idx))

        for idx, instance in enumerate(instance_list, 1):
            range_min, range_max = self.get_range(self.thePlugin, instance)
            range_threshold = 0.1 * (float(range_max) - float(range_min))
            limit_off = self.spec.get_attribute_value(instance, "FEDeviceManualRequests:Parameter Limit Off/Closed", float(range_min) + range_threshold)
            limit_on = self.spec.get_attribute_value(instance, "FEDeviceManualRequests:Parameter Limit On/Open", float(range_max) - range_threshold)
            params['DB_AnaDO_ManRequest'].append("    AnaDO_Requests[%s].PliOff:= %s;" % (idx, limit_off))
            params['DB_AnaDO_ManRequest'].append("    AnaDO_Requests[%s].PliOn:= %s;" % (idx, limit_on))

        self.writeDeviceInstances(self.process_template(".scl", params))

    def get_instance_fc(self, instance, name, idx):
        instance_fc = '''// ----------------------------------------------
// ---- AnaDO <%(idx)s>: %(name)s

// ----------------------------------------------
old_status1 := DB_bin_status_AnaDO.%(name)s.stsreg01;
old_status2 := DB_bin_status_AnaDO.%(name)s.stsreg02;
old_status1 := ROR(IN:=old_status1, N:=16);
old_status2 := ROR(IN:=old_status2, N:=16);
%(name)s.ManReg01:=DB_AnaDO_ManRequest.AnaDO_Requests[%(idx)s].ManReg01;
%(name)s.MPosR:=DB_AnaDO_ManRequest.AnaDO_Requests[%(idx)s].MPosR;
%(name)s.PLiOn:=DB_AnaDO_ManRequest.AnaDO_Requests[%(idx)s].PLiOn;
%(name)s.PLiOff:=DB_AnaDO_ManRequest.AnaDO_Requests[%(idx)s].PLiOff;
%(PWDt_link)s
%(PWDb_link)s
%(HFOn_link)s
%(HFPos_link)s
%(HLD_link)s
%(HOnR_link)s
%(HOffR_link)s
// IOError
%(io_error)s
// IOSimu
%(io_simu)s
// Calls the Baseline function
CPC_FB_AnaDO.%(name)s();
%(OutOV_link)s
%(OutOnOV_link)s

//Reset AuAuMoR and AuAlAck
%(name)s.AuAuMoR := FALSE;
%(name)s.AuAlAck := FALSE;
%(name)s.AuRStart := FALSE;

//Recopy new status
DB_bin_status_AnaDO.%(name)s.stsreg01:= %(name)s.Stsreg01;
DB_bin_status_AnaDO.%(name)s.stsreg02:= %(name)s.Stsreg02;
DB_ana_status_AnaDO.%(name)s.PosSt:= %(name)s.PosSt;
DB_ana_status_AnaDO.%(name)s.AuPosRSt:= %(name)s.AuPosRSt;
DB_ana_status_AnaDO.%(name)s.MPosRSt:= %(name)s.MPosRSt;
DB_ana_status_AnaDO.%(name)s.PosRSt:= %(name)s.PosRSt;
DB_Event_AnaDO.AnaDO_evstsreg[%(idx)s].evstsreg01 := old_status1 OR DB_bin_status_AnaDO.%(name)s.stsreg01;
DB_Event_AnaDO.AnaDO_evstsreg[%(idx)s].evstsreg02 := old_status2 OR DB_bin_status_AnaDO.%(name)s.stsreg02;
'''
        params = {
            'name': name,
            'idx': idx,
            'PWDt_link': '',
            'PWDb_link': '',
            'HFOn_link': '',
            'HFPos_link': '',
            'HLD_link': '',
            'HOnR_link': '',
            'HOffR_link': '',
            'OutOV_link': '',
            'OutOnOV_link': '',
            'io_error': '',
            'io_simu': ''
        }

        warning_delay = self.spec.get_s7db_id(instance, "FEDeviceParameters:Warning Time Delay (s)", "AnalogParameter,AnalogStatus,AnalogInput,AnalogInputReal")
        if warning_delay:
            params['PWDt_link'] = '''%s.PAnalog.PWDt := DINT_TO_TIME(REAL_TO_DINT(%s.PosSt*1000.0));''' % (name, warning_delay)

        deadband = self.spec.get_s7db_id(instance, "FEDeviceParameters:Warning Deadband Value (Unit)", "AnalogParameter,AnalogStatus,AnalogInput,AnalogInputReal")
        if deadband:
            params['PWDb_link'] = '''%s.PAnalog.PWDb := %s.PosSt;''' % (name, deadband)

        feedback_on = self.spec.get_s7db_id(instance, "FEDeviceEnvironmentInputs:Feedback On", "DigitalInput")
        if feedback_on:
            params['HFOn_link'] = '''%s.HFOn := %s.PosSt;''' % (name, feedback_on)

        feedback_analog = self.spec.get_s7db_id(instance, "FEDeviceEnvironmentInputs:Feedback Analog", "AnalogInput, AnalogInputReal, AnalogStatus, WordStatus")
        if feedback_analog:
            params['HFPos_link'] = '''%s.HFPos := %s.PosSt;''' % (name, feedback_analog)

        local_drive = self.spec.get_s7db_id(instance, "FEDeviceEnvironmentInputs:Local Drive", "DigitalInput")
        if local_drive:
            params['HLD_link'] = '''%s.HLD := %s.PosSt;''' % (name, local_drive)

        local_on = self.spec.get_s7db_id(instance, "FEDeviceEnvironmentInputs:Local On", "DigitalInput")
        if local_on:
            params['HOnR_link'] = '''%s.HOnR := %s.PosSt;''' % (name, local_on)

        local_off = self.spec.get_s7db_id(instance, "FEDeviceEnvironmentInputs:Local Off", "DigitalInput")
        if local_off:
            params['HOffR_link'] = '''%s.HOffR := %s.PosSt;''' % (name, local_off)

        analog_process_output = self.spec.get_s7db_id(instance, "FEDeviceOutputs:Analog Process Output", "AnalogOutput, AnalogOutputReal")
        if analog_process_output:
            params['OutOV_link'] = '''%s.AuposR := %s.OutOV;''' % (analog_process_output, name)

        digital_process_output = self.spec.get_s7db_id(instance, "FEDeviceOutputs:Digital Process Output", "DigitalOutput")
        if digital_process_output:
            params['OutOnOV_link'] = '''%s.AuposR := %s.OutOnOV;''' % (digital_process_output, name)

        linked_objects = [feedback_on, feedback_analog, local_drive, local_on, local_off, analog_process_output, digital_process_output]
        params['io_error'], params['io_simu'] = self.get_io_error_and_simu(name, linked_objects)

        return instance_fc % params

    @staticmethod
    def get_range(plugin, instance):
        range_min = "0.0"
        range_max = "100.0"
        analog_process_output = instance.getAttributeData("FEDeviceOutputs:Analog Process Output")
        if analog_process_output != "":
            unicos_project = plugin.getUnicosProject()
            linked_devices = unicos_project.findMatchingInstances("AnalogOutput, AnalogOutputReal", "'#DeviceIdentification:Name#'='%s'" % analog_process_output)
            if len(linked_devices) > 0:
                range_min = plugin.formatNumberPLC(linked_devices[0].getAttributeData("FEDeviceParameters:Range Min"))
                range_max = plugin.formatNumberPLC(linked_devices[0].getAttributeData("FEDeviceParameters:Range Max"))

        return range_min, range_max

    def get_parreg_value(self, instance):
        par_reg = ['0'] * 15
        par_reg[14] = self.spec.get_bit_from_attribute(instance, "FEDeviceParameters:ParReg:Fail-Safe", on_value="on/open")
        par_reg[13] = self.spec.get_bit_from_attribute(instance, "FEDeviceEnvironmentInputs:Feedback On")
        par_reg[11] = self.spec.get_bit_from_attribute(instance, "FEDeviceEnvironmentInputs:Feedback Analog")
        par_reg[10] = self.spec.get_bit_from_attribute(instance, "FEDeviceEnvironmentInputs:Local Drive")
        par_reg[9] = self.spec.get_bit_from_attribute(instance, "FEDeviceEnvironmentInputs:Hardware Analog Output")
        par_reg[6] = self.spec.get_bit_from_attribute(instance, "FEDeviceParameters:ParReg:Manual Restart after Full Stop", off_value="false")
        par_reg[5] = self.spec.get_bit_from_attribute(instance, "FEDeviceParameters:ParReg:Manual Restart after Full Stop", on_value="true even if full stop still active")
        return "".join(par_reg)

    def get_instance_db(self, instance):
        instance_db = '''
DATA_BLOCK %(name)s CPC_FB_AnaDO
BEGIN
    PAnalog.PArReg :=  2#%(PArReg)s;
    PAnalog.PWDt := T#%(PWDt)ss;
    PAnalog.PWDb := %(PWDb)s;
    PAnalog.PMinSpd := %(PMinSpd)s;
    PAnalog.PMDeSpd := %(PMDeSpd)s;
    PAnalog.PMStpInV := %(PMStpInV)s;
    PAnalog.PMStpDeV := %(PMStpDeV)s;
    PAnalog.PMaxRan := %(PMaxRan)s;
    PAnalog.PMinRan := %(PMinRan)s;
END_DATA_BLOCK
'''
        params = {
            'name': instance.getAttributeData("DeviceIdentification:Name"),
            'PArReg': self.get_parreg_value(instance)
        }

        range_min, range_max = self.get_range(self.thePlugin, instance)
        output_range = float(range_max) - float(range_min)
        params['PMaxRan'] = range_max
        params['PMinRan'] = range_min
        params['PWDt'] = self.spec.get_attribute_value(instance, "FEDeviceParameters:Warning Time Delay (s)", "5.0")
        # default increase/decrease speed makes if from 0% to 100% in 10 seconds
        params['PMinSpd'] = self.spec.get_attribute_value(instance, "FEDeviceParameters:Manual Increase Speed (Unit/s)", output_range / 10)
        params['PMDeSpd'] = self.spec.get_attribute_value(instance, "FEDeviceParameters:Manual Decrease Speed (Unit/s)", output_range / 10)
        # default step is 1% of the range
        params['PWDb'] = self.spec.get_attribute_value(instance, "FEDeviceParameters:Warning Deadband Value (Unit)", output_range / 100)
        params['PMStpInV'] = self.spec.get_attribute_value(instance, "FEDeviceParameters:Manual Increase Step (Unit)", output_range / 100)
        params['PMStpDeV'] = self.spec.get_attribute_value(instance, "FEDeviceParameters:Manual Decrease Step (Unit)", output_range / 100)

        return instance_db % params
