# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Communication Objects.
from S7Inst_Generic_Template import S7Inst_Generic_Template


class WordStatus_Template(S7Inst_Generic_Template):

    def process(self, *params):
        current_device_type = params[0]
        self.thePlugin.writeInUABLog("processInstances in Jython for %s." % self.device_name)
        instance_list = current_device_type.getAllDeviceTypeInstances()
        spec_version = self.thePlugin.getUnicosProject().getProjectDocumentation().getSpecsVersion()

        limit_size = int(self.thePlugin.getTargetDeviceInformationParam("LimitSize", "WordStatus"))
        self.thePlugin.writeDebugInUABLog("LimitSize = " + str(limit_size))

        params = {'spec_version': spec_version,
                  'instance_amount': len(instance_list),
                  'list_of_variables': [],
                  'TYPE_ana_Status': [],
                  'OPTIMIZED_BLOCK': [],
                  'OPTIMIZED_FB_CALL': [], }

        self.fill_communication_interface(self.device_type_definition.getAttributeFamily(), params)

        filter_index = 0
        for idx, instance in enumerate(instance_list, 1):
            name = instance.getAttributeData('DeviceIdentification:Name')
            params['list_of_variables'].append('''// [%s] %s''' % (idx, name))

        # split DB based on limit size
        optimized_blocks_amount = (instance_list.size() - 1) / limit_size
        for block_number in range(optimized_blocks_amount + 1):
            block = self.get_optimized_block(block_number, limit_size, instance_list)
            params['OPTIMIZED_BLOCK'].append(block)
            params['OPTIMIZED_FB_CALL'].append('''    FB_WS_all%(block_number)s.DB_WS_all%(block_number)s();''' % {'block_number': WordStatus_Template.get_block_number(block_number)})

        self.writeDeviceInstances(self.process_template(".scl", params))

    def get_optimized_block(self, block_number, limit, instance_list):
        begin_index = block_number * limit
        end_index = begin_index + limit

        params = {'block_number': WordStatus_Template.get_block_number(block_number),
                  'WS_SET': [],
                  'DB_WS_all': [],
                  'begin_index': begin_index + 1,
                  'end_index': end_index if end_index < instance_list.size() else instance_list.size()}

        for idx, instance in enumerate(instance_list[begin_index:end_index], params['begin_index']):
            name = instance.getAttributeData("DeviceIdentification:Name")
            params['WS_SET'].append('''   %s       : CPC_DB_WS;    // WS number <%s>''' % (name, idx))
            params['DB_WS_all'].append(self.get_instance_assignment(instance, idx))
            params['DB_WS_all'].append(self.get_instance_io_config(instance, name))

        return self.process_template("_optimized.scl", params)

    def get_instance_assignment(self, instance, idx):
        optimized_db_all = '''
WS_SET.%(name)s.index := %(index)s;
WS_SET.%(name)s.FEType := %(FEType)s;'''
        params = {
            'name': instance.getAttributeData("DeviceIdentification:Name"),
            'FEType': self.spec.get_attribute_value(instance, "FEDeviceIOConfig:FE Encoding Type", "0"),
            'index': idx
        }
        return optimized_db_all % params

    @staticmethod
    def get_instance_io_config(instance, name):
        result = []
        interface_param1 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1").strip().lower()
        interface_param2 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam2").strip().lower()
        fe_type = instance.getAttributeData("FEDeviceIOConfig:FE Encoding Type")
        if fe_type == '1':
            if interface_param1.startswith('pib') or interface_param1.startswith('ib'):
                read_byte = 'true'
            else:
                read_byte = 'false'
            if interface_param1.startswith('p'):
                piw_def = 'true'
                interface_param1 = interface_param1.replace('piw', '').replace('pib', '')
            else:
                piw_def = 'false'
                interface_param1 = interface_param1.replace('iw', '').replace('ib', '')
            result.append('''WS_SET.%s.InterfaceParam1 := %s;''' % (name, interface_param1))
            result.append('''WS_SET.%s.PIWDef := %s;''' % (name, piw_def))
            result.append('''WS_SET.%s.ReadByte := %s;''' % (name, read_byte))
        elif fe_type == '100':
            if interface_param1.startswith('mb'):
                read_byte = 'true'
                interface_param1 = interface_param1.replace('mb', '')
            else:
                read_byte = 'false'
                interface_param1 = interface_param1.replace('mw', '')
            result.append('''WS_SET.%s.InterfaceParam1 := %s;''' % (name, interface_param1))
            result.append('''WS_SET.%s.ReadByte := %s;''' % (name, read_byte))
        elif fe_type == '101':
            interface_param1 = interface_param1.replace('db', '')
            if interface_param2.startswith('dbb'):
                read_byte = 'true'
                interface_param2 = interface_param2.replace('dbb', '')
            else:
                read_byte = 'false'
            result.append('''WS_SET.%s.InterfaceParam1 := %s;''' % (name, interface_param1))
            result.append('''WS_SET.%s.InterfaceParam2 := %s;''' % (name, interface_param2))
            result.append('''WS_SET.%s.ReadByte := %s;''' % (name, read_byte))
        return '\n'.join(result)
