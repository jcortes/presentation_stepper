# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Communication Objects.
from S7Inst_Generic_Template import S7Inst_Generic_Template


class AnalogOutputReal_Template(S7Inst_Generic_Template):

    def process(self, *params):
        current_device_type = params[0]
        self.thePlugin.writeInUABLog("processInstances in Jython for %s." % self.device_name)
        instance_list = current_device_type.getAllDeviceTypeInstances()
        spec_version = self.thePlugin.getUnicosProject().getProjectDocumentation().getSpecsVersion()

        params = {'spec_version': spec_version,
                  'TYPE_ManRequest': [],
                  'TYPE_bin_Status': [],
                  'TYPE_event': [],
                  'TYPE_ana_Status': [],
                  'ASSIGNMENTS': [],
                  'instance_amount': instance_list.size(),
                  'list_of_variables': [],
                  'static_variables': []}

        self.fill_communication_interface(self.device_type_definition.getAttributeFamily(), params)
        for idx, instance in enumerate(instance_list, 1):
            name = instance.getAttributeData("DeviceIdentification:Name")
            params['list_of_variables'].append('''//  [%s]    %s''' % (idx, name))
            params['static_variables'].append('''   %s       : CPC_DB_AOR;''' % name)
            params['ASSIGNMENTS'].append(self.get_instance_assignment(instance, idx))
            params['ASSIGNMENTS'].append(self.get_instance_io_config(instance, name))

        self.writeDeviceInstances(self.process_template(".scl", params))

    def get_instance_assignment(self, instance, idx):
        instance_assignment = '''
AOR_SET.%(name)s.PMaxRan := %(PMaxRan)s;
AOR_SET.%(name)s.PMinRan := %(PMinRan)s;
AOR_SET.%(name)s.PMaxRaw := %(PMaxRaw)s;
AOR_SET.%(name)s.PMinRaw := %(PMinRaw)s;
AOR_SET.%(name)s.index := %(index)s;
AOR_SET.%(name)s.FEType := %(FEType)s;
AOR_SET.%(name)s.PQWDef:=%(PQWDef)s;
'''
        params = {
            'name': instance.getAttributeData("DeviceIdentification:Name"),
            'index': idx,
            'PMaxRan': self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Range Max")),
            'PMinRan': self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Range Min")),
            'PMaxRaw': self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Raw Max")),
            'PMinRaw': self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Raw Min")),
            'FEType': self.spec.get_attribute_value(instance, "FEDeviceIOConfig:FE Encoding Type", "0")
        }
        interface_param1 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1").strip().lower()
        if interface_param1.startswith('p'):
            params['PQWDef'] = "TRUE"
        else:
            params['PQWDef'] = "FALSE"

        return instance_assignment % params

    @staticmethod
    def get_instance_io_config(instance, name):
        result = []
        db_num = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1").strip().lower()
        db_pos = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam2").strip().lower()
        db_num_io_error = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam3").strip().lower()
        db_pos_io_error = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam4").strip().lower()
        db_bit_io_error = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam5").strip().lower()
        fe_type = instance.getAttributeData("FEDeviceIOConfig:FE Encoding Type")

        db_num = db_num.replace("db", "").replace("pqd", "").replace("qd", "")
        db_pos = db_pos.replace("dbd", "").replace("pqb", "").replace("qb", "")
        db_num_io_error = db_num_io_error.replace("db", "")
        db_pos_io_error = db_pos_io_error.replace("dbb", "").replace("dbx", "")
        db_bit_io_error = db_bit_io_error.replace("dbx", "")

        if fe_type == "101":
            result.append('''AOR_SET.%s.DBnum := %s;''' % (name, db_num))
            result.append('''AOR_SET.%s.DBpos := %s;''' % (name, db_pos))

        if fe_type == "102" or fe_type == "103":
            result.append('''AOR_SET.%s.DBnum := %s;''' % (name, db_num))
            result.append('''AOR_SET.%s.DBpos := %s;''' % (name, db_pos))
            result.append('''AOR_SET.%s.DBnumIOerror := %s;''' % (name, db_num_io_error))
            result.append('''AOR_SET.%s.DBposIOerror := %s;''' % (name, db_pos_io_error))
            result.append('''AOR_SET.%s.DBbitIOerror := %s;''' % (name, db_bit_io_error))

        if fe_type == "201":
            result.append('''AOR_SET.%s.DBnum := %s; // PA valve address IW%s''' % (name, db_num, db_num))

        if fe_type == "205":
            result.append('''AOR_SET.%s.DBnum:=%s; // Write Profibus DP device with 4 bytes consistency check (4 bytes for value only, IO error evaluation)''' % (name, db_num))

        return '\n'.join(result)
