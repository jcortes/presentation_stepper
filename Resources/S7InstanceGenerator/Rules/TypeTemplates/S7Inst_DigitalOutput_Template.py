# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Communication Objects.
from S7Inst_Generic_Template import S7Inst_Generic_Template


class DigitalOutput_Template(S7Inst_Generic_Template):

    def process(self, *params):
        current_device_type = params[0]
        self.thePlugin.writeInUABLog("processInstances in Jython for %s." % self.device_name)
        instance_list = current_device_type.getAllDeviceTypeInstances()
        spec_version = self.thePlugin.getUnicosProject().getProjectDocumentation().getSpecsVersion()

        params = {"spec_version": spec_version,
                  "TYPE_ManRequest": [],
                  "TYPE_bin_Status": [],
                  "TYPE_event": [],
                  "instaceNumber": str(instance_list.size()),
                  "ERROR_DB": "",
                  "DO_FI": ""}
        self.fill_communication_interface(self.device_type_definition.getAttributeFamily(), params)

        diagnostic = self.thePlugin.getPluginParameter("GeneralData:DiagnosticChannel").lower()

        FI_instance_list = self.thePlugin.get_instances_FI("DigitalOutput")
        fast_interlock_DO_present = len(FI_instance_list) != 0
        if fast_interlock_DO_present:
            params['DO_FI'] = self.get_all_blocks(FI_instance_list, fast_interlock_DO_present, True) + "\n"
            if diagnostic == "true":
                params['ERROR_DB'] = self.get_error_db(FI_instance_list, True) + "\n"
            #Continue code with not fast interlock instances
            instance_list = list(instance_list)
            for FI_instance in FI_instance_list:
                instance_list.remove(FI_instance)

        if diagnostic == "true":
            params['ERROR_DB'] += self.get_error_db(instance_list, False)
        params['DO'] = self.get_all_blocks(instance_list, fast_interlock_DO_present, False)

        self.writeDeviceInstances(self.process_template(".scl", params))

    def get_all_blocks(self, instance_list, FI_present, FI_block):
        params = {'FI': '_FI' if FI_block else '',
                  'instaceNumber': len(instance_list),
                  'LIST_OF_VARIABLES': [],
                  'block_instance': '',
                  'DO_SET': [],
                  'ASSIGNMENTS': [],
                  'FI_CALL': '',
                  'diagnostic_logic': ''}

        diagnostic = self.thePlugin.getPluginParameter("GeneralData:DiagnosticChannel").lower()
        params['diagnostic_logic'] = self.get_diagnostic(diagnostic, FI_block)

        params['block_instance'] = params['FI'] if FI_block else '_all'
        for idx, instance in enumerate(instance_list, 1):
            name = instance.getAttributeData("DeviceIdentification:Name")
            params["LIST_OF_VARIABLES"].append('''//  [%s]    %s      (DO_bin_Status)''' % (idx, name))
            params["DO_SET"].append('''%s       : CPC_DB_DO;''' % name)
            params["ASSIGNMENTS"].append(self.get_instance_assignment(instance, idx, name))
            params["ASSIGNMENTS"].append(self.get_instance_io_config(instance, name))
        if not FI_block and FI_present:
            params['FI_CALL'] = '''
    VAR_TEMP
      NbOfDelayedInterrupts : INT;
      NbOfQueuedInterrupts  : INT;
    END_VAR

    // Delay interrupt OBs until EN_AIRT is called
    NbOfDelayedInterrupts := DIS_AIRT();
    FB_DO_FI.DB_DO_FI();
    // Reenable interrupt OBs
    NbOfQueuedInterrupts := EN_AIRT();
'''

        return self.process_template("_blocks.scl", params)

    def get_instance_assignment(self, instance, idx, name):
        instance_assignment = '''DO_SET.%(name)s.index := %(index)s;
DO_SET.%(name)s.FEType := %(FEType)s;'''
        params = {
            'name': name,
            'index': idx,
            'FEType': self.spec.get_attribute_value(instance, "FEDeviceIOConfig:FE Encoding Type", "0")

        }
        return instance_assignment % params

    @staticmethod
    def get_instance_io_config(instance, name):
        result = []
        fe_type = instance.getAttributeData("FEDeviceIOConfig:FE Encoding Type")
        interface_param1 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1").strip().lower()
        interface_param2 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam2").strip().lower()
        interface_param3 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam3").strip().lower()
        interface_param4 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam4").strip().lower()
        interface_param5 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam5").strip().lower()
        interface_param6 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam6").strip().lower()
        if fe_type == "1":
            interface_param1 = interface_param1.replace("q", "")
            if "." in interface_param1:
                byte, bit = interface_param1.split(".")
                result.append('''DO_SET.%s.perByte := %s;''' % (name, byte))
                result.append('''DO_SET.%s.perBit := %s;''' % (name, bit))
        elif fe_type == "101":
            interface_param1 = interface_param1.strip("db")
            result.append('''DO_SET.%s.DBnum := %s;''' % (name, interface_param1))
            result.append('''DO_SET.%s.perByte := %s;''' % (name, interface_param2))
            result.append('''DO_SET.%s.perBit := %s;''' % (name, interface_param3))
        elif fe_type == "102" or fe_type == "103":
            db_num = interface_param1.strip("db")
            io_error = interface_param4.strip("db")
            result.append('''DO_SET.%s.DBnum := %s;''' % (name, db_num))
            result.append('''DO_SET.%s.perByte := %s;''' % (name, interface_param2))
            result.append('''DO_SET.%s.perBit := %s;''' % (name, interface_param3))
            result.append('''DO_SET.%s.DBnumIoError := %s;''' % (name, io_error))
            result.append('''DO_SET.%s.DBposIoError := %s;''' % (name, interface_param5))
            result.append('''DO_SET.%s.DBbitIoError := %s;''' % (name, interface_param6))
        result.append("")
        return "\n".join(result)

    @staticmethod
    def get_error_db(instance_list, FI_block):
        error_db = '''(*DB for IoError on Channels with OB82*)
DATA_BLOCK DO%(FI)s_ERROR
TITLE = DO%(FI)s_ERROR
//
// DB with IOError signals of DO
//
AUTHOR: AB_CO_IS
NAME: Error
FAMILY: Error
STRUCT

  IOERROR : ARRAY[1..%(instance_amount)s] OF CPC_IOERROR;

END_STRUCT

BEGIN

%(assignments)s

END_DATA_BLOCK'''
        params = {
            'FI': '_FI' if FI_block else '',
            'instance_amount': len(instance_list)}
        assignments = []
        for idx, instance in enumerate(instance_list, 1):
            interface_param1 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1").strip().lower()
            # TODO: check what type of address can be here
            if interface_param1.startswith('p'):
                interface_param1 = interface_param1[2:8]
                assignments.append('''IOERROR[%s].ADDR := %s;''' % (idx, interface_param1))
            elif interface_param1.startswith('q'):
                interface_param1 = interface_param1[1:8]
                assignments.append('''IOERROR[%s].ADDR := %s;''' % (idx, interface_param1))
        params['assignments'] = "\n".join(assignments)
        return error_db % params

    @staticmethod
    def get_diagnostic(diagnostic, FI_block):
        result = []
        if diagnostic.lower() == "true":
            result.append('''  IF (DDO[I].FEType = 1) THEN''')
            result.append('''    DDO[I].IoError :=  DO%s_ERROR.IOERROR[I].Err;''' %("_FI" if FI_block else ""))
            result.append('''  ELSE''')
            result.append('''    ; // Object without connections.''')
            result.append('''  END_IF;''')
        else:
            result.append('''  // No diagnostic''')
            result.append('''  // DDO[I].IoError :=  DO_ERROR.IOERROR[I].Err;''')
        return "\n".join(result)
