# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Communication Objects.
from S7Inst_Generic_Template import S7Inst_Generic_Template


class DigitalAlarm_Template(S7Inst_Generic_Template):

    def process(self, *params):
        current_device_type = params[0]
        self.thePlugin.writeInUABLog("processInstances in Jython for %s." % self.device_name)
        instance_list = current_device_type.getAllDeviceTypeInstances()
        spec_version = self.thePlugin.getUnicosProject().getProjectDocumentation().getSpecsVersion()

        limit_size = int(self.thePlugin.getTargetDeviceInformationParam("LimitSize", "DigitalAlarm"))
        self.thePlugin.writeDebugInUABLog("LimitSize = " + str(limit_size))

        params = {
            'spec_version': spec_version,
            'instance_amount': instance_list.size(),
            'list_of_variables': [],
            'TYPE_ManRequest': [],
            'TYPE_bin_Status': [],
            'TYPE_event': [],
            'DB_bin_status': [],
            'OPTIMIZED_BLOCK': [],
            'OPTIMIZED_FB_CALL': [],
            'DB_DA_FI': '',
            'FB_DA_FI': '',
            'FC_DA_FI': '',
            'FI_CALL': ''
        }

        self.fill_communication_interface(self.device_type_definition.getAttributeFamily(), params)

        FI_instance_list = self.thePlugin.get_instances_FI("DigitalAlarm")
        fast_interlock_DA_present = len(FI_instance_list) != 0
        if fast_interlock_DA_present:
            params['DB_DA_FI'] = self.get_DB_blocks(FI_instance_list, limit_size, True)
            params['FB_DA_FI'] = self.get_optimized_block(0, len(FI_instance_list), list(FI_instance_list), True)
            params['FC_DA_FI'] = '''FUNCTION FC_DA_FI : VOID
    FB_DA_FI.DB_DA_FI();
END_FUNCTION
'''
            params['FI_CALL'] = '''    VAR_TEMP
      NbOfDelayedInterrupts : INT;
      NbOfQueuedInterrupts  : INT;
    END_VAR

    // Delay interrupt OBs until EN_AIRT is called
    NbOfDelayedInterrupts := DIS_AIRT();
    FB_DA_FI.DB_DA_FI();
    // Reenable interrupt OBs
    NbOfQueuedInterrupts := EN_AIRT();
'''
            #Continue code with not fast interlock instances
            instance_list = list(instance_list)
            for FI_instance in FI_instance_list:
                instance_list.remove(FI_instance)
        params['DB_DA'] = self.get_DB_blocks(instance_list, limit_size, False)
        # split DB based on limit size
        optimized_blocks_amount = (len(instance_list) - 1) / limit_size
        for block_number in range(optimized_blocks_amount + 1):
            block = self.get_optimized_block(block_number, limit_size, instance_list, False)
            params['OPTIMIZED_BLOCK'].append(block)
            params['OPTIMIZED_FB_CALL'].append('''    FB_DA_all%(block_number)s.DB_DA_all%(block_number)s();''' % {'block_number': DigitalAlarm_Template.get_block_number(block_number)})

        self.writeDeviceInstances(self.process_template(".scl", params))

    def get_optimized_block(self, block_number, limit, instance_list, FI_block):
        begin_index = block_number * limit
        end_index = begin_index + limit

        params = {'DA_SET': [],
                  'DB_DA_all': [],
                  'begin_index': begin_index + 1,
                  'end_index': end_index if end_index < len(instance_list) else len(instance_list),
                  'FI': '_FI' if FI_block else '',
                  'instance_FB': ''}

        params['instance_FB'] = params['FI'] if FI_block else '_all' + DigitalAlarm_Template.get_block_number(block_number)

        for idx, instance in enumerate(instance_list[begin_index:end_index], params['begin_index']):
            name = instance.getAttributeData("DeviceIdentification:Name")
            params['DA_SET'].append('''   %s       : CPC_DB_DA;    // DA number <%s>''' % (name, idx))
            params['DB_DA_all'].append(self.get_instance_assignment(instance, name))

        return self.process_template("_optimized.scl", params)

    def get_DB_blocks(self, instance_list, limit_size, FI_block):
        params = {'FI': '_FI' if FI_block else '',
                  'instance_amount': len(instance_list),
                  'list_of_variables': []}

        block_idx = 0
        for idx, instance in enumerate(instance_list, 1):
            name = instance.getAttributeData("DeviceIdentification:Name")
            params['list_of_variables'].append("//  [%s]    %s" % (idx, name))
            if idx == limit_size:
                block_idx += 1
        return self.process_template("_DB.scl", params)

    def get_instance_assignment(self, instance, name):
        instance_assignment = '''
DA_SET.%(name)s.PAuAckAl := %(PAuAckAl)s;
%(PAlDt)s'''

        params = {
            'name': name}

        auto_acknowledgement = instance.getAttributeData('FEDeviceAlarm:Auto Acknowledge').lower().strip()
        params['PAuAckAl'] = 'TRUE' if auto_acknowledgement == 'true' else 'FALSE'

        delay = instance.getAttributeData("FEDeviceParameters:Alarm Delay (s)").strip()
        if delay == "":
            params['PAlDt'] = '''  DA_SET.%s.PAlDt := 0;''' % (name)
        elif self.thePlugin.isString(delay):
            params['PAlDt'] = '''  // The Alarm Delay is defined in the logic'''
        else:
            params['PAlDt'] = '''  DA_SET.%s.PAlDt := %s;''' % (name, int(round(float(delay))))

        return instance_assignment % params
