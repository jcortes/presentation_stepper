# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Communication Objects.
from S7Inst_Generic_Template import S7Inst_Generic_Template


class AnalogInputReal_Template(S7Inst_Generic_Template):

    def process(self, *params):
        current_device_type = params[0]
        self.thePlugin.writeInUABLog("processInstances in Jython for %s." % self.device_name)
        instance_list = current_device_type.getAllDeviceTypeInstances()
        spec_version = self.thePlugin.getUnicosProject().getProjectDocumentation().getSpecsVersion()

        limit_size = int(self.thePlugin.getTargetDeviceInformationParam("LimitSize", "AnalogInputReal"))
        self.thePlugin.writeDebugInUABLog("LimitSize = " + str(limit_size))

        params = {'spec_version': spec_version,
                  'instance_amount': instance_list.size(),
                  'list_of_variables': [],
                  'DB_AIR_ALL_S': '',
                  'FOF_AIR_all': [],
                  'FOF_amount': 0,
                  'TYPE_ana_Status': [],
                  'TYPE_ManRequest': [],
                  'TYPE_bin_Status': [],
                  'TYPE_event': [],
                  'OPTIMIZED_BLOCK': [],
                  'OPTIMIZED_FB_CALL': []}

        self.fill_communication_interface(self.device_type_definition.getAttributeFamily(), params)

        block_idx = 0
        filtered_devices_per_optimized_block = {0: 0}
        for idx, instance in enumerate(instance_list, 1):
            name = instance.getAttributeData("DeviceIdentification:Name")
            params['list_of_variables'].append("//  [%s]    %s" % (idx, name))
            filter_time = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Filtering Time (s)"))
            if filter_time != '':
                params['FOF_amount'] += 1
                params['FOF_AIR_all'].append("FOF[%s].FilterTime :=%s;" % (params['FOF_amount'], filter_time))
                filtered_devices_per_optimized_block[block_idx] += 1
            if idx == limit_size:
                block_idx += 1
                filtered_devices_per_optimized_block[block_idx] = 0

        if params['FOF_amount'] == 0:
            params['FOF_amount'] = 1

        is_large_application = self.spec.is_large_application()
        if is_large_application:
            params['DB_AIR_ALL_S'] = self.get_simplified_block(instance_list)

        # split DB based on limit size
        optimized_blocks_amount = (instance_list.size() - 1) / limit_size
        for block_number in range(optimized_blocks_amount + 1):
            block = self.get_optimized_block(block_number, limit_size, instance_list, is_large_application, filtered_devices_per_optimized_block)
            params['OPTIMIZED_BLOCK'].append(block)
            params['OPTIMIZED_FB_CALL'].append('''    FB_AIR_all%(block_number)s.DB_AIR_all%(block_number)s();''' % {'block_number': AnalogInputReal_Template.get_block_number(block_number)})

        self.writeDeviceInstances(self.process_template(".scl", params))

    def get_simplified_block(self, instance_list):
        params = {'AIR_SET': []}
        for idx, instance in enumerate(instance_list, 1):
            name = instance.getAttributeData("DeviceIdentification:Name")
            params['AIR_SET'].append('''            %s       : CPC_DB_AIR_S;    // AIR number <%s>''' % (name, idx))
        return self.process_template("_simplified.scl", params)

    def get_optimized_block(self, block_number, limit, instance_list, is_large_application, filtered_devices_per_optimized_block):
        begin_index = block_number * limit
        end_index = begin_index + limit

        params = {'block_number': AnalogInputReal_Template.get_block_number(block_number),
                  'AIR_SET': [],
                  'LARGE_APPLICATION': '',
                  'DB_AIR_all': [],
                  'FOF_START_IDX': 1,
                  'begin_index': begin_index + 1,
                  'end_index': end_index if end_index < instance_list.size() else instance_list.size()}

        for num in filtered_devices_per_optimized_block:
            if num < block_number:
                params['FOF_START_IDX'] += filtered_devices_per_optimized_block[num]

        for idx, instance in enumerate(instance_list[begin_index:end_index], params['begin_index']):
            name = instance.getAttributeData("DeviceIdentification:Name")
            params['AIR_SET'].append('''   %s       : CPC_DB_AIR;    // AIR number <%s>''' % (name, idx))
            params['DB_AIR_all'].append(self.get_instance_assignment(instance, idx))
            params['DB_AIR_all'].append(self.get_instance_io_config(instance, name))

        if is_large_application:
            params['LARGE_APPLICATION'] = '''
    DB_AIR_All_S.DD [(I-1)*SIZE_DB_AIR_S_IN_BYTES]:= REAL_TO_DWORD (AIR[I].PosSt);
    DB_AIR_All_S.DX [((I-1)*SIZE_DB_AIR_S_IN_BYTES)+OFFSET_FOMOST_IN_BYTES,BIT_FOMOST]:= AIR[I].FoMoSt;
    DB_AIR_All_S.DX [((I-1)*SIZE_DB_AIR_S_IN_BYTES)+OFFSET_IOERRORW_IN_BYTES,BIT_IOERRORW]:= AIR[I].IOErrorW;
    DB_AIR_All_S.DX [((I-1)*SIZE_DB_AIR_S_IN_BYTES)+OFFSET_IOSIMUW_IN_BYTES,BIT_IOSIMUW]:= AIR[I].IOSimuW;'''

        return self.process_template("_optimized.scl", params)

    def get_instance_assignment(self, instance, idx):
        optimized_db_all = '''// AIR number <%(index)s>
AIR_SET.%(name)s.PMaxRan := %(PMaxRan)s;
AIR_SET.%(name)s.PMinRan := %(PMinRan)s;
AIR_SET.%(name)s.PMaxRaw := %(PMaxRaw)s;
AIR_SET.%(name)s.PMinRaw := %(PMinRaw)s;
AIR_SET.%(name)s.PDb := %(PDb)s;
AIR_SET.%(name)s.index := %(index)s;
AIR_SET.%(name)s.FOFEn := %(FOFEn)s;
AIR_SET.%(name)s.FEType := %(FEType)s;
AIR_SET.%(name)s.PIWDef := %(PIWDef)s;'''
        params = {
            'name': instance.getAttributeData('DeviceIdentification:Name'),
            'index': idx,
            'PMaxRan': self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Range Max")),
            'PMinRan': self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Range Min")),
            'PMaxRaw': self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Raw Max")),
            'PMinRaw': self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Raw Min")),
            'PDb': self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Deadband (%)")),
            'FEType': self.spec.get_attribute_value(instance, "FEDeviceIOConfig:FE Encoding Type", "0")
        }
        filter_time = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Filtering Time (s)"))
        if filter_time != "":
            params['FOFEn'] = "TRUE"
        else:
            params['FOFEn'] = "FALSE"
        interface_param1 = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1").strip().lower()
        if interface_param1.startswith('p'):
            params['PIWDef'] = "TRUE"
        else:
            params['PIWDef'] = "FALSE"
        return optimized_db_all % params

    @staticmethod
    def get_instance_io_config(instance, name):
        result = []
        db_num = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam1").strip().lower()
        db_pos = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam2").strip().lower()
        db_num_io_error = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam3").strip().lower()
        db_pos_io_error = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam4").strip().lower()
        db_bit_io_error = instance.getAttributeData("FEDeviceIOConfig:FEChannel:InterfaceParam5").strip().lower()
        fe_type = instance.getAttributeData("FEDeviceIOConfig:FE Encoding Type")

        db_num = db_num.replace("db", "").replace("pid", "").replace("id", "")
        db_pos = db_pos.replace("dbd", "").replace("pib", "").replace("ib", "")
        db_num_io_error = db_num_io_error.replace("db", "")
        db_pos_io_error = db_pos_io_error.replace("dbb", "").replace("dbx", "")
        db_bit_io_error = db_bit_io_error.replace("dbx", "")

        if fe_type == "101":
            result.append('''AIR_SET.%s.DBnum:=%s;''' % (name, db_num))
            result.append('''AIR_SET.%s.DBpos:=%s;''' % (name, db_pos))

        if fe_type == "102" or fe_type == "103":
            result.append('''AIR_SET.%s.DBnum:=%s;''' % (name, db_num))
            result.append('''AIR_SET.%s.DBpos:=%s;''' % (name, db_pos))
            result.append('''AIR_SET.%s.DBnumIOerror:=%s;''' % (name, db_num_io_error))
            result.append('''AIR_SET.%s.DBposIOerror:=%s;''' % (name, db_pos_io_error))
            result.append('''AIR_SET.%s.DBbitIOerror:=%s;''' % (name, db_bit_io_error))

        if fe_type == "201":
            result.append('''AIR_SET.%s.DBnum:=%s; // PA valve address IW%s''' % (name, db_num, db_num))

        if fe_type == "202":
            result.append('''AIR_SET.%s.DBnum:=%s; // PA address (5 bytes) PID%s''' % (name, db_num, db_num))

        if fe_type == "203":
            result.append('''AIR_SET.%s.DBnum:=%s; // DP address (5 bytes)''' % (name, db_num))

        if fe_type == "204":
            result.append('''AIR_SET.%s.DBnum:=%s; // non-standard DP address (4 bytes for value only, no IO error)''' % (name, db_num))

        if fe_type == "205":
            result.append('''AIR_SET.%s.DBnum:=%s; // Read Profibus DP device with 4 bytes consistency check (4 bytes for value only, IO error evaluation)''' % (name, db_num))

        if fe_type == "206":
            result.append('''AIR_SET.%s.DBnum:=%s; // Read Profibus DP device with 4 bytes consistency check and status byte''' % (name, db_num))
            result.append('''AIR_SET.%s.DBpos:=%s;''' % (name, db_pos))
        return "\n".join(result)
