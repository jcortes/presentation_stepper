# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Encoding UTF-8 without BOM test line with accent: é
from java.util import Vector
from java.util import ArrayList
import S7Logic_DefaultAlarms_Template
# reload(S7Logic_DefaultAlarms_Template)


def BLLogic(thePlugin, theRawInstances, master, name, LparamVector, theCurrentPco, allTheAnalogAlarms, allTheDigitalAlarms):

    Lparam1, Lparam2, Lparam3, Lparam4, Lparam5, Lparam6, Lparam7, Lparam8, Lparam9, Lparam10 = S7Logic_DefaultAlarms_Template.getLparametersSplit(LparamVector)

# Step 1. Create the FUNCTION called DeviceName_BL.
    thePlugin.writeSiemensLogic('''
FUNCTION $name$_BL : VOID
TITLE = '$name$_BL'
//
// Warning of $name$
(*
 Lparam1:	$Lparam1$
 Lparam2:	$Lparam2$
 Lparam3:	$Lparam3$
 Lparam4:	$Lparam4$
 Lparam5:	$Lparam5$
 Lparam6:	$Lparam6$
 Lparam7:	$Lparam7$
 Lparam8:	$Lparam8$
 Lparam9:	$Lparam9$
 Lparam10:	$Lparam10$
*)
//
AUTHOR: 'ICE/PLC'
NAME: 'Logic_W'
FAMILY: 'WARN'

BEGIN


''')
    thePlugin.writeSiemensLogic('''
// ----------------------------------------------------- USER code <begin>------------------------------------------------------------
''')

    thePlugin.writeSiemensLogic('''
(*Error********************************)
$name$.IOError := 0
''')
    thePcoSections = theCurrentPco.getSections()
    for theCurrentSection in thePcoSections:
        if theCurrentSection.getGenerateSection():
            theCurrentSectionName = theCurrentSection.getFullSectionName()
            theCurrentSectionNameLength = len(theCurrentSectionName)
            sectionType = theCurrentSectionName[theCurrentSectionNameLength - 4:]
            if not (sectionType == "CDOL" or sectionType == "INST"):
                thePlugin.writeSiemensLogic('''
	OR DB_ERROR_SIMU.''' + theCurrentSectionName + '''_E ''')
    theDependentDevices = theCurrentPco.getDependentDevices()
    for theDependentDevice in theDependentDevices:
        theDependentDeviceName = theDependentDevice.getDeviceName()
        thePlugin.writeSiemensLogic('''
	OR $theDependentDeviceName$.IoErrorW OR DB_ERROR_SIMU.$theDependentDeviceName$_DL_E''')
    for DigitalAlarm in allTheDigitalAlarms:
        DAName = DigitalAlarm.getAttributeData("DeviceIdentification:Name")
        s7db_id_result = thePlugin.s7db_id(DAName, "DigitalAlarm")
        thePlugin.writeSiemensLogic('''
	OR ''' + s7db_id_result + DAName + '''.IoErrorW''')
    for AnalogAlarm in allTheAnalogAlarms:
        AAName = AnalogAlarm.getAttributeData("DeviceIdentification:Name")
        s7db_id_result = thePlugin.s7db_id(AAName, "AnalogAlarm")
        thePlugin.writeSiemensLogic('''
	OR ''' + s7db_id_result + AAName + '''.IoErrorW''')

    thePlugin.writeSiemensLogic('''
	;''')

    thePlugin.writeSiemensLogic('''
(*SIMU*********************************)
$name$.IOSimu :=  0
''')
    for theCurrentSection in thePcoSections:
        if theCurrentSection.getGenerateSection():
            theCurrentSectionName = theCurrentSection.getFullSectionName()
            theCurrentSectionNameLength = len(theCurrentSectionName)
            sectionType = theCurrentSectionName[theCurrentSectionNameLength - 4:]
            if not (sectionType == "CDOL" or sectionType == "INST"):
                thePlugin.writeSiemensLogic('''
	OR DB_ERROR_SIMU.''' + theCurrentSectionName + '''_S''')
    for theDependentDevice in theDependentDevices:
        theDependentDeviceName = theDependentDevice.getDeviceName()
        thePlugin.writeSiemensLogic('''
	OR $theDependentDeviceName$.IoSimuW OR $theDependentDeviceName$.FoMoSt OR DB_ERROR_SIMU.$theDependentDeviceName$_DL_S''')
    for DigitalAlarm in allTheDigitalAlarms:
        DAName = DigitalAlarm.getAttributeData("DeviceIdentification:Name")
        s7db_id_result = thePlugin.s7db_id(DAName, "DigitalAlarm")
        thePlugin.writeSiemensLogic('''
	OR ''' + s7db_id_result + DAName + '''.IoSimuW''')
    for AnalogAlarm in allTheAnalogAlarms:
        AAName = AnalogAlarm.getAttributeData("DeviceIdentification:Name")
        s7db_id_result = thePlugin.s7db_id(AAName, "AnalogAlarm")
        thePlugin.writeSiemensLogic('''
	OR ''' + s7db_id_result + AAName + '''.IoSimuW''')
    thePlugin.writeSiemensLogic('''
	;''')

    # Step 1.2: Blocked Alarm warning
    thePlugin.writeSiemensLogic('''
(*Blocked Alarm warning ********************)
$name$.AlB :=
''')
    theDAAlarms = theRawInstances.findMatchingInstances("DigitalAlarm", "$name$", "'#FEDeviceAlarm:Type#'!=''")
    theAAAlarms = theRawInstances.findMatchingInstances("AnalogAlarm", "$name$", "'#FEDeviceAlarm:Type#'!=''")

    theDAAlarmsDB1 = [alarm for alarm in theDAAlarms if thePlugin.s7db_id(alarm.getAttributeData("DeviceIdentification:Name")).upper() == "DB_DA_ALL.DA_SET."]
    generatedText = theRawInstances.createSectionText(theDAAlarmsDB1, 1, 1, '''	            DB_DA_All.DA_SET.#DeviceIdentification:Name#.MAlBRSt OR
''')
    thePlugin.writeSiemensLogic(generatedText)

    theDAAlarmsDB2 = [alarm for alarm in theDAAlarms if thePlugin.s7db_id(alarm.getAttributeData("DeviceIdentification:Name")).upper() == "DB_DA_ALL2.DA_SET."]
    generatedText = theRawInstances.createSectionText(theDAAlarmsDB2, 1, 1, '''	            DB_DA_All2.DA_SET.#DeviceIdentification:Name#.MAlBRSt OR
''')
    thePlugin.writeSiemensLogic(generatedText)

    theAAAlarmsDB1 = [alarm for alarm in theAAAlarms if thePlugin.s7db_id(alarm.getAttributeData("DeviceIdentification:Name")).upper() == "DB_AA_ALL.AA_SET."]
    generatedText = theRawInstances.createSectionText(theAAAlarmsDB1, 1, 1, '''                DB_AA_All.AA_SET.#DeviceIdentification:Name#.MAlBRSt OR
''')
    thePlugin.writeSiemensLogic(generatedText)

    theAAAlarmsDB2 = [alarm for alarm in theAAAlarms if thePlugin.s7db_id(alarm.getAttributeData("DeviceIdentification:Name")).upper() == "DB_AA_ALL2.AA_SET."]
    generatedText = theRawInstances.createSectionText(theAAAlarmsDB2, 1, 1, '''                DB_AA_All2.AA_SET.#DeviceIdentification:Name#.MAlBRSt OR
''')
    thePlugin.writeSiemensLogic(generatedText)

    for theDependentDevice in theDependentDevices:
        theDependentDeviceName = theDependentDevice.getDeviceName()
        theDependentDeviceType = theDependentDevice.getDeviceType()
        if (theDependentDeviceType == "Controller" or theDependentDeviceType == "ProcessControlObject"):
            # do nothing
            Test = "NOTHING TO DO"
        else:
            thePlugin.writeSiemensLogic('''
				$theDependentDeviceName$.AlBW OR''')
    thePlugin.writeSiemensLogic('''
      0;
''')
    thePlugin.writeSiemensLogic('''
// ----------------------------------------------------- USER code <end>------------------------------------------------------------
''')
