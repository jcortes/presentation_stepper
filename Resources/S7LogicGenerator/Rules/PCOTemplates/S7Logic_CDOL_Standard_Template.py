# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Encoding UTF-8 without BOM test line with accent: é
from java.util import Vector
from java.util import ArrayList
import S7Logic_DefaultAlarms_Template
# reload(S7Logic_DefaultAlarms_Template)


def CDOLLogic(thePlugin, theRawInstances, master, name, LparamVector, theCurrentPco):

    Lparam1, Lparam2, Lparam3, Lparam4, Lparam5, Lparam6, Lparam7, Lparam8, Lparam9, Lparam10 = S7Logic_DefaultAlarms_Template.getLparametersSplit(LparamVector)

    # Step 1: Create the FUNCTION called PCOName_SectionName.
    thePlugin.writeSiemensLogic('''
FUNCTION $name$_CDOL : VOID
TITLE = '$name$_CDOL'
//
// Common Dependant Object  Logic of $name$
//
(*
 Lparam1:	$Lparam1$
 Lparam2:	$Lparam2$
 Lparam3:	$Lparam3$
 Lparam4:	$Lparam4$
 Lparam5:	$Lparam5$
 Lparam6:	$Lparam6$
 Lparam7:	$Lparam7$
 Lparam8:	$Lparam8$
 Lparam9:	$Lparam9$
 Lparam10:	$Lparam10$
*)
//
AUTHOR: 'ICE/PLC'
NAME: 'Logic_CD'
FAMILY: 'CDOL'
BEGIN
''')

    # Gets all the PCO objects that are child of the 'master' object
    thePcoObjects = theRawInstances.findMatchingInstances("ProcessControlObject", "$name$", None)
    # Gets all the Anadig objects that are child of the 'master' object
    theAnadigObjects = theRawInstances.findMatchingInstances("AnalogDigital", "$name$", None)
    # Gets all the Analog objects that are child of the 'master' object
    theAnalogObjects = theRawInstances.findMatchingInstances("Analog", "$name$", None)
    # Gets all the AnaDO objects that are child of the 'master' object
    theAnaDOObjects = theRawInstances.findMatchingInstances("AnaDO", "$name$", None)
    # Gets all the OnOff objects that are child of the 'master' object
    theOnOffObjects = theRawInstances.findMatchingInstances("OnOff", "$name$", None)
    # Gets all the controllers objects that are child of the 'master' object
    theControllersObjects = theRawInstances.findMatchingInstances("Controller", "$name$", None)

    theDependentObjects = Vector(theAnalogObjects)
    theDependentObjects.addAll(theAnadigObjects)
    theDependentObjects.addAll(theAnaDOObjects)
    theDependentObjects.addAll(theOnOffObjects)

    theDependentDevices = theCurrentPco.getDependentDevices()  # all children

    thePlugin.writeSiemensLogic('''
// ----------------------------------------------------- USER code <end>------------------------------------------------------------
     (* Auto Auto Mode Request.*)
''')
    # Get the dependent_logic object for the current PCO
    # Step 1.1: Set the Auto Mode Request. For each Dependent Device of the current PCO with RE_RunOSt or FE_RunOSt or RE_CStopOSt or RE_AuDepOSt.

    if thePcoObjects.size() > 0 or theDependentObjects.size() > 0 or theControllersObjects.size() > 0:
        thePlugin.writeSiemensLogic('''
		IF $name$.RE_RunOSt OR $name$.FE_RunOSt OR $name$.RE_CStopOSt OR $name$.RE_AuDepOSt THEN
		''')
        for dependentDevice in theDependentDevices:
            dependentDeviceName = dependentDevice.getDeviceName()
            thePlugin.writeSiemensLogic('''
			$dependentDeviceName$.AuAuMoR := TRUE;						
			''')
        thePlugin.writeSiemensLogic('''END_IF;
		''')

    # Step 1.2: Set the Auto Dependent Request for PCO only. For each Dependent Device of the current PCO with RE_RunOSt or FE_RunOSt or RE_CStopOSt or RE_AuDepOSt.
    if thePcoObjects.size() > 0:
        thePlugin.writeSiemensLogic('''
	   (* Auto Dependent Request for PCO only.*)
	   IF $name$.RE_RunOSt 	OR 	$name$.FE_RunOSt OR	$name$.RE_CStopOSt OR $name$.RE_AuDepOSt THEN
''')

        for DependentObject in thePcoObjects:
            DependentObjectName = DependentObject.getAttributeData("DeviceIdentification:Name").strip()
            thePlugin.writeSiemensLogic('''
				$DependentObjectName$.AuAuDepR := TRUE;
				''')
        thePlugin.writeSiemensLogic('''END_IF;
			''')

    # Step 1.3: Set the Auto Alarm Acknowledge. For ANADIG, ANALOG and ONOFF Objects only. Pass the RE_AlUnAck from the current PCO to the AuAlAck of their dependent devices
    if theDependentObjects.size() > 0:
        thePlugin.writeSiemensLogic('''
	    (* Auto Alarm Acknowledge.*)
		IF $name$.E_MAlAckR OR $name$.AuAlAck THEN
''')

        for DependentObject in theDependentObjects:
            DependentObjectName = DependentObject.getAttributeData("DeviceIdentification:Name").strip()
            DependentObjectRestart = DependentObject.getAttributeData("FEDeviceParameters:ParReg:Manual Restart after Full Stop").strip()

            if DependentObjectRestart == "FALSE":
                thePlugin.writeSiemensLogic('''
	$DependentObjectName$.AuAlAck := NOT $DependentObjectName$.FuStopISt;''')
            else:
                thePlugin.writeSiemensLogic('''
	$DependentObjectName$.AuAlAck := TRUE;''')

        thePlugin.writeSiemensLogic('''
		END_IF;
				''')

    thePlugin.writeSiemensLogic('''
// ----------------------------------------------------- USER code <end>------------------------------------------------------------
''')
