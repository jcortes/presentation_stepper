# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
##
# This file contains all the common functions for feature processing
##

from java.util import Vector
from java.util import ArrayList
from java.lang import System
from research.ch.cern.unicos.utilities import SemanticVerifier

import ucpc_library.shared_decorator
reload(ucpc_library.shared_decorator)

def parseFeatures(Features):
    """
    This function takes string containing features with parameters and parses this input to dictionary
    Dictionary keys are names of features
    Values of dictionary are either:
        list of strings (if feature occurs only once in the input string)
        list of lists of strings (if feature occurs more than once in the input string - one list per request)

    The arguments are:
    @param: String with features

    This function returns:
    @return: Dictionary of features with lists of corresponding parameters

    """
    RequestedFeatures = {}  # features requested in spec
    Features = Features.replace('\n', '')
    if Features != '':
        Features = [feature for feature in Features.split(';') if feature != ""]
        for feature in Features:
            if '=' in feature:
                featureName = feature.split('=',1)[0].lower().strip()
                if featureName not in RequestedFeatures:    # not yet in the dictionary
                    RequestedFeatures[featureName] = [f.strip() for f in feature.split('=',1)[-1].split(',')]
                else:   # support feature that is used multiple times
                    if not isinstance(RequestedFeatures[featureName][0], list): # if it's not yet list of lists (=requested only once so far)
                        RequestedFeatures[featureName] = [RequestedFeatures[featureName]] # transform into list of lists
                    RequestedFeatures[featureName].append([f.strip() for f in feature.split('=',1)[-1].split(',')]) 
            else:
                RequestedFeatures[feature.lower().strip()] = []
    return RequestedFeatures

def checkIfObjectExists(objectName, typeList, theRawInstances):
    for type in typeList:
        if theRawInstances.findInstanceByName(type, objectName):
            return True
    return False
    
def checkFeatures(objectName, RequestedFeatures, SupportedFeatures, thePlugin, theRawInstances):
    """
    This function checks all the features for incorrect number of parameters

    The arguments are:
    @param: objectName: name of the object 
    @param: RequestedFeatures: dictionary of features requested by the user
    @param: SupportedFeatures: dictionary of supported features
    @param: thePlugin
    @param: theRawInstances

    """
    for feature in RequestedFeatures.keys():
        if feature not in SupportedFeatures:
            thePlugin.writeErrorInUABLog("Object " + objectName + ": Feature '" + feature + "' is not supported.")
        else:   # feature is supported
            params_to_check = RequestedFeatures[feature]
            
            # to make support for feature occurring multiple times -> convert single-features into multi-features format (list of string list)
            if len(params_to_check) == 0 or not isinstance(params_to_check[0], list):
                params_to_check = [params_to_check]
                
            for param_list in params_to_check:  # iterate through different parameters instances for given feature
                if SupportedFeatures[feature][0]!= -1 and len(param_list) != SupportedFeatures[feature][0]: 
                    thePlugin.writeErrorInUABLog("Object  " + objectName + ": Feature '" + feature + "' has wrong number of parameters (" + str(len(param_list)) + " instead of " + str(SupportedFeatures[feature][0]) + ")")
                else:
                    for (arg, type) in zip(param_list, SupportedFeatures[feature][1:]):
                        if type.lower() != "string":
                            typeList = type.split("|")
                            if not checkIfObjectExists(arg, typeList, theRawInstances):
                                thePlugin.writeErrorInUABLog("Feature '$feature$': argument $arg$: has wrong type, it's not any of: " + ",".join(typeList))

def getConditionsAndPositions(Positioning, DB_Stepper="", listOfSteps=[]):
    Decorator       = ucpc_library.shared_decorator.ExpressionDecorator()               
    if Positioning != "":
        ListOfConditions    = [Decorator.decorateExpression(position.split(':')[0], DB_Stepper, listOfSteps) for position in Positioning.split(',')]
        ListOfPositions     = [Decorator.decorateExpression(position.split(':')[1], DB_Stepper, listOfSteps) for position in Positioning.split(',')]
        return (ListOfConditions, ListOfPositions)
    else:
        return ([], [])