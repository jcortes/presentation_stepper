# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Encoding UTF-8 without BOM test line with accent: é
from java.util import Vector
from java.util import ArrayList
import S7Logic_DefaultAlarms_Template
# reload(S7Logic_DefaultAlarms_Template)
import ucpc_library.shared_decorator
reload(ucpc_library.shared_decorator)


def TLLogic(thePlugin, theRawInstances, master, name, LparamVector):

    Lparam1, Lparam2, Lparam3, Lparam4, Lparam5, Lparam6, Lparam7, Lparam8, Lparam9, Lparam10 = S7Logic_DefaultAlarms_Template.getLparametersSplit(LparamVector)
	
#Lparam 3features parsed
    RequestedFeatures = ProcessFeatures.parseFeatures(Lparam3)
    Lparam3_pconame_0 = RequestedFeatures['pconame'][0]
    Lparam3_motora_0 = RequestedFeatures['motora'][0]
    Lparam3_motorb_0 = RequestedFeatures['motorb'][0]

    
    Decorator = ucpc_library.shared_decorator.ExpressionDecorator()
    # Automatically generated variables
    

# Step 1: Create the FUNCTION called PCOName_SectionName.
    thePlugin.writeSiemensLogic('''
FUNCTION $name$_TL : VOID
TITLE = '$name$_TL'
//
// Transition Logic of $name$
//
(*
 Lparam1: $Lparam1$
 Lparam2: $Lparam2$
 Lparam3: $Lparam3$
 Lparam4: $Lparam4$
 Lparam5: $Lparam5$
 Lparam6: $Lparam6$
 Lparam7: $Lparam7$
 Lparam8: $Lparam8$
 Lparam9: $Lparam9$
 Lparam10:  $Lparam10$
*)
//
AUTHOR: 'ICE/PLC'
NAME: 'Logic_TL'
FAMILY: 'TL'
VAR_TEMP
    pco_transitions0 : WORD;
    pco_transitions0_bit AT pco_transitions0: ARRAY [0..15] OF BOOL;
    pco_transitions1 : WORD;
    pco_transitions1_bit AT pco_transitions1: ARRAY [0..15] OF BOOL;


END_VAR
BEGIN

''')
    thePlugin.writeSiemensLogic('''
// ----------------------------------------------------- USER code <begin>------------------------------------------------------------
''')

    thePlugin.writeSiemensLogic('''

DB_ERROR_SIMU.$name$_TL_E := 0 ; // To complete
DB_ERROR_SIMU.$name$_TL_S := 0 ; // To complete

''')

    thePlugin.writeSiemensLogic('''(* Automatic generated code from StepperSpecs.xlsx using the Siemens_Expert_Stepper_Template <begin> *)
''')

    thePlugin.writeSiemensLogic('''(* VARIABLES COMPUTATION *)



''')

    thePlugin.writeSiemensLogic(Decorator.decorateExpression('''(* TRANSITIONS COMPUTATION *)

(* 1 TO 2 *) Graph_Red.SHUTDOWN_STOP := $Lparam3_pconame_0$.EnRStartSt;
(* 2 TO 1 *) Graph_Red.STOP_SHUTDOWN := $Lparam3_pconame_0$.FuStopISt OR $Lparam3_pconame_0$.TStopISt;
(* 2 TO 3 *) Graph_Red.STOP_STANDBY := $Lparam3_pconame_0$.RunOSt;
(* 3 TO 1 *) Graph_Red.STANDBY_SHUTDOWN := $Lparam3_pconame_0$.FuStopISt OR $Lparam3_pconame_0$.TStopISt;
(* 3 TO 2 *) Graph_Red.STANDBY_STOP := NOT $Lparam3_pconame_0$.RunOSt;
(* 3 TO 4 *) Graph_Red.Start1 := Button05;
(* 3 TO 8 *) Graph_Red.STANDBY_MotorB_In := Button06;
(* 4 TO 1 *) Graph_Red.MotorA_In_SHUTDOWN := $Lparam3_pconame_0$.FuStopISt OR $Lparam3_pconame_0$.TStopISt;
(* 4 TO 2 *) Graph_Red.MotorA_In_STOP := NOT $Lparam3_pconame_0$.RunOSt;
(* 4 TO 5 *) Graph_Red.IN_TO_STAY := $Lparam3_motora_0$_PositionIn;
(* 5 TO 1 *) Graph_Red.MotorA_Stay_SHUTDOWN := $Lparam3_pconame_0$.FuStopISt OR $Lparam3_pconame_0$.TStopISt;
(* 5 TO 2 *) Graph_Red.MotorA_Stay_STOP := NOT $Lparam3_pconame_0$.RunOSt;
(* 5 TO 6 *) Graph_Red.STAY_TO_OUT := $Lparam3_motora_0$_TimerIn> TIME_TO_DINT(Graph_Red.MotorA_Stay.T)*1000.0;
(* 6 TO 1 *) Graph_Red.MotorA_Out_SHUTDOWN := $Lparam3_pconame_0$.FuStopISt OR $Lparam3_pconame_0$.TStopISt;
(* 6 TO 2 *) Graph_Red.MotorA_Out_STOP := NOT $Lparam3_pconame_0$.RunOSt;
(* 6 TO 7 *) Graph_Red.OUT_TO_TIMEOUT := $Lparam3_motora_0$_PositionOut;
(* 7 TO 1 *) Graph_Red.MotorA_Timeout_SHUTDOWN := $Lparam3_pconame_0$.FuStopISt OR $Lparam3_pconame_0$.TStopISt;
(* 7 TO 2 *) Graph_Red.MotorA_Timeout_STOP := NOT $Lparam3_pconame_0$.RunOSt;
(* 7 TO 8 *) Graph_Red.MotorA_Timeout_MotorB_In := $Lparam3_motora_0$_TimerIn> TIME_TO_DINT(Graph_Red.MotorA_Timeout.T)*1000.0;
(* 8 TO 1 *) Graph_Red.MotorB_In_SHUTDOWN := $Lparam3_pconame_0$.FuStopISt OR $Lparam3_pconame_0$.TStopISt;
(* 8 TO 2 *) Graph_Red.MotorB_In_STOP := NOT $Lparam3_pconame_0$.RunOSt;
(* 8 TO 9 *) Graph_Red.MotorB_In_MotorB_Stay := $Lparam3_motorb_0$_PositionIn;
(* 9 TO 1 *) Graph_Red.MotorB_Stay_SHUTDOWN := $Lparam3_pconame_0$.FuStopISt OR $Lparam3_pconame_0$.TStopISt;
(* 9 TO 2 *) Graph_Red.MotorB_Stay_STOP := NOT $Lparam3_pconame_0$.RunOSt;
(* 9 TO 10 *) Graph_Red.MotorB_Stay_MotorB_Out := $Lparam3_motorb_0$_TimerIn> TIME_TO_DINT(Graph_Red.MotorA_Stay.T)*1000.0;
(* 10 TO 1 *) Graph_Red.MotorB_Out_SHUTDOWN := $Lparam3_pconame_0$.FuStopISt OR $Lparam3_pconame_0$.TStopISt;
(* 10 TO 2 *) Graph_Red.MotorB_Out_STOP := NOT $Lparam3_pconame_0$.RunOSt;
(* 11 TO 1 *) Graph_Red.MotorB_Timeout_SHUTDOWN := $Lparam3_pconame_0$.FuStopISt OR $Lparam3_pconame_0$.TStopISt;
(* 11 TO 2 *) Graph_Red.MotorB_Timeout_STOP := NOT $Lparam3_pconame_0$.RunOSt;
(* 11 TO 3 *) Graph_Red.TIMEOUT_TO_STANDBY := $Lparam3_motorb_0$_TimerOut> TIME_TO_DINT(Graph_Red.MotorA_Out.T)*1000.0;
'''))

    thePlugin.writeSiemensLogic('''
(* WORD STATUS FOR STEPS *)
DB_WS_ALL.WS_SET.MachineRedSt.AuPosR := INT_TO_WORD(Graph_Red.S_NO);
''')

    thePlugin.writeSiemensLogic('''
(* WORD STATUS FOR TRANSITIONS *)
pco_transitions0_bit[8]  := Graph_Red.SHUTDOWN_STOP;                                                         (* 0*)
pco_transitions0_bit[9]  := Graph_Red.STOP_SHUTDOWN;                                                         (* 1*)
pco_transitions0_bit[10] := Graph_Red.STOP_STANDBY;                                                          (* 2*)
pco_transitions0_bit[11] := Graph_Red.STANDBY_SHUTDOWN;                                                      (* 3*)
pco_transitions0_bit[12] := Graph_Red.STANDBY_STOP;                                                          (* 4*)
pco_transitions0_bit[13] := Graph_Red.Start1;                                                                (* 5*)
pco_transitions0_bit[14] := Graph_Red.STANDBY_MotorB_In;                                                     (* 6*)
pco_transitions0_bit[15] := Graph_Red.MotorA_In_SHUTDOWN;                                                    (* 7*)
pco_transitions0_bit[0]  := Graph_Red.MotorA_In_STOP;                                                        (* 8*)
pco_transitions0_bit[1]  := Graph_Red.IN_TO_STAY;                                                            (* 9*)
pco_transitions0_bit[2]  := Graph_Red.MotorA_Stay_SHUTDOWN;                                                  (*10*)
pco_transitions0_bit[3]  := Graph_Red.MotorA_Stay_STOP;                                                      (*11*)
pco_transitions0_bit[4]  := Graph_Red.STAY_TO_OUT;                                                           (*12*)
pco_transitions0_bit[5]  := Graph_Red.MotorA_Out_SHUTDOWN;                                                   (*13*)
pco_transitions0_bit[6]  := Graph_Red.MotorA_Out_STOP;                                                       (*14*)
pco_transitions0_bit[7]  := Graph_Red.OUT_TO_TIMEOUT;                                                        (*15*)
pco_transitions1_bit[8]  := Graph_Red.MotorA_Timeout_SHUTDOWN;                                               (* 0*)
pco_transitions1_bit[9]  := Graph_Red.MotorA_Timeout_STOP;                                                   (* 1*)
pco_transitions1_bit[10] := Graph_Red.MotorA_Timeout_MotorB_In;                                              (* 2*)
pco_transitions1_bit[11] := Graph_Red.MotorB_In_SHUTDOWN;                                                    (* 3*)
pco_transitions1_bit[12] := Graph_Red.MotorB_In_STOP;                                                        (* 4*)
pco_transitions1_bit[13] := Graph_Red.MotorB_In_MotorB_Stay;                                                 (* 5*)
pco_transitions1_bit[14] := Graph_Red.MotorB_Stay_SHUTDOWN;                                                  (* 6*)
pco_transitions1_bit[15] := Graph_Red.MotorB_Stay_STOP;                                                      (* 7*)
pco_transitions1_bit[0]  := Graph_Red.MotorB_Stay_MotorB_Out;                                                (* 8*)
pco_transitions1_bit[1]  := Graph_Red.MotorB_Out_SHUTDOWN;                                                   (* 9*)
pco_transitions1_bit[2]  := Graph_Red.MotorB_Out_STOP;                                                       (*10*)
pco_transitions1_bit[3]  := Graph_Red.MotorB_Timeout_SHUTDOWN;                                               (*11*)
pco_transitions1_bit[4]  := Graph_Red.MotorB_Timeout_STOP;                                                   (*12*)
pco_transitions1_bit[5]  := Graph_Red.TIMEOUT_TO_STANDBY;                                                    (*13*)
pco_transitions1_bit[6]  := false;                                                                           (*14*)
pco_transitions1_bit[7]  := false;                                                                           (*15*)


DB_WS_ALL.WS_SET.MachineRedTr.AuPosR := pco_transitions0;
DB_WS_ALL.WS_SET.MachineRedTr.AuPosR := pco_transitions1;

''')

    thePlugin.writeSiemensLogic('''
(* Automatic generated code from StepperSpecs.xlsx using the Siemens_Expert_Stepper_Template <end> *)''')

    thePlugin.writeSiemensLogic('''
// ----------------------------------------------------- USER code <end>------------------------------------------------------------
''')
