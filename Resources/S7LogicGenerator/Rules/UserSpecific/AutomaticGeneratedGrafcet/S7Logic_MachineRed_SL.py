# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Encoding UTF-8 without BOM test line with accent: é
from java.util import Vector
from java.util import ArrayList
import S7Logic_DefaultAlarms_Template
# reload(S7Logic_DefaultAlarms_Template)


def SLLogic(thePlugin, theRawInstances, master, name, LparamVector):

    Lparam1, Lparam2, Lparam3, Lparam4, Lparam5, Lparam6, Lparam7, Lparam8, Lparam9, Lparam10 = S7Logic_DefaultAlarms_Template.getLparametersSplit(LparamVector)
	
#Lparam 3features parsed
    RequestedFeatures = ProcessFeatures.parseFeatures(Lparam3)
    Lparam3_pconame_0 = RequestedFeatures['pconame'][0]
    Lparam3_motora_0 = RequestedFeatures['motora'][0]
    Lparam3_motorb_0 = RequestedFeatures['motorb'][0]

    
    # Automatically generated variables
    

# Step 1: Create the FUNCTION called PCOName_SectionName.
    thePlugin.writeSiemensLogic('''
FUNCTION $name$_SL : VOID
TITLE = '$name$_SL'
//
// Sequencer Logic of $name$
//
(*
 Lparam1: $Lparam1$
 Lparam2: $Lparam2$
 Lparam3: $Lparam3$
 Lparam4: $Lparam4$
 Lparam5: $Lparam5$
 Lparam6: $Lparam6$
 Lparam7: $Lparam7$
 Lparam8: $Lparam8$
 Lparam9: $Lparam9$
 Lparam10:  $Lparam10$
*)
//
AUTHOR: 'ICE/PLC'
NAME: 'Logic_SL'
FAMILY: 'SL'
VAR_TEMP


END_VAR
BEGIN

''')
    thePlugin.writeSiemensLogic('''
// ----------------------------------------------------- USER code <begin>------------------------------------------------------------
''')

    thePlugin.writeSiemensLogic('''(* Automatic generated code from StepperSpecs.xlsx using the Siemens_Expert_Stepper_Template <begin> *)
''')

    thePlugin.writeSiemensLogic('''(* VARIABLES COMPUTATION *)



''')

    thePlugin.writeSiemensLogic('''Graph_Machine_Par.Graph_Red();

(* Automatic generated code from StepperSpecs.xlsx using the Siemens_Expert_Stepper_Template <end> *)''')

    thePlugin.writeSiemensLogic('''
// ----------------------------------------------------- USER code <end>------------------------------------------------------------
''')
