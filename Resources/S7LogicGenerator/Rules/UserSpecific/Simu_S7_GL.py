# -*- coding: utf-8 -*-
from java.util import Vector
from java.util import ArrayList
import S7Logic_DefaultAlarms_Template
reload(S7Logic_DefaultAlarms_Template)
import ucpc_library.shared_decorator
reload(ucpc_library.shared_decorator)


###############################  HOW TO USE THIS TEMPLATE TO SIMULATE YOUR I/O IN YOUR PLC ?  ###############################
##                                                                                                                          #
##  List of features managed by this template:                                                                              #
##      - Simulate all feedbacks or your actuators (ONOFF, ANALOG, ANADO)                                                   #
##      - Simulate all Measured Values of your PID regulation loops as a first order transfert function                     #
##      - Write specific values in other DI/AI/AIR (either constants, either a simple PLC code)                             #
##                                                                                                                          #
##  Nota Bene: this template makes use of DB60 to store data and can use DB61 and so-on for each PID regulation loop        #
##              If you want to change this DB number, change the variable "n" at line 39                                    #
##                                                                                                                          #
##  To use this simulation template:                                                                                        #
##  1 - Spec modification                                                                                                   #
##      1.1 - For all your inputs address(DI,AI,AIR), change your "FE Encoding Type" to "0"                                 #
##      1.2 - For all your inputs you want to force (DI,AI,AIR), put the forced value in the "Parameter2" column.           #
##              Note that you can specify either a constant value (e.g "1"), either PLC code (e.g "MyPCO.RunOSt")           #
##      1.3 - Choose a non-used PCO (SPARE) and assign this template (Simu_S7_GL.py) as GL Template.                        #
##  2 - Regenerate with UAB the instances for DI,AI,AIR and the Global Logic function for the spare PCO you choose          #
##  3 - Recompile all these SCL files and reload all related blocs to the PLC                                               #
##                                                                                                                          #
##                                                                                                                          #
#############################################################################################################################
def GLLogic(thePlugin, theRawInstances, master, name, LparamVector):

    Lparam1,Lparam2,Lparam3,Lparam4,Lparam5,Lparam6,Lparam7,Lparam8,Lparam9,Lparam10=S7Logic_DefaultAlarms_Template.getLparametersSplit(LparamVector)
    ED = ucpc_library.shared_decorator.ExpressionDecorator()

# Step 1. Create the FUNCTION called DeviceName_GL.

    theDI= theRawInstances.findMatchingInstances("DigitalInput", "'#DeviceDocumentation:Description#'!='SPARE'")
    theAI= theRawInstances.findMatchingInstances("AnalogInput", "'#DeviceDocumentation:Description#'!='SPARE'")
    theAIR= theRawInstances.findMatchingInstances("AnalogInputReal", "'#DeviceDocumentation:Description#'!='SPARE'")
    thePID= theRawInstances.findMatchingInstances("Controller", "'#DeviceDocumentation:Description#'!='SPARE'")
    AIinstancesVector = theRawInstances.findMatchingInstances("AnalogInput", "'#DeviceIdentification:Name#'!=''")
    AIinstanceNb = str(AIinstancesVector.size())
    AOinstancesVector = theRawInstances.findMatchingInstances("AnalogOutput", "'#DeviceIdentification:Name#'!=''")
    AOinstanceNb = str(AOinstancesVector.size())
    Delay = "2" #Delay in second to simulate the digital feedbacks
    n = 60      #DB number to begin to generate

    
    #Creation of global DB
    thePlugin.writeSiemensLogic('''
DATA_BLOCK DB$n$
//
// Global DB for simulation
//
STRUCT
AIIOErrorNr :INT;
AOIOErrorNr :INT;
IOErrorSetManRegBlock: BOOL;
IOErrorRManRegBlockCount: DINT;
IOErrorResetManRegBlock: BOOL;
''')

    for instDI in theDI:
        DIName = instDI.getAttributeData ("DeviceIdentification:Name")
        Fon_Ana= theRawInstances.findMatchingInstances("Analog,AnaDO,AnalogDigital", "'#FEDeviceEnvironmentInputs:Feedback On#'='$DIName$'")
        Foff_Ana= theRawInstances.findMatchingInstances("Analog,AnalogDigital", "'#FEDeviceEnvironmentInputs:Feedback Off#'='$DIName$'")
        Fon_OnOff= theRawInstances.findMatchingInstances("OnOff", "'#FEDeviceEnvironmentInputs:Feedback On#'='$DIName$'")
        Foff_OnOff= theRawInstances.findMatchingInstances("OnOff", "'#FEDeviceEnvironmentInputs:Feedback Off#'='$DIName$'")
 
        for inst in Fon_Ana: 
            Actuator_Name = inst.getAttributeData ("DeviceIdentification:Name")

            thePlugin.writeSiemensLogic('''
$Actuator_Name$_TON: DINT;''')

        for inst in Foff_Ana: 
            Actuator_Name = inst.getAttributeData ("DeviceIdentification:Name")

            thePlugin.writeSiemensLogic('''
$Actuator_Name$_TOFF: DINT;''')

        for inst in Fon_OnOff: 
            Actuator_Name = inst.getAttributeData ("DeviceIdentification:Name")

            thePlugin.writeSiemensLogic('''
$Actuator_Name$_TON: DINT;''')

        for inst in Foff_OnOff: 
            Actuator_Name = inst.getAttributeData ("DeviceIdentification:Name")

            thePlugin.writeSiemensLogic('''
$Actuator_Name$_TOFF: DINT;''')

    thePlugin.writeSiemensLogic('''
END_STRUCT
BEGIN
END_DATA_BLOCK
''')

    #Creation of instance DB for PID
    i=n
    for inst in thePID: 
        PID_Name = inst.getAttributeData ("DeviceIdentification:Name")
        i = i+1
        
        thePlugin.writeSiemensLogic('''
DATA_BLOCK DB$str(i)$ LAG1ST
//
// LAG1ST block for simulation of PID: $PID_Name$ 
//
BEGIN

END_DATA_BLOCK
''')
        
        
    thePlugin.writeSiemensLogic('''

FUNCTION $name$_GL : VOID
TITLE = '$name$_GL'
//
// Global Logic of $name$
//
// Lparam1:    $Lparam1$
// Lparam2:    $Lparam2$
// Lparam3:    $Lparam3$
// Lparam4:    $Lparam4$
// Lparam5:    $Lparam5$
// Lparam6:    $Lparam6$
// Lparam7:    $Lparam7$
// Lparam8:    $Lparam8$
// Lparam9:    $Lparam9$
// Lparam10:    $Lparam10$
//
AUTHOR: 'ICE/PLC'
NAME: 'Logic_GL'
FAMILY: 'GL'
''')

    # Sent Manual Error Block/Unblock request for all AIs
    # When all sensors Unblocked,reload DB_AI_ManRequest 
    thePlugin.writeSiemensLogic('''
(*Set Manual Error Block to TRUE for all AI/AO*)    

IF NOT DB$n$.IOErrorSetManRegBlock THEN
'''    )

    if int(AIinstanceNb) > 0:
        thePlugin.writeSiemensLogic('''
    FOR DB$n$.AIIOErrorNr:=1 TO $AIinstanceNb$ DO
        DB_AI_ManRequest.AI_Requests[DB$n$.AIIOErrorNr].Manreg01:=16#0400; 
    END_FOR;

'''    )

    
    if int(AOinstanceNb) > 0:
        thePlugin.writeSiemensLogic('''
    FOR DB$n$.AOIOErrorNr:=1 TO $AOinstanceNb$ DO
        DB_AO_ManRequest.AO_Requests[DB$n$.AOIOErrorNr].Manreg01:=16#0400; 
    END_FOR;  
    
'''    )

    thePlugin.writeSiemensLogic('''
    DB$n$.IOErrorRManRegBlockCount := DB_WinCCOA.UNICOS_LiveCounter;
    DB$n$.IOErrorSetManRegBlock := TRUE;
END_IF; 

IF NOT DB$n$.IOErrorResetManRegBlock AND DB_WinCCOA.UNICOS_LiveCounter - DB$n$.IOErrorRManRegBlockCount > 2 THEN
'''    )

    if int(AIinstanceNb) > 0:
        thePlugin.writeSiemensLogic('''
    FOR DB$n$.AIIOErrorNr:=1 TO $AIinstanceNb$ DO
        DB_AI_ManRequest.AI_Requests[DB$n$.AIIOErrorNr].Manreg01:=16#0000; 
    END_FOR;

'''    )

    
    if int(AOinstanceNb) > 0:
        thePlugin.writeSiemensLogic('''
    FOR DB$n$.AOIOErrorNr:=1 TO $AOinstanceNb$ DO
        DB_AO_ManRequest.AO_Requests[DB$n$.AOIOErrorNr].Manreg01:=16#0000; 
    END_FOR;
    
'''    )

    thePlugin.writeSiemensLogic('''
    DB$n$.IOErrorResetManRegBlock:= TRUE;
    
END_IF;


(*Set AIIOError/AOIOError status*) 
'''    )
    
    #Set AI IOError based on MIOErBRSt status 
    i=1
    for AIIOerror in AIinstancesVector:   
        thePlugin.writeSiemensLogic(''' 
AI_ERROR.IOERROR[$i$].Err:=1;'''    )
        i=i+1

    #Set AO IOError based on MIOErBRSt status 
    i=1
    for AOIOerror in AOinstancesVector:   
        thePlugin.writeSiemensLogic(''' 
AO_ERROR.IOERROR[$i$].Err:=1;'''    )
        i=i+1

       
        
    # Write inside DI if forced value specified in Param2    
    thePlugin.writeSiemensLogic('''

    
    (*Forcing of  DI*)'''    )
 
    for instDI in theDI:
        DIName = instDI.getAttributeData ("DeviceIdentification:Name")
        Param2 = ED.decorateExpression(instDI.getAttributeData ("LogicDeviceDefinitions:CustomLogicParameters:Parameter2"))

        if Param2 <> "":
            db_id = thePlugin.s7db_id(DIName)
            thePlugin.writeSiemensLogic('''
$db_id$$DIName$.HFPos := $Param2$;'''    )

    # Write inside AI/AIR if forced value specified in Param2    
    thePlugin.writeSiemensLogic('''
    (*Forcing of  AI*)'''    )
 
    for instAI in theAI:
        AIName = instAI.getAttributeData ("DeviceIdentification:Name")
        Param2 = ED.decorateExpression(instAI.getAttributeData ("LogicDeviceDefinitions:CustomLogicParameters:Parameter2"))

        if Param2 <> "":
            db_id = thePlugin.s7db_id(AIName)
            thePlugin.writeSiemensLogic('''
$db_id$$AIName$.HFPos := REAL_TO_INT( INT_TO_REAL($db_id$$AIName$.PMaxRaw-$db_id$$AIName$.PMinRaw) 
    *  $Param2$/($db_id$$AIName$.PMaxRan-$db_id$$AIName$.PMinRan))+$db_id$$AIName$.PMinRaw;'''    )
    
    thePlugin.writeSiemensLogic('''
    (*Forcing of  AIR*)'''    )
 
    for instAIR in theAIR:
        AIRName = instAIR.getAttributeData ("DeviceIdentification:Name")
        Param2 = ED.decorateExpression(instAIR.getAttributeData ("LogicDeviceDefinitions:CustomLogicParameters:Parameter2"))

        if Param2 <> "":
            db_id = thePlugin.s7db_id(AIRName)
            thePlugin.writeSiemensLogic('''
$db_id$$AIRName$.HFPos :=  ($db_id$$AIRName$.PMaxRaw-$db_id$$AIRName$.PMinRaw) 
    *  $Param2$/($db_id$$AIRName$.PMaxRan-$db_id$$AIRName$.PMinRan)+$db_id$$AIRName$.PMinRaw;'''    )
    
    

    # Feedbacks for DI    
    thePlugin.writeSiemensLogic('''
    
 (*Feedbacks of Digital actuators*)'''    )
 
    for instDI in theDI:
        DIName = instDI.getAttributeData ("DeviceIdentification:Name")
        Param2 = ED.decorateExpression(instDI.getAttributeData ("LogicDeviceDefinitions:CustomLogicParameters:Parameter2"))
        Fon_AnaDO= theRawInstances.findMatchingInstances("AnaDO", "'#FEDeviceEnvironmentInputs:Feedback On#'='$DIName$'")
        Fon_Ana= theRawInstances.findMatchingInstances("Analog", "'#FEDeviceEnvironmentInputs:Feedback On#'='$DIName$'")
        Foff_Ana= theRawInstances.findMatchingInstances("Analog", "'#FEDeviceEnvironmentInputs:Feedback Off#'='$DIName$'")
        Fon_AnaDig= theRawInstances.findMatchingInstances("AnalogDigital", "'#FEDeviceEnvironmentInputs:Feedback On#'='$DIName$'")
        Foff_AnaDig= theRawInstances.findMatchingInstances("AnalogDigital", "'#FEDeviceEnvironmentInputs:Feedback Off#'='$DIName$'")
        Fon_OnOff= theRawInstances.findMatchingInstances("OnOff", "'#FEDeviceEnvironmentInputs:Feedback On#'='$DIName$'")
        Foff_OnOff= theRawInstances.findMatchingInstances("OnOff", "'#FEDeviceEnvironmentInputs:Feedback Off#'='$DIName$'")
        
        db_id = thePlugin.s7db_id(DIName)
        
        if Param2 == "": # so if user sets Param2, he will not get code in 2 places, regardless
            for inst in Fon_AnaDO: 
                Actuator_Name = inst.getAttributeData ("DeviceIdentification:Name")
                thePlugin.writeSiemensLogic('''
IF NOT $Actuator_Name$.OutOnOVSt THEN
    DB$n$.$Actuator_Name$_TON := DB_WinCCOA.UNICOS_LiveCounter;
END_IF;

$db_id$$DIName$.HFPos := DB_WinCCOA.UNICOS_LiveCounter -DB$n$.$Actuator_Name$_TON > $Delay$ ;'''    )

            for inst in Fon_Ana: 
                Actuator_Name = inst.getAttributeData ("DeviceIdentification:Name")
                
                thePlugin.writeSiemensLogic('''
IF $Actuator_Name$.PosRSt < 90.0 THEN
    DB$n$.$Actuator_Name$_TON := DB_WinCCOA.UNICOS_LiveCounter;
END_IF;

$db_id$$DIName$.HFPos := DB_WinCCOA.UNICOS_LiveCounter -DB$n$.$Actuator_Name$_TON > $Delay$ ;'''    )

            for instDI in Foff_Ana: 
                Actuator_Name = instDI.getAttributeData ("DeviceIdentification:Name")
                
                thePlugin.writeSiemensLogic('''
IF $Actuator_Name$.PosRSt > 10.0 THEN
    DB$n$.$Actuator_Name$_TOFF := DB_WinCCOA.UNICOS_LiveCounter;
END_IF;

$db_id$$DIName$.HFPos := DB_WinCCOA.UNICOS_LiveCounter -DB$n$.$Actuator_Name$_TOFF > $Delay$ ;'''    )

            for inst in Fon_AnaDig: 
                Actuator_Name = inst.getAttributeData ("DeviceIdentification:Name")

                thePlugin.writeSiemensLogic('''
IF NOT $Actuator_Name$.DOutOnOV THEN
    DB$n$.$Actuator_Name$_TON := DB_WinCCOA.UNICOS_LiveCounter;
END_IF;

IF DB_WinCCOA.UNICOS_LiveCounter -DB$n$.$Actuator_Name$_TON > $Delay$ THEN
    $db_id$$DIName$.HFPos := TRUE;
END_IF;

IF $Actuator_Name$.DOutOffOV THEN
    $db_id$$DIName$.HFPos := FALSE;
END_IF;
''')

            for inst in Foff_AnaDig: 
                Actuator_Name = inst.getAttributeData ("DeviceIdentification:Name")

                thePlugin.writeSiemensLogic('''
IF NOT $Actuator_Name$.DOutOffOV THEN
    DB$n$.$Actuator_Name$_TOFF := DB_WinCCOA.UNICOS_LiveCounter;
END_IF;

IF DB_WinCCOA.UNICOS_LiveCounter -DB$n$.$Actuator_Name$_TOFF > $Delay$ THEN
    $db_id$$DIName$.HFPos := TRUE;
END_IF;

IF $Actuator_Name$.DOutOnOV THEN
    $db_id$$DIName$.HFPos := FALSE;
END_IF;
''')

            for inst in Fon_OnOff: 
                Actuator_Name = inst.getAttributeData ("DeviceIdentification:Name")
                POutOff = inst.getAttributeData ("FEDeviceOutputs:Process Output Off").strip()
                POutOn = inst.getAttributeData ("FEDeviceOutputs:Process Output").strip()
                
                #For Pulse Valve must have both "Process Output" and "Process Output Off" commands
                if POutOff == "" or POutOn == "": 
                    # Code for non-pulsed valves
                    thePlugin.writeSiemensLogic('''
IF NOT $Actuator_Name$.OutOnOVSt THEN
    DB$n$.$Actuator_Name$_TON := DB_WinCCOA.UNICOS_LiveCounter;
END_IF;

$db_id$$DIName$.HFPos := DB_WinCCOA.UNICOS_LiveCounter -DB$n$.$Actuator_Name$_TON > $Delay$ ;'''    )
                else: 
                    #Code for pulsed valves
                    thePlugin.writeSiemensLogic('''
IF NOT $Actuator_Name$.OutOnOVSt THEN
    DB$n$.$Actuator_Name$_TON := DB_WinCCOA.UNICOS_LiveCounter;
END_IF;

IF DB_WinCCOA.UNICOS_LiveCounter -DB$n$.$Actuator_Name$_TON > $Delay$ THEN
    $db_id$$DIName$.HFPos := TRUE;
END_IF;

IF $Actuator_Name$.OutOffOV THEN
    $db_id$$DIName$.HFPos := FALSE;
END_IF;
''')

            for inst in Foff_OnOff: 
                Actuator_Name = inst.getAttributeData ("DeviceIdentification:Name")
                POutOff = inst.getAttributeData ("FEDeviceOutputs:Process Output Off").strip()
                POutOn = inst.getAttributeData ("FEDeviceOutputs:Process Output").strip()
                
                #For Pulse Valve must have both "Process Output" and "Process Output Off" commands
                if POutOff == "" or POutOn == "": 
                    # Code for non-pulsed valves
                    thePlugin.writeSiemensLogic('''
IF $Actuator_Name$.OutOnOVSt THEN
    DB$n$.$Actuator_Name$_TOFF := DB_WinCCOA.UNICOS_LiveCounter;
END_IF;

$db_id$$DIName$.HFPos := DB_WinCCOA.UNICOS_LiveCounter -DB$n$.$Actuator_Name$_TOFF > $Delay$ ;'''    )
                else:
                    #Code for pulsed valves
                    thePlugin.writeSiemensLogic('''
IF NOT $Actuator_Name$.OutOffOV THEN
    DB$n$.$Actuator_Name$_TOFF := DB_WinCCOA.UNICOS_LiveCounter;
END_IF;

IF DB_WinCCOA.UNICOS_LiveCounter -DB$n$.$Actuator_Name$_TOFF > $Delay$ THEN
    $db_id$$DIName$.HFPos := TRUE;
END_IF;

IF $Actuator_Name$.OutOnOVSt THEN
    $db_id$$DIName$.HFPos := FALSE;
END_IF;
''')

    # Feedbacks for AI    
    thePlugin.writeSiemensLogic(''' 
    
    (*Feedbacks of Analog actuators *)''')    
    
    for instAI in theAI:
        AIName = instAI.getAttributeData ("DeviceIdentification:Name")
        Param2 = ED.decorateExpression(instAI.getAttributeData ("LogicDeviceDefinitions:CustomLogicParameters:Parameter2"))
        PMinRan = instAI.getAttributeData ("FEDeviceParameters:Range Min")
        PMaxRan = instAI.getAttributeData ("FEDeviceParameters:Range Max")
        PMinRaw = instAI.getAttributeData ("FEDeviceParameters:Raw Min")
        PMaxRaw = instAI.getAttributeData ("FEDeviceParameters:Raw Max")
        FAnalog_Actuator= theRawInstances.findMatchingInstances("Analog,AnaDO", "'#FEDeviceEnvironmentInputs:Feedback Analog#'='$AIName$'")
        FAnaDig_Actuator= theRawInstances.findMatchingInstances("AnalogDigital", "'#FEDeviceEnvironmentInputs:Feedback Analog#'='$AIName$'")        
        PID= theRawInstances.findMatchingInstances("Controller", "'#FEDeviceEnvironmentInputs:Measured Value#'='$AIName$'")
        
        db_id = thePlugin.s7db_id(AIName)
        
        if Param2 == "": # so if user sets Param2, he will not get code in 2 places, regardless
            for inst in FAnalog_Actuator: 
                Actuator_Name = inst.getAttributeData ("DeviceIdentification:Name")
                thePlugin.writeSiemensLogic(''' 
$db_id$$AIName$.HFPos := REAL_TO_INT( INT_TO_REAL($db_id$$AIName$.PMaxRaw-$db_id$$AIName$.PMinRaw) 
    *  $Actuator_Name$.PosRSt/($db_id$$AIName$.PMaxRan-$db_id$$AIName$.PMinRan))+$db_id$$AIName$.PMinRaw;
'''    )
            for inst in FAnaDig_Actuator: 
                Actuator_Name = inst.getAttributeData ("DeviceIdentification:Name")
                thePlugin.writeSiemensLogic(''' 
$db_id$$AIName$.HFPos := REAL_TO_INT( INT_TO_REAL($db_id$$AIName$.PMaxRaw-$db_id$$AIName$.PMinRaw) 
    *  $Actuator_Name$.PosRSt/($db_id$$AIName$.PMaxRan-$db_id$$AIName$.PMinRan))+$db_id$$AIName$.PMinRaw;
'''    )

    #PID for AI: assume stable 1st order system
    
    thePlugin.writeSiemensLogic(''' 
    
    (*PID LOOPS *)''')
    
    i = n
    for inst in thePID: 
        i = i+1
        PID_Name = inst.getAttributeData ("DeviceIdentification:Name")
        RA = inst.getAttributeData ("FEDeviceParameters:Controller Parameters:RA")
        PID_Kc = inst.getAttributeData ("FEDeviceVariables:Default PID Parameters:Kc")
        PID_Ti = inst.getAttributeData ("FEDeviceVariables:Default PID Parameters:Ti")
        PID_SP = inst.getAttributeData ("FEDeviceVariables:Default PID Parameters:Setpoint")
        MV = inst.getAttributeData ("FEDeviceEnvironmentInputs:Measured Value")
        PID_SP_instance = theRawInstances.findInstanceByName(PID_SP)
        PID_Ti_instance = theRawInstances.findInstanceByName(PID_Ti)

        if PID_SP_instance:
            PID_SP_instanceType = PID_SP_instance.getDeviceType()
            PID_SP_instanceTypeName = PID_SP_instanceType.getDeviceTypeName().upper()
            if PID_SP_instanceTypeName in ["ANALOGSTATUS", "ANALOGINPUTREAL"]:  # set middle of the range
                rangeMin = float(PID_SP_instance.getAttributeData("FEDeviceParameters:Range Min"))
                rangeMax = float(PID_SP_instance.getAttributeData("FEDeviceParameters:Range Max"))
                PID_SP = rangeMin + (rangeMax - rangeMin)/2.0
            elif PID_SP_instanceTypeName == "ANALOGPARAMETER":  # set default value
                PID_SP = PID_SP_instance.getAttributeData("FEDeviceParameters:Default Value")
            else:
                thePlugin.writeErrorInUABLog("Device " + PID_Name + ": Setpoint"  + PID_SP + " is of incorrect type [" + PID_SP_instanceTypeName + "]")
        elif PID_SP == "":
            PID_SP = 1.0    # any arbitrary value
        
        if PID_Ti_instance:
            PID_Ti_instanceType = PID_Ti_instance.getDeviceType()
            PID_Ti_instanceTypeName = PID_Ti_instance.getDeviceTypeName().upper()
            if PID_Ti_instanceTypeName in ["ANALOGSTATUS", "ANALOGINPUTREAL"]:  # set middle of the range
                rangeMin = float(PID_Ti_instance.getAttributeData("FEDeviceParameters:Range Min"))
                rangeMax = float(PID_Ti_instance.getAttributeData("FEDeviceParameters:Range Max"))
                PID_Ti = rangeMin + (rangeMax - rangeMin)/2.0
            elif PID_Ti_instanceTypeName == "ANALOGPARAMETER":  # set default value
                PID_Ti = PID_Ti_instance.getAttributeData("FEDeviceParameters:Default Value")
            else:
                thePlugin.writeErrorInUABLog("Device " + PID_Name + ": Setpoint"  + PID_Ti + " is of incorrect type [" + PID_Ti_instanceTypeName + "]")
        elif PID_Ti == "":
            PID_Ti = 1.0    # any arbitrary value
            

        if MV <> "":
            MVType=thePlugin.s7db_id(MV, "AnalogInput, AnalogInputReal, AnalogStatus")
            DesiredActuator = 70
            
            if RA == "FALSE":
                Offset = str(float(PID_SP)*0.5)
                sign = "+"

            else:
                Offset = str(float(PID_SP)*1.5)
                sign = "-"

            TimeConstant = str(float(PID_Ti)/3.0)
            Gain = sign + str(float(PID_SP)/DesiredActuator)

            
            thePlugin.writeSiemensLogic('''
//Calculate the MV $MV$ for $PID_Name$ based on requirement to get $DesiredActuator$ actuator position '''    )

            if MVType.upper() == "DB_AI_ALL.AI_SET." or MVType.upper() == "DB_AI_ALL2.AI_SET.":
                thePlugin.writeSiemensLogic('''
DB$str(i)$.INV := $Gain$ * $PID_Name$.OutOV;
DB$str(i)$.TM_LAG := T#$TimeConstant$s;
DB$str(i)$.CYCLE  := T#1s;
LAG1ST.DB$str(i)$();'''    )
 
                thePlugin.writeSiemensLogic('''
$MVType$$MV$.HFPos := REAL_TO_INT( INT_TO_REAL($MVType$$MV$.PMaxRaw-$MVType$$MV$.PMinRaw) 
        *($Offset$ + DB$str(i)$.OUTV)/($MVType$$MV$.PMaxRan-$MVType$$MV$.PMinRan));    '''    )                
            elif MVType.upper() == "DB_AIR_ALL.AIR_SET." or MVType.upper() == "DB_AIR_ALL2.AIR_SET.":
                thePlugin.writeSiemensLogic('''
DB$str(i)$.INV := $Gain$ * $PID_Name$.OutOV;
DB$str(i)$.TM_LAG := T#$TimeConstant$s;
DB$str(i)$.CYCLE  := T#1s;
LAG1ST.DB$str(i)$();
//Nominally don't write to AIR because this could be the result of a calculation, and therefore will overwrite the user calculation
(*$MVType$$MV$.HFPos := REAL_TO_INT( $MVType$$MV$.PMaxRaw-$MVType$$MV$.PMinRaw
        *($Offset$ + DB$str(i)$.OUTV)/($MVType$$MV$.PMaxRan-$MVType$$MV$.PMinRan));  *)  '''    )
            else:
                thePlugin.writeSiemensLogic('''
DB$str(i)$.INV := $PID_Name$.OutOV;
DB$str(i)$.TM_LAG := T#$TimeConstant$s;
DB$str(i)$.CYCLE  := T#1s;
LAG1ST.DB$str(i)$();
$MVType$$MV$.AuPosR := $Offset$ + DB$str(i)$.OUTV;'''    )
