# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Communication Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin  # REQUIRED
from java.util import ArrayList
from java.util import Vector
from time import strftime
import S7Logic_SteppingMotor_DL_Standard_Template
import S7Logic_DefaultAlarms_Template
import sys


class SteppingMotor_DL_Template(IUnicosTemplate):
    thePlugin = 0
    theUnicosProject = 0

    def initialize(self):
        self.thePlugin = APlugin.getPluginInterface()
        self.thePlugin.writeInUABLog("initialize in Jython for STPMOT DL.")
        reload(S7Logic_SteppingMotor_DL_Standard_Template)
        # reload(S7Logic_DefaultAlarms_Template)
        # reload(sys)

    def check(self):
        self.thePlugin.writeInUABLog("check in Jython for STPMOT DL.")

    def begin(self):
        self.thePlugin.writeInUABLog("begin in Jython for STPMOT DL.")
        dateAndTime = strftime("%Y-%m-%d %H:%M:%S")  # '2007-03-03 22:14:39'

    def process(self, *params):
        theCurrentPco = params[0]
        theCurrentDevice = params[1]
        callUserTemplate = str(params[2])
        userTemplateName = str(params[3])
        name = theCurrentDevice.getDeviceName()
        self.thePlugin.writeInUABLog("processInstances in Jython for STPMOT for the $name$.")

        # General Steps for Dependent Logic templates:
        #   1.1. Create all the variable that we're using on this Function
        #	1.3. Position Request calculation. The user should complet this part according the logic process. By default we put that to "0".
        #	1.4. Interlock Conditions (both for analog and digital alarms) and IOError/IOSimu for the Alarms (both for analog and digital alarms)
        #	1.5. Fill the IOError and IOSimu from the Inputs linked by the user logic (if procceds)
        #   1.6. IoSimu and IoError: Adding of the IoError from the Logic related to the OnOff object
        #   1.7. Instantiation of the AuAlAck for the DigitalAlarm objects related to the OnOff object
        #   1.8. Interlock: Both for DA and AA (SI, FS, TS, AL)
        #   1.9. Blocked Alarm warning: both for DA and AA
        #   1.10. No master

        # 1. Create the FUNCTION called DeviceName_DL.
        # Step 1.1: Create all the variable that we're using on this Function
        master = theCurrentPco.getDeviceName()
        theRawInstances = self.thePlugin.getUnicosProject()
        ApplicationName = self.thePlugin.getApplicationParameter("ApplicationName")
        instVector = theRawInstances.findMatchingInstances("SteppingMotor", "'#DeviceIdentification:Name#'='$name$'")
        if (instVector.size() == 0):
            self.thePlugin.writeErrorInUABLog("The instance $name$ does not exist in the specification file.")
            self.thePlugin.writeInfoInUABLog("Reload the specification file by clicking on the Reload Specs. button in the wizard.")
            return
        for inst in instVector:
            LparamVector = S7Logic_DefaultAlarms_Template.getLparameters(inst)
            description = inst.getAttributeData("DeviceDocumentation:Description")

        self.thePlugin.writeSiemensLogic('''(*Device Logic of $name$ ($description$) ********** Application: $ApplicationName$ *****)
''')

        # Gets all the Digital Alarms that are children of the current object
        theDigitalAlarms, theDigitalAlarmsMultiple, allTheDigitalAlarms, DAListPosition = S7Logic_DefaultAlarms_Template.getDigitalAlarms(theRawInstances, name)

        # Gets all the Analog Alarms that are children of the current object
        theAnalogAlarms, theAnalogAlarmsMultiple, allTheAnalogAlarms, AAListPosition = S7Logic_DefaultAlarms_Template.getAnalogAlarms(theRawInstances, name)

        # Gets all the "FS Alarms" that are children of the current object.
        theDAFsAlarms, theDAFsAlarmsMultiple, allTheDAFsAlarms, theAAFsAlarms, theAAFsAlarmsMultiple, allTheAAFsAlarms = S7Logic_DefaultAlarms_Template.getFsAlarms(theRawInstances, name)

        # Gets all the "TS Alarms" that are children of the current object.
        theDATsAlarms, theDATsAlarmsMultiple, allTheDATsAlarms, theAATsAlarms, theAATsAlarmsMultiple, allTheAATsAlarms = S7Logic_DefaultAlarms_Template.getTsAlarms(theRawInstances, name)

        # Gets all the "SI Alarms" that are children of the current object.
        theDASiAlarms, theDASiAlarmsMultiple, allTheDASiAlarms, theAASiAlarms, theAASiAlarmsMultiple, allTheAASiAlarms = S7Logic_DefaultAlarms_Template.getSiAlarms(theRawInstances, name)

        # Gets all the "AL Alarms" that are children of the current object.
        theDAAlAlarms, theDAAlAlarmsMultiple, allTheDAAlAlarms, theAAAlAlarms, theAAAlAlarmsMultiple, allTheAAAlAlarms = S7Logic_DefaultAlarms_Template.getAlAlarms(theRawInstances, name)

        # call the standard template or the custom one
        if callUserTemplate == "true":
            eval(userTemplateName).SteppingMotorLogic(self.thePlugin, theRawInstances, master, name, LparamVector)
        else:
            S7Logic_SteppingMotor_DL_Standard_Template.SteppingMotorLogic(self.thePlugin, theRawInstances, master, name, LparamVector)

        # Configured parameters of alarms
        S7Logic_DefaultAlarms_Template.writeConfiguredDAParameters(self.thePlugin, theDigitalAlarms, theDigitalAlarmsMultiple, DAListPosition)
        S7Logic_DefaultAlarms_Template.writeConfiguredAAParameters(self.thePlugin, theRawInstances, theAnalogAlarms, theAnalogAlarmsMultiple, AAListPosition)

        # Step 1.6. IoSimu and IoError: Adding of the IoError from the Logic related to the current object
        S7Logic_DefaultAlarms_Template.logicErrorSimuAssignment(self.thePlugin, theRawInstances, allTheDigitalAlarms, allTheAnalogAlarms, name)

        # Step 1.7: Instantiation of the AuAlAck for the Alarm objects related to the current object
        S7Logic_DefaultAlarms_Template.AuAlAckAlarmsMethod(self.thePlugin, theRawInstances, name, master, theDigitalAlarms, theDigitalAlarmsMultiple, theAnalogAlarms, theAnalogAlarmsMultiple, DAListPosition, AAListPosition)

        # Step 1.8: Interlock: Both for DA and AA (SI, FS, TS, AL)
        S7Logic_DefaultAlarms_Template.interlockAlarmsMethod(self.thePlugin, theRawInstances, allTheDASiAlarms, allTheAASiAlarms, allTheDAFsAlarms, allTheAAFsAlarms, allTheDATsAlarms, allTheAATsAlarms, allTheDAAlAlarms, allTheAAAlAlarms, name)

        # Step 1.9: Blocked Alarm warning: both for DA and AA
        S7Logic_DefaultAlarms_Template.blockAlarmsMethod(self.thePlugin, theRawInstances, allTheDigitalAlarms, allTheAnalogAlarms, name)

        # Step 1.10: No master
        if master.lower() == "no_master":
            self.thePlugin.writeSiemensLogic('''(* No master defined (master of masters) *)
$name$.IhAuMRW := 1;
''')

        self.thePlugin.writeSiemensLogic('''

END_FUNCTION''')

    def end(self):
        self.thePlugin.writeInUABLog("end in Jython for STPMOT DL.")

    def shutdown(self):
        self.thePlugin.writeInUABLog("shutdown in Jython for STPMOT DL.")
