# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Encoding UTF-8 without BOM test line with accent: é
from java.util import Vector
from java.util import ArrayList
import S7Logic_DefaultAlarms_Template
# reload(S7Logic_DefaultAlarms_Template)


def SteppingMotorLogic(thePlugin, theRawInstances, master, name, LparamVector):

    Lparam1, Lparam2, Lparam3, Lparam4, Lparam5, Lparam6, Lparam7, Lparam8, Lparam9, Lparam10 = S7Logic_DefaultAlarms_Template.getLparametersSplit(LparamVector)

    thePlugin.writeSiemensLogic('''//
FUNCTION $name$_DL : VOID
TITLE = '$name$_DL'
//
// Dependent Logic of $name$
//
//
// Master: 	$master$
// Name: 	$name$
(*
 Lparam1:	$Lparam1$
 Lparam2:	$Lparam2$
 Lparam3:	$Lparam3$
 Lparam4:	$Lparam4$
 Lparam5:	$Lparam5$
 Lparam6:	$Lparam6$
 Lparam7:	$Lparam7$
 Lparam8:	$Lparam8$
 Lparam9:	$Lparam9$
 Lparam10:	$Lparam10$
*)
AUTHOR: 'UNICOS'
NAME: 'Logic_DL'
FAMILY: 'STPMOT'
''')

# Step 1.1: Create all the variable that we're using on this Function
    thePlugin.writeSiemensLogic('''
VAR_TEMP
   old_status: DWORD;
END_VAR
BEGIN
''')

    thePlugin.writeSiemensLogic('''
// ----------------------------------------------------- USER code <begin>------------------------------------------------------------
''')


# Step 1.5: Fill the IOError and IOSimu from the Inputs linked by the user logic (if procceds)
    thePlugin.writeSiemensLogic('''
	
(*IoSimu and IoError*****************)
// The user must connect the IOError and IOSimu from the linked devices ("IN" variable) if proceeds
DB_ERROR_SIMU.$name$_DL_E := 0; // To complete

DB_ERROR_SIMU.$name$_DL_S :=  0; // To complete

''')

    # Not configured alarm parameters
    # Gets all the Digital Alarms that are child of the 'master' object
    theDigitalAlarms, theDigitalAlarmsMultiple, allTheDigitalAlarms, DAListPosition = S7Logic_DefaultAlarms_Template.getDigitalAlarms(theRawInstances, name)
    # Gets all the Analog Alarms that are child of the 'master' object
    theAnalogAlarms, theAnalogAlarmsMultiple, allTheAnalogAlarms, AAListPosition = S7Logic_DefaultAlarms_Template.getAnalogAlarms(theRawInstances, name)

    #-------------------------------------------------------------------------------------------------------------
    # The "writeNotConfiguredDAParameters" function writes default values for DA parameters:
    #
    # If Input (in spec) is empty, writes IOError, IOSimu and I
    # Else if Input is not empty, IOError, IOSimu and I will be written by main template function
    # If Delay (in spec) is "logic", writes PAlDt
    # Else if Delay is not "logic", PAlDt will be written by main template function
    #
    # NOTE: the user can choose to remove this function call from the user template, in which case he will be
    # responsible for writing the code for the appropriate pins himself
    #
    #-------------------------------------------------------------------------------------------------------------
    S7Logic_DefaultAlarms_Template.writeNotConfiguredDAParameters(thePlugin, theDigitalAlarms, theDigitalAlarmsMultiple, DAListPosition)

    #-------------------------------------------------------------------------------------------------------------
    # The "writeNotConfiguredAAParameters" function writes default values for AA parameters:
    #
    # If HH Alarm (in spec) is a number, writes AuEHH. In this case HH will be written by main template function
    # If HH Alarm is "logic", writes AuEHH and HH
    # If HH Alarm is a reference to an object, writes AuEHH. In this case HH will be written by main template function
    # NOTE: the same applies for H Warning, L Warning, and LL Alarm
    # If Input (in spec) is empty, writes IOError, IOSimu and I
    # If Delay (in spec) is "logic", writes PAlDt
    #
    # NOTE: the user can choose to remove this function call from the user template, in which case he will be
    # responsible for writing the code for the appropriate pins himself
    #
    #-------------------------------------------------------------------------------------------------------------
    S7Logic_DefaultAlarms_Template.writeNotConfiguredAAParameters(thePlugin, theAnalogAlarms, theAnalogAlarmsMultiple, AAListPosition)

    thePlugin.writeSiemensLogic('''
// ----------------------------------------------------- USER code <end>------------------------------------------------------------
''')
