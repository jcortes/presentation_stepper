# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
from research.ch.cern.unicos.templateshandling import IUnicosTemplate   #REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin          #REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin          #REQUIRED

from time import strftime

from java.lang import System
from java.lang import Runtime

import re
import sys

from research.ch.cern.unicos.utilities import AbsolutePathBuilder
from research.ch.cern.unicos.utilities import IDeviceInstance
import os, fnmatch


import ucpc_library.shared_usage_finder
reload(ucpc_library.shared_usage_finder)
import ucpc_library.shared_generic_functions
reload(ucpc_library.shared_generic_functions)
import ucpc_library.shared_grafcet_parsing
reload(ucpc_library.shared_grafcet_parsing)

outputFolder = AbsolutePathBuilder.getTechnicalPathParameter("ExpertUserGenerator:OutputParameters:OutputFolder")

class generateGrafcets_Template(IUnicosTemplate, ucpc_library.shared_grafcet_parsing.SharedGrafcetParsing, ucpc_library.shared_usage_finder.Finder, ucpc_library.shared_generic_functions.SharedGenericFunctions):
    thePlugin = 0
    theUnicosProject = 0   
    
    PVSSSystemName = "dist_101:"
    PVSSAddTransitionLabels = False
    panelW = 1270
    panelH = 830
            
    def initialize(self):
        # Get a reference to the plug-in
        self.thePlugin = APlugin.getPluginInterface() 
        # Get a reference to the specs file.
        self.theUnicosProject = self.thePlugin.getUnicosProject()
        self.thePlugin.writeInUABLog("initialize findMissingCCCAlarms_Template.")

    def check(self):
        self.thePlugin.writeInUABLog("check findMissingCCCAlarms_Template.")

    def begin(self):
        self.thePlugin.writeInUABLog("begin in Jython for findMissingCCCAlarms_Template.")
        self.dateAndTime = strftime("%Y-%m-%d %H:%M:%S")  #'2007-03-03 22:14:39'
        self.theApplicationName = self.thePlugin.getApplicationName()
        self.thePluginId = self.thePlugin.getId()
        self.thePluginVersion = self.thePlugin.getVersionId()
        self.theUserName = System.getProperty("user.name")
        self.CRLF = System.getProperty("line.separator")
    
    # Copied from http://code.activestate.com/recipes/499305-locating-files-throughout-a-directory-tree/
    # and modified to find directory
    def locateDir(self, pattern, root=os.curdir):
        '''Locate all directories matching supplied pattern in and below
        supplied root directory.'''
        for path, dirs, files in os.walk(os.path.abspath(root)):
            for dirname in fnmatch.filter(dirs, pattern):
                return os.path.join(path, dirname)

    def process(self, *params):
        grafcetName = ""       # grafcet name in case of Siemens/pco in case of Schneider

        # Look for sources in *exported_src directory underneath parent directory.
        # If does not exist, will use generated output directory by default
        if self.isSiemens():
            pathToSources = str(self.locateDir("*exported_src", AbsolutePathBuilder.getAbsolutePath("../"))) + "/"
        elif self.isSchneider():
            pathToSources = str(self.locateFile("s??", "*.xef", AbsolutePathBuilder.getAbsolutePath("../")))
        else:
            pathToSources = ""

        useOutputDirectory = True
            
        # use generated output
        if useOutputDirectory:
            ProjectSources = self.loadSources()
            self.thePlugin.writeInUABLog("Loaded sources.")
        elif not os.path.exists(pathToSources):
            self.thePlugin.writeWarningInUABLog("Cannot find *exported_src path (" + str(pathToSources) + ") in parent hierarchy. Using generated output directory by default.")
            ProjectSources = self.loadSources()
            self.thePlugin.writeInUABLog("Loaded sources.")

        # use exported directory
        else:
            self.thePlugin.writeWarningInUABLog("path=" + pathToSources)
            ProjectSources = self.loadSources(pathToSources)
            self.thePlugin.writeInUABLog("Loaded sources.")

        # in case of no sources..
        if not ProjectSources:
            self.thePlugin.writeInUABLog("Could not load sources.")
            pass
        
        PCODevice = self.theUnicosProject.getDeviceType("ProcessControlObject")
        PCOInstances = PCODevice.getAllDeviceTypeInstances()

        grafcetTransitions = {}
        grafcetSteps = {}

        for instance in PCOInstances:
            PCOName = instance.getAttributeData("DeviceIdentification:Name")
            PCODescription = instance.getAttributeData("DeviceDocumentation:Description").replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")

            if PCODescription.lower() not in ("spare", "reserve"):
                # for Siemens:
                # 1. find _SL file for PCO and check if there is call to grafcet
                # 2. if so, extract the grafcet name
                # 3. find the grafcet name in user resources file and check its FB number
                # 4. find the file in sources that corresponds to the FB number
                if self.isSiemens():
                    if PCOName.lower() + "_sl.scl" not in ProjectSources or PCOName.lower() + "_tl.scl" not in ProjectSources:
                        self.thePlugin.writeWarningInUABLog("PCO $PCOName$_SL.SCL or $PCOName$_TL.SCL does not exist in ProjectSources.")
                    else:
                        pattern = re.compile("^(.+?)(?:\.)(.+?)(?:\(\);)", re.IGNORECASE | re.MULTILINE)  # patter looks for expression: GRAFCET_NAME.DB_GRAFCET();
                        matchResults = re.search(pattern, ProjectSources[PCOName.lower() + "_sl.scl"])  # check if sl file contains call to grafcet

                        if not matchResults:
                            self.thePlugin.writeInUABLog("PCO unit $PCOName$ has no call to grafcet in SL file.")
                        # if the call was found, we have grafcet name
                        else:
                            grafcetName = matchResults.group(1).strip()
                            grafcetDB = matchResults.group(2)
                            self.thePlugin.writeInUABLog("PCO unit $PCOName$ has call to grafcet $grafcetName$ in SL file: $grafcetDB$.")

                            if grafcetName not in grafcetSteps:  # check if the grafcet of name grafcetName wasn't parsed before
                                # load user resources for grafcet processing

                                # this is based on UAB user resources
                                # theUserResourcesPath    = AbsolutePathBuilder.getApplicationPathParameter("SiemensSpecificParameters:UserResources:FixUserResourcesFile");
                                # theUserResources        = XMLInstancesFacade(theUserResourcesPath)
                                # theUserResourcesVector  = theUserResources.getDeviceType("UserResources").getAllDeviceTypeInstances()
                                # check what is the FB address of the found grafcet
                                # grafcetResource = find(lambda resource:resource.getAttributeData("UserResources:Symbol") == grafcetName, theUserResourcesVector)
                                # grafcetFB = grafcetResource.getAttributeData("UserResources:Address")

                                # this is based on UAB user resources

                                pattern = re.compile("^\"$grafcetName$\s*\",\"FB\s*(.+?)\s*\"", re.IGNORECASE | re.MULTILINE)        # patter looks for expression: GRAFCET_NAME.DB_GRAFCET();
                                symbolFile = self.find(lambda filename: filename.endswith(".sdf"), ProjectSources)

                                if not symbolFile:
                                    self.thePlugin.writeWarningInUABLog("No symbol (.sdf) file in ProjectSources folder, please export symbols to *exported_src folder")
                                else:
                                    #grafcetFB = re.search(pattern, ProjectSources[symbolFile]).group(1)
                                    grafcetFB = self.getFBofDBStepper(grafcetDB, ProjectSources[symbolFile])
                                    if grafcetFB == "":
                                        self.thePlugin.writeWarningInUABLog("No grafcetFB found in symbolFile for $grafcetDB$.")
                                    else:
                                        self.thePlugin.writeInUABLog("grafcet $grafcetName$ FB: $grafcetFB$")

                                        # find the source file that corresponds to the found FB number
                                        pattern = re.compile("^FUNCTION_BLOCK\s*(FB$grafcetFB$|\"$grafcetName$\")", re.IGNORECASE | re.MULTILINE)
                                        GrafcetSourceFileName = self.find(lambda fname: re.search(pattern, ProjectSources[fname]), ProjectSources)

                                        if GrafcetSourceFileName is None:
                                            self.thePlugin.writeWarningInUABLog("No source file found for grafcet $grafcetName$.")
                                        else:
                                            # parse grafcet
                                            (grafcetSteps[grafcetName], grafcetTransitions[grafcetName]) = self.parseGrafcet(grafcetName, ProjectSources[PCOName.lower() + "_tl.scl"], ProjectSources[GrafcetSourceFileName])

                            if grafcetName in grafcetSteps:  # check if the grafcet of name grafcetName was correctly parsed
                                graphvizPath = AbsolutePathBuilder.getTechnicalPathParameter(self.thePlugin.getId() + ":OutputParameters:OutputFolder") + "graphviz"
                                if not os.path.exists(graphvizPath):
                                    os.makedirs(graphvizPath)
                                    self.thePlugin.writeInUABLog("Created missing directory:" + graphvizPath)
                                
                                graphfileBasename = os.path.join(graphvizPath,PCOName)

                                # create dotfile in a nice format
                                dotFile = self.exportGrafcetToGraphviz(grafcetSteps[grafcetName], grafcetTransitions[grafcetName])

                                # save .dot with nice graph
                                dotOK = 0
                                try:
                                    f = open(graphfileBasename + ".dot", "w")
                                    f.write(dotFile)
                                    f.close()
                                    dotOK = 1
                                except:
                                    self.thePlugin.writeWarningInUABLog("Error handling the file: " + graphfileBasename + ".dot")

                                # save .dot with layout for pvss .xml
                                simpleDotTmpOK = 0
                                if dotOK == 1:
                                    try:
                                        f = open(graphfileBasename + "_simple.dot.tmp", "w")
                                        f.write(dotFile.replace("shape=Mrecord", "shape=point, width=0"))
                                        f.close()
                                        simpleDotTmpOK = 1
                                    except:
                                        self.thePlugin.writeWarningInUABLog("Error handling the file: " + graphfileBasename + "_simple.dot.tmp")

                                # generate dot files
                                dotExecOK = 0
                                if simpleDotTmpOK == 1:
                                    try:
                                        # generate SVG
                                        process = Runtime.getRuntime().exec('dot -Tsvg "$graphfileBasename$.dot" -o "$graphfileBasename$.svg"')
                                        process.waitFor()   # wait until process is finished
                                        # generate simple dot with positions for pvss .xml
                                        process = Runtime.getRuntime().exec('dot -Tplain "$graphfileBasename$_simple.dot.tmp" -o "$graphfileBasename$_simple.dot"')
                                        process.waitFor()   # wait until process is finished
                                        os.remove(graphfileBasename + "_simple.dot.tmp")
                                        dotExecOK = 1
                                    except:
                                        self.thePlugin.writeWarningInUABLog("Graphviz .dot processing error:" + str(sys.exc_info()))

                                # generate xml based on .dot with layout
                                if dotExecOK == 1:
                                    try:
                                        f = open(graphfileBasename + "_simple.dot", "r")
                                        headerText = self.thePlugin.getApplicationName() + " " + PCOName + " Stepper"
                                        outputXmlFile = self.exportGrafcetToXml(f, PCOName, ProjectSources[PCOName.lower() + "_tl.scl"], grafcetDB, grafcetSteps[grafcetName], grafcetTransitions[grafcetName], self.PVSSSystemName, headerText, self.PVSSAddTransitionLabels, self.panelW, self.panelH)
                                        f.close()

                                        f = open(graphfileBasename + "_Stepper.xml", "w")
                                        f.write(outputXmlFile)
                                        f.close()
                                    except Exception, e:
                                        self.thePlugin.writeWarningInUABLog("Stepper to xml error: " + str(e))

                elif self.isSchneider():
                    grafcetName = instance.getAttributeData("DeviceIdentification:Name")
                    if grafcetName not in grafcetSteps:  # check if the grafcet of name grafcetName wasn't parsed before...
                        (grafcetSteps[grafcetName], grafcetTransitions[grafcetName]) = self.parseGrafcet(grafcetName)
                        graphvizPath = AbsolutePathBuilder.getTechnicalPathParameter(self.thePlugin.getId() + ":OutputParameters:OutputFolder") + "graphviz"
                        if not os.path.exists(graphvizPath):
                            os.makedirs(graphvizPath)
                        
                        graphfileBasename = os.path.join(graphvizPath,PCOName)

                        if (grafcetName in grafcetSteps) and (len(grafcetSteps[grafcetName]) > 0):  # check if the grafcet of name grafcetName was correctly parsed
                            # create dotfile in a nice format
                            dotFile = self.exportGrafcetToGraphviz(grafcetSteps[grafcetName], grafcetTransitions[grafcetName])

                            # save .dot with nice graph
                            dotOK = 0
                            try:
                                f = open(graphfileBasename + ".dot", "w")
                                f.write(dotFile)
                                f.close()
                                dotOK = 1
                            except:
                                self.thePlugin.writeWarningInUABLog("Error handling the file: " + graphfileBasename + ".dot")

                            # save .dot with layout for pvss .xml
                            simpleDotTmpOK = 0
                            if dotOK == 1:
                                try:
                                    f = open(graphfileBasename + "_simple.dot.tmp", "w")
                                    f.write(dotFile.replace("shape=Mrecord", "shape=point, width=0"))
                                    f.close()
                                    simpleDotTmpOK = 1
                                except:
                                    self.thePlugin.writeWarningInUABLog("Error handling the file: " + graphfileBasename + "_simple.dot.tmp")

                            # generate dot files
                            dotExecOK = 0
                            if simpleDotTmpOK == 1:
                                try:
                                    # generate SVG
                                    process = Runtime.getRuntime().exec('dot -Tsvg "$graphfileBasename$.dot" -o "$graphfileBasename$.svg"')
                                    process.waitFor()   # wait until process is finished
                                    # generate simple dot with positions for pvss .xml
                                    process = Runtime.getRuntime().exec('dot -Tplain "$graphfileBasename$_simple.dot.tmp" -o "$graphfileBasename$_simple.dot"')
                                    process.waitFor()   # wait until process is finished
                                    os.remove(graphfileBasename + "_simple.dot.tmp")
                                    dotExecOK = 1
                                except:
                                    self.thePlugin.writeWarningInUABLog("Graphviz .dot processing error:" + str(sys.exc_info()))

                            # generate xml based on .dot with layout
                            if dotExecOK == 1:
                                try:
                                    f = open(graphfileBasename + "_simple.dot", "r")
                                    headerText = self.thePlugin.getApplicationName() + " " + PCOName + " Stepper"
                                    outputXmlFile = self.exportGrafcetToXml(f, PCOName, ProjectSources[PCOName + "_TL"], "", grafcetSteps[grafcetName], grafcetTransitions[grafcetName], self.PVSSSystemName, headerText, self.PVSSAddTransitionLabels, self.panelW, self.panelH, GLfile=ProjectSources[PCOName + "_GL"])
                                    f.close()
                                    f = open(graphfileBasename + "_Stepper.xml", "w")
                                    f.write(outputXmlFile)
                                    f.close()
                                except Exception, e:
                                    self.thePlugin.writeWarningInUABLog("Stepper to xml error: " + str(e))        
        
        self.thePlugin.writeInUABLog("Starting processing findMissingCCCAlarms_Template.")
       
    def end(self):
        self.thePlugin.writeInUABLog("end in Jython for findMissingCCCAlarms_Template.")


    def shutdown(self):
        self.thePlugin.writeInUABLog("shutdown in Jython for findMissingCCCAlarms_Template.")
