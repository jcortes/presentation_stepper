== Description ==
Folder reserved to locate jython scripts available for all plug-ins.
To use the functions of the jython scripts included in this folder from a plug-in template you must import and reload the required scripts.

Example: If this folder contains a script file named sharedFunctions.py containing a function named shared(), 
use the following code to execute the shared funcion:

=====================================================================================
# Import and reload the sharedFunctions script
import sharedFunctions
reload (sharedFunctions)

class Example_Template(IUnicosTemplate): 
   thePlugin = 0
   theUnicosProject = 0
 
   def initialize(self):
   	self.thePlugin = APlugin.getPluginInterface()
   	self.theUnicosProject = self.thePlugin.getUnicosProject()
   	# Call to the method defined in the sharedFunctions.py script
   	sharedFunctions.shared()
 
 
=====================================================================================