# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
import openpyxl # Version 1.2.3 used by FlexExtractor. Same one here
import io

class GridElement():
    def __init__(self):
        self.posX = 0
        self.posY = 0

class GridStep(GridElement):
    def __init__(self, name):
        GridElement.__init__(self)
        self.name = name
        self.WS_step = 0
        self.WS_pattern = ''
        self.treated = False
        self.located = False

class GridTrans(GridElement):
    def __init__(self, name):
        GridElement.__init__(self)
        self.name = name
        self.or_step = None
        self.dest_step = None
        self.conn_type = ''
        self.condition = ''
        self.altbranch = None
        self.altbranch_idx = 0

class GridAltBranch(GridElement):
    def __init__(self):
        GridElement.__init__(self)
        self.length = 0
        self.group = 0

class GridConn(GridElement):
    def __init__(self):
        GridElement.__init__(self)
        self.length = 0
        self.dest = ''

class Grid():
    def __init__(self):
        # Initialize instance attributes
        self.steps = []
        self.trans = []
        self.altbranches = []
        self.conns = []

    def add_step(self, name, WS_step):
        step = GridStep(name)
        if WS_step:
            step.WS_step = WS_step
        else:
            step.WS_step = len(self.steps) + 1
        self.steps.append(step)

    def add_step_pattern(self, name, WS_step_pattern):
        for step in self.steps:
            if step.name == name.split('-')[-1]:
                step.WS_pattern = WS_step_pattern

    def add_trans(self, name, cond, or_step, dest_step, WS_trans):
        trans = GridTrans(name)
        for step in self.steps:
            if step.name == or_step:
                trans.or_step = step
            if step.name == dest_step:
                trans.dest_step = step
        if cond:
            trans.condition = cond + ";"
        if WS_trans or WS_trans == 0:
            trans.WS_trans = WS_trans
        self.trans.append(trans)

    def move_elements(self):
        for step in self.steps:
            if step.posY > self.posY:
                step.posY += 2
            elif step.posY == self.posY and step.posX < self.posX and step.posX == max([current_step.posX for current_step in self.steps if current_step.posY == self.posY and current_step.posX < self.posX]):
                conn = GridConn()
                conn.posY = step.posY
                conn.posX = step.posX
                conn.length = 1
                conn.dest = "altBranch" if [altbranch for altbranch in self.altbranches if altbranch.posX == step.posX and altbranch.posY == step.posY+1] else "transition" 
                self.conns.append(conn)
        for trans in self.trans:
            if trans.posY > self.posY:
                trans.posY += 2
        for conn in self.conns:
            if conn.posX < self.posX and conn.posY + conn.length > self.posY:
                conn.length += 2
        for altbranch in self.altbranches:
            if altbranch.posY > self.posY:
                altbranch.posY += 2

    def locate_trans(self, all_transitions):
        # Generate alt branches
        if len(all_transitions) > 1:
            altbranch = GridAltBranch()
            altbranch.posX = self.posX
            altbranch.posY = self.posY + 1
            altbranch.length = len(all_transitions)
            altbranch.group = max([current_altbranch.group for current_altbranch in self.altbranches]) + 1 if len(self.altbranches) else 1
            self.altbranches.append(altbranch)

        # Locate steps and transitions
        for trans_idx, trans in enumerate(all_transitions, 1):
            trans.posX = self.posX
            trans.posY = self.posY + 1
            if not trans.dest_step.located:
                trans.dest_step.posX = self.posX
                trans.dest_step.posY = self.posY+2
                trans.dest_step.located = True
                trans.conn_type = 'n'
            else:
                trans.conn_type = 'j'
            if len(all_transitions) > 1:
                trans.altbranch = altbranch
                trans.altbranch_idx = trans_idx
            self.posX += 1

        # Prepare next iteration and generate connections
        next_steps = [current_step for current_step in self.steps if not current_step.treated and current_step.located]
        if next_steps:
            self.posY = min([current_step.posY for current_step in next_steps])
            self.posX = min([current_step.posX for current_step in next_steps if current_step.posY == self.posY])
            self.move_elements()
            next_step = [current_step for current_step in next_steps if current_step.posY == self.posY and current_step.posX == self.posX][0]
            next_step.treated = True
            return next_step.name
        else:
            return None

    def create_graph(self, init_step):
        self.posX = 0
        self.posY = 0

        # Treat initial step
        for step in self.steps:
            if step.name == init_step:
                step.posX = self.posX
                step.posY = self.posY
                step.conn_type = 'n'
                step.treated = True
                step.located = True

        next_step = init_step
        # Iterate over transitions of the step given as 'next_step'
        while(next_step):
            all_transitions = []
            for trans in self.trans:
                if trans.or_step.name == next_step:
                    all_transitions.append(trans)
            next_step = self.locate_trans(all_transitions)

class GraphInstance():
    def __init__(self):
        self.DB_name = ''
        self.WS_step = ''
        self.WS_trans = ''
        self.SL_file_name = ''
        self.TL_file_name = ''

class Graph():
    def __init__(self):
        self.out_file_name = ''
        self.FB_name = ''
        self.init_step = ''
        self.graph_instances = {}
        self.grid_instance = None

class SharedGrafcetData ():
    thePlugin = None
    siemens_PLC = None
    unity_PLC = None
    TIA_PLC = None
    wrong_data = False

    def __init__(self, thePlugin, file_name):
        self.thePlugin = thePlugin
        self.file_name = file_name
        self.siemens_PLC = self.thePlugin.getXMLConfig().getSiemensPLCDeclarations()
        self.unity_PLC = self.thePlugin.getXMLConfig().getSchneiderPLCDeclarations()
        self.TIA_PLC = self.thePlugin.getXMLConfig().getTiaPLCDeclarations()
        self.thePlugin.writeInUABLog("Processing file \"%s\"" % self.file_name)
        self.wrong_data = self.check_data()
        if not self.wrong_data:
            self.get_data()

    def check_data(self):
        check_data_error = False
        import zipfile
        import re
        try:
            with open(self.file_name, "rb") as f:
                in_mem_file = io.BytesIO(f.read())
            wb = openpyxl.reader.excel.load_workbook(in_mem_file)

            if openpyxl.__version__ == "1.2.3":
                tabname = 'Description'
                description_sheet = wb.get_sheet_by_name(tabname)
                description_sheet_content = [[current_cell.value for current_cell in current_row] for current_row in description_sheet.range(description_sheet.calculate_dimension())]
            # else:
                # version 2.4.9

            if self.siemens_PLC or self.TIA_PLC:
                max_length = 24
            elif self.unity_PLC:
                max_length = 32

            # Class to contain the information of each column of the description spreadsheet
            class description_column():
                def __init__(self, title, individual_data, mandatory_platforms):
                    self.title = title
                    self.individual_data = individual_data
                    self.mandatory_platforms = mandatory_platforms
                    self.value = ''

            description_header = description_sheet_content[0]
            description_fields = [description_column("File Name", False, ["siemens"]),
                                  description_column("FB Name", False, ["siemens"]),
                                  description_column("Tab Name", False, ["siemens","schneider"]),
                                  description_column("Init Step", False, ["siemens","schneider"]),
                                  description_column("Version", False, ["siemens","schneider"]),
                                  description_column("PCO Name", True, ["siemens","schneider"]),
                                  description_column("DB Name", True, ["siemens"]),
                                  description_column("WS Step", True, []),
                                  description_column("WS Trans", True, []),
                                  description_column("SL Logic File", True, []),
                                  description_column("TL Logic File", True, [])]

            # Pattern for steps and transitions
            step_name_pattern = "([1-9][0-9]*-)?[a-zA-Z0-9_\\\x24]+"
            transition_name_pattern = "([a-zA-Z0-9_\\\x24]+:=)?[a-zA-Z0-9_.<>=\*/\-\+\\\x24\(\)\[\]\n ]*"

            # Description spreadsheet check
            for description_row in description_sheet_content[1:]:
                description_individual_mandatory_fields = []
                description_collective_mandatory_fields = []
                for description_field in description_fields:
                    if description_field.title in description_header:
                        # Get values of existent columns in the description spreadsheet
                        description_field_idx = description_header.index(description_field.title)
                        description_field.value = description_row[description_field_idx]
                        if description_field.title == "Tab Name":
                            tabname = description_field.value
                        elif description_field.title == "Init Step":
                            init_step = description_field.value
                    if self.thePlugin.getPlcManufacturer().lower() in description_field.mandatory_platforms:
                        # Store mandatory fields
                        if description_field.individual_data:
                            description_individual_mandatory_fields.append(description_field)
                        else:
                            description_collective_mandatory_fields.append(description_field)
                # Check mandatory fields in description spreadsheet
                for description_individual_field in description_individual_mandatory_fields:
                    if not description_individual_field.value:
                        self.thePlugin.writeErrorInUABLog("\"Description\" spreadsheet: %s field is mandatory" % description_individual_field.title)
                        check_data_error = True
                filled_description_collective_fields = [description_collective_field for description_collective_field in description_collective_mandatory_fields if description_collective_field.value]
                if filled_description_collective_fields != description_collective_mandatory_fields and (description_row == description_sheet_content[1] or len(filled_description_collective_fields) > 0):
                    for description_collective_field in description_collective_mandatory_fields:
                        if description_collective_field not in filled_description_collective_fields:
                            self.thePlugin.writeErrorInUABLog("\"Description\" spreadsheet: %s field is mandatory" % description_collective_field.title)
                            check_data_error = True
                if tabname:
                    # Graph spreadsheets check
                    if openpyxl.__version__ == "1.2.3":
                        graph_sheet = wb.get_sheet_by_name(tabname)
                        graph_sheet_content = [[current_cell.value for current_cell in current_row] for current_row in graph_sheet.range(graph_sheet.calculate_dimension())]
                    # else:
                        # version 2.4.9
                    graph_sheet_row_header = graph_sheet_content[0]
                    graph_sheet_column_header = [current_row[0] for current_row in graph_sheet_content]
                    # Check row and column headers
                    if graph_sheet_row_header != graph_sheet_column_header:
                        self.thePlugin.writeErrorInUABLog("Row and column header in \"%s\" spreadsheet do not match" % tabname)
                        check_data_error = True
                    # Check initial step in headers
                    if init_step not in [graph_sheet_header_step.split('-')[-1] for graph_sheet_header_step in graph_sheet_row_header[1:]]:
                        self.thePlugin.writeErrorInUABLog("Initial step not present in \"%s\" spreadsheet steps" % tabname)
                        check_data_error = True
                    # Check steps and transitions format
                    for graph_sheet_index in range(1, len(graph_sheet_content)):
                        if graph_sheet_content[graph_sheet_index][graph_sheet_index]:
                            self.thePlugin.writeErrorInUABLog("Spreadsheet \"%s\": A step cannot transition to itself" % tabname)
                            check_data_error = True
                        if not re.match(ur"^" + step_name_pattern + ur"\u0024", graph_sheet_row_header[graph_sheet_index]):
                            self.thePlugin.writeErrorInUABLog("Spreadsheet \"%s\": Wrong step format: %s" % (tabname, graph_sheet_row_header[graph_sheet_index]))
                            check_data_error = True
                        if len(graph_sheet_row_header[graph_sheet_index].split("-")[-1]) > max_length:
                            self.thePlugin.writeErrorInUABLog("Spreadsheet \"%s\": Step name too long: %s" % (tabname, graph_sheet_row_header[graph_sheet_index].split("-")[-1]))
                            check_data_error = True
                        for graph_sheet_index_2 in range(graph_sheet_index+1, len(graph_sheet_content)):
                            if graph_sheet_content[graph_sheet_index][graph_sheet_index_2] and not re.match(ur"^" + transition_name_pattern + ur"\u0024", graph_sheet_content[graph_sheet_index][graph_sheet_index_2]):
                                self.thePlugin.writeErrorInUABLog("Spreadsheet \"%s\": Wrong transition format: %s" % (tabname, graph_sheet_content[graph_sheet_index][graph_sheet_index_2]))
                                check_data_error = True
                            if graph_sheet_content[graph_sheet_index_2][graph_sheet_index] and not re.match(ur"^" + transition_name_pattern + ur"\u0024", graph_sheet_content[graph_sheet_index_2][graph_sheet_index]):
                                self.thePlugin.writeErrorInUABLog("Spreadsheet \"%s\": Wrong transition format: %s" % (tabname, graph_sheet_content[graph_sheet_index_2][graph_sheet_index]))
                                check_data_error = True
                            if graph_sheet_content[graph_sheet_index][graph_sheet_index_2]:
                                if ":=" in graph_sheet_content[graph_sheet_index][graph_sheet_index_2]:
                                    if len(graph_sheet_content[graph_sheet_index][graph_sheet_index_2].split(":=")[0]) > max_length:
                                        self.thePlugin.writeErrorInUABLog("Spreadsheet \"%s\": Transition name too long: %s" % (tabname, graph_sheet_content[graph_sheet_index][graph_sheet_index_2].split(":=")[0]))
                                        check_data_error = True
                                else:
                                    if len(graph_sheet_row_header[graph_sheet_index_2].split("-")[-1]) + len(graph_sheet_column_header[graph_sheet_index].split("-")[-1]) + 1 > max_length:
                                        self.thePlugin.writeErrorInUABLog("Spreadsheet \"%s\": Transition name too long based on steps: %s and %s" % (tabname, graph_sheet_column_header[graph_sheet_index], graph_sheet_row_header[graph_sheet_index_2]))
                                        check_data_error = True
                            if graph_sheet_content[graph_sheet_index_2][graph_sheet_index]:
                                if ":=" in graph_sheet_content[graph_sheet_index_2][graph_sheet_index]:
                                    if len(graph_sheet_content[graph_sheet_index_2][graph_sheet_index].split(":=")[0]) > max_length:
                                        self.thePlugin.writeErrorInUABLog("Spreadsheet \"%s\": Transition name too long: %s" % (tabname, graph_sheet_content[graph_sheet_index_2][graph_sheet_index].split(":=")[0]))
                                        check_data_error = True
                                else:
                                    if len(graph_sheet_row_header[graph_sheet_index].split("-")[-1]) + len(graph_sheet_column_header[graph_sheet_index_2].split("-")[-1]) + 1 > max_length:
                                        self.thePlugin.writeErrorInUABLog("Spreadsheet \"%s\": Transition name too long based on steps: %s and %s" % (tabname, graph_sheet_column_header[graph_sheet_index_2], graph_sheet_row_header[graph_sheet_index]))
                                        check_data_error = True
        except IOError:
            self.thePlugin.writeErrorInUABLog("File \"%s\" does not exist" % self.file_name)
            check_data_error = True
        except zipfile.BadZipfile:
            self.thePlugin.writeErrorInUABLog("File \"%s\" does not have the correct format" % self.file_name)
            check_data_error = True
        except AttributeError:
            self.thePlugin.writeErrorInUABLog("\"%s\" spreadsheet not found in file" % tabname)
            check_data_error = True
        finally:
            return check_data_error

    def get_data(self):
        with open(self.file_name, "rb") as f:
            in_mem_file = io.BytesIO(f.read())
        wb = openpyxl.reader.excel.load_workbook(in_mem_file)

        # Get raw data from the workbook
        if openpyxl.__version__ == "1.2.3":
            description_sheet = wb.get_sheet_by_name('Description')
            description_sheet_content = [[current_cell.value for current_cell in current_row] for current_row in description_sheet.range(description_sheet.calculate_dimension())]
        else:
            # version 2.4.9
            description_sheet = wb['Description']
            description_sheet_content = [current_row for current_row in description_sheet.values]

        self.graphs = {}
        graph_sheet_content = {}
        graph_sheet_vars_content = {}
        graph_sheet_pattern_content = {}
        description_header = description_sheet_content[0]
        description_tabname_idx = description_header.index("Tab Name")
        for description_row in description_sheet_content[1:]:
            if openpyxl.__version__ == "1.2.3":
                graph_sheet = wb.get_sheet_by_name(description_row[description_tabname_idx])
                if graph_sheet:
                    self.graphs[graph_sheet.title] = Graph()
                    graph_sheet_content[graph_sheet.title] = [[current_cell.value for current_cell in current_row] for current_row in graph_sheet.range(graph_sheet.calculate_dimension())]
                    graph_sheet_vars = wb.get_sheet_by_name(graph_sheet.title + "_Variables")
                    if graph_sheet_vars:
                        graph_sheet_vars_content[graph_sheet.title] = [[current_cell.value for current_cell in current_row] for current_row in graph_sheet_vars.range(graph_sheet_vars.calculate_dimension())]
                    graph_sheet_pattern = wb.get_sheet_by_name(graph_sheet.title + "_Pattern")
                    if graph_sheet_pattern:
                        graph_sheet_pattern_content[graph_sheet.title] = [[current_cell.value for current_cell in current_row] for current_row in graph_sheet_pattern.range(graph_sheet_pattern.calculate_dimension())]
                elif description_row[description_tabname_idx]:
                    self.thePlugin.writeErrorInUABLog("Worksheet \"$description_row[description_tabname_idx]$\" not found in workbook")
            else:
                # version 2.4.9
                graph_sheet = wb[description_row[description_tabname_idx]]
                if graph_sheet:
                    self.graphs[graph_sheet.title] = Graph()
                    graph_sheet_content[graph_sheet.title] = [current_row for current_row in graph_sheet.values]
                    graph_sheet_vars = wb[graph_sheet.title + "_Variables"]
                    if graph_sheet_vars:
                        graph_sheet_vars_content[graph_sheet.title] = [current_row for current_row in graph_sheet_vars.values]
                    graph_sheet_pattern = wb[graph_sheet.title + "_Pattern"]
                    if graph_sheet_pattern:
                        graph_sheet_pattern_content[graph_sheet.title] = [current_row for current_row in graph_sheet_pattern.values]
                elif description_row[description_tabname_idx]:
                    self.thePlugin.writeErrorInUABLog("Worksheet \"$description_row[description_tabname_idx]$\" not found in workbook")
                wb.close()

        # Extract data
        description_table = [current_row for current_row in description_sheet_content[1:]]
        self.all_data = {}
        for graph_sheet_name, transition_sheet_content in graph_sheet_content.items():
            current_graph = self.graphs[graph_sheet_name]
            # Description tab
            description_row_idx = [row_idx for row_idx in range(len(description_table)) if description_table[row_idx][description_tabname_idx] == graph_sheet_name][0]
            current_graph.out_file_name = description_table[description_row_idx][description_header.index("File Name")]
            current_graph.FB_name = description_table[description_row_idx][description_header.index("FB Name")]
            current_graph.init_step = description_table[description_row_idx][description_header.index("Init Step")]
            current_graph.graph_instances = {}
            current_graph_instances = current_graph.graph_instances
            while description_row_idx < len(description_table) and (description_table[description_row_idx][description_tabname_idx] == graph_sheet_name or not description_table[description_row_idx][description_tabname_idx]):
                PCO_name = description_table[description_row_idx][description_header.index("PCO Name")]
                current_graph_instances[PCO_name] = GraphInstance()
                current_graph_instances[PCO_name].DB_name = description_table[description_row_idx][description_header.index("DB Name")]
                current_graph_instances[PCO_name].WS_step = description_table[description_row_idx][description_header.index("WS Step")]
                current_graph_instances[PCO_name].WS_trans = description_table[description_row_idx][description_header.index("WS Trans")]
                current_graph_instances[PCO_name].SL_file_name = description_table[description_row_idx][description_header.index("SL Logic File")]
                current_graph_instances[PCO_name].TL_file_name = description_table[description_row_idx][description_header.index("TL Logic File")]
                description_row_idx += 1
            # Transition tabs
            steps_content = [current_row[0] for current_row in transition_sheet_content[1:]]
            dest_steps = transition_sheet_content[0][1:]
            transition_table = [current_row[1:] for current_row in transition_sheet_content[1:]]
            current_graph.grid_instance = Grid()
            current_grid_instance = current_graph.grid_instance
            conditions = []
            for current_step in steps_content:
                if current_step.count("-"):
                    current_step_WS, current_step = current_step.split("-")
                else:
                    current_step_WS = None
                current_grid_instance.add_step(current_step, current_step_WS)
            for current_step_idx, current_step in enumerate(steps_content):
                current_step = current_step.split("-")[-1]
                for current_dest_step_idx, current_dest_step in enumerate(dest_steps):
                    transition_dest = transition_table[current_step_idx][current_dest_step_idx]
                    if transition_dest:
                        trans_dest_step = current_dest_step.split("-")[-1]
                        if transition_dest.count(":="):
                            trans_var, trans_cond = transition_dest.split(":=")
                        else:
                            trans_var = "_".join([current_step, trans_dest_step])
                            trans_cond = transition_dest
                        if trans_var not in conditions:
                            trans_WS = len(conditions)
                            conditions.append(trans_var)
                        else:
                            trans_WS = None
                        current_grid_instance.add_trans(trans_var, trans_cond, current_step, trans_dest_step, trans_WS)
            # Variables tabs
            current_graph.template_vars = []
            current_graph.code_vars = []
            current_graph.code_vars_declaration = []
            if graph_sheet_name in graph_sheet_vars_content.keys():
                for variables_row in graph_sheet_vars_content[graph_sheet_name][1:]:
                    var_name = variables_row[0]
                    var_value = str(variables_row[1])
                    if '\x24' in var_name:
                        current_graph.template_vars.append(var_name[1:-1] + " = \"" + var_value + "\"")
                    else:
                        current_graph.code_vars.append(var_name + " := " + var_value + ";")
                        current_graph.code_vars_declaration.append(var_name)
            # Pattern tabs
            if graph_sheet_name in graph_sheet_pattern_content.keys():
                pattern_table = [current_row for current_row in graph_sheet_pattern_content[graph_sheet_name][1:]]
                for pattern_row in pattern_table:
                    WS_step_name = pattern_row[0]
                    WS_step_pattern = pattern_row[1]
                    current_grid_instance.add_step_pattern(WS_step_name, WS_step_pattern)

    def get_grid(self, tab_name):
        graph_instance = self.graphs[tab_name]
        grid_instance = graph_instance.grid_instance
        grid_instance.create_graph(graph_instance.init_step)

    def get_graph_tab_name(self, PCO_name):
        for current_graph_name, current_graph in self.graphs.items():
            for current_PCO_name in current_graph.graph_instances:
                if current_PCO_name == PCO_name:
                    return current_graph_name
        return None

    def get_PCO_list(self):
        PCO_list = []
        for current_graph in self.graphs.values():
            PCO_list.extend(current_graph.graph_instances.keys())
        return PCO_list

    def get_logic_file(self, PCO_name, logic_file_name):
        current_graph_name = self.get_graph_tab_name(PCO_name)
        if current_graph_name:
            current_graph = self.graphs[current_graph_name]
            if logic_file_name == "SL":
                return current_graph.graph_instances[PCO_name].SL_file_name
            elif logic_file_name == "TL":
                return current_graph.graph_instances[PCO_name].TL_file_name
            else:
                self.thePlugin.writeErrorInUABLog("Logic file not existent for this generation")
                return None

    def get_graph_file(self, PCO_name):
        self.thePlugin.writeInUABLog("Getting graph file for PCO \"%s\"" % PCO_name)
        current_graph_name = self.get_graph_tab_name(PCO_name)
        if current_graph_name:
            current_graph = self.graphs[current_graph_name]
            if 'posX' not in dir(current_graph.grid_instance):
                self.get_grid(current_graph_name)
            current_grid = current_graph.grid_instance

            if self.siemens_PLC:
                header = '''FUNCTION_BLOCK "$current_graph.FB_name$"
(*\x24_COM Block comment*)

(*\x24_CMPSET
         S7Graph-Version: 5000
         WarningLevel: All
         FBInterface: UserDefined
         SSBLevel: Individual
         ASMsgLevel: No
         TargetMachine: AS300StdBlock
         RuntimeBlock: FC 72
         CritAvail: F
         SSkipOn: F
         AckReq: T
         SyncAvail: F
         PermILMan: F
         SWMLocked: F
         InitVarStat: 4401
*)

(*\x24_SETTINGS
         ProgLang: LAD
*)\n\n'''

                vars = '''VAR_INPUT 
    OFF_SQ   : Bool := FALSE;(*Turn sequencer off (OFF_SEQUENCE)*)
    INIT_SQ   : Bool := FALSE;(*Set sequencer to initial state (INIT_SEQUENCE)*)
    ACK_EF   : Bool := FALSE;(*Acknowledge all errors and disturbances (ACKNOWLEDGE_ERROR_FAULT)*)
    S_PREV   : Bool := FALSE;(*Display previous step in S_NO (PREVIOUS_STEP)*)
    S_NEXT   : Bool := FALSE;(*Display next step in S_NO (NEXT_STEP)*)
    SW_AUTO   : Bool := FALSE;(*Set automatic mode (SWITCH_MODE_AUTOMATIC)*)
    SW_TAP   : Bool := FALSE;(*Set inching mode 'T and T_PUSH' (SWITCH_MODE_TRANSITION_AND_PUSH)*)
    SW_MAN   : Bool := FALSE;(*Set manual mode (SWITCH_MODE_MANUAL)*)
    S_SEL   : INT := 0;(*Select step for display in S_NO (STEP_SELECT)*)
    S_ON   : Bool := FALSE;(*Activate step displayed in S_NO (STEP_ON)*)
    S_OFF   : Bool := FALSE;(*Deactivate step displayed in (STEP_OFF)*)
    T_PUSH   : Bool := FALSE;(*Enable transition to switch in SW_TAP and SW_TOP (PUSH_TRANSITION)*)'''

                step_txt = ''
                for step in current_grid.steps:
                    if step.name != current_graph.init_step:
                        step_txt += '''\n\nSTEP $step.name$ (*\x24_NUM $step.WS_step$*):
(*\x24_COM Step comment*)

END_STEP '''
                    else:
                        init_step_num = step.WS_step
                step_txt = '''\n\n\nINITIAL_STEP $current_graph.init_step$ (*\x24_NUM $init_step_num$*):
(*\x24_COM Step comment*)

END_STEP''' + step_txt

                transition_txt = '\n'
                for trans_idx, trans in reversed(list(enumerate(current_grid.trans, 1))):
                    jump_txt = ' (*\x24_JUMP*)' if trans.conn_type =='j' else ''
        
                    transition_txt += '''
TRANSITION Trans$trans_idx$ (*\x24_NUM $trans_idx$*)
  FROM $trans.or_step.name$
  TO $trans.dest_step.name$$jump_txt$
CONDITION := $trans.name$ 
END_TRANSITION 
'''
                    if 'WS_trans' in dir(trans):
                        vars += '''\n    $trans.name$   : Bool := FALSE;'''

                vars += '''
END_VAR
VAR_OUTPUT 
    S_NO   : INT := 0;(*Step number (STEP_NUMBER)*)
    S_MORE   : Bool := FALSE;(*More steps available for display (MORE_STEPS)*)
    S_ACTIVE   : Bool := FALSE;(*Step S_NO is active (STEP_ACTIVE)*)
    ERR_FLT   : Bool := FALSE;(*Group error interlocks or supervisions  (IL_ERROR_OR_SV_FAULT)*)
    AUTO_ON   : Bool := FALSE;(*SW_AUTO mode is active (AUTOMATIC_IS_ON)*)
    TAP_ON   : Bool := FALSE;(*SW_TAP mode is active (T_AND_PUSH_IS_ON)*)
    MAN_ON   : Bool := FALSE;(*SW_MAN mode is active (MANUAL_IS_ON)*)
END_VAR'''
                
                footer = '\nEND_FUNCTION_BLOCK'

                return "%s.GR7" % current_graph.out_file_name, header + vars + step_txt + transition_txt + footer

            elif self.unity_PLC:
                step_txt = '''                <step stepType="initialStep" stepName="$current_graph.init_step$">
                    <objPosition posX="0" posY="0"></objPosition>
                    <literals max="" min="" delay=""></literals>
                </step>\n'''
                jump_txt = ''
                transition_txt = ''
                alt_branch_txt = ''
                link_txt = ''
                variables = ''

                for step in current_grid.steps:
                    if step.name != current_graph.init_step:
                        step_txt += '''                <step stepType="step" stepName="$step.name$">
                    <objPosition posX="$step.posX$" posY="$step.posY$"></objPosition>
                    <literals max="" min="" delay=""></literals>
                </step>\n'''
                for trans in current_grid.trans:
                    transition_txt += '''                <transition>
                    <objPosition posX="$trans.posX$" posY="$trans.posY$"></objPosition>
                    <transitionCondition invertLogic="false">
                        <variableName>$trans.name$</variableName>
                    </transitionCondition>
                </transition>\n'''
                    if trans.conn_type == 'j':
                        jump_txt += '''                <jumpSFC stepName="$trans.dest_step.name$">
                    <objPosition posX="$trans.posX$" posY="$trans.posY+1$"></objPosition>
                </jumpSFC>\n'''
                    if 'WS_trans' in dir(trans):
                        variables += '''<variables name="$trans.name$" typeName="BOOL"></variables>\n'''
                for alt_branch in current_grid.altbranches:
                    alt_branch_txt += '''                <altBranch width="$alt_branch.length$" relativePos="0">
                    <objPosition posX="$alt_branch.posX$" posY="$alt_branch.posY$"></objPosition>
                </altBranch>\n'''
                for conn in current_grid.conns:
                    link_txt += '''                <linkSFC>
                    <directedLinkSource objectType="step">
                        <objPosition posX="$conn.posX$" posY="$conn.posY$"></objPosition>
                    </directedLinkSource>
                    <directedLinkDestination objectType="$conn.dest$">
                        <objPosition posX="$conn.posX$" posY="$conn.posY+conn.length$"></objPosition>
                    </directedLinkDestination>
                </linkSFC>\n'''

                # Write text file
                return "%s.xsf" % current_graph.out_file_name, [step_txt + jump_txt + transition_txt + alt_branch_txt + link_txt, variables]

            elif self.TIA_PLC:
                header = '''<?xml version="1.0" encoding="utf-8"?>
<Document>
  <Engineering version="V15" />
  <DocumentInfo>
    <Created>2018-03-19T16:03:34.6330616Z</Created>
    <ExportSetting>WithDefaults, WithReadOnly</ExportSetting>
    <InstalledProducts>
      <Product>
        <DisplayName>Totally Integrated Automation Portal</DisplayName>
        <DisplayVersion>V15 Update 4</DisplayVersion>
      </Product>
      <OptionPackage>
        <DisplayName>TIA Portal Openness</DisplayName>
        <DisplayVersion>V15 Update 4</DisplayVersion>
      </OptionPackage>
      <Product>
        <DisplayName>STEP 7 Professional</DisplayName>
        <DisplayVersion>V15 Update 4</DisplayVersion>
      </Product>
    </InstalledProducts>
  </DocumentInfo>
  <SW.Blocks.FB ID="0">
    <AttributeList>
      <AcknowledgeErrorsRequired>true</AcknowledgeErrorsRequired>
      <AutoNumber>true</AutoNumber>
      <CodeModifiedDate ReadOnly="true">2018-03-19T16:02:46.4592447Z</CodeModifiedDate>
      <CompileDate ReadOnly="true">2018-03-19T16:02:46.4582446Z</CompileDate>
      <CreationDate ReadOnly="true">2018-03-19T16:02:03.7349727Z</CreationDate>
      <GraphVersion>2.0</GraphVersion>
      <HandleErrorsWithinBlock ReadOnly="true">false</HandleErrorsWithinBlock>
      <HeaderAuthor />
      <HeaderFamily />
      <HeaderName />
      <HeaderVersion>0.1</HeaderVersion>
      <Interface><Sections xmlns="http://www.siemens.com/automation/Openness/SW/Interface/v3">
  <Section Name="Base">
    <Sections Datatype="GRAPH_BASE" Version="1.0">
      <Section Name="Input" />
      <Section Name="Output" />
      <Section Name="InOut" />
      <Section Name="Static">
        <Member Name="OFF_SQ_BASE" Datatype="Bool" />
      </Section>
    </Sections>
  </Section>\n'''

                variables = '''  <Section Name="Input">
    <Member Name="OFF_SQ" Datatype="Bool" Remanence="NonRetain" Accessibility="Public"><AttributeList><BooleanAttribute Name="ExternalAccessible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="HmiVisible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="UserVisible" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserReadOnly" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserDeletable" Informative="true" SystemDefined="true">true</BooleanAttribute></AttributeList><Comment Informative="true"><MultiLanguageText Lang="en-US">Turn sequence off</MultiLanguageText></Comment><StartValue Informative="true">false</StartValue></Member>
    <Member Name="INIT_SQ" Datatype="Bool" Remanence="NonRetain" Accessibility="Public"><AttributeList><BooleanAttribute Name="ExternalAccessible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="HmiVisible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="UserVisible" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserReadOnly" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserDeletable" Informative="true" SystemDefined="true">true</BooleanAttribute></AttributeList><Comment Informative="true"><MultiLanguageText Lang="en-US">Set sequence to initial state</MultiLanguageText></Comment><StartValue Informative="true">false</StartValue></Member>
    <Member Name="ACK_EF" Datatype="Bool" Remanence="NonRetain" Accessibility="Public"><AttributeList><BooleanAttribute Name="ExternalAccessible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="HmiVisible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="UserVisible" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserReadOnly" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserDeletable" Informative="true" SystemDefined="true">true</BooleanAttribute></AttributeList><Comment Informative="true"><MultiLanguageText Lang="en-US">Acknowledge all errors and faults</MultiLanguageText></Comment><StartValue Informative="true">false</StartValue></Member>
    <Member Name="S_PREV" Datatype="Bool" Remanence="NonRetain" Accessibility="Public"><AttributeList><BooleanAttribute Name="ExternalAccessible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="HmiVisible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="UserVisible" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserReadOnly" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserDeletable" Informative="true" SystemDefined="true">true</BooleanAttribute></AttributeList><Comment Informative="true"><MultiLanguageText Lang="en-US">Output previous step in parameter S_NO</MultiLanguageText></Comment><StartValue Informative="true">false</StartValue></Member>
    <Member Name="S_NEXT" Datatype="Bool" Remanence="NonRetain" Accessibility="Public"><AttributeList><BooleanAttribute Name="ExternalAccessible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="HmiVisible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="UserVisible" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserReadOnly" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserDeletable" Informative="true" SystemDefined="true">true</BooleanAttribute></AttributeList><Comment Informative="true"><MultiLanguageText Lang="en-US">Indicate next step in parameter S_NO</MultiLanguageText></Comment><StartValue Informative="true">false</StartValue></Member>
    <Member Name="SW_AUTO" Datatype="Bool" Remanence="NonRetain" Accessibility="Public"><AttributeList><BooleanAttribute Name="ExternalAccessible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="HmiVisible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="UserVisible" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserReadOnly" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserDeletable" Informative="true" SystemDefined="true">true</BooleanAttribute></AttributeList><Comment Informative="true"><MultiLanguageText Lang="en-US">Automatic mode</MultiLanguageText></Comment><StartValue Informative="true">false</StartValue></Member>
    <Member Name="SW_TAP" Datatype="Bool" Remanence="NonRetain" Accessibility="Public"><AttributeList><BooleanAttribute Name="ExternalAccessible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="HmiVisible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="UserVisible" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserReadOnly" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserDeletable" Informative="true" SystemDefined="true">true</BooleanAttribute></AttributeList><Comment Informative="true"><MultiLanguageText Lang="en-US">Semiautomatic/switch with transition</MultiLanguageText></Comment><StartValue Informative="true">false</StartValue></Member>
    <Member Name="SW_TOP" Datatype="Bool" Remanence="NonRetain" Accessibility="Public"><AttributeList><BooleanAttribute Name="ExternalAccessible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="HmiVisible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="UserVisible" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserReadOnly" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserDeletable" Informative="true" SystemDefined="true">true</BooleanAttribute></AttributeList><Comment Informative="true"><MultiLanguageText Lang="en-US">Semiautomatic/ignore transition</MultiLanguageText></Comment><StartValue Informative="true">false</StartValue></Member>
    <Member Name="SW_MAN" Datatype="Bool" Remanence="NonRetain" Accessibility="Public"><AttributeList><BooleanAttribute Name="ExternalAccessible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="HmiVisible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="UserVisible" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserReadOnly" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserDeletable" Informative="true" SystemDefined="true">true</BooleanAttribute></AttributeList><Comment Informative="true"><MultiLanguageText Lang="en-US">Manual mode</MultiLanguageText></Comment><StartValue Informative="true">false</StartValue></Member>
    <Member Name="S_SEL" Datatype="Int" Remanence="NonRetain" Accessibility="Public"><AttributeList><BooleanAttribute Name="ExternalAccessible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="HmiVisible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="UserVisible" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserReadOnly" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserDeletable" Informative="true" SystemDefined="true">true</BooleanAttribute></AttributeList><Comment Informative="true"><MultiLanguageText Lang="en-US">Select step to be output to S_NO</MultiLanguageText></Comment><StartValue Informative="true">0</StartValue></Member>
    <Member Name="S_ON" Datatype="Bool" Remanence="NonRetain" Accessibility="Public"><AttributeList><BooleanAttribute Name="ExternalAccessible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="HmiVisible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="UserVisible" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserReadOnly" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserDeletable" Informative="true" SystemDefined="true">true</BooleanAttribute></AttributeList><Comment Informative="true"><MultiLanguageText Lang="en-US">Activate step indicated in S_NO</MultiLanguageText></Comment><StartValue Informative="true">false</StartValue></Member>
    <Member Name="S_OFF" Datatype="Bool" Remanence="NonRetain" Accessibility="Public"><AttributeList><BooleanAttribute Name="ExternalAccessible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="HmiVisible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="UserVisible" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserReadOnly" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserDeletable" Informative="true" SystemDefined="true">true</BooleanAttribute></AttributeList><Comment Informative="true"><MultiLanguageText Lang="en-US">Deactivate step indicated S_NO</MultiLanguageText></Comment><StartValue Informative="true">false</StartValue></Member>
    <Member Name="T_PUSH" Datatype="Bool" Remanence="NonRetain" Accessibility="Public"><AttributeList><BooleanAttribute Name="ExternalAccessible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="HmiVisible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="UserVisible" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserReadOnly" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserDeletable" Informative="true" SystemDefined="true">true</BooleanAttribute></AttributeList><Comment Informative="true"><MultiLanguageText Lang="en-US">Enable transition to switch in semi automatic mode</MultiLanguageText></Comment><StartValue Informative="true">false</StartValue></Member>\n'''

                for trans in current_grid.trans:
                    if 'WS_trans' in dir(trans):
                        variables += '    <Member Name="$trans.name$" Datatype="Bool" Remanence="NonRetain" Accessibility="Public"><AttributeList><BooleanAttribute Name="ExternalAccessible" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="HmiVisible" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserVisible" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserReadOnly" Informative="true" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="UserDeletable" Informative="true" SystemDefined="true">true</BooleanAttribute></AttributeList></Member>\n'

                # OFFSETs in RT_DATA not configured (all set to 0. Values will be changed on importation)
                variables += '''  </Section>
  <Section Name="Output">
    <Member Name="S_NO" Datatype="Int" Remanence="NonRetain" Accessibility="Public"><AttributeList><BooleanAttribute Name="ExternalAccessible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="HmiVisible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="UserVisible" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserReadOnly" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserDeletable" Informative="true" SystemDefined="true">true</BooleanAttribute></AttributeList><Comment Informative="true"><MultiLanguageText Lang="en-US">Step number</MultiLanguageText></Comment><StartValue Informative="true">0</StartValue></Member>
    <Member Name="S_MORE" Datatype="Bool" Remanence="NonRetain" Accessibility="Public"><AttributeList><BooleanAttribute Name="ExternalAccessible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="HmiVisible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="UserVisible" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserReadOnly" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserDeletable" Informative="true" SystemDefined="true">true</BooleanAttribute></AttributeList><Comment Informative="true"><MultiLanguageText Lang="en-US">More steps are available and can be shown in S_NO</MultiLanguageText></Comment><StartValue Informative="true">false</StartValue></Member>
    <Member Name="S_ACTIVE" Datatype="Bool" Remanence="NonRetain" Accessibility="Public"><AttributeList><BooleanAttribute Name="ExternalAccessible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="HmiVisible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="UserVisible" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserReadOnly" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserDeletable" Informative="true" SystemDefined="true">true</BooleanAttribute></AttributeList><Comment Informative="true"><MultiLanguageText Lang="en-US">Step indicated in S_NO is active</MultiLanguageText></Comment><StartValue Informative="true">false</StartValue></Member>
    <Member Name="ERR_FLT" Datatype="Bool" Remanence="NonRetain" Accessibility="Public"><AttributeList><BooleanAttribute Name="ExternalAccessible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="HmiVisible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="UserVisible" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserReadOnly" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserDeletable" Informative="true" SystemDefined="true">true</BooleanAttribute></AttributeList><Comment Informative="true"><MultiLanguageText Lang="en-US">Interlock or supervision group error</MultiLanguageText></Comment><StartValue Informative="true">false</StartValue></Member>
    <Member Name="AUTO_ON" Datatype="Bool" Remanence="NonRetain" Accessibility="Public"><AttributeList><BooleanAttribute Name="ExternalAccessible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="HmiVisible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="UserVisible" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserReadOnly" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserDeletable" Informative="true" SystemDefined="true">true</BooleanAttribute></AttributeList><Comment Informative="true"><MultiLanguageText Lang="en-US">Automatic mode is active</MultiLanguageText></Comment><StartValue Informative="true">false</StartValue></Member>
    <Member Name="TAP_ON" Datatype="Bool" Remanence="NonRetain" Accessibility="Public"><AttributeList><BooleanAttribute Name="ExternalAccessible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="HmiVisible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="UserVisible" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserReadOnly" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserDeletable" Informative="true" SystemDefined="true">true</BooleanAttribute></AttributeList><Comment Informative="true"><MultiLanguageText Lang="en-US">Semiautomatic mode/step with transition enabled</MultiLanguageText></Comment><StartValue Informative="true">false</StartValue></Member>
    <Member Name="TOP_ON" Datatype="Bool" Remanence="NonRetain" Accessibility="Public"><AttributeList><BooleanAttribute Name="ExternalAccessible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="HmiVisible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="UserVisible" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserReadOnly" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserDeletable" Informative="true" SystemDefined="true">true</BooleanAttribute></AttributeList><Comment Informative="true"><MultiLanguageText Lang="en-US">Semiautomatic mode/ignore transition enabled</MultiLanguageText></Comment><StartValue Informative="true">false</StartValue></Member>
    <Member Name="MAN_ON" Datatype="Bool" Remanence="NonRetain" Accessibility="Public"><AttributeList><BooleanAttribute Name="ExternalAccessible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="HmiVisible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="UserVisible" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserReadOnly" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserDeletable" Informative="true" SystemDefined="true">true</BooleanAttribute></AttributeList><Comment Informative="true"><MultiLanguageText Lang="en-US">Manual mode is active</MultiLanguageText></Comment><StartValue Informative="true">false</StartValue></Member>
  </Section>
  <Section Name="InOut" />
  <Section Name="Static">
    <Member Name="RT_DATA" Datatype="G7_RTDataPlus_V2" Version="1.0" Remanence="NonRetain" Accessibility="Public"><AttributeList><BooleanAttribute Name="ExternalAccessible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="HmiVisible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="UserVisible" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserReadOnly" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserDeletable" Informative="true" SystemDefined="true">false</BooleanAttribute></AttributeList><Comment Informative="true"><MultiLanguageText Lang="en-US">Internal data area</MultiLanguageText></Comment><Sections><Section Name="None"><Member Name="S_DISPLAY" Datatype="Int" /><Member Name="S_SEL_OLD" Datatype="Int" /><Member Name="S_DISPIDX" Datatype="USInt"><StartValue Informative="true">255</StartValue></Member><Member Name="T_DISPIDX" Datatype="USInt"><StartValue Informative="true">255</StartValue></Member><Member Name="MOP_EDGE" Datatype="G7_MOPPlus_V2" Version="1.0"><Sections><Section Name="None"><Member Name="AUTO" Datatype="Bool" /><Member Name="MAN" Datatype="Bool" /><Member Name="TAP" Datatype="Bool" /><Member Name="TOP" Datatype="Bool" /><Member Name="ACK_S" Datatype="Bool" /><Member Name="REG_S" Datatype="Bool" /><Member Name="T_PREV" Datatype="Bool" /><Member Name="T_NEXT" Datatype="Bool" /><Member Name="LOCK" Datatype="Bool" /><Member Name="SUP" Datatype="Bool" /><Member Name="ACKREQ" Datatype="Bool" /><Member Name="SSKIP" Datatype="Bool" /><Member Name="OFF" Datatype="Bool" /><Member Name="INIT" Datatype="Bool" /><Member Name="HALT" Datatype="Bool" /><Member Name="TMS_HALT" Datatype="Bool" /><Member Name="OPS_ZERO" Datatype="Bool" /><Member Name="SACT_DISP" Datatype="Bool" /><Member Name="SEF_DISP" Datatype="Bool" /><Member Name="SALL_DISP" Datatype="Bool" /><Member Name="S_PREV" Datatype="Bool" /><Member Name="S_NEXT" Datatype="Bool" /><Member Name="S_SELOK" Datatype="Bool" /><Member Name="S_ON" Datatype="Bool" /><Member Name="S_OFF" Datatype="Bool" /><Member Name="T_PUSH" Datatype="Bool" /><Member Name="REG" Datatype="Bool" /><Member Name="ACK" Datatype="Bool" /><Member Name="IL_PERM" Datatype="Bool" /><Member Name="T_PERM" Datatype="Bool" /><Member Name="ILP_MAN" Datatype="Bool" /><Member Name="LMODE" Datatype="Bool" /></Section></Sections></Member><Member Name="MOP" Datatype="G7_MOPPlus_V2" Version="1.0"><Sections><Section Name="None"><Member Name="AUTO" Datatype="Bool"><StartValue Informative="true">true</StartValue></Member><Member Name="MAN" Datatype="Bool" /><Member Name="TAP" Datatype="Bool" /><Member Name="TOP" Datatype="Bool" /><Member Name="ACK_S" Datatype="Bool" /><Member Name="REG_S" Datatype="Bool" /><Member Name="T_PREV" Datatype="Bool" /><Member Name="T_NEXT" Datatype="Bool" /><Member Name="LOCK" Datatype="Bool"><StartValue Informative="true">true</StartValue></Member><Member Name="SUP" Datatype="Bool"><StartValue Informative="true">true</StartValue></Member><Member Name="ACKREQ" Datatype="Bool"><StartValue Informative="true">true</StartValue></Member><Member Name="SSKIP" Datatype="Bool" /><Member Name="OFF" Datatype="Bool" /><Member Name="INIT" Datatype="Bool"><StartValue Informative="true">true</StartValue></Member><Member Name="HALT" Datatype="Bool" /><Member Name="TMS_HALT" Datatype="Bool" /><Member Name="OPS_ZERO" Datatype="Bool" /><Member Name="SACT_DISP" Datatype="Bool"><StartValue Informative="true">true</StartValue></Member><Member Name="SEF_DISP" Datatype="Bool" /><Member Name="SALL_DISP" Datatype="Bool" /><Member Name="S_PREV" Datatype="Bool" /><Member Name="S_NEXT" Datatype="Bool" /><Member Name="S_SELOK" Datatype="Bool" /><Member Name="S_ON" Datatype="Bool" /><Member Name="S_OFF" Datatype="Bool" /><Member Name="T_PUSH" Datatype="Bool" /><Member Name="REG" Datatype="Bool" /><Member Name="ACK" Datatype="Bool" /><Member Name="IL_PERM" Datatype="Bool" /><Member Name="T_PERM" Datatype="Bool" /><Member Name="ILP_MAN" Datatype="Bool" /><Member Name="LMODE" Datatype="Bool" /></Section></Sections></Member><Member Name="TIME_DELTA" Datatype="Time" /><Member Name="SQ_FLAGS" Datatype="G7_SQFlagsPlus_V2" Version="1.0"><Sections><Section Name="None"><Member Name="ERR_FLT" Datatype="Bool" /><Member Name="ERROR" Datatype="Bool" /><Member Name="FAULT" Datatype="Bool" /><Member Name="RT_FAIL" Datatype="Bool" /><Member Name="NO_SNO" Datatype="Bool" /><Member Name="NF_OFL" Datatype="Bool" /><Member Name="SA_OFL" Datatype="Bool" /><Member Name="TV_OFL" Datatype="Bool" /><Member Name="MSG_OFL" Datatype="Bool" /><Member Name="NO_SWI" Datatype="Bool" /><Member Name="CYC_OP" Datatype="Bool" /><Member Name="AS_MSG" Datatype="Bool" /><Member Name="AS_SEND" Datatype="Bool" /><Member Name="SQ_BUSY" Datatype="Bool" /><Member Name="SA_BUSY" Datatype="Bool" /><Member Name="AS_SIG" Datatype="Bool" /></Section></Sections></Member><Member Name="PRE_CNT" Datatype="USInt"><StartValue Informative="true">1</StartValue></Member><Member Name="POST_CNT" Datatype="USInt"><StartValue Informative="true">1</StartValue></Member><Member Name="SQ_CNT" Datatype="USInt"><StartValue Informative="true">1</StartValue></Member><Member Name="S_CNT" Datatype="USInt"><StartValue Informative="true">$len(current_grid.steps)$</StartValue></Member><Member Name="LOCK_CNT" Datatype="USInt" /><Member Name="SUP_CNT" Datatype="USInt" /><Member Name="T_CNT" Datatype="USInt"><StartValue Informative="true">$len(current_grid.trans)$</StartValue></Member><Member Name="SQ_PART_CNT" Datatype="USInt"><StartValue Informative="true">$len(current_grid.altbranches)$</StartValue></Member><Member Name="MAX_TVAL" Datatype="USInt"><StartValue Informative="true">$len(current_grid.trans)-1$</StartValue></Member><Member Name="MAX_SACT" Datatype="USInt"><StartValue Informative="true">1</StartValue></Member><Member Name="AS_MSG" Datatype="Byte"><StartValue Informative="true">16#F</StartValue></Member><Member Name="EXEC_BITS" Datatype="Array[0..249] of Bool" /><Member Name="OFFSETS" Datatype="G7_OffsetsPlus_V2" Version="1.0"><Sections><Section Name="None"><Member Name="SINI_OFFSET" Datatype="UInt" /><Member Name="LSTT_OFFSET" Datatype="UInt"><StartValue Informative="true">0</StartValue></Member><Member Name="ATAJ_OFFSET" Datatype="UInt"><StartValue Informative="true">0</StartValue></Member><Member Name="ATAB_OFFSET" Datatype="UInt"><StartValue Informative="true">0</StartValue></Member><Member Name="PSTT_OFFSET" Datatype="UInt"><StartValue Informative="true">0</StartValue></Member><Member Name="NSTT_OFFSET" Datatype="UInt"><StartValue Informative="true">0</StartValue></Member><Member Name="ASSJ_OFFSET" Datatype="UInt"><StartValue Informative="true">0</StartValue></Member><Member Name="ASSB_OFFSET" Datatype="UInt"><StartValue Informative="true">0</StartValue></Member><Member Name="PTTS_OFFSET" Datatype="UInt"><StartValue Informative="true">0</StartValue></Member><Member Name="NTTS_OFFSET" Datatype="UInt"><StartValue Informative="true">0</StartValue></Member><Member Name="SW_SQTS_OFFSET" Datatype="UInt"><StartValue Informative="true">0</StartValue></Member><Member Name="SWITCH_OFFSET" Datatype="UInt"><StartValue Informative="true">0</StartValue></Member><Member Name="TVX_OFFSET" Datatype="UInt"><StartValue Informative="true">0</StartValue></Member><Member Name="TTX_OFFSET" Datatype="UInt"><StartValue Informative="true">0</StartValue></Member><Member Name="TSX_OFFSET" Datatype="UInt"><StartValue Informative="true">0</StartValue></Member><Member Name="S00X_OFFSET" Datatype="UInt"><StartValue Informative="true">0</StartValue></Member><Member Name="SOFFX_OFFSET" Datatype="UInt"><StartValue Informative="true">0</StartValue></Member><Member Name="SONX_OFFSET" Datatype="UInt"><StartValue Informative="true">0</StartValue></Member><Member Name="SAX_OFFSET" Datatype="UInt"><StartValue Informative="true">0</StartValue></Member><Member Name="SERRX_OFFSET" Datatype="UInt"><StartValue Informative="true">0</StartValue></Member><Member Name="SMX_OFFSET" Datatype="UInt"><StartValue Informative="true">0</StartValue></Member><Member Name="S0X_OFFSET" Datatype="UInt"><StartValue Informative="true">0</StartValue></Member><Member Name="S1X_OFFSET" Datatype="UInt"><StartValue Informative="true">0</StartValue></Member></Section></Sections></Member><Member Name="THRESHOLD_SUP" Datatype="USInt" /><Member Name="THRESHOLD_WARN" Datatype="USInt" /></Section></Sections></Member>\n'''

                for trans_idx, trans in enumerate(current_grid.trans, 1):
                    variables += '    <Member Name="Trans$trans_idx$" Datatype="G7_TransitionPlus_V2" Version="1.0" Remanence="NonRetain" Accessibility="Public"><AttributeList><BooleanAttribute Name="ExternalAccessible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="HmiVisible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="UserVisible" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserReadOnly" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserDeletable" Informative="true" SystemDefined="true">false</BooleanAttribute></AttributeList><Comment Informative="true"><MultiLanguageText Lang="en-US">Transition structure</MultiLanguageText></Comment><Sections><Section Name="None"><Member Name="TV" Datatype="Bool" /><Member Name="TT" Datatype="Bool" /><Member Name="TS" Datatype="Bool" /><Member Name="TNO" Datatype="Int"><StartValue Informative="true">$trans_idx$</StartValue></Member></Section></Sections></Member>\n'

                for step in current_grid.steps:
                    variables += '    <Member Name="$step.name$" Datatype="G7_StepPlus_V2" Version="1.0" Remanence="NonRetain" Accessibility="Public"><AttributeList><BooleanAttribute Name="ExternalAccessible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="HmiVisible" SystemDefined="true">false</BooleanAttribute><BooleanAttribute Name="UserVisible" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserReadOnly" Informative="true" SystemDefined="true">true</BooleanAttribute><BooleanAttribute Name="UserDeletable" Informative="true" SystemDefined="true">false</BooleanAttribute></AttributeList><Comment Informative="true"><MultiLanguageText Lang="en-US">Step structure</MultiLanguageText></Comment><Sections><Section Name="None"><Member Name="S1" Datatype="Bool" /><Member Name="L1" Datatype="Bool" /><Member Name="V1" Datatype="Bool" /><Member Name="R1" Datatype="Bool" /><Member Name="A1" Datatype="Bool" /><Member Name="S0" Datatype="Bool" /><Member Name="L0" Datatype="Bool" /><Member Name="V0" Datatype="Bool" /><Member Name="X" Datatype="Bool" /><Member Name="LA" Datatype="Bool" /><Member Name="VA" Datatype="Bool" /><Member Name="RA" Datatype="Bool" /><Member Name="AA" Datatype="Bool" /><Member Name="SS" Datatype="Bool" /><Member Name="LS" Datatype="Bool" /><Member Name="VS" Datatype="Bool" /><Member Name="SNO" Datatype="Int"><StartValue Informative="true">$step.WS_step$</StartValue></Member><Member Name="T" Datatype="Time" /><Member Name="U" Datatype="Time" /><Member Name="T_MAX" Datatype="Time"><StartValue Informative="true">T#10S</StartValue></Member><Member Name="T_WARN" Datatype="Time"><StartValue Informative="true">T#7S</StartValue></Member><Member Name="SM" Datatype="Bool" /><Member Name="H_IL_ERR" Datatype="Byte" /><Member Name="H_SV_FLT" Datatype="Byte" /></Section></Sections></Member>\n'

                variables += '''  </Section>
  <Section Name="Temp" />
  <Section Name="Constant" />
</Sections></Interface>
      <InterfaceModifiedDate ReadOnly="true">2018-03-19T16:02:45.8581846Z</InterfaceModifiedDate>
      <IsConsistent ReadOnly="true">true</IsConsistent>
      <IsIECCheckEnabled>false</IsIECCheckEnabled>
      <IsKnowHowProtected ReadOnly="true">false</IsKnowHowProtected>
      <ISMultiInstanceCapable ReadOnly="true">false</ISMultiInstanceCapable>
      <IsWriteProtected ReadOnly="true">false</IsWriteProtected>
      <LanguageInNetworks>LAD</LanguageInNetworks>
      <LibraryConformanceStatus ReadOnly="true">The object is library-conformant.</LibraryConformanceStatus>
      <LockOperatingMode>false</LockOperatingMode>
      <MemoryLayout>Optimized</MemoryLayout>
      <ModifiedDate ReadOnly="true">2018-03-19T16:02:46.4592447Z</ModifiedDate>
      <Name>$current_graph.FB_name$</Name>
      <ParameterModified ReadOnly="true">2018-03-19T16:02:03.7349727Z</ParameterModified>
      <PermanentILProcessingInMANMode>false</PermanentILProcessingInMANMode>
      <PLCSimAdvancedSupport ReadOnly="true">false</PLCSimAdvancedSupport>
      <ProgrammingLanguage>GRAPH</ProgrammingLanguage>
      <SetENOAutomatically>false</SetENOAutomatically>
      <SkipSteps>false</SkipSteps>
      <StructureModified ReadOnly="true">2018-03-19T16:02:45.8581846Z</StructureModified>
      <UDABlockProperties />
      <UDAEnableTagReadback>false</UDAEnableTagReadback>
      <WithAlarmHandling>true</WithAlarmHandling>
    </AttributeList>\n'''


                step_txt = ''

                for step in current_grid.steps:
                    if step.name != current_graph.init_step:
                        step_txt += '''      <Step Number="$step.WS_step$" Init="false" Name="$step.name$" MaximumStepTime="t#10s" WarningTime="t#7s">
        <Actions>
          <Action />
        </Actions>
        <Supervisions>
          <Supervision ProgrammingLanguage="LAD">
            <FlgNet>
              <Parts>
                <Part Name="SvCoil" UId="21" />
              </Parts>
              <Wires>
                <Wire UId="22">
                  <Powerrail />
                  <NameCon UId="21" Name="in" />
                </Wire>
              </Wires>
            </FlgNet>
          </Supervision>
        </Supervisions>
        <Interlocks>
          <Interlock ProgrammingLanguage="LAD">
            <FlgNet>
              <Parts>
                <Part Name="IlCoil" UId="21" />
              </Parts>
              <Wires>
                <Wire UId="22">
                  <Powerrail />
                  <NameCon UId="21" Name="in" />
                </Wire>
              </Wires>
            </FlgNet>
          </Interlock>
        </Interlocks>
      </Step>\n'''
                    else:
                        init_step_num = step.WS_step
                step_txt = '''    <ObjectList>
      <MultilingualText ID="1" CompositionName="Comment">
        <ObjectList>
          <MultilingualTextItem ID="2" CompositionName="Items">
            <AttributeList>
              <Culture>en-US</Culture>
              <Text />
            </AttributeList>
          </MultilingualTextItem>
        </ObjectList>
      </MultilingualText>
      <SW.Blocks.CompileUnit ID="3" CompositionName="CompileUnits">
        <AttributeList>
          <NetworkSource><Graph xmlns="http://www.siemens.com/automation/Openness/SW/NetworkSource/Graph/v2">
  <PreOperations>
    <PermanentOperation ProgrammingLanguage="LAD" />
  </PreOperations>
  <Sequence>
    <Title />
    <Comment>
      <MultiLanguageText Lang="en-US" />
    </Comment>
    <Steps>
      <Step Number="$init_step_num$" Init="true" Name="$current_graph.init_step$" MaximumStepTime="t#10s" WarningTime="t#7s">
        <Actions>
          <Action />
        </Actions>
        <Supervisions>
          <Supervision ProgrammingLanguage="LAD">
            <FlgNet>
              <Parts>
                <Part Name="SvCoil" UId="21" />
              </Parts>
              <Wires>
                <Wire UId="22">
                  <Powerrail />
                  <NameCon UId="21" Name="in" />
                </Wire>
              </Wires>
            </FlgNet>
          </Supervision>
        </Supervisions>
        <Interlocks>
          <Interlock ProgrammingLanguage="LAD">
            <FlgNet>
              <Parts>
                <Part Name="IlCoil" UId="21" />
              </Parts>
              <Wires>
                <Wire UId="22">
                  <Powerrail />
                  <NameCon UId="21" Name="in" />
                </Wire>
              </Wires>
            </FlgNet>
          </Interlock>
        </Interlocks>
      </Step>\n''' + step_txt + '    </Steps>\n'

                transition_txt = '    <Transitions>\n'
                connections_txt = '    <Connections>\n'

                for trans_idx, trans in enumerate(current_grid.trans, 1):
                    transition_txt += '''      <Transition IsMissing="false" Name="Trans$trans_idx$" Number="$trans_idx$" ProgrammingLanguage="LAD">
        <FlgNet>
          <Parts>
            <Access Scope="LocalVariable" UId="21">
              <Symbol>
                <Component Name="$trans.name$" />
              </Symbol>
            </Access>
            <Part Name="Contact" UId="22" />
            <Part Name="TrCoil" UId="23" />
          </Parts>
          <Wires>
            <Wire UId="24">
              <Powerrail />
              <NameCon UId="22" Name="in" />
            </Wire>
            <Wire UId="25">
              <IdentCon UId="21" />
              <NameCon UId="22" Name="operand" />
            </Wire>
            <Wire UId="26">
              <NameCon UId="22" Name="out" />
              <NameCon UId="23" Name="in" />
            </Wire>
          </Wires>
        </FlgNet>
      </Transition>\n'''
                    if not trans.altbranch: # Only one transition
                        connections_txt += '''      <Connection>
        <NodeFrom>
          <StepRef Number="$trans.or_step.WS_step$" />
        </NodeFrom>
        <NodeTo>
          <TransitionRef Number="$trans_idx$" />
        </NodeTo>
        <LinkType>Direct</LinkType>
      </Connection>\n'''
                    else:
                        if trans.altbranch_idx == 1: # New alternative branch
                            connections_txt += '''      <Connection>
        <NodeFrom>
          <StepRef Number="$trans.or_step.WS_step$" />
        </NodeFrom>
        <NodeTo>
          <BranchRef Number="$trans.altbranch.group$" In="0" />
        </NodeTo>
        <LinkType>Direct</LinkType>
      </Connection>\n'''
                        connections_txt += '''      <Connection>
        <NodeFrom>
          <BranchRef Number="$trans.altbranch.group$" Out="$trans.altbranch_idx - 1$" />
        </NodeFrom>
        <NodeTo>
          <TransitionRef Number="$trans_idx$" />
        </NodeTo>
        <LinkType>Direct</LinkType>
      </Connection>\n'''
                    jump_txt = 'Direct' if trans.conn_type == 'n' else 'Jump'
                    connections_txt += '''      <Connection>
        <NodeFrom>
          <TransitionRef Number="$trans_idx$" />
        </NodeFrom>
        <NodeTo>
          <StepRef Number="$trans.dest_step.WS_step$" />
        </NodeTo>
        <LinkType>$jump_txt$</LinkType>
      </Connection>\n'''

                alt_branch_txt = '    <Branches>\n'

                for alt_branch in current_grid.altbranches:
                    alt_branch_txt += '      <Branch Number="$alt_branch.group$" Type="AltBegin" Cardinality="$alt_branch.length$" />\n'
                transition_txt += '    </Transitions>\n'
                alt_branch_txt += '    </Branches>\n'
                connections_txt += '    </Connections>\n'


                footer = '''  </Sequence>
  <PostOperations>
    <PermanentOperation ProgrammingLanguage="LAD" />
  </PostOperations>
  <AlarmsSettings>
    <AlarmSupervisionCategories>
      <AlarmSupervisionCategory Id="1" DisplayClass="0" />
      <AlarmSupervisionCategory Id="2" DisplayClass="0" />
      <AlarmSupervisionCategory Id="3" DisplayClass="0" />
      <AlarmSupervisionCategory Id="4" DisplayClass="0" />
      <AlarmSupervisionCategory Id="5" DisplayClass="0" />
      <AlarmSupervisionCategory Id="6" DisplayClass="0" />
      <AlarmSupervisionCategory Id="7" DisplayClass="0" />
      <AlarmSupervisionCategory Id="8" DisplayClass="0" />
    </AlarmSupervisionCategories>
    <AlarmInterlockCategory Id="1" />
    <AlarmWarningCategory Id="2" />
  </AlarmsSettings>
</Graph></NetworkSource>
          <ProgrammingLanguage>GRAPH</ProgrammingLanguage>
        </AttributeList>
        <ObjectList>
          <MultilingualText ID="4" CompositionName="Comment">
            <ObjectList>
              <MultilingualTextItem ID="5" CompositionName="Items">
                <AttributeList>
                  <Culture>en-US</Culture>
                  <Text />
                </AttributeList>
              </MultilingualTextItem>
            </ObjectList>
          </MultilingualText>
          <MultilingualText ID="6" CompositionName="Title">
            <ObjectList>
              <MultilingualTextItem ID="7" CompositionName="Items">
                <AttributeList>
                  <Culture>en-US</Culture>
                  <Text />
                </AttributeList>
              </MultilingualTextItem>
            </ObjectList>
          </MultilingualText>
        </ObjectList>
      </SW.Blocks.CompileUnit>
      <MultilingualText ID="8" CompositionName="Title">
        <ObjectList>
          <MultilingualTextItem ID="9" CompositionName="Items">
            <AttributeList>
              <Culture>en-US</Culture>
              <Text />
            </AttributeList>
          </MultilingualTextItem>
        </ObjectList>
      </MultilingualText>
    </ObjectList>
  </SW.Blocks.FB>
</Document>'''

                # Write text file
                return "%s.xml" % current_graph.out_file_name, header + variables + step_txt + transition_txt + alt_branch_txt + connections_txt + footer

            else:
                self.thePlugin.writeErrorInUABLog("Platform not supported")

    def write_graph_file(self, PCO_name, folder_name=""):
        file_name, content = self.get_graph_file(PCO_name)
        if self.siemens_PLC or self.TIA_PLC:
            self.thePlugin.writeFile(folder_name + file_name, content)
        elif self.unity_PLC:
            self.thePlugin.writeProgram(content[0])
            self.thePlugin.writeVariable(content[1])
        else:
            self.thePlugin.writeErrorInUABLog("Platform not supported")

    def get_DB_instance(self, PCO_name):
        self.thePlugin.writeInUABLog("Getting instance DB file")
        current_graph_name = self.get_graph_tab_name(PCO_name)
        if current_graph_name:
            current_graph = self.graphs[current_graph_name]
            if self.TIA_PLC:
                return '''DATA_BLOCK "%s"
{ S7_Optimized_Access := 'TRUE' }
"%s"
BEGIN
END_DATA_BLOCK\n\n''' % (current_graph.graph_instances[PCO_name].DB_name, current_graph.FB_name)
            else:
                self.thePlugin.writeErrorInUABLog("Platform not supported")

    def write_DB_instances(self, folder_name=""):
        if self.TIA_PLC:
            instances_DB = ""
            for PCO_name in self.get_PCO_list():
                instances_DB += self.get_DB_instance(PCO_name)
            self.thePlugin.writeFile(folder_name + "instances_DB.scl", instances_DB.replace("\n", "\r\n"))
        
    def get_graph_call(self, PCO_name):
        self.thePlugin.writeInUABLog("Getting graph call for PCO \"%s\"" % PCO_name)
        current_graph_name = self.get_graph_tab_name(PCO_name)
        if current_graph_name:
            current_graph = self.graphs[current_graph_name]
            if self.siemens_PLC:
                return current_graph.FB_name + "." + current_graph.graph_instances[PCO_name].DB_name + "();\n"
            elif self.TIA_PLC:
                return "\"" + current_graph.graph_instances[PCO_name].DB_name + "\"();\n"
            else:
                self.thePlugin.writeErrorInUABLog("Platform not supported")
                return ""

    def write_graph_call(self, PCO_name):
        if self.siemens_PLC:
            self.thePlugin.writeSiemensLogic("\n    " + self.get_graph_call(PCO_name))
        elif self.TIA_PLC:
            self.thePlugin.writeTIALogic("\n    " + self.get_graph_call(PCO_name))
        else:
            self.thePlugin.writeErrorInUABLog("Platform not supported")

    def get_transition_conditions(self, PCO_name):
        self.thePlugin.writeInUABLog("Getting transition conditions for PCO \"%s\"" % PCO_name)
        current_graph_name = self.get_graph_tab_name(PCO_name)
        if current_graph_name:
            current_graph = self.graphs[current_graph_name]
            current_grid = current_graph.grid_instance
            conditions_txt = "(* TRANSITIONS COMPUTATION *)\n\n"
            for trans in current_grid.trans:
                if 'WS_trans' in dir(trans):
                    transition_numbers = "(* %s TO %s *)" % (trans.or_step.WS_step, trans.dest_step.WS_step)
                    if self.siemens_PLC or self.TIA_PLC:
                        if not trans.condition:
                            trans.condition = "0; // To complete"
                        conditions_txt += '''$transition_numbers$ $current_graph.graph_instances[PCO_name].DB_name$.$trans.name$ := $trans.condition$\n'''
                    elif self.unity_PLC:
                        if not trans.condition:
                            trans.condition = "0; (* To complete *)"
                        conditions_txt += '''$transition_numbers$ $trans.name$ := $trans.condition$\n'''
                    else:
                        self.thePlugin.writeErrorInUABLog("Platform not supported")
            return conditions_txt

    def write_transition_conditions(self, PCO_name):
        if self.siemens_PLC:
            self.thePlugin.writeSiemensLogic('\n\n')
            self.thePlugin.writeSiemensLogic(self.get_transition_conditions(PCO_name))
        elif self.TIA_PLC:
            self.thePlugin.writeTIALogic('\n\n')
            self.thePlugin.writeTIALogic(self.get_transition_conditions(PCO_name))
        elif self.unity_PLC:
            self.thePlugin.writeProgram('\n')
            self.thePlugin.writeProgram(self.get_transition_conditions(PCO_name))
        else:
            self.thePlugin.writeErrorInUABLog("Platform not supported")

    def get_word_status_steps(self, PCO_name):
        self.thePlugin.writeInUABLog("Getting steps word status for PCO \"%s\"" % PCO_name)
        current_graph_name = self.get_graph_tab_name(PCO_name)
        if current_graph_name:
            current_graph = self.graphs[current_graph_name]
            current_grid = current_graph.grid_instance
            WS_step = current_graph.graph_instances[PCO_name].WS_step
            if WS_step:
                word_status_step_txt = '(* WORD STATUS FOR STEPS *)\n'
                if self.siemens_PLC:
                    word_status_step_txt += 'DB_WS_ALL.WS_SET.%s.AuPosR := INT_TO_WORD(%s.S_NO);\n' % (WS_step, current_graph.graph_instances[PCO_name].DB_name)
                elif self.TIA_PLC:
                    word_status_step_txt += '%s.AuPosR := INT_TO_WORD(%s.S_NO);\n' % (WS_step, current_graph.graph_instances[PCO_name].DB_name)
                elif self.unity_PLC:
                    word_status_step_txt += '''IF %s.X THEN
    %s_AuPosR := %s;
''' % (current_grid.steps[0].name, WS_step, current_grid.steps[0].WS_step)
                    for step in current_graph.grid_instance.steps[1:]:
                        word_status_step_txt += '''ELSIF %s.X THEN
    %s_AuPosR := %s;
''' % (step.name, WS_step, step.WS_step)
                    word_status_step_txt += 'END_IF;\n'
                else:
                    self.thePlugin.writeErrorInUABLog("Platform not supported")
                return word_status_step_txt
            else:
                return None

    def write_word_status_steps(self, PCO_name):
        if self.siemens_PLC:
            self.thePlugin.writeSiemensLogic('\n\n')
            self.thePlugin.writeSiemensLogic(self.get_word_status_steps(PCO_name))
        elif self.TIA_PLC:
            self.thePlugin.writeTIALogic('\n\n')
            self.thePlugin.writeTIALogic(self.get_word_status_steps(PCO_name))
        elif self.unity_PLC:
            self.thePlugin.writeProgram('\n')
            self.thePlugin.writeProgram(self.get_word_status_steps(PCO_name))
        else:
            self.thePlugin.writeErrorInUABLog('Platform not supported')

    def get_word_status_transitions(self, PCO_name):
        self.thePlugin.writeInUABLog("Getting transitions word status for PCO \"%s\"" % PCO_name)
        current_graph_name = self.get_graph_tab_name(PCO_name)
        if current_graph_name:
            current_graph = self.graphs[current_graph_name]
            current_grid = current_graph.grid_instance
            WS_trans = current_graph.graph_instances[PCO_name].WS_trans
            if WS_trans:
                trans_cond_txt = ''
                word_status_trans_txt = '(* WORD STATUS FOR TRANSITIONS *)\n'
                word_status_trans_assignement = ''
                conditions = [trans for trans in current_grid.trans if 'WS_trans' in dir(trans)]
                if self.siemens_PLC:
                    for trans in conditions:
                        if not trans.WS_trans%16:
                            trans_cond_txt += '''    pco_transitions%i : WORD;
    pco_transitions%i_bit AT pco_transitions%i: ARRAY [0..15] OF BOOL;
''' % (trans.WS_trans/16, trans.WS_trans/16, trans.WS_trans/16)
                            word_status_trans_assignement += 'DB_WS_ALL.WS_SET.%s.AuPosR := pco_transitions%i;\n' % (WS_trans.replace('#', str(trans.WS_trans / 16)), trans.WS_trans / 16)
                        word_status_trans_txt += 'pco_transitions%i_bit%-5s:= %-80s (*%2i*)\n' % (trans.WS_trans/16, '[%i]' % ((trans.WS_trans+8)%16), '%s.%s;'% (current_graph.graph_instances[PCO_name].DB_name, trans.name), trans.WS_trans%16)
                    if len(conditions)%16:
                        for remaining_WS_idx in range(len(conditions)%16, 16):
                            word_status_trans_txt += 'pco_transitions%i_bit%-5s:= %-80s (*%2i*)\n' % (len(conditions)/16, '[%i]' % ((remaining_WS_idx+8)%16), 'false;', remaining_WS_idx%16)
                    word_status_trans_txt += '\n\n' + word_status_trans_assignement + '\n'
                    return word_status_trans_txt, trans_cond_txt
                elif self.TIA_PLC:
                    for trans in conditions:
                        if not trans.WS_trans%16:
                            trans_cond_txt += '''    pco_transitions%i : WORD;
''' % (trans.WS_trans / 16)
                            word_status_trans_assignement += '%s.AuPosR := pco_transitions%i;\n' % (WS_trans.replace('#', str(trans.WS_trans / 16)), trans.WS_trans / 16)
                        word_status_trans_txt += 'pco_transitions%i.%%X%-3s:= %s.%s;\n' % (trans.WS_trans / 16, '%i' % (trans.WS_trans % 16), current_graph.graph_instances[PCO_name].DB_name, trans.name)
                    if len(conditions)%16:
                        for remaining_WS_idx in range(len(conditions)%16, 16):
                            word_status_trans_txt += 'pco_transitions%i.%%X%-3s:= false;\n' % (trans.WS_trans / 16, '%i' % (remaining_WS_idx%16))
                    word_status_trans_txt += '\n\n' + word_status_trans_assignement + '\n'
                    return word_status_trans_txt, trans_cond_txt
                elif self.unity_PLC:
                    for trans in conditions:
                        if not trans.WS_trans%16:
                            word_status_trans_txt += '''
%s_AuPosR := BIT_TO_WORD (
''' % (WS_trans.replace('#', str(trans.WS_trans/16)))
                        word_status_trans_txt += '            BIT%s := %s' % (str(trans.WS_trans%16), trans.name)
                        if not (trans.WS_trans+1)%16:
                            word_status_trans_txt += '\n);\n'
                        elif trans != conditions[-1]:
                            word_status_trans_txt += ',\n'
                    if len(conditions)%16:
                        word_status_trans_txt += '\n);\n'
                    return word_status_trans_txt
                else:
                    self.thePlugin.writeErrorInUABLog("Platform not supported")
            else:
                return None

    def write_word_status_transitions_variables(self, PCO_name):
        if self.siemens_PLC:
            self.thePlugin.writeSiemensLogic(self.get_word_status_transitions(PCO_name)[1])
        elif self.TIA_PLC:
            self.thePlugin.writeTIALogic(self.get_word_status_transitions(PCO_name)[1])
        else:
            self.thePlugin.writeErrorInUABLog("Platform not supported")

    def write_word_status_transitions(self, PCO_name):
        if self.siemens_PLC:
            self.thePlugin.writeSiemensLogic('\n\n')
            self.thePlugin.writeSiemensLogic(self.get_word_status_transitions(PCO_name)[0])
        elif self.TIA_PLC:
            self.thePlugin.writeTIALogic('\n\n')
            self.thePlugin.writeTIALogic(self.get_word_status_transitions(PCO_name)[0])
        elif self.unity_PLC:
            self.thePlugin.writeProgram('\n')
            self.thePlugin.writeProgram(self.get_word_status_transitions(PCO_name))
        else:
            self.thePlugin.writeErrorInUABLog('Platform not supported')

    def get_template_variables(self, PCO_name):
        self.thePlugin.writeInUABLog("Getting template variables for PCO \"%s\"" % PCO_name)
        current_graph_name = self.get_graph_tab_name(PCO_name)
        if current_graph_name:
            current_graph = self.graphs[current_graph_name]
            return "    # Automatically generated variables\n    " + "\n    ".join(current_graph.template_vars)
    
    def get_logic_parameters(self, PCO_name, theUnicosProject):
        self.thePlugin.writeInUABLog("Getting Logic Parameters for PCO \"%s\"" % PCO_name)
    
        ParamsParsed = ""
        PCOInst = theUnicosProject.findMatchingInstances("ProcessControlObject","'#DeviceIdentification:Name#'contains'$PCO_name$'")
        if len(PCOInst) > 0:
            for i in range(1,10):
                Params = PCOInst[0].getAttributeData("LogicDeviceDefinitions:CustomLogicParameters:Parameter" + str(i))
                if "=" in Params: #PCO has param features
                    ParamsParsed = ParamsParsed + "#Lparam " + str(i) + "features parsed\n    RequestedFeatures = ProcessFeatures.parseFeatures(Lparam" + str(i) + ")\n"
                    ParamsFeatures = [feature for feature in Params.replace(" ","").split(';') if feature != ""]
                    for feature in ParamsFeatures:
                        feature_title = feature.split('=')[0].lower()
                        self.thePlugin.writeDebugInUABLog("feature: " + feature + " . feature_title: " + feature_title)
                        self.thePlugin.writeDebugInUABLog("Parsing feature $feature_title$ of Param" + str(i))
                        for j,feature_item in enumerate(feature.split('=')[1].split(',')):
                            ParamsParsed = ParamsParsed + "    Lparam" + str(i) + "_" + feature_title + "_" + str(j) + " = RequestedFeatures[\x27"+feature_title+"\x27]["+str(j)+"]\n"
        return ParamsParsed

    def write_template_variables(self, PCO_name):
        if self.siemens_PLC:
            self.thePlugin.writeSiemensLogic('\n\n')
            self.thePlugin.writeSiemensLogic(self.get_template_variables(PCO_name))
        elif self.TIA_PLC:
            self.thePlugin.writeTIALogic('\n\n')
            self.thePlugin.writeTIALogic(self.get_template_variables(PCO_name))
        elif self.unity_PLC:
            self.thePlugin.writeProgram('\n')
            self.thePlugin.writeProgram(self.get_template_variables(PCO_name))
        else:
            self.thePlugin.writeErrorInUABLog('Platform not supported')

    def get_code_variables(self, PCO_name):
        self.thePlugin.writeInUABLog("Getting code variables for PCO \"%s\"" % PCO_name)
        current_graph_name = self.get_graph_tab_name(PCO_name)
        if current_graph_name:
            current_graph = self.graphs[current_graph_name]
            return "(* VARIABLES COMPUTATION *)\n\n" + "\n".join(current_graph.code_vars)

    def write_code_variables(self, PCO_name):
        if self.siemens_PLC:
            self.thePlugin.writeSiemensLogic('\n\n')
            self.thePlugin.writeSiemensLogic(self.get_code_variables(PCO_name))
        elif self.TIA_PLC:
            self.thePlugin.writeTIALogic('\n\n')
            self.thePlugin.writeTIALogic(self.get_code_variables(PCO_name))
        elif self.unity_PLC:
            self.thePlugin.writeProgram('\n')
            self.thePlugin.writeProgram(self.get_code_variables(PCO_name))
        else:
            self.thePlugin.writeErrorInUABLog('Platform not supported')

    def get_code_variables_declaration(self, PCO_name):
        self.thePlugin.writeInUABLog("Getting code variables declaration for PCO \"%s\"" % PCO_name)
        current_graph_name = self.get_graph_tab_name(PCO_name)
        if current_graph_name:
            current_graph = self.graphs[current_graph_name]
            if self.siemens_PLC or self.TIA_PLC:
                return "\n".join(["    %s: BOOL;" % current_var for current_var in current_graph.code_vars_declaration])
            elif self.unity_PLC:
                return "\n".join(["""<variables name="%s" typeName="BOOL"></variables>""" % current_var for current_var in current_graph.code_vars_declaration])
            else:
                self.thePlugin.writeErrorInUABLog('Platform not supported')

    def write_code_variables_declaration(self, PCO_name):
        if self.siemens_PLC:
            self.thePlugin.writeSiemensLogic('\n\n')
            self.thePlugin.writeSiemensLogic(self.get_code_variables_declaration(PCO_name))
        elif self.TIA_PLC:
            self.thePlugin.writeTIALogic('\n\n')
            self.thePlugin.writeTIALogic(self.get_code_variables_declaration(PCO_name))
        elif self.unity_PLC:
            self.thePlugin.writeProgram('\n')
            self.thePlugin.writeProgram(self.get_code_variables_declaration(PCO_name))
        else:
            self.thePlugin.writeErrorInUABLog('Platform not supported')

    def get_WS_pattern(self, PCO_name):
        self.thePlugin.writeInUABLog("Getting word status pattern for PCO \"%s\"" % PCO_name)
        current_graph_name = self.get_graph_tab_name(PCO_name)
        if current_graph_name:
            current_graph = self.graphs[current_graph_name]
            current_grid = current_graph.grid_instance
            return "(*" + ",".join([str(current_step.WS_step) + "=" + str(current_step.WS_step) + "-" + current_step.WS_pattern for current_step in current_grid.steps if current_step.WS_pattern]) + "*)"

    def write_WS_pattern(self, PCO_name):
        if self.siemens_PLC:
            self.thePlugin.writeSiemensLogic('\n\n')
            self.thePlugin.writeSiemensLogic(self.get_WS_pattern(PCO_name))
        elif self.TIA_PLC:
            self.thePlugin.writeTIALogic('\n\n')
            self.thePlugin.writeTIALogic(self.get_WS_pattern(PCO_name))
        elif self.unity_PLC:
            self.thePlugin.writeProgram('\n')
            self.thePlugin.writeProgram(self.get_WS_pattern(PCO_name))
        else:
            self.thePlugin.writeErrorInUABLog('Platform not supported')
