== Description ==
In this folder the user can add specific semantic check rules and the
generator will execute them automatically.

1.	Copy and paste the UserSpecificSemanticCheckRulesExample.py
2.	Rename it.
3.	Complete it with the required code.

Remark: Do not change the class name (leave it as class
UserSpecific_Template(IUnicosTemplate)) and do not remove any of the
existing functions (initialize, check, begin, etc). However you can
modify the contents of each function, of course to implement your
desired semantic rules.


