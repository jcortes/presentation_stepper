# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
from research.ch.cern.unicos.templateshandling import IUnicosTemplate  # REQUIRED
from research.ch.cern.unicos.utilities import SemanticVerifier
from research.ch.cern.unicos.plugins.interfaces import APlugin


class UserSpecific_Template(IUnicosTemplate):
    theSemanticVerifier = 0
    thePlugin = 0
    isDataValid = 1

    def initialize(self):
        self.theSemanticVerifier = SemanticVerifier.getUtilityInterface()
        self.thePlugin = APlugin.getPluginInterface()
        self.thePlugin.writeInUABLog("User Specific Check Rules: initialize")

    def check(self):
        self.thePlugin.writeInUABLog("User Specific Check Rules: check")

    def begin(self):
        self.thePlugin.writeInUABLog("User Specific Check Rules: begin")

    def process(self, *params):
        theUnicosProject = params[0]
        self.thePlugin.writeInUABLog("CHECKING USER SPECIFIC RULES FILE")

    def end(self):
        self.thePlugin.writeInUABLog("User Specific Check Rules: end")

    def shutdown(self):
        self.thePlugin.writeInUABLog("User Specific Check Rules: shutdown")
