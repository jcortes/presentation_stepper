# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
from Semantic_Generic_Template import Semantic_Generic_Template

class Local_Template(Semantic_Generic_Template):

    def process(self, *params):
        current_device_type_name = params[0]
        current_device_type_definition = params[1]
        current_device_type = self.unicos_project.getDeviceType(current_device_type_name)
        self.plugin.writeInUABLog("process in Jython in %s." % self.__class__.__name__)

        name_length_limit = self.getMaxNameLength(str(current_device_type_name), self.the_manufacturer)

        for instance in current_device_type.getAllDeviceTypeInstances():

            # Check the length of the name
            self.checkNameLength(instance, name_length_limit)

            # Check the FEDevice inputs
            self.checkObjectsInAttributes(instance, ["FEDeviceEnvironmentInputs:Feedback On", 
                                                     "FEDeviceEnvironmentInputs:Feedback Off"], valid_types=["DigitalInput"])
