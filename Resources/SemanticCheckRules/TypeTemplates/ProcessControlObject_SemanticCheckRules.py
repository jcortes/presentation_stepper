# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
from Semantic_Generic_Template import Semantic_Generic_Template

class ProcessControlObject_Template(Semantic_Generic_Template):

    def process(self, *params):
        current_device_type_name = params[0]
        current_device_type_definition = params[1]
        current_device_type = self.unicos_project.getDeviceType(current_device_type_name)
        self.plugin.writeInUABLog("process in Jython in %s." % self.__class__.__name__)

        name_length_limit = self.getMaxNameLength(str(current_device_type_name), self.the_manufacturer)

        option_mode_pattern = re.compile(ur"\Z|[01]{8}\Z")  # matches empty string or 8-char string composed of 0 and 1

        for instance in current_device_type.getAllDeviceTypeInstances():

            # Check the length of the name
            self.checkNameLength(instance, name_length_limit)

            # Check the display name
            display_name = instance.getAttributeData("SCADADeviceGraphics:Display Name").strip()
            if len(display_name) > 8 :
                self.writeSemanticWarning(instance, "The Display Name (" + display_name + ") is longer than 8 characters and might be incorrectly displayed in SCADA.")

            # Check the option mode allowance tables
            for i in range(1,9):
                option_mode = instance.getAttributeData("FEDeviceParameters:Option Mode Allowance Table:Option Mode " + str(i) + " Allowance").strip()
                if not option_mode_pattern.match(option_mode):
                    self.writeSemanticError(instance, "The Option Mode " + str(i) + " Allowance Table is not correct: " + option_mode)

            # Check the option mode labels
            for i in range (1,9):
                self.checkDependentAttibutes(instance, ["SCADADeviceFunctionals:Mode Label:Option Mode " + str(i) + " Label"], ["FEDeviceParameters:Option Mode Allowance Table:Option Mode " + str(i) + " Allowance"])
            
            # Check if the master name is not the PCO's name itself
            if instance.getAttributeData("DeviceIdentification:Name").upper() == instance.getAttributeData("LogicDeviceDefinitions:Master").upper():
                self.writeSemanticError(instance, "The Master name of a 'No Master' PCO should be left blank, not set equal to device Name itself.")
