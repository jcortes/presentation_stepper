# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
from Semantic_Generic_Template import Semantic_Generic_Template

class SteppingMotor_Template(Semantic_Generic_Template):

    def process(self, *params):
        current_device_type_name = params[0]
        current_device_type_definition = params[1]
        current_device_type = self.unicos_project.getDeviceType(current_device_type_name)
        self.plugin.writeInUABLog("process in Jython in %s." % self.__class__.__name__)

        name_length_limit = self.getMaxNameLength(str(current_device_type_name), self.the_manufacturer)

        for instance in current_device_type.getAllDeviceTypeInstances():

            # Check the length of the name
            self.checkNameLength(instance, name_length_limit)

            # Check the FEDevice inputs
            self.checkObjectsInAttributes(instance, ["FEDeviceEnvironmentInputs:Feedback Analog"], valid_types=["AnalogInput", "AnalogInputReal", "AnalogStatus", "Encoder"])
                                                     
            self.checkObjectsInAttributes(instance, ["FEDeviceEnvironmentInputs:ClockWise Limit",
                                                     "FEDeviceEnvironmentInputs:CounterClockWise Limit"], valid_types=["DigitalInput"])

            self.checkDeadband(instance, "SCADADeviceDataArchiving:Deadband Value")
            self.checkDeadband(instance, "SCADADriverDataSmoothing:Deadband Value")

            # Check the SteppingMotor specific configuration
            switches = instance.getAttributeData("FEDeviceParameters:ParReg:Switches Configuration").strip()
            clockwise_limit = instance.getAttributeData("FEDeviceEnvironmentInputs:ClockWise Limit").strip()
            counter_clockwise_limit = instance.getAttributeData("FEDeviceEnvironmentInputs:CounterClockWise Limit").strip()
            if switches == "2 End Switches + Ref. Switch" or switches == "2 End Switches with faked Ref. Switch":
                if not clockwise_limit or not counter_clockwise_limit:
                    self.writeSemanticError(instance, "The Switches configuration is defined as " + switches + " and one of them is not defined.")
            else:
                if clockwise_limit or counter_clockwise_limit:
                    self.writeSemanticError(instance, "The Switches configuration is defined as " + switches + " and there are end switches defined.")

            feedback = instance.getAttributeData("FEDeviceParameters:ParReg:Feedback").strip()
            feedback_analog = instance.getAttributeData("FEDeviceEnvironmentInputs:Feedback Analog").strip()

            self.checkDependentAttibutes(instance, ["FEDeviceEnvironmentInputs:Feedback Analog"], ["FEDeviceParameters:ParReg:Feedback"])
            self.checkDependentAttibutes(instance, ["FEDeviceParameters:ParReg:Feedback"], ["FEDeviceEnvironmentInputs:Feedback Analog"])

            # Check the FEDevice outputs
            self.checkObjectsInAttributes(instance, ["FEDeviceOutputs:ClockWise to Reference Switch", 
                                                     "FEDeviceOutputs:Simulated Reference Switch",
                                                     "FEDeviceOutputs:Driver Enable"], valid_types=["DigitalOutput"])

            cw_ref = instance.getAttributeData("FEDeviceOutputs:ClockWise to Reference Switch").strip()
            sim_ref = instance.getAttributeData("FEDeviceOutputs:Simulated Reference Switch").strip()
            
            self.checkFormat(instance)

            if feedback == "Encoder":
                driver_enable = instance.getAttributeData("FEDeviceOutputs:Driver Enable").strip()
                if not driver_enable:
                    self.writeSemanticError(instance, "There is no Driver Enable defined. Is mandatory when the Feedback is provided by an encoder.")
                if not sim_ref:
                    self.writeSemanticError(instance, "There is no Simulated Reference Switch defined. Is mandatory when the Feedback is provided by an encoder.")

            if switches == "2 End Switches with faked Ref. Switch":
                if not cw_ref or not sim_ref:
                    self.writeSemanticError(instance, "The Simulated Reference Switch or ClockWise to Reference Switch is not defined. They are mandatory when the system is configured with faked Ref. Switch.")
            elif switches in ["2 End Switches + Ref. Switch", "2 Switches plugged into the 1STEP"]:
                if cw_ref or sim_ref:
                    self.writeSemanticError(instance, "There are digital outputs defined for simulate the reference Switch but they are not needed with this Switch configuration.")
                    
            self.checkArchiveConfig(instance)

            # Check the FE Encoding Type
            fe_type = instance.getAttributeData("FEDeviceIOConfig:FE Encoding Type").strip()
            if fe_type not in ["", "0", "1"]:
                self.writeSemanticError(instance, "The FE Encoding Type defined " + fe_type + " is not allowed.")
            elif (fe_type == "1"):
                self.checkInterfaceParameter(instance, fe_type, "FEDeviceIOConfig:FEChannel:InterfaceParam1", "pid[0-9]+", "a DOUBLE WORD (PIDxxx where xxx is a number).")
                self.checkInterfaceParameter(instance, fe_type, "FEDeviceIOConfig:FEChannel:InterfaceParam5", "pqd[0-9]+", "a DOUBLE WORD (PQDxxx where xxx is a number).")
