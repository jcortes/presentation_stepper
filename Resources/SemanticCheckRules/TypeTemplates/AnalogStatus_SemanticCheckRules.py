# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
from Semantic_Generic_Template import Semantic_Generic_Template

class AnalogStatus_Template(Semantic_Generic_Template):

    def process(self, *params):
        current_device_type_name = params[0]
        current_device_type_definition = params[1]
        current_device_type = self.unicos_project.getDeviceType(current_device_type_name)
        self.plugin.writeInUABLog("process in Jython in %s." % self.__class__.__name__)

        name_length_limit = self.getMaxNameLength(str(current_device_type_name), self.the_manufacturer)

        for instance in current_device_type.getAllDeviceTypeInstances():

            # Check the length of the name
            self.checkNameLength(instance, name_length_limit)

            # Check the ranges
            self.checkRanges(instance)

            # Check the alarm configuration
            self.checkDependentAttibutes(instance, ["SCADADeviceAlarms:Alarm Config:Auto Acknowledge", "SCADADeviceAlarms:Alarm Config:Masked"], ["SCADADeviceAlarms:Analog Thresholds:HH Alarm", "SCADADeviceAlarms:Analog Thresholds:LL Alarm"])
            self.checkDependentAttibutes(instance, ["SCADADeviceAlarms:Alarm Config:SMS Category", "SCADADeviceAlarms:Message"], ["SCADADeviceAlarms:Analog Thresholds:HH Alarm", "SCADADeviceAlarms:Analog Thresholds:H Warning", "SCADADeviceAlarms:Analog Thresholds:L Warning", "SCADADeviceAlarms:Analog Thresholds:LL Alarm"], severity="warning")

            self.check5RangeAlertThresholds(instance)

            self.checkDeadband(instance, "SCADADeviceDataArchiving:Deadband Value")
            self.checkDeadband(instance, "SCADADriverDataSmoothing:Deadband Value")
            
            self.checkArchiveConfig(instance)
            
            self.checkFormat(instance)

            # Check the FE Encoding Type
            if (self.the_manufacturer.lower() == "siemens"):
                fe_type = instance.getAttributeData("FEDeviceIOConfig:FE Encoding Type").strip()
                if fe_type not in ["", "0", "1", "100", "101"]:
                    self.writeSemanticError(instance, "The FE Encoding Type defined " + fe_type + " is not allowed.")

                elif (fe_type == "1"):
                    self.checkInterfaceParameter(instance, fe_type, "FEDeviceIOConfig:FEChannel:InterfaceParam1", "p?id[0-9]+", "a DOUBLE WORD (PIDxxx or IDxxx where xxx is a number).")

                elif (fe_type == "101"):
                    if self.siemens_plc_declarations:
                        self.checkInterfaceParameter(instance, fe_type, "FEDeviceIOConfig:FEChannel:InterfaceParam1", "(db)?[0-9]+", "DBxx, where xx is a number.")
                        self.checkInterfaceParameter(instance, fe_type, "FEDeviceIOConfig:FEChannel:InterfaceParam2", "(dbd)?[0-9]+", "DBDxx, where xx is a number.")
                    else: # TIA Portal
                        self.checkInterfaceParameter(instance, fe_type, "FEDeviceIOConfig:FEChannel:InterfaceParam1", "[a-zA-Z0-9_]*\.[a-zA-Z0-9_]*", "DB_name.variable_name.")

                elif (fe_type == "100"):
                    self.checkInterfaceParameter(instance, fe_type, "FEDeviceIOConfig:FEChannel:InterfaceParam1", "md[0-9]+", "a DOUBLE WORD (MDxxx where xxx is a number).")
