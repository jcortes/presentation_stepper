# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
from java.util import Vector
from java.util import ArrayList


def getPrivileges(DeviceTypeName):

    Privileges = ""

    # To complete if you want to specify specific privileges per device type in your application
    # keywords correspond to panels buttons defined in :  \CPC\Baseline\WinCCOA\unCPC\panels\vision\cpcObjects\xxx.pnl
    # key names are defined in : C:\PVSS_projects\xxx\installed_components\scripts\libs\unicos_declarations.ctl
    # Example of privileges per device type:

    # if DeviceTypeName == "DigitalInput" or DeviceTypeName == "DigitalOutput":
    # OperatorPrivileges= "Select,Auto,AckAlarm,OnOpen,OffClose"
    # ExpertPrivileges = "MaskAlarm,Forced,UnmaskAlarm,AlarmLimits"
    # AdminPrivileges = "Mask Event"
    # elif DeviceTypeName == "AnalogInput" or DeviceTypeName == "AnalogOutput" or DeviceTypeName == "AnalogInputReal" or DeviceTypeName == "AnalogOutputReal":
    # OperatorPrivileges= "Select,Auto,AckAlarm,SetValue"
    # ExpertPrivileges = "MaskAlarm,Forced,UnmaskAlarm,AlarmLimits"
    # AdminPrivileges = "Mask Event"
    # elif DeviceTypeName == "Local":
    # OperatorPrivileges= "AckAlarm"
    # ExpertPrivileges = ""
    # AdminPrivileges = ""
    # elif DeviceTypeName == "OnOff":
    # OperatorPrivileges= "Select,Auto,Manual,AckAlarm,OnOpen,OffClose,AllowRestart"
    # ExpertPrivileges = "Forced"
    # AdminPrivileges = "Mask Event"
    # elif DeviceTypeName == "Analog" or DeviceTypeName == "AnalogDigital" or DeviceTypeName == "AnaDO":
    # OperatorPrivileges= "Select,Auto,Manual,AckAlarm,SetValue,AllowRestart,Increase,Decrease,OnOpen,OffClose"
    # ExpertPrivileges = "StatusLimits,Forced"
    # AdminPrivileges = "Mask Event"
    # elif DeviceTypeName == "Controller":
    # OperatorPrivileges= "Select,Auto,Manual,Output,Regulation,OutputPositioning,PIDParam,Setpoint,PIDLimits"
    # ExpertPrivileges = "SaveValue,Forced,AutoTune"
    # AdminPrivileges = "Mask Event"
    # elif DeviceTypeName == "ProcessControlObject":
    # OperatorPrivileges= "Select,Auto,Manual,AckAlarm,AllowRestart,OnOpen,OffClose,ControlledStop,OptionMode,AutoToDep"
    # ExpertPrivileges = "Forced,BlockAlarm,DeblockAlarm"
    # AdminPrivileges = "Mask Event,SetTempAsFull,ResetTempAsFull"
    # elif DeviceTypeName == "DigitalAlarm":
    # OperatorPrivileges= "Select,AckAlarm"
    # ExpertPrivileges = "MaskAlarm,UnmaskAlarm,BlockAlarm,DeblockAlarm,SetDAMailSMS"
    # AdminPrivileges = "Mask Event"
    # elif DeviceTypeName == "AnalogAlarm":
    # OperatorPrivileges= "Select,AckAlarm,SetAALimits"
    # ExpertPrivileges = "MaskAlarm,UnmaskAlarm,BlockAlarm,DeblockAlarm"
    # AdminPrivileges = "Mask Event"
    # elif DeviceTypeName == "DigitalParameter":
    # OperatorPrivileges= "Select,DigitalParameter_SetValue"
    # ExpertPrivileges = "SaveValue"
    # AdminPrivileges = ""
    # elif DeviceTypeName == "WordParameter" or DeviceTypeName == "AnalogParameter":
    # OperatorPrivileges= "Select,Parameter_SetValue"
    # ExpertPrivileges = "SaveValue"
    # AdminPrivileges = ""
    # elif DeviceTypeName == "WordStatus" or DeviceTypeName == "AnalogStatus":
    # OperatorPrivileges= "Select,AckAlarm"
    # ExpertPrivileges = "MaskAlarm,UnmaskAlarm,AlarmLimits"
    # AdminPrivileges = ""
    # elif DeviceTypeName == "MassFlowController":
    # OperatorPrivileges= "Select,Auto,Manual,AckAlarm,AllowRestart,MFCSetParameters"
    # ExpertPrivileges = "Forced"
    # AdminPrivileges = ""
    # else:
    # OperatorPrivileges= ""
    # ExpertPrivileges = ""
    # AdminPrivileges = ""

    # Privileges = OperatorPrivileges + "|" + ExpertPrivileges + "|"  + AdminPrivileges + "|"

    return Privileges
