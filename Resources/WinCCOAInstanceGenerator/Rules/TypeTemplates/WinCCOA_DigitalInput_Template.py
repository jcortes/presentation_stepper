# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for DigitalInput Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin  # REQUIRED
from research.ch.cern.unicos.cpc.interfaces import ISCADAPlugin  # REQUIRED
from research.ch.cern.unicos.core import CoreManager  # REQUIRED
from java.lang import System
import WinCCOA_CommonMethods
from research.ch.cern.unicos.plugins.interfaces import AGenerationPlugin
import ucpc_library.shared_decorator
reload(ucpc_library.shared_decorator)
try:
    # Try to import the user privileges file
    import WinCCOA_Privileges
except:
    # If the previous import failed, try to import the privileges template file
    import WinCCOA_Privileges_Template


class DigitalInput_Template(IUnicosTemplate):
    thePlugin = 0
    theDeviceType = "DigitalInput"
    # Default name for the privileges file
    privilegesFileName = "WinCCOA_Privileges"

    def deviceFormat(self):
        return ["deviceType", "deviceNumber", "aliasDeviceLinkList", "description", "diagnostics", "wwwLink", "synoptic", "domain", "nature", "widgetType",
                "archiveMode", "smsCategory", "alarmMessage", "alarmAck", "normalPosition", "addressStsReg01", "addressEvStsReg01", "addressManReg01", "booleanArchive",
                "analogArchive", "eventArchive", "maskEvent", "parameters", "master", "parents", "children", "type", "secondAlias"]

    def initialize(self):
        self.thePlugin = APlugin.getPluginInterface()
        self.thePlugin.writeInUABLog("initialize in Jython $self.theDeviceType$ template")
        reload(WinCCOA_CommonMethods)
        self.decorator = ucpc_library.shared_decorator.ExpressionDecorator()
        try:
            # Try to reload the user privileges file
            reload(WinCCOA_Privileges)
        except:
            # If the reload failed, reload the privileges template file
            self.privilegesFileName = "WinCCOA_Privileges_Template"
            reload(WinCCOA_Privileges_Template)

    def check(self):
        self.thePlugin.writeInUABLog("check in Jython $self.theDeviceType$ template")

    def begin(self):
        self.thePlugin.writeInUABLog("begin in Jython $self.theDeviceType$ template")

    def process(self, *params):
        self.thePlugin.writeInUABLog("processInstances in Jython $self.theDeviceType$ template")
        theCurrentDeviceType = params[0]
        self.theUnicosProject = theRawInstances = self.thePlugin.getUnicosProject()
        strAllDeviceTypes = theRawInstances.getAllDeviceTypesString()
        theApplicationName = self.thePlugin.getApplicationParameter("ApplicationName")
        instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
        instancesNumber = str(len(instancesVector))
        DeviceTypeName = theCurrentDeviceType.getDeviceTypeName()
        deviceNumber = 0
        CRLF = System.getProperty("line.separator")

        # Get the config (UnicosApplication.xml)
        config = CoreManager.getITechnicalParameters().getXMLConfigMapper()
        # Query a PLC parameter
        PLCType = config.getPLCDeclarations().get(0).getPLCType().getValue()

        self.thePlugin.writeDBHeader("#$self.theDeviceType$: $instancesNumber$")
        DeviceTypeFormat = "CPC_" + DeviceTypeName + ";deviceNumber;Alias[,DeviceLinkList];Description - ElectricalDiagram;Diagnostics;WWWLink;Synoptic;Domain;Nature;WidgetType;ArchiveMode;SMSCat;AlarmMessage;AlarmAck;NormalPosition;addr_StsReg01;addr_EvStsReg01;addr_ManReg01;BooleanArchive;AnalogArchive;EventArchive;MaskEvent;Parameters;Master;Parents;children;Type;SecondAlias;"
        self.thePlugin.writeComment("#$CRLF$# Object: " + DeviceTypeName + "$CRLF$#$CRLF$#Config Line : " + DeviceTypeFormat + "$CRLF$#")

        # Set Privileges
        Privileges = eval(self.privilegesFileName).getPrivileges(DeviceTypeName)

        for instance in instancesVector:
            if (instance is not None):

                # Check if user pressed 'cancel' button:
                if AGenerationPlugin.isGenerationInterrupted():
                    return

                deviceNumber = int(int(deviceNumber) + 1)
                deviceNumber = str(deviceNumber)

                # 1. Common Unicos fields
                # 2. Specific fields
                # 3. SCADA Device Data Archiving
                # 4. Data Treatment
                # 5. Addresses computation
                # 6. write the instance information in the database file

                # 1. Common Unicos fields
                name = Name = Alias = instance.getAttributeData("DeviceIdentification:Name")
                expertName = instance.getAttributeData("DeviceIdentification:Expert Name")
                Description = instance.getAttributeData("DeviceDocumentation:Description")
                Diagnostics = instance.getAttributeData("SCADADeviceGraphics:Diagnostic")
                WWWLink = instance.getAttributeData("SCADADeviceGraphics:WWW Link")
                Synoptic = instance.getAttributeData("SCADADeviceGraphics:Synoptic")
                WidgetType = instance.getAttributeData("SCADADeviceGraphics:Widget Type")
                Domain = instance.getAttributeData("SCADADeviceFunctionals:SCADADeviceClassificationTags:Domain")
                AccessControlDomain = instance.getAttributeData("SCADADeviceFunctionals:Access Control Domain").replace(" ", "")
                Nature = instance.getAttributeData("SCADADeviceFunctionals:SCADADeviceClassificationTags:Nature")
                DeviceLinkListSpec = instance.getAttributeData("SCADADeviceFunctionals:SCADADeviceClassificationTags:Device Links").replace(" ", "")

                # 2. Specific fields
                ElectricalDiagram = instance.getAttributeData("DeviceDocumentation:Electrical Diagram")
                SMSCat = instance.getAttributeData("SCADADeviceAlarms:Alarm Config:SMS Category")
                AlarmAck = instance.getAttributeData("SCADADeviceAlarms:Alarm Config:Auto Acknowledge")
                alarmMasked = instance.getAttributeData("SCADADeviceAlarms:Alarm Config:Masked").lower()
                alarmOnState = instance.getAttributeData("SCADADeviceAlarms:Binary State:Alarm On State").lower()
                AlarmMessage = instance.getAttributeData("SCADADeviceAlarms:Message")
                MaskEvent = instance.getAttributeData("SCADADeviceFunctionals:Mask Event")

                # 3. SCADA Device Data Archiving
                ArchiveMode = instance.getAttributeData("SCADADeviceDataArchiving:Archive Mode")
                BooleanArchive = instance.getAttributeData("SCADADeviceDataArchiving:Boolean Archive")
                AnalogArchive = instance.getAttributeData("SCADADeviceDataArchiving:Analog Archive")
                EventArchive = instance.getAttributeData("SCADADeviceDataArchiving:Event Archive")

                # 4. Data Treatment
                S_MaskEvent = MaskEvent.lower()
                S_AlarmAck = AlarmAck.lower()

                S_Domain = Domain.replace(" ", "")
                S_Nature = Nature.replace(" ", "")
                S_SMSCat = SMSCat.replace(" ", "")

                # Build DeviceLinkList and children from related objects
                DeviceLinkList = ""

                # Append Device Link list from Spec
                DeviceLinkList = WinCCOA_CommonMethods.appendDeviceLinkListFromSpec(DeviceLinkListSpec, DeviceLinkList, self.thePlugin, strAllDeviceTypes)

                # Avoid duplications on the deviceLinks
                DeviceLinkList = WinCCOA_CommonMethods.removeDuplicatesAndGivenObjects(DeviceLinkList, [Name, expertName])

                # Default values if domain or nature empty
                if S_Domain == "":
                    Domain = theApplicationName

                if AccessControlDomain != "":
                    AccessControlDomain = AccessControlDomain + "|"

                if S_Nature == "":
                    Nature = "DI"

                # Mask Value
                if S_MaskEvent == "true":
                    MaskEvent = "0"
                else:
                    MaskEvent = "1"

                # AlarmMasked
                if alarmMasked == "":
                    alarmMasked = "false"

                # AlarmAck
                AlarmAck = WinCCOA_CommonMethods.getPvssAlarmAckDigital(S_AlarmAck, alarmOnState)

                # Alarm Message
                if S_SMSCat != "" and AlarmMessage == "":
                    AlarmMessage = "No message"
                    self.thePlugin.writeWarningInUABLog("$self.theDeviceType$ instance: $Alias$. An Alarm message is required: forced it to ($AlarmMessage$).")

                # Boolean Normal Position
                if alarmOnState == "":
                    normalPosition = "2"
                elif alarmOnState == "true" and alarmMasked == "false":
                    normalPosition = "0"
                elif alarmOnState == "false" and alarmMasked == "false":
                    normalPosition = "1"
                elif alarmOnState == "true" and alarmMasked == "true":
                    normalPosition = "3"
                elif alarmOnState == "false" and alarmMasked == "true":
                    normalPosition = "4"
                else:
                    normalPosition = "2"

                # Archive Mode
                if (ArchiveMode.lower() == "no"):
                    ArchiveMode = str("N")
                elif (ArchiveMode.lower() == "old/new comparison"):
                    ArchiveMode = str("Y")
                else:
                    ArchiveMode = str("N")

                # 5. Addresses computation
                addr_StsReg01 = self.getAddressSCADA("$Alias$_StsReg01", PLCType)
                addr_EvStsReg01 = self.getAddressSCADA("$Alias$_EvStsReg01", PLCType)
                addr_ManReg01 = self.getAddressSCADA("$Alias$_ManReg01", PLCType)

                # 6. New relationship information in all objects:
                parents = []
                parents += self.thePlugin.getRIndex().get(Name, "OnOff",
                                                          ["FEDeviceEnvironmentInputs:Feedback On",
                                                           "FEDeviceEnvironmentInputs:Feedback Off",
                                                           "FEDeviceEnvironmentInputs:Local Drive",
                                                           "FEDeviceEnvironmentInputs:Local On",
                                                           "FEDeviceEnvironmentInputs:Local Off"]).split(',')

                parents += self.thePlugin.getRIndex().get(Name, "Analog",
                                                          ["FEDeviceEnvironmentInputs:Feedback On",
                                                           "FEDeviceEnvironmentInputs:Feedback Off",
                                                           "FEDeviceEnvironmentInputs:Local Drive"]).split(',')

                parents += self.thePlugin.getRIndex().get(Name, "AnaDO",
                                                          ["FEDeviceEnvironmentInputs:Feedback On",
                                                           "FEDeviceEnvironmentInputs:Local Drive",
                                                           "FEDeviceEnvironmentInputs:Local On",
                                                           "FEDeviceEnvironmentInputs:Local Off"]).split(',')

                parents += self.thePlugin.getRIndex().get(Name, "AnalogDigital",
                                                          ["FEDeviceEnvironmentInputs:Feedback On",
                                                           "FEDeviceEnvironmentInputs:Feedback Off",
                                                           "FEDeviceEnvironmentInputs:Local Drive"]).split(',')

                parents += self.thePlugin.getRIndex().get(Name, "Local",
                                                          ["FEDeviceEnvironmentInputs:Feedback On",
                                                           "FEDeviceEnvironmentInputs:Feedback Off"]).split(',')

                parents += self.thePlugin.getRIndex().get(Name, "DigitalAlarm", "FEDeviceEnvironmentInputs:Input").split(',')

                parents += self.thePlugin.getRIndex().get(Name, "AnalogAlarm", "FEDeviceAlarm:Enable Condition").split(',')

                parents = [x for x in parents if x]  # remove empty strings

                uniqueList = []
                for parent in parents:
                    if parent not in uniqueList:
                        uniqueList.append(parent)
                parents = uniqueList
                parents = [WinCCOA_CommonMethods.getExpertName(self.theUnicosProject.findInstanceByName(parent)) for parent in parents]
                stringParents = ",".join(parents)
                type = instance.getAttributeData("FEDeviceIOConfig:FE Encoding Type")
                children = ""
                master = ""

                # Expert Name Logic
                S_Name = name.strip()
                S_ExpertName = expertName.strip()
                if S_ExpertName == "":
                    secondAlias = Alias
                else:
                    secondAlias = Alias
                    Alias = S_ExpertName

                # 7. Parameters field for all objects
                Parameters = ""

                # 8. write the instance information in the database file
                self.thePlugin.writeInstanceInfo("CPC_$DeviceTypeName$;$deviceNumber$;$Alias$$DeviceLinkList$;$Description$ - $ElectricalDiagram$;$Diagnostics$;$WWWLink$;$Synoptic$;$AccessControlDomain$$Domain$;$Privileges$$Nature$;$WidgetType$;$ArchiveMode$;$SMSCat$;$AlarmMessage$;$AlarmAck$;$normalPosition$;$addr_StsReg01$;$addr_EvStsReg01$;$addr_ManReg01$;$BooleanArchive$;$AnalogArchive$;$EventArchive$;$MaskEvent$;$Parameters$;$master$;$stringParents$;$children$;$type$;$secondAlias$;")

    def end(self):
        self.thePlugin.writeInUABLog("end in Jython $self.theDeviceType$ template")

    def shutdown(self):
        self.thePlugin.writeInUABLog("shutdown in Jython $self.theDeviceType$ template")

    def getAddressSCADA(self, DPName, PLCType):
        if PLCType.lower() == "quantum":
            address = str(int(self.thePlugin.computeAddress(DPName)) - 1)
        else:
            address = str(self.thePlugin.computeAddress(DPName))

        return address
