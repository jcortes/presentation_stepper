# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for AnalogAlarm Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin  # REQUIRED
from research.ch.cern.unicos.cpc.interfaces import ISCADAPlugin  # REQUIRED
from research.ch.cern.unicos.core import CoreManager  # REQUIRED
from java.lang import System
import WinCCOA_CommonMethods
from research.ch.cern.unicos.plugins.interfaces import AGenerationPlugin
from research.ch.cern.unicos.utilities import MasterDeviceUtils
import ucpc_library.shared_decorator
reload(ucpc_library.shared_decorator)
try:
    # Try to import the user privileges file
    import WinCCOA_Privileges
except:
    # If the previous import failed, try to import the privileges template file
    import WinCCOA_Privileges_Template


class AnalogAlarm_Template(IUnicosTemplate):
    thePlugin = 0
    theDeviceType = "AnalogAlarm"
    # Default name for the privileges file
    privilegesFileName = "WinCCOA_Privileges"

    def deviceFormat(self):
        return ["deviceType", "deviceNumber", "aliasDeviceLinkList", "description", "diagnostics", "wwwLink", "synoptic", "domain", "nature", "widgetType", "unit",
                "format", "archiveModeI", "archiveModeT", "timeFilterT", "smsCategory", "alarmMessage", "alarmAck", "level", "normalPosition", "addressStsReg01",
                "addressStsReg02", "addressEvStsReg01", "addressEvStsReg02", "addressPosSt", "addressHHSt", "addressHSt", "addressLSt", "addressLLSt", "addressManReg01",
                "addressHH", "addressH", "addressL", "addressLL", "booleanArchive", "analogArchive", "eventArchive", "maskEvent", "parameters", "master", "parents",
                "children", "type", "secondAlias"]

    def initialize(self):
        self.thePlugin = APlugin.getPluginInterface()
        self.thePlugin.writeInUABLog("initialize in Jython $self.theDeviceType$ template")
        reload(WinCCOA_CommonMethods)
        self.decorator = ucpc_library.shared_decorator.ExpressionDecorator()
        try:
            # Try to reload the user privileges file
            reload(WinCCOA_Privileges)
        except:
            # If the reload failed, reload the privileges template file
            self.privilegesFileName = "WinCCOA_Privileges_Template"
            reload(WinCCOA_Privileges_Template)

    def check(self):
        self.thePlugin.writeInUABLog("check in Jython $self.theDeviceType$ template")

    def begin(self):
        self.thePlugin.writeInUABLog("begin in Jython $self.theDeviceType$ template")

    def process(self, *params):
        self.thePlugin.writeInUABLog("processInstances in Jython $self.theDeviceType$ template")
        theCurrentDeviceType = params[0]
        self.theUnicosProject = theRawInstances = self.thePlugin.getUnicosProject()
        strAllDeviceTypes = theRawInstances.getAllDeviceTypesString()
        theApplicationName = self.thePlugin.getApplicationParameter("ApplicationName")
        instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
        instancesNumber = str(len(instancesVector))
        DeviceTypeName = theCurrentDeviceType.getDeviceTypeName()
        deviceNumber = 0
        CRLF = System.getProperty("line.separator")

        # Get the config (UnicosApplication.xml)
        config = CoreManager.getITechnicalParameters().getXMLConfigMapper()
        # Query a PLC parameter
        PLCType = config.getPLCDeclarations().get(0).getPLCType().getValue()
        theManufacturer = self.thePlugin.getPlcManufacturer()

        self.thePlugin.writeDBHeader("#$self.theDeviceType$: $instancesNumber$")
        DeviceTypeFormat = "CPC_AnalogAlarm;deviceNumber;Alias[,DeviceLinkList];Description;Diagnostics;WWWLink;Synoptic;Domain;Nature;WidgetType;Unit;Format;ArchiveModeI;ArchiveModeT;TimeFilterT;SMSCat;AlarmMessage;AlarmAck;Level;NormalPosition;addr_StsReg01;addr_StsReg02;addr_EvStsReg01;addr_EvStsReg02;addr_PosSt;addr_HHSt;addr_HSt;addr_LSt;addr_LLSt;addr_ManReg01;addr_HH;addr_H;addr_L;addr_LL;BooleanArchive;AnalogArchive;EventArchive;MaskEvent;Parameters;Master;Parents;children;Type;SecondAlias;"
        self.thePlugin.writeComment("#$CRLF$# Object: " + DeviceTypeName + "$CRLF$#$CRLF$#Config Line : " + DeviceTypeFormat + "$CRLF$#")

        # Set Privileges
        Privileges = eval(self.privilegesFileName).getPrivileges(DeviceTypeName)

        for instance in instancesVector:
            if (instance is not None):

                # Check if user pressed 'cancel' button:
                if AGenerationPlugin.isGenerationInterrupted():
                    return

                deviceNumber = int(int(deviceNumber) + 1)
                deviceNumber = str(deviceNumber)

                # 1. Common Unicos fields
                # 2. Specific fields
                # 3. SCADA Device Data Archiving
                # 4. Data Treatment
                # 5. Addresses computation
                # 6. New relationship information in all objects
                # 7. Parameters field
                # 8. write the instance information in the database file

                # 1. Common Unicos fields
                name = Name = Alias = instance.getAttributeData("DeviceIdentification:Name")
                expertName = instance.getAttributeData("DeviceIdentification:Expert Name")
                Description = instance.getAttributeData("DeviceDocumentation:Description")
                Diagnostics = instance.getAttributeData("SCADADeviceGraphics:Diagnostic")
                WWWLink = instance.getAttributeData("SCADADeviceGraphics:WWW Link")
                Synoptic = instance.getAttributeData("SCADADeviceGraphics:Synoptic")
                WidgetType = instance.getAttributeData("SCADADeviceGraphics:Widget Type")
                Domain = instance.getAttributeData("SCADADeviceFunctionals:SCADADeviceClassificationTags:Domain")
                AccessControlDomain = instance.getAttributeData("SCADADeviceFunctionals:Access Control Domain").replace(" ", "")
                Nature = instance.getAttributeData("SCADADeviceFunctionals:SCADADeviceClassificationTags:Nature")
                DeviceLinkListSpec = instance.getAttributeData("SCADADeviceFunctionals:SCADADeviceClassificationTags:Device Links").replace(" ", "")
                master = MasterDevice = instance.getAttributeData("LogicDeviceDefinitions:Master")

                # 2. Specific fields
                smsCat = instance.getAttributeData("SCADADeviceAlarms:Alarm Config:SMS Category").replace(" ", "")
                AlarmAck = instance.getAttributeData("FEDeviceAlarm:Auto Acknowledge")
                AlarmMasked = instance.getAttributeData("SCADADeviceAlarms:Alarm Config:Masked")
                AlarmMessage = instance.getAttributeData("SCADADeviceAlarms:Message")
                MaskEvent = instance.getAttributeData("SCADADeviceFunctionals:Mask Event")
                HH = instance.getAttributeData("FEDeviceManualRequests:HH Alarm").strip()
                LL = instance.getAttributeData("FEDeviceManualRequests:LL Alarm").strip()
                H = instance.getAttributeData("FEDeviceManualRequests:H Warning").strip()
                L = instance.getAttributeData("FEDeviceManualRequests:L Warning").strip()
                Input = I = instance.getAttributeData("FEDeviceEnvironmentInputs:Input").strip()
                Delay = instance.getAttributeData("FEDeviceParameters:Alarm Delay (s)").strip()
                EnableCondition = instance.getAttributeData("FEDeviceAlarm:Enable Condition")

                # 3. SCADA Device Data Archiving
                ArchiveModeI = instance.getAttributeData("SCADADeviceDataArchiving:Interlock Archiving:Archive Mode").lower()
                ArchiveModeT = "old/new comparison or time"  # instance.getAttributeData("SCADADeviceDataArchiving:Threshold Archiving:Archive Mode").lower()
                TimeFilterT = "10.0"  # self.thePlugin.formatNumberPLC(instance.getAttributeData("SCADADeviceDataArchiving:Threshold Archiving:Time Filter (s)"))
                BooleanArchive = instance.getAttributeData("SCADADeviceDataArchiving:Boolean Archive")
                AnalogArchive = instance.getAttributeData("SCADADeviceDataArchiving:Analog Archive")
                EventArchive = instance.getAttributeData("SCADADeviceDataArchiving:Event Archive")

                # default value
                Unit = ""
                Format = "###.###"

                # 4. Data Treatment
                S_HH = HH.lower().strip()
                S_LL = LL.lower().strip()
                S_L = L.lower().strip()
                S_H = H.lower().strip()
                S_I = I.lower().strip()
                S_Delay = Delay.lower().strip()

                S_ArchiveModeI = ArchiveModeI.lower()
                S_AlarmMasked = AlarmMasked.lower()

                S_MaskEvent = MaskEvent.lower()
                S_AlarmMasked = AlarmMasked.lower()
                S_AlarmAck = AlarmAck.lower()

                # Build DeviceLinkList and children from related objects
                DeviceLinkList = ""
                # MasterDevice
                if (MasterDevice != ""):
                    MasterList = []
                    MasterDevice = MasterDevice.strip().replace(",", " ")
                    MasterList = MasterDevice.split()
                    MasterListExpert = MasterList
                    i = 0
                    for Master in MasterList:
                        MasterDeviceWinCCOAName = self.thePlugin.getLinkedExpertName(Master, ", ".join(MasterDeviceUtils.getAllMasterTypes(config)) + ", OnOff, Analog, AnalogDigital, AnaDO, MassFlowController")
                        MasterListExpert[i] = (MasterDeviceWinCCOAName)
                        DeviceLinkList = self.thePlugin.appendLinkedDevice(DeviceLinkList, MasterDeviceWinCCOAName)
                        i = i + 1

                children = []
                # I : Extract Unit, Format
                I_Unit = ""
                I_Format = ""
                DeadbandTypeT = ""
                DeadbandValueT = ""

                if (S_I != ""):
                    DeviceLinkList, children = WinCCOA_CommonMethods.updateDeviceLinksAndChildren(Input, DeviceLinkList, children, self.thePlugin, self.decorator)
                    listOfInputObjects = self.decorator.getListOfUNICOSObjects(Input)

                    # use 1st linked instance that has unit specified
                    for inst in listOfInputObjects:
                        inst_I = theRawInstances.findInstanceByName(inst)
                        if inst_I is not None and inst_I.getDeviceType().doesSpecificationAttributeExist("SCADADeviceGraphics:Unit"):

                            I_Unit = WinCCOA_CommonMethods.getAttributeDataIfExist(inst_I, "SCADADeviceGraphics:Unit")
                            I_Format = WinCCOA_CommonMethods.getAttributeDataIfExist(inst_I, "SCADADeviceGraphics:Format")
                            ArchiveModeT = WinCCOA_CommonMethods.getAttributeDataIfExist(inst_I, "SCADADeviceDataArchiving:Archive Mode", ArchiveModeT).lower()
                            TimeFilterT = self.thePlugin.formatNumberPLC(WinCCOA_CommonMethods.getAttributeDataIfExist(inst_I, "SCADADeviceDataArchiving:Time Filter (s)", TimeFilterT))
                            DeadbandTypeT = WinCCOA_CommonMethods.getAttributeDataIfExist(inst_I, "SCADADeviceDataArchiving:Deadband Type").lower()
                            DeadbandValueT = self.thePlugin.formatNumberPLC(WinCCOA_CommonMethods.getAttributeDataIfExist(inst_I, "SCADADeviceDataArchiving:Deadband Value"))

                #HH, H, L, LL
                if self.thePlugin.isString(S_HH) and S_HH <> "logic":
                    if (S_HH != ""):
                        DeviceLinkList, children = WinCCOA_CommonMethods.updateDeviceLinksAndChildren(HH, DeviceLinkList, children, self.thePlugin, self.decorator)

                if self.thePlugin.isString(S_H) and S_H <> "logic":
                    if (S_H != ""):
                        DeviceLinkList, children = WinCCOA_CommonMethods.updateDeviceLinksAndChildren(H, DeviceLinkList, children, self.thePlugin, self.decorator)

                if self.thePlugin.isString(S_L) and S_L <> "logic":
                    if (S_L != ""):
                        DeviceLinkList, children = WinCCOA_CommonMethods.updateDeviceLinksAndChildren(L, DeviceLinkList, children, self.thePlugin, self.decorator)

                if self.thePlugin.isString(S_LL) and S_LL <> "logic":
                    if (S_LL != ""):
                        DeviceLinkList, children = WinCCOA_CommonMethods.updateDeviceLinksAndChildren(LL, DeviceLinkList, children, self.thePlugin, self.decorator)

                # Delay
                if self.thePlugin.isString(S_Delay) and S_Delay <> "logic":
                    if (S_Delay != ""):
                        DelayWinCCOAName = self.thePlugin.getLinkedExpertName(Delay, "AnalogParameter, AnalogStatus")
                        DeviceLinkList = self.thePlugin.appendLinkedDevice(DeviceLinkList, DelayWinCCOAName)
                        children.append(DelayWinCCOAName)

                # EnableCondition
                if self.thePlugin.isString(EnableCondition) and EnableCondition <> "logic":
                    if (EnableCondition != ""):
                        DeviceLinkList, children = WinCCOA_CommonMethods.updateDeviceLinksAndChildren(EnableCondition, DeviceLinkList, children, self.thePlugin, self.decorator)

                # Append Device Link list from Spec
                DeviceLinkList = WinCCOA_CommonMethods.appendDeviceLinkListFromSpec(DeviceLinkListSpec, DeviceLinkList, self.thePlugin, strAllDeviceTypes)

                # Avoid duplications on the deviceLinks
                DeviceLinkList = WinCCOA_CommonMethods.removeDuplicatesAndGivenObjects(DeviceLinkList, [Name, expertName])

                # Default values if domain or nature empty
                if Domain == "":
                    Domain = theApplicationName

                if AccessControlDomain != "":
                    AccessControlDomain = AccessControlDomain + "|"

                if Nature == "":
                    Nature = "AA"

                # Mask Value
                if S_MaskEvent == "true":
                    MaskEvent = "0"
                else:
                    MaskEvent = "1"

                # AlarmMasked
                if S_AlarmMasked == "":
                    S_AlarmMasked = "false"

                # AlarmAck
                if S_AlarmAck == "":
                    AlarmAck = "true"
                elif S_AlarmAck.strip() == "false":
                    AlarmAck = "true"
                elif S_AlarmAck.strip() == "true":
                    AlarmAck = "false"

                # Alarm Message
                if smsCat != "":
                    if AlarmMessage == "":
                        AlarmMessage = Description
                    if AlarmMessage == "":			# Alarm Message and Description field are empty.
                        AlarmMessage = "No message"
                        self.thePlugin.writeWarningInUABLog("$self.theDeviceType$ instance: $Alias$. An Alarm message or a description are required: forced it to ($AlarmMessage$).")

                # Selection of the Unit and format.

                if (I_Unit != ""):				# unit of instance connected to I is prioritarily
                    Unit = I_Unit

                if (I_Format != ""):				# Format of instance connected to I is prioritarily
                    Format = I_Format

                # Boolean Normal Position
                if S_AlarmMasked == "false":
                    normalPosition = "0"
                elif S_AlarmMasked == "true":
                    normalPosition = "3"
                else:
                    normalPosition = "0"
                    self.thePlugin.writeWarningInUABLog("$self.theDeviceType$ instance: $Alias$. AlarmMasked is in an unexpected configuration: force normalPosition to 0")

                # Interlock Archive Mode
                if (S_ArchiveModeI == "no"):
                    S_ArchiveModeI = str("N")
                elif (S_ArchiveModeI == "old/new comparison"):
                    S_ArchiveModeI = str("Y")
                else:
                    S_ArchiveModeI = str("N")

                # PosSt Archive Mode
                if (ArchiveModeT == "no"):
                    ArchiveModeT = str("N")
                elif (ArchiveModeT == "deadband" and DeadbandTypeT == "absolute"):
                    ArchiveModeT = str("VA,$DeadbandValueT$,N")
                elif (ArchiveModeT == "deadband" and DeadbandTypeT == "relative"):
                    ArchiveModeT = str("VR,$DeadbandValueT$,N")
                elif (ArchiveModeT == "time"):
                    ArchiveModeT = str("Y")
                elif (ArchiveModeT == "deadband and time" and DeadbandTypeT == "absolute"):
                    ArchiveModeT = str("VA,$DeadbandValueT$,A")
                elif (ArchiveModeT == "deadband and time" and DeadbandTypeT == "relative"):
                    ArchiveModeT = str("VR,$DeadbandValueT$,A")
                elif (ArchiveModeT == "deadband or time" and DeadbandTypeT == "absolute"):
                    ArchiveModeT = str("VA,$DeadbandValueT$,O")
                elif (ArchiveModeT == "deadband or time" and DeadbandTypeT == "relative"):
                    ArchiveModeT = str("VR,$DeadbandValueT$,O")
                elif (ArchiveModeT == "old/new comparison"):
                    ArchiveModeT = str("O")
                    TimeFilterT = "0"
                elif (ArchiveModeT == "old/new comparison and time"):
                    ArchiveModeT = str("A")
                elif (ArchiveModeT == "old/new comparison or time"):
                    ArchiveModeT = str("O")
                else:
                    ArchiveModeT = str("N")

                # 5. Addresses computation
                addr_StsReg01 = self.getAddressSCADA("$Alias$_StsReg01", PLCType)
                addr_EvStsReg01 = self.getAddressSCADA("$Alias$_EvStsReg01", PLCType)
                addr_StsReg02 = self.getAddressSCADA("$Alias$_StsReg02", PLCType)
                addr_EvStsReg02 = self.getAddressSCADA("$Alias$_EvStsReg02", PLCType)
                addr_PosSt = self.getAddressSCADA("$Alias$_PosSt", PLCType)
                addr_HHSt = self.getAddressSCADA("$Alias$_HHSt", PLCType)
                addr_HSt = self.getAddressSCADA("$Alias$_HSt", PLCType)
                addr_LSt = self.getAddressSCADA("$Alias$_LSt", PLCType)
                addr_LLSt = self.getAddressSCADA("$Alias$_LLSt", PLCType)
                addr_ManReg01 = self.getAddressSCADA("$Alias$_ManReg01", PLCType)
                addr_HH = self.getAddressSCADA("$Alias$_HH", PLCType)
                addr_H = self.getAddressSCADA("$Alias$_H", PLCType)
                addr_L = self.getAddressSCADA("$Alias$_L", PLCType)
                addr_LL = self.getAddressSCADA("$Alias$_LL", PLCType)

                # 6. New relationship information in all objects:
                type = instance.getAttributeData("FEDeviceAlarm:Type")
                parents = instance.getAttributeData("LogicDeviceDefinitions:Master")
                master = ""

                if type == "Multiple":
                    type = ""
                    typeList = instance.getAttributeData("FEDeviceAlarm:Multiple Types").strip().replace(",", " ").split()
                    i = 0
                    for typeInst in typeList:
                        type = type + "," + typeInst + ":" + MasterListExpert[i]
                        i = i + 1
                    type = type[1:]
                    parentsList = []
                    parentDevice = parents.strip().replace(",", " ")
                    parentList = parentDevice.split()
                    parentListExpert = []
                    for parent in parentList:
                        parentDeviceWinCCOAName = self.thePlugin.getLinkedExpertName(parent, "ProcessControlObject, OnOff, Analog, AnalogDigital, AnaDO, MassFlowController")
                        parentListExpert.append(parentDeviceWinCCOAName)

                    stringparents = ",".join(parentListExpert)

                elif type <> "" and MasterDevice <> "":
                    type = type + ":" + MasterListExpert[0]
                    stringparents = self.thePlugin.getLinkedExpertName(parents, "ProcessControlObject, OnOff, Analog, AnalogDigital, AnaDO, MassFlowController")

                elif type <> "" and MasterDevice == "":
                    stringparents = ""

                else:
                    type = ""
                    stringparents = ""

                uniqueList = []
                for child in children:
                    if child not in uniqueList:
                        uniqueList.append(child)
                children = uniqueList
                stringchildren = ",".join(children)

                # Expert Name Logic
                S_Name = name.strip()
                S_ExpertName = expertName.strip()
                if S_ExpertName == "":
                    secondAlias = Alias
                else:
                    secondAlias = Alias
                    Alias = S_ExpertName

                # Level logic
                LevelAlarm = instance.getAttributeData("SCADADeviceAlarms:Alarm Config:Level")
                if LevelAlarm.strip() == "":
                    LevelAlarm = "Alarm"
                if LevelAlarm.lower() == "information":
                    Level = "0"
                elif LevelAlarm.lower() == "warning":
                    Level = "1"
                elif LevelAlarm.lower() == "alarm":
                    Level = "2"
                elif LevelAlarm.lower() == "safety alarm":
                    Level = "3"

                # 7. Parameters field
                parametersArray = []
                if Delay == "":
                    Delay = "0"
                elif not self.thePlugin.isString(Delay) and (theManufacturer.lower() == "siemens"):  # Delay.isnumeric():
                    Delay = int(round(float(Delay)))
                elif not self.thePlugin.isString(Delay):
                    Delay = Delay  # do not round delay for Schneider (UCPC-1387)
                elif Delay.lower() == "logic":
                    Delay = "logic"
                else:
                    Delay = self.thePlugin.getLinkedExpertName(Delay, "AnalogParameter, AnalogStatus")
                parametersArray.append("PAlDt=" + str(Delay))
                # only create SOURCE parameters if single UNICOS object is linked to the field
                parametersArray.append("INPUT_SOURCE=" + str(self.thePlugin.getLinkedExpertName(self.decorator.extractSingleUnicosObjectFromPLCExpression(Input, False))))
                # if thresholds are blank, set =disabled, which will gray out the field in WinCCOA (UCPC-2074)
                if HH.strip() == "":
                    parametersArray.append("HH_SOURCE=disabled")
                else:
                    parametersArray.append("HH_SOURCE=" + str(self.thePlugin.getLinkedExpertName(self.decorator.extractSingleUnicosObjectFromPLCExpression(HH, False))))
                if H.strip() == "":
                    parametersArray.append("H_SOURCE=disabled")
                else:
                    parametersArray.append("H_SOURCE=" + str(self.thePlugin.getLinkedExpertName(self.decorator.extractSingleUnicosObjectFromPLCExpression(H, False))))
                if L.strip() == "":
                    parametersArray.append("L_SOURCE=disabled")
                else:
                    parametersArray.append("L_SOURCE=" + str(self.thePlugin.getLinkedExpertName(self.decorator.extractSingleUnicosObjectFromPLCExpression(L, False))))
                if LL.strip() == "":
                    parametersArray.append("LL_SOURCE=disabled")
                else:
                    parametersArray.append("LL_SOURCE=" + str(self.thePlugin.getLinkedExpertName(self.decorator.extractSingleUnicosObjectFromPLCExpression(LL, False))))

                categories = smsCat.split(",")
                smsCat = []
                for cat in categories:
                    if cat.upper() == "NO_SMS_ON_WARNING":
                        parametersArray.append("HWSt_NO_MAIL=true")
                        parametersArray.append("LWSt_NO_MAIL=true")
                    elif cat.upper() == "NO_SMS_ON_ALERT":
                        parametersArray.append("HHAlSt_NO_MAIL=true")
                        parametersArray.append("LLAlSt_NO_MAIL=true")
                    else:
                        if ':' in cat:
                            cat,modifier = cat.split(':')
                            if modifier.upper() == "NO_SMS_ON_WARNING":
                                parametersArray.append(cat + "_LWSt_NO_MAIL=true")
                                parametersArray.append(cat + "_HWSt_NO_MAIL=true")
                            elif modifier.upper() == "NO_SMS_ON_ALERT":
                                parametersArray.append(cat + "_HHAlSt_NO_MAIL=true")
                                parametersArray.append(cat + "_LLAlSt_NO_MAIL=true")
                        smsCat.append(cat)

                parameters = ",".join(parametersArray)
                smsCat = ",".join(smsCat)

                # 8. write the instance information in the database file
                self.thePlugin.writeInstanceInfo("CPC_AnalogAlarm;$deviceNumber$;$Alias$$DeviceLinkList$;$Description$;$Diagnostics$;$WWWLink$;$Synoptic$;$AccessControlDomain$$Domain$;$Privileges$$Nature$;$WidgetType$;$Unit$;$Format$;$S_ArchiveModeI$;$ArchiveModeT$;$TimeFilterT$;$smsCat$;$AlarmMessage$;$AlarmAck$;$Level$;$normalPosition$;$addr_StsReg01$;$addr_StsReg02$;$addr_EvStsReg01$;$addr_EvStsReg02$;$addr_PosSt$;$addr_HHSt$;$addr_HSt$;$addr_LSt$;$addr_LLSt$;$addr_ManReg01$;$addr_HH$;$addr_H$;$addr_L$;$addr_LL$;$BooleanArchive$;$AnalogArchive$;$EventArchive$;$MaskEvent$;$parameters$;$master$;$stringparents$;$stringchildren$;$type$;$secondAlias$;")

    def end(self):
        self.thePlugin.writeInUABLog("end in Jython $self.theDeviceType$ template")

    def shutdown(self):
        self.thePlugin.writeInUABLog("shutdown in Jython $self.theDeviceType$ template")

    def getAddressSCADA(self, DPName, PLCType):
        if PLCType.lower() == "quantum":
            address = str(int(self.thePlugin.computeAddress(DPName)) - 1)
        else:
            address = str(self.thePlugin.computeAddress(DPName))

        return address
