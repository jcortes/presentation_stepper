# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for Controller Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin  # REQUIRED
from research.ch.cern.unicos.cpc.interfaces import ISCADAPlugin  # REQUIRED
from research.ch.cern.unicos.core import CoreManager  # REQUIRED
from java.lang import System
import WinCCOA_CommonMethods
from research.ch.cern.unicos.plugins.interfaces import AGenerationPlugin
from research.ch.cern.unicos.utilities import MasterDeviceUtils
import ucpc_library.shared_decorator
reload(ucpc_library.shared_decorator)
try:
    # Try to import the user privileges file
    import WinCCOA_Privileges
except:
    # If the previous import failed, try to import the privileges template file
    import WinCCOA_Privileges_Template


class Controller_Template(IUnicosTemplate):
    thePlugin = 0
    theDeviceType = "Controller"
    # Default name for the privileges file
    privilegesFileName = "WinCCOA_Privileges"

    def deviceFormat(self):
        return ["deviceType", "deviceNumber", "aliasDeviceLinkList", "description", "diagnostics", "wwwLink", "synoptic", "domain", "nature", "widgetType", "alias",
                "hmvUnit", "hmvFormat", "hmvRangeMax", "hmvRangeMin", "outUnit", "outFormat", "outRangeMax", "outRangeMin", "scalingMethod", "driverDeadbandValueOut",
                "driverDeadbandTypeOut", "driverDeadbandValueHMV", "driverDeadbandTypeHMV", "archiveModeOut", "timeFilterOut", "archiveModeHMV", "timeFilterHMV",
                "addressStsReg01", "addressStsReg02", "addressEvStsReg01", "addressEvStsReg02", "addressActSP", "addressActSPL", "addressActSPH", "addressOutOV",
                "addressActOutL", "addressActOutH", "addressMV", "addressMSPSt", "addressAuSPSt", "addressMPosRSt", "addressAuPosRSt", "addressActKc", "addressActTi",
                "addressActTd", "addressActTds", "addressManReg01", "addressManReg02", "addressMPosR", "addressMSP", "addressMSPL", "addressMSPH", "addressMOutL",
                "addressMOutH", "addressMKc", "addressMTi", "addressMTd", "addressMTds", "booleanArchive", "analogArchive", "eventArchive", "maskEvent", "defaultKc",
                "defaultTi", "defaultTd", "defaultTds", "defaultSP", "defaultSPH", "defaultSPL", "defaultOutH", "defaultOutL", "parameters", "master", "parents", "children", "type",
                "secondAlias"]

    def initialize(self):
        self.thePlugin = APlugin.getPluginInterface()
        self.thePlugin.writeInUABLog("initialize in Jython $self.theDeviceType$ template")
        reload(WinCCOA_CommonMethods)
        self.decorator = ucpc_library.shared_decorator.ExpressionDecorator()
        try:
            # Try to reload the user privileges file
            reload(WinCCOA_Privileges)
        except:
            # If the reload failed, reload the privileges template file
            self.privilegesFileName = "WinCCOA_Privileges_Template"
            reload(WinCCOA_Privileges_Template)

    def check(self):
        self.thePlugin.writeInUABLog("check in Jython $self.theDeviceType$ template")

    def begin(self):
        self.thePlugin.writeInUABLog("begin in Jython $self.theDeviceType$ template")

    def process(self, *params):
        self.thePlugin.writeInUABLog("processInstances in Jython $self.theDeviceType$ template")
        theCurrentDeviceType = params[0]
        self.theUnicosProject = theRawInstances = self.thePlugin.getUnicosProject()
        strAllDeviceTypes = theRawInstances.getAllDeviceTypesString()
        theApplicationName = self.thePlugin.getApplicationParameter("ApplicationName")
        instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
        instancesNumber = str(len(instancesVector))
        DeviceTypeName = theCurrentDeviceType.getDeviceTypeName()
        deviceNumber = 0
        CRLF = System.getProperty("line.separator")

        # Get the config (UnicosApplication.xml)
        config = CoreManager.getITechnicalParameters().getXMLConfigMapper()
        # Query a PLC parameter
        PLCType = config.getPLCDeclarations().get(0).getPLCType().getValue()

        # Get all instances of the spec to have the possibility to look inside other device types
        theRawInstances = self.thePlugin.getUnicosProject()

        self.thePlugin.writeDBHeader("#$self.theDeviceType$: $instancesNumber$")
        DeviceTypeFormat = "CPC_$DeviceTypeName$;deviceNumber;Alias[,DeviceLinkList];Description;Diagnostics;WWWLink;Synoptic;Domain;Nature;WidgetType;Alias;HMV_Unit;HMV_Format;HMV_RangeMax;HMV_RangeMin;Out_Unit;Out_Format;Out_RangeMax;Out_RangeMin;ScaMethod;DriverDeadbandValueOut;DriverDeadbandTypeOut;DriverDeadbandValueHMV;DriverDeadbandTypeHMV;ArchiveModeOut;TimeFilterOut;ArchiveModeHMV;TimeFilterHMV;addr_StsReg01;addr_StsReg02;addr_EvStsReg01;addr_EvStsReg02;addr_ActSP;addr_ActSPL;addr_ActSPH;addr_OutOV;addr_ActOutL;addr_ActOutH;addr_MV;addr_MSPSt;addr_AuSPSt;addr_MPosRSt;addr_AuPosRSt;addr_ActKc;addr_ActTi;addr_ActTd;addr_ActTds;addr_ManReg01;addr_ManReg02;addr_MPosR;addr_MSP;addr_MSPL;addr_MSPH;addr_MOutL;addr_MOutH;addr_MKc;addr_MTi;addr_MTd;addr_MTds;BooleanArchive;AnalogArchive;EventArchive;MaskEvent;Def_Kc;Def_Ti;Def_Td;Def_Tds;Def_SP;Def_SPH;Def_SPL;Def_OutH;Def_OutL;Parameters;Master;parents;stringchildren;type;secondAlias;"
        self.thePlugin.writeComment("#$CRLF$# Object: " + DeviceTypeName + "$CRLF$#$CRLF$#Config Line : " + DeviceTypeFormat + "$CRLF$#")

        # Set Privileges
        Privileges = eval(self.privilegesFileName).getPrivileges(DeviceTypeName)

        for instance in instancesVector:
            if (instance is not None):
               # Check if user pressed 'cancel' button:
                if AGenerationPlugin.isGenerationInterrupted():
                    return

                deviceNumber = int(int(deviceNumber) + 1)
                deviceNumber = str(deviceNumber)

                # 1. Common Unicos fields
                # 2. Specific fields
                # 3. SCADA Device Data Archiving
                # 4. Data Treatment
                # 5. Addresses computation
                # 6. write the instance information in the database file

                # 1. Common Unicos fields
                name = Name = Alias = instance.getAttributeData("DeviceIdentification:Name")
                expertName = instance.getAttributeData("DeviceIdentification:Expert Name")
                Description = instance.getAttributeData("DeviceDocumentation:Description")
                Diagnostics = instance.getAttributeData("SCADADeviceGraphics:Diagnostic")
                WWWLink = instance.getAttributeData("SCADADeviceGraphics:WWW Link")
                Synoptic = instance.getAttributeData("SCADADeviceGraphics:Synoptic")
                WidgetType = instance.getAttributeData("SCADADeviceGraphics:Widget Type")
                Domain = instance.getAttributeData("SCADADeviceFunctionals:SCADADeviceClassificationTags:Domain")
                AccessControlDomain = instance.getAttributeData("SCADADeviceFunctionals:Access Control Domain").replace(" ", "")
                Nature = instance.getAttributeData("SCADADeviceFunctionals:SCADADeviceClassificationTags:Nature")
                DeviceLinkListSpec = instance.getAttributeData("SCADADeviceFunctionals:SCADADeviceClassificationTags:Device Links").replace(" ", "")

                # 2. Specific fields
                RA = instance.getAttributeData("FEDeviceParameters:Controller Parameters:RA")
                PID_Sampling = instance.getAttributeData("FEDeviceParameters:Controller Parameters:PID Cycle (s)")
                ScaMethod = instance.getAttributeData("FEDeviceParameters:Controller Parameters:Scaling Method")
                Out_RangeMax = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Controller Parameters:Output Range Max"))
                Out_RangeMin = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Controller Parameters:Output Range Min"))
                HMVName = instance.getAttributeData("FEDeviceEnvironmentInputs:Measured Value")
                ControlledObjectNames = instance.getAttributeData("FEDeviceOutputs:Controlled Objects").replace(",", " ")
                Def_Kc = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceVariables:Default PID Parameters:Kc"))
                Def_Ti = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceVariables:Default PID Parameters:Ti"))
                Def_Td = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceVariables:Default PID Parameters:Td"))
                Def_Tds = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceVariables:Default PID Parameters:Tds"))
                Def_SPH = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceVariables:Default PID Parameters:SP High Limit"))
                Def_SPL = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceVariables:Default PID Parameters:SP Low Limit"))
                Def_OutH = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceVariables:Default PID Parameters:Out High Limit"))
                Def_OutL = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceVariables:Default PID Parameters:Out Low Limit"))
                Def_SP = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceVariables:Default PID Parameters:Setpoint"))
                DriverDeadbandTypeHMV = instance.getAttributeData("SCADADriverDataSmoothing:MV and SP Smoothing:Deadband Type")
                DriverDeadbandValueHMV = instance.getAttributeData("SCADADriverDataSmoothing:MV and SP Smoothing:Deadband Value")
                DriverDeadbandTypeOut = instance.getAttributeData("SCADADriverDataSmoothing:Output Smoothing:Deadband Type")
                DriverDeadbandValueOut = instance.getAttributeData("SCADADriverDataSmoothing:Output Smoothing:Deadband Value")
                MasterDevice = instance.getAttributeData("LogicDeviceDefinitions:Master")
                ExternalMasterDevice = instance.getAttributeData("LogicDeviceDefinitions:External Master")
                MaskEvent = instance.getAttributeData("SCADADeviceFunctionals:Mask Event")
                NormalPosition = "0"

                # 3. SCADA Device Data Archiving
                ArchiveModeHMV = instance.getAttributeData("SCADADeviceDataArchiving:MV and SP Archiving:Archive Mode")
                TimeFilterHMV = self.thePlugin.formatNumberPLC(instance.getAttributeData("SCADADeviceDataArchiving:MV and SP Archiving:Time Filter (s)"))
                DeadbandTypeHMV = instance.getAttributeData("SCADADeviceDataArchiving:MV and SP Archiving:Deadband Type")
                DeadbandValueHMV = self.thePlugin.formatNumberPLC(instance.getAttributeData("SCADADeviceDataArchiving:MV and SP Archiving:Deadband Value"))
                ArchiveModeOut = instance.getAttributeData("SCADADeviceDataArchiving:Output Archiving:Archive Mode")
                TimeFilterOut = self.thePlugin.formatNumberPLC(instance.getAttributeData("SCADADeviceDataArchiving:Output Archiving:Time Filter (s)"))
                DeadbandTypeOut = instance.getAttributeData("SCADADeviceDataArchiving:Output Archiving:Deadband Type")
                DeadbandValueOut = self.thePlugin.formatNumberPLC(instance.getAttributeData("SCADADeviceDataArchiving:Output Archiving:Deadband Value"))
                BooleanArchive = instance.getAttributeData("SCADADeviceDataArchiving:Boolean Archive")
                AnalogArchive = instance.getAttributeData("SCADADeviceDataArchiving:Analog Archive")
                EventArchive = instance.getAttributeData("SCADADeviceDataArchiving:Event Archive")

                HMV_ArchiveMode = ""
                HMV_TimeFilter = ""
                HMV_DeadbandType = ""
                HMV_DeadbandValue = ""
                HMV_Unit = "%"
                HMV_Format = "###.#"
                HMV_RangeMax = "100.0"
                HMV_RangeMin = "0.0"
                Out_ArchiveMode = ""
                Out_TimeFilter = ""
                Out_DeadbandType = ""
                Out_DeadbandValue = ""
                Out_Unit = "%"
                Out_Format = "###.#"
                # 4. Data Treatment
                ControlledObjectList = ControlledObjectNames.split()
                ControlledObjectNumber = len(ControlledObjectList)

                S_ArchiveModeHMV = ArchiveModeHMV.lower()
                S_DriverDeadbandTypeHMV = DriverDeadbandTypeHMV.lower()
                S_DeadbandTypeHMV = DeadbandTypeHMV.lower()

                S_ArchiveModeOut = ArchiveModeOut.lower()
                S_DriverDeadbandTypeOut = DriverDeadbandTypeOut.lower()
                S_DeadbandTypeOut = DeadbandTypeOut.lower()

                S_ScaMethod = ScaMethod.lower()

                if(S_ScaMethod == "input scaling"):
                    ScaMethod = "1"
                if(S_ScaMethod == "input/output scaling"):
                    ScaMethod = "2"
                if(S_ScaMethod == "no scaling"):
                    ScaMethod = "3"

                # Build DeviceLinkList and children from related objects
                DeviceLinkList = ""

                if MasterDevice != "":
                    MasterDevice = MasterWinCCOAName = self.thePlugin.getLinkedExpertName(MasterDevice, ", ".join(MasterDeviceUtils.getAllMasterTypes(config)))
                    DeviceLinkList = self.thePlugin.appendLinkedDevice(DeviceLinkList, MasterWinCCOAName)
                elif (ExternalMasterDevice != ""):
                    MasterDevice = MasterDeviceWinCCOAName = ExternalMasterDevice
                    DeviceLinkList = self.thePlugin.appendLinkedDevice(DeviceLinkList, MasterDeviceWinCCOAName)

                children = []
                ParamMV = ""
                # HMV : Extract Archive mode, Unit, Format, Range Max, Range Min
                if (HMVName != ""):
                    inst_HMV = theRawInstances.findMatchingInstances("AnalogInput, AnalogInputReal, AnalogStatus, MassFlowController", "'#DeviceIdentification:Name#'='$HMVName$'")
                    for instance_HMV in inst_HMV:
                        HMVNameWinCCOAName = WinCCOA_CommonMethods.getExpertName(instance_HMV)
                        DeviceLinkList = self.thePlugin.appendLinkedDevice(DeviceLinkList, HMVNameWinCCOAName)
                        children.append(HMVNameWinCCOAName)
                        ParamMV = HMVNameWinCCOAName
                        if instance_HMV.getDeviceType().getDeviceTypeName().lower() == "massflowcontroller":
                            # xxx = instance_HMV.getAttributeData("DeviceIdentification:Name")
                            # self.thePlugin.writeWarningInUABLog("PID instance for loop massflow : $xxx$")
                            HMV_ArchiveMode = instance_HMV.getAttributeData("SCADADeviceDataArchiving:Flow:Archive Mode")
                            HMV_TimeFilter = self.thePlugin.formatNumberPLC(instance_HMV.getAttributeData("SCADADeviceDataArchiving:Flow:Time Filter (s)"))
                            HMV_DeadbandType = instance_HMV.getAttributeData("SCADADeviceDataArchiving:Flow:Deadband Type")
                            HMV_DeadbandValue = self.thePlugin.formatNumberPLC(instance_HMV.getAttributeData("SCADADeviceDataArchiving:Flow:Deadband Value"))
                            HMV_Unit = instance_HMV.getAttributeData("SCADADeviceGraphics:Flow:Unit")
                            HMV_Format = instance_HMV.getAttributeData("SCADADeviceGraphics:Flow:Format")
                            # HMV_RangeMax         = self.thePlugin.formatNumberPLC(instance_HMV.getAttributeData("FEDeviceParameters:Range Max"))
                            # HMV_RangeMin         = self.thePlugin.formatNumberPLC(instance_HMV.getAttributeData("FEDeviceParameters:Range Min"))
                            HMV_RangeMax = self.thePlugin.formatNumberPLC(instance_HMV.getAttributeData("FEDeviceParameters:CC0 Max Flow (Unit/time)"))
                            HMV_RangeMax1 = self.thePlugin.formatNumberPLC(instance_HMV.getAttributeData("FEDeviceParameters:CC1 Max Flow (Unit/time)"))
                            HMV_RangeMax2 = self.thePlugin.formatNumberPLC(instance_HMV.getAttributeData("FEDeviceParameters:CC2 Max Flow (Unit/time)"))
                            PFlConversion = self.thePlugin.formatNumberPLC(instance_HMV.getAttributeData("FEDeviceParameters:Flow Conversion").replace(" ", ""))
                            if HMV_RangeMax1 == "":
                                HMV_RangeMax1 = "987654321.0"
                            if HMV_RangeMax2 == "":
                                HMV_RangeMax2 = "987654321.0"
                            if float(HMV_RangeMax) < float(HMV_RangeMax1) and (HMV_RangeMax1 != "987654321.0"):
                                HMV_RangeMax = HMV_RangeMax1
                            if float(HMV_RangeMax) < float(HMV_RangeMax2) and (HMV_RangeMax2 != "987654321.0"):
                                HMV_RangeMax = HMV_RangeMax2
                            HMV_RangeMin = "0.0"
                            HMV_RangeMax = str(float(HMV_RangeMax) * float(PFlConversion))
                        else:
                            HMV_ArchiveMode = instance_HMV.getAttributeData("SCADADeviceDataArchiving:Archive Mode")
                            HMV_TimeFilter = self.thePlugin.formatNumberPLC(instance_HMV.getAttributeData("SCADADeviceDataArchiving:Time Filter (s)"))
                            HMV_DeadbandType = instance_HMV.getAttributeData("SCADADeviceDataArchiving:Deadband Type")
                            HMV_DeadbandValue = self.thePlugin.formatNumberPLC(instance_HMV.getAttributeData("SCADADeviceDataArchiving:Deadband Value"))
                            HMV_Unit = instance_HMV.getAttributeData("SCADADeviceGraphics:Unit")
                            HMV_Format = instance_HMV.getAttributeData("SCADADeviceGraphics:Format")
                            HMV_RangeMax = self.thePlugin.formatNumberPLC(instance_HMV.getAttributeData("FEDeviceParameters:Range Max"))
                            HMV_RangeMin = self.thePlugin.formatNumberPLC(instance_HMV.getAttributeData("FEDeviceParameters:Range Min"))

                ParamOUT = []
                for ControlledObject in ControlledObjectList:
                    ControlledObjectNameWinCCOAName = self.thePlugin.getLinkedExpertName(ControlledObject, "Analog, AnalogDigital, Controller, AnaDO, MassFlowController")
                    DeviceLinkList = self.thePlugin.appendLinkedDevice(DeviceLinkList, ControlledObjectNameWinCCOAName)
                    children.append(ControlledObjectNameWinCCOAName)
                    ParamOUT.append(ControlledObjectNameWinCCOAName)

                # UCPC-745 Add DeviceLink list and children for new object links
                if self.thePlugin.isString(Def_SP) and Def_SP <> "":
                    DeviceLinkList, children = WinCCOA_CommonMethods.updateDeviceLinksAndChildren(Def_SP, DeviceLinkList, children, self.thePlugin, self.decorator)
                if self.thePlugin.isString(Def_Kc) and Def_Kc <> "":
                    DeviceLinkList, children = WinCCOA_CommonMethods.updateDeviceLinksAndChildren(Def_Kc, DeviceLinkList, children, self.thePlugin, self.decorator)
                if self.thePlugin.isString(Def_Ti) and Def_Ti <> "":
                    DeviceLinkList, children = WinCCOA_CommonMethods.updateDeviceLinksAndChildren(Def_Ti, DeviceLinkList, children, self.thePlugin, self.decorator)
                if self.thePlugin.isString(Def_Td) and Def_Td <> "":
                    DeviceLinkList, children = WinCCOA_CommonMethods.updateDeviceLinksAndChildren(Def_Td, DeviceLinkList, children, self.thePlugin, self.decorator)
                if self.thePlugin.isString(Def_Tds) and Def_Tds <> "":
                    DeviceLinkList, children = WinCCOA_CommonMethods.updateDeviceLinksAndChildren(Def_Tds, DeviceLinkList, children, self.thePlugin, self.decorator)
                if self.thePlugin.isString(Def_SPH) and Def_SPH <> "":
                    DeviceLinkList, children = WinCCOA_CommonMethods.updateDeviceLinksAndChildren(Def_SPH, DeviceLinkList, children, self.thePlugin, self.decorator)
                if self.thePlugin.isString(Def_SPL) and Def_SPL <> "":
                    DeviceLinkList, children = WinCCOA_CommonMethods.updateDeviceLinksAndChildren(Def_SPL, DeviceLinkList, children, self.thePlugin, self.decorator)
                if self.thePlugin.isString(Def_OutH) and Def_OutH <> "":
                    DeviceLinkList, children = WinCCOA_CommonMethods.updateDeviceLinksAndChildren(Def_OutH, DeviceLinkList, children, self.thePlugin, self.decorator)
                if self.thePlugin.isString(Def_OutL) and Def_OutL <> "":
                    DeviceLinkList, children = WinCCOA_CommonMethods.updateDeviceLinksAndChildren(Def_OutL, DeviceLinkList, children, self.thePlugin, self.decorator)

                # Append Device Link list from Spec
                DeviceLinkList = WinCCOA_CommonMethods.appendDeviceLinkListFromSpec(DeviceLinkListSpec, DeviceLinkList, self.thePlugin, strAllDeviceTypes)

                # Avoid duplications on the deviceLinks
                DeviceLinkList = WinCCOA_CommonMethods.removeDuplicatesAndGivenObjects(DeviceLinkList, [Name, expertName])

                # Controlled Object 1 : Extract Archive mode, Unit, Format, Range Max, Range Min
                if (ControlledObjectNumber > 0):
                    inst_Controlled1 = theRawInstances.findMatchingInstances("Analog, AnalogDigital, Controller, AnaDO, MassFlowController", "'#DeviceIdentification:Name#'='$ControlledObjectList[0]$'")
                    for instance_Controlled1 in inst_Controlled1:
                        Out_Nature = instance_Controlled1.getDeviceType().getDeviceTypeName()
                        # controlled object is a controller

                        if (Out_Nature.lower() == "controller"):
                            OutHMV_Name = instance_Controlled1.getAttributeData("FEDeviceEnvironmentInputs:Measured Value")
                            inst_OutHMV = theRawInstances.findMatchingInstances("AnalogInput, AnalogInputReal, AnalogStatus", "'#DeviceIdentification:Name#'='$OutHMV_Name$'")
                            for instance_OutHMV in inst_OutHMV:
                                Out_ArchiveMode = instance_OutHMV.getAttributeData("SCADADeviceDataArchiving:Archive Mode")
                                Out_TimeFilter = self.thePlugin.formatNumberPLC(instance_OutHMV.getAttributeData("SCADADeviceDataArchiving:Time Filter (s)"))
                                Out_DeadbandType = instance_OutHMV.getAttributeData("SCADADeviceDataArchiving:Deadband Type")
                                Out_DeadbandValue = self.thePlugin.formatNumberPLC(instance_OutHMV.getAttributeData("SCADADeviceDataArchiving:Deadband Value"))
                                Out_Unit = instance_OutHMV.getAttributeData("SCADADeviceGraphics:Unit")
                                Out_Format = instance_OutHMV.getAttributeData("SCADADeviceGraphics:Format")
                                if Out_RangeMax == "":
                                    Out_RangeMax = self.thePlugin.formatNumberPLC(instance_OutHMV.getAttributeData("FEDeviceParameters:Range Max"))
                                if Out_RangeMin == "":
                                    Out_RangeMin = self.thePlugin.formatNumberPLC(instance_OutHMV.getAttributeData("FEDeviceParameters:Range Min"))
                        # controlled object is an analog or anadig
                        elif (Out_Nature.lower() == "analog" or Out_Nature.lower() == "analogdigital" or Out_Nature.lower() == "anado"):
                            Out_ArchiveMode = instance_Controlled1.getAttributeData("SCADADeviceDataArchiving:Archive Mode")
                            Out_TimeFilter = self.thePlugin.formatNumberPLC(instance_Controlled1.getAttributeData("SCADADeviceDataArchiving:Time Filter (s)"))
                            Out_DeadbandType = instance_Controlled1.getAttributeData("SCADADeviceDataArchiving:Deadband Type")
                            Out_DeadbandValue = self.thePlugin.formatNumberPLC(instance_Controlled1.getAttributeData("SCADADeviceDataArchiving:Deadband Value"))
                            Out_Unit = instance_Controlled1.getAttributeData("SCADADeviceGraphics:Unit")
                            Out_Format = instance_Controlled1.getAttributeData("SCADADeviceGraphics:Format")

                            # For Analog Objects, Get Ranges and Archive Mode if necessary from the connected AO, else take it directly (for Anadig)
                            if (Out_Nature.lower() == "analog"):
                                Out_AO = instance_Controlled1.getAttributeData("FEDeviceOutputs:Process Output")
                                if (Out_AO != ""):
                                    inst_AO = theRawInstances.findMatchingInstances("AnalogOutput, AnalogOutputReal", "'#DeviceIdentification:Name#'='$Out_AO$'")
                                    if inst_AO:
                                        for instance_AO in inst_AO:
                                            if Out_RangeMax == "":
                                                Out_RangeMax = self.thePlugin.formatNumberPLC(instance_AO.getAttributeData("FEDeviceParameters:Range Max"))
                                            if Out_RangeMin == "":
                                                Out_RangeMin = self.thePlugin.formatNumberPLC(instance_AO.getAttributeData("FEDeviceParameters:Range Min"))
                                            if Out_ArchiveMode == "":
                                                Out_ArchiveMode = instance_AO.getAttributeData("SCADADeviceDataArchiving:Archive Mode")
                                                Out_TimeFilter = self.thePlugin.formatNumberPLC(instance_AO.getAttributeData("SCADADeviceDataArchiving:Time Filter (s)"))
                                                Out_DeadbandType = instance_AO.getAttributeData("SCADADeviceDataArchiving:Deadband Type")
                                                Out_DeadbandValue = self.thePlugin.formatNumberPLC(instance_AO.getAttributeData("SCADADeviceDataArchiving:Deadband Value"))
                                else:
                                    if Out_RangeMax == "":
                                        Out_RangeMax = "100.0"
                                    if Out_RangeMin == "":
                                        Out_RangeMin = "0.0"

                            elif (Out_Nature.lower() == "anado"):
                                Out_AO = instance_Controlled1.getAttributeData("FEDeviceOutputs:Analog Process Output")
                                if (Out_AO != ""):
                                    inst_AO = theRawInstances.findMatchingInstances("AnalogOutput, AnalogOutputReal", "'#DeviceIdentification:Name#'='$Out_AO$'")
                                    if inst_AO:
                                        for instance_AO in inst_AO:
                                            if Out_RangeMax == "":
                                                Out_RangeMax = self.thePlugin.formatNumberPLC(instance_AO.getAttributeData("FEDeviceParameters:Range Max"))
                                            if Out_RangeMin == "":
                                                Out_RangeMin = self.thePlugin.formatNumberPLC(instance_AO.getAttributeData("FEDeviceParameters:Range Min"))
                                            if Out_ArchiveMode == "":
                                                Out_ArchiveMode = instance_AO.getAttributeData("SCADADeviceDataArchiving:Archive Mode")
                                                Out_TimeFilter = self.thePlugin.formatNumberPLC(instance_AO.getAttributeData("SCADADeviceDataArchiving:Time Filter (s)"))
                                                Out_DeadbandType = instance_AO.getAttributeData("SCADADeviceDataArchiving:Deadband Type")
                                                Out_DeadbandValue = self.thePlugin.formatNumberPLC(instance_AO.getAttributeData("SCADADeviceDataArchiving:Deadband Value"))
                                else:
                                    if Out_RangeMax == "":
                                        Out_RangeMax = "100.0"
                                    if Out_RangeMin == "":
                                        Out_RangeMin = "0.0"

                            else:  # AnaDig
                                if Out_RangeMax == "":
                                    Out_RangeMax = self.thePlugin.formatNumberPLC(instance_Controlled1.getAttributeData("FEDeviceParameters:Range Max"))
                                if Out_RangeMin == "":
                                    Out_RangeMin = self.thePlugin.formatNumberPLC(instance_Controlled1.getAttributeData("FEDeviceParameters:Range Min"))

                        elif (Out_Nature.lower() == "massflowcontroller"):
                            Out_ArchiveMode = instance_Controlled1.getAttributeData("SCADADeviceDataArchiving:Flow:Archive Mode")
                            Out_TimeFilter = self.thePlugin.formatNumberPLC(instance_Controlled1.getAttributeData("SCADADeviceDataArchiving:Flow:Time Filter (s)"))
                            Out_DeadbandType = instance_Controlled1.getAttributeData("SCADADeviceDataArchiving:Flow:Deadband Type")
                            Out_DeadbandValue = self.thePlugin.formatNumberPLC(instance_Controlled1.getAttributeData("SCADADeviceDataArchiving:Flow:Deadband Value"))
                            Out_Unit = instance_Controlled1.getAttributeData("SCADADeviceGraphics:Flow:Unit")
                            Out_Format = instance_Controlled1.getAttributeData("SCADADeviceGraphics:Flow:Format")
                            Out_RangeMin = "0.0"
                            Out_RangeMax = self.thePlugin.formatNumberPLC(instance_Controlled1.getAttributeData("FEDeviceParameters:CC0 Max Flow (Unit/time)"))
                            Out_RangeMax1 = self.thePlugin.formatNumberPLC(instance_Controlled1.getAttributeData("FEDeviceParameters:CC1 Max Flow (Unit/time)"))
                            Out_RangeMax2 = self.thePlugin.formatNumberPLC(instance_Controlled1.getAttributeData("FEDeviceParameters:CC2 Max Flow (Unit/time)"))
                            if (Out_RangeMax1 != "") and (float(Out_RangeMax) < float(Out_RangeMax1)):
                                Out_RangeMax = Out_RangeMax1
                            if (Out_RangeMax2 != "") and (float(Out_RangeMax) < float(Out_RangeMax2)):
                                Out_RangeMax = Out_RangeMax2

                            PFlConversion = self.thePlugin.formatNumberPLC(instance_Controlled1.getAttributeData("FEDeviceParameters:Flow Conversion"))
                            Out_RangeMax = str(float(Out_RangeMax) * float(PFlConversion))

                else:
                    if Out_RangeMax == "":
                        Out_RangeMax = "100.0"
                    if Out_RangeMin == "":
                        Out_RangeMin = "0.0"

                # In percentage scaling, PID output is 0/100 %
                if (ScaMethod == "1"):
                    Out_Unit = "%"
                    Out_RangeMax = "100.0"
                    Out_RangeMin = "0.0"
                    Out_Format = "###.#"

                # If no Archive mode, Deadband or TimeFilter defined, take archive mode of MV
                if (ArchiveModeHMV == ""):
                    ArchiveModeHMV = HMV_ArchiveMode
                if (DeadbandTypeHMV == ""):
                    DeadbandTypeHMV = HMV_DeadbandType
                if (DeadbandValueHMV == ""):
                    DeadbandValueHMV = HMV_DeadbandValue
                if (TimeFilterHMV == ""):
                    TimeFilterHMV = HMV_TimeFilter

                S_ArchiveModeHMV = ArchiveModeHMV.lower()
                S_DriverDeadbandTypeHMV = DriverDeadbandTypeHMV.lower()
                S_DeadbandTypeHMV = DeadbandTypeHMV.lower()

                # If no Archive mode, Deadband or TimeFilter defined, take archive mode of Controlled Object1
                if (ArchiveModeOut == ""):
                    ArchiveModeOut = Out_ArchiveMode
                if (DeadbandTypeOut == ""):
                    DeadbandTypeOut = Out_DeadbandType
                if (DeadbandValueOut == ""):
                    DeadbandValueOut = Out_DeadbandValue
                if (TimeFilterOut == ""):
                    TimeFilterOut = Out_TimeFilter

                S_ArchiveModeOut = ArchiveModeOut.lower()
                S_DriverDeadbandTypeOut = DriverDeadbandTypeOut.lower()
                S_DeadbandTypeOut = DeadbandTypeOut.lower()

                # Default values if domain or nature empty
                if Domain == "":
                    Domain = theApplicationName

                if AccessControlDomain != "":
                    AccessControlDomain = AccessControlDomain + "|"

                if Nature == "":
                    Nature = "PID"

                # Default archive DB in case of Null
                if not (S_ArchiveModeHMV == "no" or S_ArchiveModeHMV == "old/new comparison" or S_ArchiveModeHMV == "time" or S_ArchiveModeHMV == "old/new comparison and time" or S_ArchiveModeHMV == "old/new comparison or time"):
                    if S_DeadbandTypeHMV == "" or DeadbandValueHMV == "":
                        S_DeadbandTypeHMV = "relative"
                        DeadbandValueHMV = "5.0"
                        self.thePlugin.writeWarningInUABLog("PID instance: $Alias$. HMV Archive Deadband Type and Value are required : forced to ($S_DeadbandTypeHMV$ $DeadbandValueHMV$ %).")

                # Default archive DB in case of Null
                if not (S_ArchiveModeOut == "no" or S_ArchiveModeOut == "old/new comparison" or S_ArchiveModeOut == "time" or S_ArchiveModeOut == "old/new comparison and time" or S_ArchiveModeOut == "old/new comparison or time"):
                    if S_DeadbandTypeOut == "" or DeadbandValueOut == "":
                        S_DeadbandTypeOut = "relative"
                        DeadbandValueOut = "5.0"
                        self.thePlugin.writeWarningInUABLog("PID instance: $Alias$. Output Archive Deadband Type and Value are required : forced to ($S_DeadbandTypeOut$ $DeadbandValueOut$ %).")

                # Mask Value
                if (MaskEvent.lower() == "true"):
                    MaskEvent = "0"
                else:
                    MaskEvent = "1"

                # Time Filter
                if TimeFilterHMV == "" and S_ArchiveModeHMV not in ["no", ""]:
                    TimeFilterHMV = "0"
                    if S_ArchiveModeHMV == "time" or S_ArchiveModeHMV == "deadband and time" or S_ArchiveModeHMV == "deadband or time" or S_ArchiveModeHMV == "old/new comparison and time" or S_ArchiveModeHMV == "old/new comparison or time":
                        self.thePlugin.writeWarningInUABLog("Controller instance: $Alias$. HMV Archive Time Filter is required by the Archive Mode selected: forced to ($TimeFilterHMV$).")

                if TimeFilterOut == "" and S_ArchiveModeOut not in ["no", ""]:
                    TimeFilterOut = "0"
                    if S_ArchiveModeOut == "time" or S_ArchiveModeOut == "deadband and time" or S_ArchiveModeOut == "deadband or time" or S_ArchiveModeOut == "old/new comparison and time" or S_ArchiveModeOut == "old/new comparison or time":
                        self.thePlugin.writeWarningInUABLog("PID instance: $Alias$. Output Archive Time Filter is required by the Archive Mode selected: forced to ($TimeFilterOut$).")

                # Deadband value threatment
                if DriverDeadbandValueHMV == "":
                    DriverDeadbandValueHMV = "0.0"
                else:
                    DriverDeadbandValueHMV = self.thePlugin.formatNumberPLC(DriverDeadbandValueHMV)
                if DriverDeadbandValueOut == "":
                    DriverDeadbandValueOut = "0.0"
                else:
                    DriverDeadbandValueOut = self.thePlugin.formatNumberPLC(DriverDeadbandValueOut)

                # Driver Deadband versus Archive Deadband
                if (S_DriverDeadbandTypeHMV == "absolute" or S_DriverDeadbandTypeHMV == "relative") and (S_ArchiveModeHMV == "deadband" or S_ArchiveModeHMV == "deadband and time" or S_ArchiveModeHMV == "deadband or time"):
                    if S_DriverDeadbandTypeHMV == S_DeadbandTypeHMV:  # both are of the same type relative or absolute
                        if float(DriverDeadbandValueHMV) > float(DeadbandValueHMV):
                            self.thePlugin.writeWarningInUABLog("$self.theDeviceType$ instance: $Alias$. HMV, Driver Deadband ($DriverDeadbandValueHMV$) and Archive Deadband ($DeadbandValueHMV$) are not consistent.")
                    else:
                        self.thePlugin.writeWarningInUABLog("$self.theDeviceType$ instance: $Alias$. HMV, Driver Deadband and Archive Deadband are not of the same type (relative or aboslute).")

                if (S_DriverDeadbandTypeOut == "absolute" or S_DriverDeadbandTypeOut == "relative") and (S_ArchiveModeOut == "deadband" or S_ArchiveModeOut == "deadband and time" or S_ArchiveModeOut == "deadband or time"):
                    if S_DriverDeadbandTypeOut == S_DeadbandTypeOut:  # both are of the same type relative or absolute
                        if float(DriverDeadbandValueOut) > float(DeadbandValueOut):
                            self.thePlugin.writeWarningInUABLog("$self.theDeviceType$ instance: $Alias$. Output, Driver Deadband ($DriverDeadbandValueOut$) and Archive Deadband ($DeadbandValueOut$) are not consistent.")
                    else:
                        self.thePlugin.writeWarningInUABLog("$self.theDeviceType$ instance: $Alias$. Output, Driver Deadband and Archive Deadband are not of the same type (relative or aboslute).")

                # Driver Smooting for HMV
                if (S_DriverDeadbandTypeHMV == "no"):
                    DriverDeadbandTypeHMV = str("0")
                elif (S_DriverDeadbandTypeHMV == "absolute"):
                    DriverDeadbandTypeHMV = str("1")
                elif (S_DriverDeadbandTypeHMV == "relative"):
                    DriverDeadbandTypeHMV = str("2")
                elif (S_DriverDeadbandTypeHMV == "old/new"):
                    DriverDeadbandTypeHMV = str("3")
                else:
                    DriverDeadbandTypeHMV = str("0")

                # Driver Smooting for Output
                if (S_DriverDeadbandTypeOut == "no"):
                    DriverDeadbandTypeOut = str("0")
                elif (S_DriverDeadbandTypeOut == "absolute"):
                    DriverDeadbandTypeOut = str("1")
                elif (S_DriverDeadbandTypeOut == "relative"):
                    DriverDeadbandTypeOut = str("2")
                elif (S_DriverDeadbandTypeOut == "old/new"):
                    DriverDeadbandTypeOut = str("3")
                else:
                    DriverDeadbandTypeOut = str("0")

                # Archiving Mode for HMV
                if (S_ArchiveModeHMV == "no"):
                    ArchiveModeHMV = str("N")
                elif (S_ArchiveModeHMV == "deadband" and S_DeadbandTypeHMV == "absolute"):
                    ArchiveModeHMV = str("VA,$DeadbandValueHMV$,N")
                elif (S_ArchiveModeHMV == "deadband" and S_DeadbandTypeHMV == "relative"):
                    ArchiveModeHMV = str("VR,$DeadbandValueHMV$,N")
                elif (S_ArchiveModeHMV == "time"):
                    ArchiveModeHMV = str("Y")
                elif (S_ArchiveModeHMV == "deadband and time" and S_DeadbandTypeHMV == "absolute"):
                    ArchiveModeHMV = str("VA,$DeadbandValueHMV$,A")
                elif (S_ArchiveModeHMV == "deadband and time" and S_DeadbandTypeHMV == "relative"):
                    ArchiveModeHMV = str("VR,$DeadbandValueHMV$,A")
                elif (S_ArchiveModeHMV == "deadband or time" and S_DeadbandTypeHMV == "absolute"):
                    ArchiveModeHMV = str("VA,$DeadbandValueHMV$,O")
                elif (S_ArchiveModeHMV == "deadband or time" and S_DeadbandTypeHMV == "relative"):
                    ArchiveModeHMV = str("VR,$DeadbandValueHMV$,O")
                elif (S_ArchiveModeHMV == "old/new comparison"):
                    ArchiveModeHMV = str("O")
                    TimeFilter = "0"
                elif (S_ArchiveModeHMV == "old/new comparison and time"):
                    ArchiveModeHMV = str("A")
                elif (S_ArchiveModeHMV == "old/new comparison or time"):
                    ArchiveModeHMV = str("O")
                else:
                    ArchiveModeHMV = str("N")

                # Archiving Mode for Controlled Objects
                if (S_ArchiveModeOut == "no"):
                    ArchiveModeOut = str("N")
                elif (S_ArchiveModeOut == "deadband" and S_DeadbandTypeOut == "absolute"):
                    ArchiveModeOut = str("VA,$DeadbandValueOut$,N")
                elif (S_ArchiveModeOut == "deadband" and S_DeadbandTypeOut == "relative"):
                    ArchiveModeOut = str("VR,$DeadbandValueOut$,N")
                elif (S_ArchiveModeOut == "time"):
                    ArchiveModeOut = str("Y")
                elif (S_ArchiveModeOut == "deadband and time" and S_DeadbandTypeOut == "absolute"):
                    ArchiveModeOut = str("VA,$DeadbandValueOut$,A")
                elif (S_ArchiveModeOut == "deadband and time" and S_DeadbandTypeOut == "relative"):
                    ArchiveModeOut = str("VR,$DeadbandValueOut$,A")
                elif (S_ArchiveModeOut == "deadband or time" and S_DeadbandTypeOut == "absolute"):
                    ArchiveModeOut = str("VA,$DeadbandValueOut$,O")
                elif (S_ArchiveModeOut == "deadband or time" and S_DeadbandTypeOut == "relative"):
                    ArchiveModeOut = str("VR,$DeadbandValueOut$,O")
                elif (S_ArchiveModeOut == "old/new comparison"):
                    ArchiveModeOut = str("O")
                    TimeFilter = "0"
                elif (S_ArchiveModeOut == "old/new comparison and time"):
                    ArchiveModeOut = str("A")
                elif (S_ArchiveModeOut == "old/new comparison or time"):
                    ArchiveModeOut = str("O")
                else:
                    ArchiveModeOut = str("N")

                # 5. Addresses computation
                addr_ManReg01 = self.getAddressSCADA("$Alias$_ManReg01", PLCType)
                addr_ManReg02 = self.getAddressSCADA("$Alias$_ManReg02", PLCType)
                addr_MPosR = self.getAddressSCADA("$Alias$_MPosR", PLCType)
                addr_MSP = self.getAddressSCADA("$Alias$_MSP", PLCType)
                addr_MKc = self.getAddressSCADA("$Alias$_MKc", PLCType)
                addr_MTi = self.getAddressSCADA("$Alias$_MTi", PLCType)
                addr_MTd = self.getAddressSCADA("$Alias$_MTd", PLCType)
                addr_MTds = self.getAddressSCADA("$Alias$_MTds", PLCType)
                addr_MSPH = self.getAddressSCADA("$Alias$_MSPH", PLCType)
                addr_MSPL = self.getAddressSCADA("$Alias$_MSPL", PLCType)
                addr_MOutH = self.getAddressSCADA("$Alias$_MOutH", PLCType)
                addr_MOutL = self.getAddressSCADA("$Alias$_MOutL", PLCType)

                # Outputs
                addr_StsReg01 = self.getAddressSCADA("$Alias$_StsReg01", PLCType)
                addr_StsReg02 = self.getAddressSCADA("$Alias$_StsReg02", PLCType)
                addr_EvStsReg01 = self.getAddressSCADA("$Alias$_EvStsReg01", PLCType)
                addr_EvStsReg02 = self.getAddressSCADA("$Alias$_EvStsReg02", PLCType)
                addr_OutOV = self.getAddressSCADA("$Alias$_OutOV", PLCType)
                addr_AuSPSt = self.getAddressSCADA("$Alias$_AuSPSt", PLCType)
                addr_AuPosRSt = self.getAddressSCADA("$Alias$_AuPosRSt", PLCType)
                addr_ActSP = self.getAddressSCADA("$Alias$_ActSP", PLCType)
                addr_MSPSt = self.getAddressSCADA("$Alias$_MSPSt", PLCType)
                addr_MPosRSt = self.getAddressSCADA("$Alias$_MPosRSt", PLCType)
                addr_MV = self.getAddressSCADA("$Alias$_MV", PLCType)
                addr_ActKc = self.getAddressSCADA("$Alias$_ActKc", PLCType)
                addr_ActTi = self.getAddressSCADA("$Alias$_ActTi", PLCType)
                addr_ActTd = self.getAddressSCADA("$Alias$_ActTd", PLCType)
                addr_ActTds = self.getAddressSCADA("$Alias$_ActTds", PLCType)
                addr_ActSPH = self.getAddressSCADA("$Alias$_ActSPH", PLCType)
                addr_ActSPL = self.getAddressSCADA("$Alias$_ActSPL", PLCType)
                addr_ActOutH = self.getAddressSCADA("$Alias$_ActOutH", PLCType)
                addr_ActOutL = self.getAddressSCADA("$Alias$_ActOutL", PLCType)

                # 6. New relationship information in all objects:
                parents = ""
                type = ""

                uniqueList = []
                for child in children:
                    if child not in uniqueList:
                        uniqueList.append(child)
                children = uniqueList
                stringchildren = ",".join(children)

                # Expert Name Logic
                S_Name = name.strip()
                S_ExpertName = expertName.strip()
                if S_ExpertName == "":
                    secondAlias = Alias
                else:
                    secondAlias = Alias
                    Alias = S_ExpertName

                # 7. Parameters field
                ParametersArray = []
                if PID_Sampling != "":
                    ParametersArray.append("SAMPLING_TIME=" + PID_Sampling)
                else:
                    ParametersArray.append("SAMPLING_TIME=")
                if RA != "":
                    ParametersArray.append("RA=" + RA)
                else:
                    ParametersArray.append("RA=")
                if ParamMV != "":
                    ParametersArray.append("MV=" + ParamMV)
                if ParamOUT:
                    ParametersArray.append("OUT=" + "|".join(ParamOUT))
                if self.thePlugin.isString(Def_SP) and Def_SP <> "":
                    ParametersArray.append("SP_SOURCE=" + self.thePlugin.getLinkedExpertName(Def_SP))
                if self.thePlugin.isString(Def_Kc) and Def_Kc <> "":
                    ParametersArray.append("KC_SOURCE=" + self.thePlugin.getLinkedExpertName(Def_Kc))
                if self.thePlugin.isString(Def_Ti) and Def_Ti <> "":
                    ParametersArray.append("TI_SOURCE=" + self.thePlugin.getLinkedExpertName(Def_Ti))
                if self.thePlugin.isString(Def_Td) and Def_Td <> "":
                    ParametersArray.append("TD_SOURCE=" + self.thePlugin.getLinkedExpertName(Def_Td))
                if self.thePlugin.isString(Def_Tds) and Def_Tds <> "":
                    ParametersArray.append("TDS_SOURCE=" + self.thePlugin.getLinkedExpertName(Def_Tds))
                if self.thePlugin.isString(Def_SPH) and Def_SPH <> "":
                    ParametersArray.append("SPH_SOURCE=" + self.thePlugin.getLinkedExpertName(Def_SPH))
                if self.thePlugin.isString(Def_SPL) and Def_SPL <> "":
                    ParametersArray.append("SPL_SOURCE=" + self.thePlugin.getLinkedExpertName(Def_SPL))
                if self.thePlugin.isString(Def_OutH) and Def_OutH <> "":
                    ParametersArray.append("OUTH_SOURCE=" + self.thePlugin.getLinkedExpertName(Def_OutH))
                if self.thePlugin.isString(Def_OutL) and Def_OutL <> "":
                    ParametersArray.append("OUTL_SOURCE=" + self.thePlugin.getLinkedExpertName(Def_OutL))

                Parameters = ",".join(ParametersArray)

                # 8.default PID values
                if Def_Kc == "" or self.thePlugin.isString(Def_Kc):
                    Def_Kc = "1.0"
                if Def_Ti == "" or self.thePlugin.isString(Def_Ti):
                    Def_Ti = "100.0"
                if Def_Td == "" or self.thePlugin.isString(Def_Td):
                    Def_Td = "0.0"
                if Def_Tds == "" or self.thePlugin.isString(Def_Tds):
                    Def_Tds = "0.0"
                if Def_SPH == "" or self.thePlugin.isString(Def_SPH):
                    Def_SPH = HMV_RangeMax
                elif float(Def_SPH) > float(HMV_RangeMax):
                    Def_SPH = HMV_RangeMax
                elif float(Def_SPH) < float(HMV_RangeMin):
                    Def_SPH = HMV_RangeMin
                if Def_SPL == "" or self.thePlugin.isString(Def_SPL):
                    Def_SPL = HMV_RangeMin
                elif float(Def_SPL) < float(HMV_RangeMin):
                    Def_SPL = HMV_RangeMin
                elif float(Def_SPL) > float(HMV_RangeMax):
                    Def_SPL = HMV_RangeMax
                if Def_OutH == "" or self.thePlugin.isString(Def_OutH):
                    Def_OutH = Out_RangeMax
                elif float(Def_OutH) > float(Out_RangeMax):
                    Def_OutH = Out_RangeMax
                elif float(Def_OutH) < float(Out_RangeMin):
                    Def_OutH = Out_RangeMin
                if Def_OutL == "" or self.thePlugin.isString(Def_OutL):
                    Def_OutL = Out_RangeMin
                elif float(Def_OutL) < float(Out_RangeMin):
                    Def_OutL = Out_RangeMin
                elif float(Def_OutL) > float(Out_RangeMax):
                    Def_OutL = Out_RangeMax
                if Def_SP == "" or self.thePlugin.isString(Def_SP):
                    Def_SP = "0.0"

                # 9. write the instance information in the database file
                self.thePlugin.writeInstanceInfo("CPC_$DeviceTypeName$;$deviceNumber$;$Alias$$DeviceLinkList$;$Description$;$Diagnostics$;$WWWLink$;$Synoptic$;$AccessControlDomain$$Domain$;$Privileges$$Nature$;$WidgetType$;$Alias$;$HMV_Unit$;$HMV_Format$;$HMV_RangeMax$;$HMV_RangeMin$;$Out_Unit$;$Out_Format$;$Out_RangeMax$;$Out_RangeMin$;$ScaMethod$;$DriverDeadbandValueOut$;$DriverDeadbandTypeOut$;$DriverDeadbandValueHMV$;$DriverDeadbandTypeHMV$;$ArchiveModeOut$;$TimeFilterOut$;$ArchiveModeHMV$;$TimeFilterHMV$;$addr_StsReg01$;$addr_StsReg02$;$addr_EvStsReg01$;$addr_EvStsReg02$;$addr_ActSP$;$addr_ActSPL$;$addr_ActSPH$;$addr_OutOV$;$addr_ActOutL$;$addr_ActOutH$;$addr_MV$;$addr_MSPSt$;$addr_AuSPSt$;$addr_MPosRSt$;$addr_AuPosRSt$;$addr_ActKc$;$addr_ActTi$;$addr_ActTd$;$addr_ActTds$;$addr_ManReg01$;$addr_ManReg02$;$addr_MPosR$;$addr_MSP$;$addr_MSPL$;$addr_MSPH$;$addr_MOutL$;$addr_MOutH$;$addr_MKc$;$addr_MTi$;$addr_MTd$;$addr_MTds$;$BooleanArchive$;$AnalogArchive$;$EventArchive$;$MaskEvent$;$Def_Kc$;$Def_Ti$;$Def_Td$;$Def_Tds$;$Def_SP$;$Def_SPH$;$Def_SPL$;$Def_OutH$;$Def_OutL$;$Parameters$;$MasterDevice$;$parents$;$stringchildren$;$type$;$secondAlias$;")

    def end(self):
        self.thePlugin.writeInUABLog("end in Jython $self.theDeviceType$ template")

    def shutdown(self):
        self.thePlugin.writeInUABLog("shutdown in Jython $self.theDeviceType$ template")

    def getAddressSCADA(self, DPName, PLCType):
        if PLCType.lower() == "quantum":
            address = str(int(self.thePlugin.computeAddress(DPName)) - 1)
        else:
            address = str(self.thePlugin.computeAddress(DPName))

        return address
