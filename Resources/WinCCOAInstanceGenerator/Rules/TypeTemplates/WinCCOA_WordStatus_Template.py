# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for WordStatus Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin  # REQUIRED
from research.ch.cern.unicos.cpc.interfaces import ISCADAPlugin  # REQUIRED
from research.ch.cern.unicos.core import CoreManager  # REQUIRED
from java.lang import System
import WinCCOA_CommonMethods
from research.ch.cern.unicos.plugins.interfaces import AGenerationPlugin
import ucpc_library.shared_decorator
reload(ucpc_library.shared_decorator)
try:
    # Try to import the user privileges file
    import WinCCOA_Privileges
except:
    # If the previous import failed, try to import the privileges template file
    import WinCCOA_Privileges_Template


class WordStatus_Template(IUnicosTemplate):
    thePlugin = 0
    theDeviceType = "WordStatus"
    # Default name for the privileges file
    privilegesFileName = "WinCCOA_Privileges"

    def deviceFormat(self, type):
        if type == "WordStatus":
            return ["deviceType", "deviceNumber", "aliasDeviceLinkList", "description", "diagnostics", "wwwLink", "synoptic", "domain", "nature", "widgetType", "unit",
                    "format", "driverDeadbandValue", "driverDeadbandType", "rangeMax", "rangeMin", "archiveMode", "timeFilter", "addressPosSt", "pattern", "booleanArchive",
                    "analogArchive", "eventArchive", "parameters", "master", "parents", "children", "type", "secondAlias"]
        elif type == "Word2AnalogStatus":
            return ["deviceType", "deviceNumber", "aliasDeviceLinkList", "description", "diagnostics", "wwwLink", "synoptic", "domain", "nature", "widgetType", "unit",
                    "format", "driverDeadbandValue", "driverDeadbandType", "rangeMax", "rangeMin", "fRangeMax", "fRangeMin", "archiveMode", "timeFilter", "addressPosSt",
                    "booleanArchive", "analogArchive", "eventArchive", "parameters", "master", "parents", "children", "type", "secondAlias"]
        else:
            return []

    def initialize(self):
        self.thePlugin = APlugin.getPluginInterface()
        self.thePlugin.writeInUABLog("initialize in Jython $self.theDeviceType$ template")
        reload(WinCCOA_CommonMethods)
        self.decorator = ucpc_library.shared_decorator.ExpressionDecorator()
        try:
            # Try to reload the user privileges file
            reload(WinCCOA_Privileges)
        except:
            # If the reload failed, reload the privileges template file
            self.privilegesFileName = "WinCCOA_Privileges_Template"
            reload(WinCCOA_Privileges_Template)

    def check(self):
        self.thePlugin.writeInUABLog("check in Jython $self.theDeviceType$ template")

    def begin(self):
        self.thePlugin.writeInUABLog("begin in Jython $self.theDeviceType$ template")

    def get_alarm_message(self, instance):
        alarm_message = instance.getAttributeData("SCADADeviceAlarms:Message")
        if instance.getAttributeData("SCADADeviceAlarms:Alarm Config:SMS Category").replace(" ", "") != "" and alarm_message == "":
            alarm_message = "No message"
            self.thePlugin.writeWarningInUABLog("WS instance: " + instance.getAttributeData("DeviceIdentification:Name") + "An Alarm message is required: forced it to (%s)." % alarm_message)
        return alarm_message

    def process(self, *params):
        self.thePlugin.writeInUABLog("processInstances in Jython $self.theDeviceType$ template")
        theCurrentDeviceType = params[0]
        self.theUnicosProject = theRawInstances = self.thePlugin.getUnicosProject()
        strAllDeviceTypes = theRawInstances.getAllDeviceTypesString()
        theApplicationName = self.thePlugin.getApplicationParameter("ApplicationName")
        instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
        instancesNumber = str(len(instancesVector))
        DeviceTypeName = theCurrentDeviceType.getDeviceTypeName()
        deviceNumber = 0
        CRLF = System.getProperty("line.separator")

        # Get the config (UnicosApplication.xml)
        config = CoreManager.getITechnicalParameters().getXMLConfigMapper()
        # Query a PLC parameter
        PLCType = config.getPLCDeclarations().get(0).getPLCType().getValue()

        self.thePlugin.writeDBHeader("#$self.theDeviceType$: $instancesNumber$")
        DeviceTypeFormatWS = "CPC_" + DeviceTypeName + ";deviceNumber;Alias[,DeviceLinkList];Description;Diagnostics;WWWLink;Synoptic;Domain;Nature;WidgetType;Unit;Format;DriverDeadbandValue;DriverDeadbandType;RangeMax;RangeMin;ArchiveMode;TimeFilter;addr_PosSt;Pattern;BooleanArchive;AnalogArchive;EventArchive;Parameters;master;parents;children;type;secondAlias;"
        DeviceTypeFormatW2A = "CPC_Word2AnalogStatus;deviceNumber;Alias[,DeviceLinkList];Description;Diagnostics;WWWLink;Synoptic;Domain;Nature;WidgetType;Unit;Format;DriverDeadbandValue;DriverDeadbandType;RangeMax;RangeMin;FRangeMax;FRangeMin;ArchiveMode;TimeFilter;addr_PosSt;BooleanArchive;AnalogArchive;EventArchive;Parameters;master;parents;children;type;secondAlias;"
        self.thePlugin.writeComment("#$CRLF$# Object: " + DeviceTypeName + " and Word2AnalogStatus $CRLF$#$CRLF$#Config Line : " + DeviceTypeFormatWS + "$CRLF$#Config Line : " + DeviceTypeFormatW2A + "$CRLF$#")

        # Set Privileges
        Privileges = eval(self.privilegesFileName).getPrivileges(DeviceTypeName)

        for instance in instancesVector:
            if (instance is not None):

                # Check if user pressed 'cancel' button:
                if AGenerationPlugin.isGenerationInterrupted():
                    return

                deviceNumber = int(int(deviceNumber) + 1)
                deviceNumber = str(deviceNumber)

                # 1. Common Unicos fields
                # 2. Specific fields
                # 3. SCADA Device Data Archiving
                # 4. Data Treatment
                # 5. Addresses computation
                # 6. write the instance information in the database file

                # 1. Common Unicos fields
                name = Name = Alias = instance.getAttributeData("DeviceIdentification:Name")
                expertName = instance.getAttributeData("DeviceIdentification:Expert Name")
                Description = instance.getAttributeData("DeviceDocumentation:Description")
                Diagnostics = instance.getAttributeData("SCADADeviceGraphics:Diagnostic")
                WWWLink = instance.getAttributeData("SCADADeviceGraphics:WWW Link")
                Synoptic = instance.getAttributeData("SCADADeviceGraphics:Synoptic")
                WidgetType = instance.getAttributeData("SCADADeviceGraphics:Widget Type")
                Domain = instance.getAttributeData("SCADADeviceFunctionals:SCADADeviceClassificationTags:Domain")
                AccessControlDomain = instance.getAttributeData("SCADADeviceFunctionals:Access Control Domain").replace(" ", "")
                Nature = instance.getAttributeData("SCADADeviceFunctionals:SCADADeviceClassificationTags:Nature")
                DeviceLinkListSpec = instance.getAttributeData("SCADADeviceFunctionals:SCADADeviceClassificationTags:Device Links").replace(" ", "")

                # 2. Specific fields
                Unit = instance.getAttributeData("SCADADeviceGraphics:Unit").replace(" ", "")
                Format = instance.getAttributeData("SCADADeviceGraphics:Format").replace(" ", "")
                RangeMax = instance.getAttributeData("FEDeviceParameters:Range Max").replace(" ", "")
                RangeMin = instance.getAttributeData("FEDeviceParameters:Range Min").replace(" ", "")
                DriverDeadbandType = instance.getAttributeData("SCADADriverDataSmoothing:Deadband Type")
                DriverDeadbandValue = self.thePlugin.formatNumberPLC(instance.getAttributeData("SCADADriverDataSmoothing:Deadband Value"))
                pattern = instance.getAttributeData("SCADADeviceGraphics:Pattern")
                FRangeMin = instance.getAttributeData("SCADADeviceParameters:F Range Min")
                FRangeMax = instance.getAttributeData("SCADADeviceParameters:F Range Max")

                # 3. SCADA Device Data Archiving
                ArchiveMode = instance.getAttributeData("SCADADeviceDataArchiving:Archive Mode")
                TimeFilter = self.thePlugin.formatNumberPLC(instance.getAttributeData("SCADADeviceDataArchiving:Time Filter (s)"))
                DeadbandType = instance.getAttributeData("SCADADeviceDataArchiving:Deadband Type")
                DeadbandValue = self.thePlugin.formatNumberPLC(instance.getAttributeData("SCADADeviceDataArchiving:Deadband Value"))
                BooleanArchive = instance.getAttributeData("SCADADeviceDataArchiving:Boolean Archive")
                AnalogArchive = instance.getAttributeData("SCADADeviceDataArchiving:Analog Archive")
                EventArchive = instance.getAttributeData("SCADADeviceDataArchiving:Event Archive")

                # 4. Data Treatment

                S_DriverDeadbandType = DriverDeadbandType.lower()
                S_ArchiveMode = ArchiveMode.lower()
                S_DeadbandType = DeadbandType.lower()

                # Process pattern to remove no_status= and multiple_status= messages and set PNoStatus and PMultipleStatus parameters
                patternList = pattern.split(",") if (pattern != "") else []
                patternList = filter(lambda pair: pair.strip() != "", patternList)  # remove empty elements
                patternList = map(lambda pair: pair.split("="), patternList)  # split k=v
                for pair in patternList:
                    while len(pair) < 2:
                        pair.append("")
                    pair[0] = pair[0].lower().strip()  # format key
                    pair[1] = pair[1]                 # format value

                patternErrors = self.validateKeyValueList(patternList)
                for error in patternErrors:
                    self.thePlugin.writeErrorInUABLog("WS instance: $Alias$. pattern error: " + error)

                customLabels = filter(lambda pair: self.labelFilter(pair), patternList)  # extract custom labels
                patternList = filter(lambda pair: not self.labelFilter(pair), patternList)  # remove custom labels from pattern
                pattern = ",".join(map(lambda pair: "=".join(pair), patternList))

                if WidgetType != "WordStatusBit" and len(customLabels) > 0:
                    self.thePlugin.writeWarningInUABLog("WS instance: $Alias$. no_status= and multiple_status= parameters will only be active if WidgetType = WordStatusBit")

                if WidgetType == "Word2AnalogStatus":
                    if RangeMax == "":
                        RangeMax = "100.0"
                        self.thePlugin.writeWarningInUABLog("WS instance: $Alias$. Force the RangeMax:($RangeMax$).")
                    if RangeMin == "":
                        RangeMin = "0.0"
                        self.thePlugin.writeWarningInUABLog("WS instance: $Alias$. Force the RangeMin:($RangeMin$).")
                    if pattern != "":
                        self.thePlugin.writeWarningInUABLog("WS instance: $Alias$. Possible mistake, the Word2AnalogStatus does not require a pattern: ($pattern$).")
                    if Format.replace(" ", "") == "":
                        Format = "#####.#"
                        self.thePlugin.writeWarningInUABLog("WS instance: $Alias$. Format is missing, set as Format: ($Format$).")
                    if Format.find('.') == -1:
                        Format = "#####.#"
                        self.thePlugin.writeWarningInUABLog("WS instance: $Alias$. Format should be decimal, set Format: ($Format$).")
                    if int(float(RangeMax)) < int(float(RangeMin)):
                        self.thePlugin.writeWarningInUABLog("WS instance: $Alias$. The value Max:($RangeMax$) should be bigger than the Min:($RangeMin$).")
                    if FRangeMin == "":
                        FRangeMin = "0.0"
                        self.thePlugin.writeWarningInUABLog("WS instance: $Alias$. The A Factor is missing. W2AS Value In: :($FRangeMin$).")
                    if FRangeMax == "":
                        FRangeMax = "0.0"
                        self.thePlugin.writeWarningInUABLog("WS instance: $Alias$. The B Factor is missing. W2AS Value Out: :($FRangeMax$).")

                else:
                    # case "WordStatus" or   "WordStatusWide" or   "WordStatusBit" or   "WordStatusBitBig"
                    if pattern != "": 		# bit decoding
                        if RangeMax == "":
                            RangeMax = "65535"
                        if RangeMin == "":
                            RangeMin = "0"
                        if RangeMax != "65535" or RangeMin != "0":
                            self.thePlugin.writeWarningInUABLog("WS instance: $Alias$. Warning not all bit will be covered because the mask is RangeMax:($RangeMax$) rangeMin:($RangeMin$), it should be 0/65535.")
                        Format = "######"  # format is useless but WinCCOA require format, '-' is not accepted.
                        Unit = "-"
                    else:			# integer.
                        if RangeMax == "":
                            RangeMax = "100"
                            self.thePlugin.writeWarningInUABLog("WS instance: $Alias$. Force the RangeMax:($RangeMax$).")
                        if RangeMin == "":
                            RangeMin = "0"
                            self.thePlugin.writeWarningInUABLog("WS instance: $Alias$. Force the RangeMin:($RangeMin$).")
                        if Format.replace(" ", "") == "":
                            Format = "######"
                            self.thePlugin.writeWarningInUABLog("WS instance: $Alias$. Format is missing, set Format: ($Format$).")

                        if (Format.find('.') != -1) and WidgetType != "Word2AnalogStatus":
                            Format = "######"
                            self.thePlugin.writeWarningInUABLog("WS instance: $Alias$. Format ($WidgetType$) cannot be decimal, set Format: ($Format$).")

                        if int(float(RangeMax)) < int(float(RangeMin)):
                            self.thePlugin.writeWarningInUABLog("WS instance: $Alias$. The value Max:($RangeMax$) should be bigger than the Min:($RangeMin$).")

                # Build DeviceLinkList and children from related objects
                DeviceLinkList = ""

                # Append Device Link list from Spec
                DeviceLinkList = WinCCOA_CommonMethods.appendDeviceLinkListFromSpec(DeviceLinkListSpec, DeviceLinkList, self.thePlugin, strAllDeviceTypes)

                # Avoid duplications on the deviceLinks
                DeviceLinkList = WinCCOA_CommonMethods.removeDuplicatesAndGivenObjects(DeviceLinkList, [Name, expertName])

                # Default values if domain or nature empty
                if Domain == "":
                    Domain = theApplicationName

                if AccessControlDomain != "":
                    AccessControlDomain = AccessControlDomain + "|"

                if Nature == "":
                    Nature = "WS"

                # Time Filter
                if TimeFilter == "" and S_ArchiveMode != "no":
                    TimeFilter = "0"
                    if S_ArchiveMode == "time" or S_ArchiveMode == "old/new comparison and time" or S_ArchiveMode == "old/new comparison or time" or S_ArchiveMode == "deadband or time" or S_ArchiveMode == "deadband and time":
                        self.thePlugin.writeWarningInUABLog("WS instance: $Alias$. Archive Time Filter is required by the Archive Mode selected: forced to ($TimeFilter$).")

                # Driver Deadband Value
                if DriverDeadbandValue == "":
                    DriverDeadbandValue = "0.0"
                    if S_DriverDeadbandType == "absolute" or S_DriverDeadbandType == "relative":
                        self.thePlugin.writeWarningInUABLog("WS instance: $Alias$. Driver Deadband Value is required : forced to ($DriverDeadbandValue$).")

                # Deadband Value
                if DeadbandValue == "":
                    DeadbandValue = "0.0"
                    if S_DeadbandType == "absolute" or S_DeadbandType == "relative":
                        self.thePlugin.writeWarningInUABLog("WS instance: $Alias$. Archive Deadband Value is required : forced to ($DeadbandValue$).")

                # Default archive DB in case of Null
                if not (S_ArchiveMode == "no" or S_ArchiveMode == "old/new comparison" or S_ArchiveMode == "time" or S_ArchiveMode == "old/new comparison and time" or S_ArchiveMode == "old/new comparison or time"):
                    if S_DeadbandType == "" or DeadbandValue == "":
                        S_DeadbandType = "relative"
                        DeadbandValue = "5.0"
                        self.thePlugin.writeWarningInUABLog("WS instance: $Alias$. Archive Deadband Type and Value are required : forced to ($S_DeadbandType$ $DeadbandValue$ %).")

                # Driver Deadband versus Archive Deadband
                if S_DriverDeadbandType == "absolute" or S_DriverDeadbandType == "relative":
                    if float(DeadbandValue) < float(DriverDeadbandValue):
                        self.thePlugin.writeWarningInUABLog("WS instance: $Alias$. Driver Deadband ($DriverDeadbandValue$) should be lower than the Archive Deadband ($DeadbandValue$).")

                # Driver Smooting
                if (S_DriverDeadbandType == "no"):
                    DriverDeadbandType = str("0")
                elif (S_DriverDeadbandType == "absolute"):
                    DriverDeadbandType = str("1")
                elif (S_DriverDeadbandType == "relative"):
                    DriverDeadbandType = str("2")
                elif (S_DriverDeadbandType == "old/new"):
                    DriverDeadbandType = str("3")
                else:
                    DriverDeadbandType = str("0")

                # Archiving Mode
                if (S_ArchiveMode == "no"):
                    ArchiveMode = str("N")
                elif (S_ArchiveMode == "deadband" and S_DeadbandType == "absolute"):
                    ArchiveMode = str("VA,$DeadbandValue$,N")
                elif (S_ArchiveMode == "deadband" and S_DeadbandType == "relative"):
                    ArchiveMode = str("VR,$DeadbandValue$,N")
                elif (S_ArchiveMode == "time"):
                    ArchiveMode = str("Y")
                elif (S_ArchiveMode == "deadband and time" and S_DeadbandType == "absolute"):
                    ArchiveMode = str("VA,$DeadbandValue$,A")
                elif (S_ArchiveMode == "deadband and time" and S_DeadbandType == "relative"):
                    ArchiveMode = str("VR,$DeadbandValue$,A")
                elif (S_ArchiveMode == "deadband or time" and S_DeadbandType == "absolute"):
                    ArchiveMode = str("VA,$DeadbandValue$,O")
                elif (S_ArchiveMode == "deadband or time" and S_DeadbandType == "relative"):
                    ArchiveMode = str("VR,$DeadbandValue$,O")
                elif (S_ArchiveMode == "old/new comparison"):
                    ArchiveMode = str("O")
                    TimeFilter = "0"
                elif (S_ArchiveMode == "old/new comparison and time"):
                    ArchiveMode = str("A")
                elif (S_ArchiveMode == "old/new comparison or time"):
                    ArchiveMode = str("O")
                else:
                    ArchiveMode = str("N")

                # 5. Addresses computation
                addr_PosSt = self.getAddressSCADA("$Alias$_PosSt", PLCType)

                # 6. New relationship information in all objects:
                master = ""
                children = ""
                type = ""
                parents = []

                # Digital Alarm parents
                parents += self.thePlugin.getRIndex().get(Name, "DigitalAlarm", "FEDeviceEnvironmentInputs:Input").split(',')
                # Analog Alarm parents
                parents += self.thePlugin.getRIndex().get(Name, "AnalogAlarm", "FEDeviceAlarm:Enable Condition").split(',')
                parents = [x for x in parents if x]  # remove empty strings

                uniqueList = []
                for parent in parents:
                    if parent not in uniqueList:
                        uniqueList.append(parent)

                parents = uniqueList
                parents = [WinCCOA_CommonMethods.getExpertName(self.theUnicosProject.findInstanceByName(parent)) for parent in parents]
                stringParents = ",".join(parents)

                # Expert Name Logic
                S_Name = name.strip()
                S_ExpertName = expertName.strip()
                if S_ExpertName == "":
                    secondAlias = Alias
                else:
                    secondAlias = Alias
                    Alias = S_ExpertName

                # 7. Parameters field for all objects
                ParametersArray = []
                customLabels = dict(customLabels)
                if "no_status" in customLabels:
                    ParametersArray.append("PNoStatus=" + customLabels["no_status"])
                if "multiple_status" in customLabels:
                    ParametersArray.append("PMultipleStatus=" + customLabels["multiple_status"])
                
                alarm_values_low = instance.getAttributeData("SCADADeviceAlarms:Alarm Values:Low").replace(" ", "").replace(",", "|")
                if alarm_values_low:
                    ParametersArray.append("ALARM_VALUES_LOW=" + alarm_values_low)
                
                alarm_values_medium = instance.getAttributeData("SCADADeviceAlarms:Alarm Values:Medium").replace(" ", "").replace(",", "|")
                if alarm_values_medium:
                    ParametersArray.append("ALARM_VALUES_MEDIUM=" + alarm_values_medium)
                    
                alarm_values_high = instance.getAttributeData("SCADADeviceAlarms:Alarm Values:High").replace(" ", "").replace(",", "|")
                if alarm_values_high:
                    ParametersArray.append("ALARM_VALUES_HIGH=" + alarm_values_high)
                    
                alarm_values_safety = instance.getAttributeData("SCADADeviceAlarms:Alarm Values:Safety").replace(" ", "").replace(",", "|")
                if alarm_values_safety:
                    ParametersArray.append("ALARM_VALUES_SAFETY=" + alarm_values_safety)
                    
                alarm_sms = instance.getAttributeData("SCADADeviceAlarms:Alarm Config:SMS Category").replace(" ", "").replace(",", "|")
                if alarm_sms:
                    ParametersArray.append("ALARM_SMS=" + alarm_sms)
                    
                # append ALARM_ACK=false only if alarm_auto_ack is true (ack=false)
                alarm_auto_ack = instance.getAttributeData("SCADADeviceAlarms:Alarm Config:Auto Acknowledge").replace(" ", "").lower()
                if alarm_auto_ack == "true":
                    ParametersArray.append("ALARM_ACK=false")
                    
                # append ALARM_ACTIVE=false only if alarm_masked is true
                alarm_masked = instance.getAttributeData("SCADADeviceAlarms:Alarm Config:Masked").replace(" ", "").lower()
                if alarm_masked == 'true':
                    ParametersArray.append("ALARM_ACTIVE=false")
                
                alarm_message= self.get_alarm_message(instance)
                if alarm_message:
                    if ',' in alarm_message:
                        self.thePlugin.writeWarningInUABLog("WS instance: " + instance.getAttributeData("DeviceIdentification:Name") + ": Alarm message contains commas. They will be removed.")
                        alarm_message = alarm_message.replace(",", " ")
                    ParametersArray.append("ALARM_MESSAGE=" + alarm_message)
                
                Parameters = ",".join(ParametersArray)

                # 8. write the instance information in the database file
                if WidgetType == "Word2AnalogStatus":
                    self.thePlugin.writeInstanceInfo("CPC_Word2AnalogStatus;$deviceNumber$;$Alias$$DeviceLinkList$;$Description$;$Diagnostics$;$WWWLink$;$Synoptic$;$AccessControlDomain$$Domain$;$Privileges$$Nature$;$WidgetType$;$Unit$;$Format$;$DriverDeadbandValue$;$DriverDeadbandType$;$RangeMax$;$RangeMin$;$FRangeMax$;$FRangeMin$;$ArchiveMode$;$TimeFilter$;$addr_PosSt$;$BooleanArchive$;$AnalogArchive$;$EventArchive$;$Parameters$;$master$;$stringParents$;$children$;$type$;$secondAlias$;")
                else:
                    self.thePlugin.writeInstanceInfo("CPC_$DeviceTypeName$;$deviceNumber$;$Alias$$DeviceLinkList$;$Description$;$Diagnostics$;$WWWLink$;$Synoptic$;$AccessControlDomain$$Domain$;$Privileges$$Nature$;$WidgetType$;$Unit$;$Format$;$DriverDeadbandValue$;$DriverDeadbandType$;$RangeMax$;$RangeMin$;$ArchiveMode$;$TimeFilter$;$addr_PosSt$;$pattern$;$BooleanArchive$;$AnalogArchive$;$EventArchive$;$Parameters$;$master$;$stringParents$;$children$;$type$;$secondAlias$;")

    def end(self):
        self.thePlugin.writeInUABLog("end in Jython $self.theDeviceType$ template")

    def shutdown(self):
        self.thePlugin.writeInUABLog("shutdown in Jython $self.theDeviceType$ template")

    def getAddressSCADA(self, DPName, PLCType):
        if PLCType.lower() == "quantum":
            address = str(int(self.thePlugin.computeAddress(DPName)) - 1)
        else:
            address = str(self.thePlugin.computeAddress(DPName))

        return address

    def validateKeyValueList(self, list):
        errors = []
        for pair in list:
            if len(pair) > 2:
                errors.append("wrong format '" + "=".join(pair) + "'")
            else:
                key, value = pair
                if key == "":
                    errors.append("key should not be empty: =" + value)
                elif value == "":
                    errors.append("missing value for key: " + key + "=")
        keys = map(lambda pair: pair[0], list)
        if len(keys) != len(set(keys)):
            errors.append("contains duplicate keys: " + str(list))
        return errors

    def testValidateKeyValueList(self):
        assert self.validateKeyValueList("1=a,2=b".split(",")) == []
        assert self.validateKeyValueList("1=a,=b".split(",")) == ["key should not be empty"]
        assert self.validateKeyValueList("1=a, 2=b".split(",")) == ["key should not contain spaces"]
        assert self.validateKeyValueList("1=a, 2 =b".split(",")) == ["key should not contain spaces"]
        assert self.validateKeyValueList("1=a,2 =b".split(",")) == ["key should not contain spaces"]
        assert self.validateKeyValueList("1=a,2=".split(",")) == ["missed value for key 2"]
        assert self.validateKeyValueList("1=a,1=b".split(",")) == ["description contains key's duplicates"]

    def labelFilter(self, pair):
        return pair[0] == "no_status" or pair[0] == "multiple_status"
