# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for OnOff Objects.
from research.ch.cern.unicos.templateshandling import IUnicosTemplate  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import APlugin  # REQUIRED
from research.ch.cern.unicos.plugins.interfaces import IPlugin  # REQUIRED
from research.ch.cern.unicos.cpc.interfaces import ISCADAPlugin  # REQUIRED
from research.ch.cern.unicos.core import CoreManager  # REQUIRED
from java.lang import System
import WinCCOA_CommonMethods
from research.ch.cern.unicos.plugins.interfaces import AGenerationPlugin
from research.ch.cern.unicos.utilities import MasterDeviceUtils
import ucpc_library.shared_decorator
reload(ucpc_library.shared_decorator)
try:
    # Try to import the user privileges file
    import WinCCOA_Privileges
except:
    # If the previous import failed, try to import the privileges template file
    import WinCCOA_Privileges_Template


class OnOff_Template(IUnicosTemplate):
    thePlugin = 0
    theDeviceType = "OnOff"
    # Default name for the privileges file
    privilegesFileName = "WinCCOA_Privileges"

    def deviceFormat(self):
        return ["deviceType", "deviceNumber", "aliasDeviceLinkList", "description", "diagnostics", "wwwLink", "synoptic", "domain", "nature", "widgetType",
                "normalPosition", "addressStsReg01", "addressStsReg02", "addressEvStsReg01", "addressEvStsReg02", "addressManReg01", "booleanArchive", "analogArchive",
                "eventArchive", "maskEvent", "labelOn", "labelOff", "parameters", "master", "parents", "children", "type", "secondAlias"]

    def initialize(self):
        self.thePlugin = APlugin.getPluginInterface()
        self.thePlugin.writeInUABLog("initialize in Jython $self.theDeviceType$ template")
        reload(WinCCOA_CommonMethods)
        self.decorator = ucpc_library.shared_decorator.ExpressionDecorator()
        try:
            # Try to reload the user privileges file
            reload(WinCCOA_Privileges)
        except:
            # If the reload failed, reload the privileges template file
            self.privilegesFileName = "WinCCOA_Privileges_Template"
            reload(WinCCOA_Privileges_Template)

    def check(self):
        self.thePlugin.writeInUABLog("check in Jython $self.theDeviceType$ template")

    def begin(self):
        self.thePlugin.writeInUABLog("begin in Jython $self.theDeviceType$ template")

    def process(self, *params):
        self.thePlugin.writeInUABLog("processInstances in Jython $self.theDeviceType$ template")
        theCurrentDeviceType = params[0]
        self.theUnicosProject = theRawInstances = self.thePlugin.getUnicosProject()
        strAllDeviceTypes = theRawInstances.getAllDeviceTypesString()
        theApplicationName = self.thePlugin.getApplicationParameter("ApplicationName")
        instancesVector = theCurrentDeviceType.getAllDeviceTypeInstances()
        instancesNumber = str(len(instancesVector))
        DeviceTypeName = theCurrentDeviceType.getDeviceTypeName()
        deviceNumber = 0
        CRLF = System.getProperty("line.separator")

        # Get the config (UnicosApplication.xml)
        config = CoreManager.getITechnicalParameters().getXMLConfigMapper()
        # Query a PLC parameter
        PLCType = config.getPLCDeclarations().get(0).getPLCType().getValue()

        self.thePlugin.writeDBHeader("#$self.theDeviceType$: $instancesNumber$")
        DeviceTypeFormat = "CPC_" + DeviceTypeName + ";deviceNumber;Alias[,DeviceLinkList];Description;Diagnostics;WWWLink;Synoptic;Domain;Nature;WidgetType;NormalPosition;addr_StsReg01;addr_StsReg02;addr_EvStsReg01;addr_EvStsReg02;addr_ManReg01;BooleanArchive;AnalogArchive;EventArchive;MaskEvent;LabelOn;LabelOff;Parameters;Master;Parents;children;Type;SecondAlias;"
        self.thePlugin.writeComment("#$CRLF$# Object: " + DeviceTypeName + "$CRLF$#$CRLF$#Config Line : " + DeviceTypeFormat + "$CRLF$#")

        # Set Privileges
        Privileges = eval(self.privilegesFileName).getPrivileges(DeviceTypeName)

        for instance in instancesVector:
            if (instance is not None):

                # Check if user pressed 'cancel' button:
                if AGenerationPlugin.isGenerationInterrupted():
                    return

                deviceNumber = int(int(deviceNumber) + 1)
                deviceNumber = str(deviceNumber)

                # 1. Common Unicos fields
                # 2. Specific fields
                # 3. SCADA Device Data Archiving
                # 4. Data Treatment
                # 5. Addresses computation
                # 6. write the instance information in the database file
                # 7. fill the parameters field

                # 1. Common Unicos fields
                name = Name = Alias = instance.getAttributeData("DeviceIdentification:Name")
                expertName = instance.getAttributeData("DeviceIdentification:Expert Name")
                Description = instance.getAttributeData("DeviceDocumentation:Description")
                Diagnostics = instance.getAttributeData("SCADADeviceGraphics:Diagnostic")
                WWWLink = instance.getAttributeData("SCADADeviceGraphics:WWW Link")
                Synoptic = instance.getAttributeData("SCADADeviceGraphics:Synoptic")
                WidgetType = instance.getAttributeData("SCADADeviceGraphics:Widget Type")
                Domain = instance.getAttributeData("SCADADeviceFunctionals:SCADADeviceClassificationTags:Domain")
                AccessControlDomain = instance.getAttributeData("SCADADeviceFunctionals:Access Control Domain").replace(" ", "")
                Nature = instance.getAttributeData("SCADADeviceFunctionals:SCADADeviceClassificationTags:Nature")
                DeviceLinkListSpec = instance.getAttributeData("SCADADeviceFunctionals:SCADADeviceClassificationTags:Device Links").replace(" ", "")

                # 2. Specific fields
                PFSPosOn = FailSafe = instance.getAttributeData("FEDeviceParameters:ParReg:Fail-Safe")
                PHFOn = FeedbackOn = instance.getAttributeData("FEDeviceEnvironmentInputs:Feedback On")
                PHFOff = FeedbackOff = instance.getAttributeData("FEDeviceEnvironmentInputs:Feedback Off")
                LocalDrive = instance.getAttributeData("FEDeviceEnvironmentInputs:Local Drive").strip()
                if LocalDrive <> "":
                    if LocalDrive[0:4].lower() == "not ":
                        LocalDrive = LocalDrive[4:]
                LocalOn = instance.getAttributeData("FEDeviceEnvironmentInputs:Local On")
                if LocalOn <> "":
                    if LocalOn[0:4].lower() == "not ":
                        LocalOn = LocalOn[4:]
                LocalOff = instance.getAttributeData("FEDeviceEnvironmentInputs:Local Off")
                if LocalOff <> "":
                    if LocalOff[0:4].lower() == "not ":
                        LocalOff = LocalOff[4:]
                ProcessOutput = instance.getAttributeData("FEDeviceOutputs:Process Output")
                ProcessOutputOff = instance.getAttributeData("FEDeviceOutputs:Process Output Off")
                MasterDevice = instance.getAttributeData("LogicDeviceDefinitions:Master")
                ExternalMasterDevice = instance.getAttributeData("LogicDeviceDefinitions:External Master")
                MaskEvent = instance.getAttributeData("SCADADeviceFunctionals:Mask Event")
                NormalPosition = "0"
                WarningDelay = self.thePlugin.formatNumberPLC(instance.getAttributeData("FEDeviceParameters:Warning Time Delay (s)"))

                # 3. SCADA Device Data Archiving
                BooleanArchive = instance.getAttributeData("SCADADeviceDataArchiving:Boolean Archive")
                AnalogArchive = instance.getAttributeData("SCADADeviceDataArchiving:Analog Archive")
                EventArchive = instance.getAttributeData("SCADADeviceDataArchiving:Event Archive")

                # 4. Data Treatment
                # Build DeviceLinkList and children from related objects
                DeviceLinkList = ""
                # MasterDevice
                if (MasterDevice != ""):
                    MasterDevice = MasterDeviceWinCCOAName = self.thePlugin.getLinkedExpertName(MasterDevice, ", ".join(MasterDeviceUtils.getAllMasterTypes(config)))
                    DeviceLinkList = self.thePlugin.appendLinkedDevice(DeviceLinkList, MasterDeviceWinCCOAName)
                elif (ExternalMasterDevice != ""):
                    MasterDevice = MasterDeviceWinCCOAName = ExternalMasterDevice
                    DeviceLinkList = self.thePlugin.appendLinkedDevice(DeviceLinkList, MasterDeviceWinCCOAName)

                children = []
                # feedback On
                if (FeedbackOn != ""):
                    FeedbackOnWinCCOAName = self.thePlugin.getLinkedExpertName(FeedbackOn, "DigitalInput")
                    DeviceLinkList = self.thePlugin.appendLinkedDevice(DeviceLinkList, FeedbackOnWinCCOAName)
                    children.append(FeedbackOnWinCCOAName)

                # feedback Off
                if (FeedbackOff != ""):
                    FeedbackOffWinCCOAName = self.thePlugin.getLinkedExpertName(FeedbackOff, "DigitalInput")
                    DeviceLinkList = self.thePlugin.appendLinkedDevice(DeviceLinkList, FeedbackOffWinCCOAName)
                    children.append(FeedbackOffWinCCOAName)

                # Local Drive
                if (LocalDrive != ""):
                    LocalDriveWinCCOAName = self.thePlugin.getLinkedExpertName(LocalDrive, "DigitalInput")
                    DeviceLinkList = self.thePlugin.appendLinkedDevice(DeviceLinkList, LocalDriveWinCCOAName)
                    children.append(LocalDriveWinCCOAName)

                # Local On
                if (LocalOn != ""):
                    LocalOnWinCCOAName = self.thePlugin.getLinkedExpertName(LocalOn, "DigitalInput")
                    DeviceLinkList = self.thePlugin.appendLinkedDevice(DeviceLinkList, LocalOnWinCCOAName)
                    children.append(LocalOnWinCCOAName)

                # Local Off
                if (LocalOff != ""):
                    LocalOffWinCCOAName = self.thePlugin.getLinkedExpertName(LocalOff, "DigitalInput")
                    DeviceLinkList = self.thePlugin.appendLinkedDevice(DeviceLinkList, LocalOffWinCCOAName)
                    children.append(LocalOffWinCCOAName)

                # Process Output
                if (ProcessOutput != ""):
                    ProcessOutputWinCCOAName = self.thePlugin.getLinkedExpertName(ProcessOutput, "DigitalOutput")
                    DeviceLinkList = self.thePlugin.appendLinkedDevice(DeviceLinkList, ProcessOutputWinCCOAName)
                    children.append(ProcessOutputWinCCOAName)

                # Process Output Off
                if (ProcessOutputOff != ""):
                    ProcessOutputOffWinCCOAName = self.thePlugin.getLinkedExpertName(ProcessOutputOff, "DigitalOutput")
                    DeviceLinkList = self.thePlugin.appendLinkedDevice(DeviceLinkList, ProcessOutputOffWinCCOAName)
                    children.append(ProcessOutputOffWinCCOAName)

                # UCPC-1400 Add DeviceLink list and children for new object links
                if self.thePlugin.isString(WarningDelay) and WarningDelay <> "":
                    DeviceLinkList, children = WinCCOA_CommonMethods.updateDeviceLinksAndChildren(WarningDelay, DeviceLinkList, children, self.thePlugin, self.decorator)

                # Append Device Link list from Spec
                DeviceLinkList = WinCCOA_CommonMethods.appendDeviceLinkListFromSpec(DeviceLinkListSpec, DeviceLinkList, self.thePlugin, strAllDeviceTypes)

                # Avoid duplications on the deviceLinks
                DeviceLinkList = WinCCOA_CommonMethods.removeDuplicatesAndGivenObjects(DeviceLinkList, [Name, expertName])

                # Default values if domain or nature empty
                if Domain == "":
                    Domain = theApplicationName

                if AccessControlDomain != "":
                    AccessControlDomain = AccessControlDomain + "|"

                if Nature == "":
                    Nature = DeviceTypeName

                # Mask Value
                if (MaskEvent.lower() == "true"):
                    MaskEvent = "0"
                else:
                    MaskEvent = "1"

                # 5. Addresses computation
                addr_StsReg01 = self.getAddressSCADA("$Alias$_StsReg01", PLCType)
                addr_StsReg02 = self.getAddressSCADA("$Alias$_StsReg02", PLCType)
                addr_EvStsReg01 = self.getAddressSCADA("$Alias$_EvStsReg01", PLCType)
                addr_EvStsReg02 = self.getAddressSCADA("$Alias$_EvStsReg02", PLCType)
                addr_ManReg01 = self.getAddressSCADA("$Alias$_ManReg01", PLCType)

                # Labels
                LabelOn = instance.getAttributeData("SCADADeviceGraphics:Label On").strip()
                LabelOff = instance.getAttributeData("SCADADeviceGraphics:Label Off").strip()

                # 6. New relationship information in all objects:
                PEnRstart = instance.getAttributeData("FEDeviceParameters:ParReg:Manual Restart after Full Stop").strip()
                parents = ""
                type = ""

                # AA, DA Children
                AlarmChildren = self.theUnicosProject.findMatchingInstances("AnalogAlarm, DigitalAlarm", "$Name$", "")
                for AlarmChild in AlarmChildren:
                    AlarmChild_Name = WinCCOA_CommonMethods.getExpertName(AlarmChild)
                    children.append(AlarmChild_Name)

                uniqueList = []
                for child in children:
                    if child not in uniqueList:
                        uniqueList.append(child)
                children = uniqueList
                stringchildren = ",".join(children)

                # Expert Name Logic
                S_Name = name.strip()
                S_ExpertName = expertName.strip()
                if S_ExpertName == "":
                    secondAlias = Alias
                else:
                    secondAlias = Alias
                    Alias = S_ExpertName

                # 7. Parameters field
                PPulseSt = instance.getAttributeData("FEDeviceParameters:Pulse Duration (s)")
                PAnimSt = instance.getAttributeData("FEDeviceParameters:ParReg:Full/Empty Animation")
                ProcessOutputOff = instance.getAttributeData("FEDeviceOutputs:Process Output Off")

                ParamPFSPosOn = "PFSPosOn=TRUE"
                Parameters = ""
                if PFSPosOn.lower() == "on/open":
                    ParamPFSPosOn = "PFSPosOn=TRUE"
                elif PFSPosOn.lower() == "off/close":
                    ParamPFSPosOn = "PFSPosOn=FALSE"
                elif PFSPosOn.lower() == "2 do off":
                    ParamPFSPosOn = "PFSPosOn=2DOOFF"
                elif PFSPosOn.lower() == "2 do on":
                    ParamPFSPosOn = "PFSPosOn=2DOON"
                if PHFOn.strip() == "":
                    ParamPHFOn = "PHFOnSt=FALSE"
                else:
                    ParamPHFOn = "PHFOnSt=TRUE"
                if PHFOff.strip() == "":
                    ParamPHFOff = "PHFOffSt=FALSE"
                else:
                    ParamPHFOff = "PHFOffSt=TRUE"
                if PPulseSt.strip() == "" or PPulseSt == "0":
                    ParamPPulseSt = "PPulseSt=FALSE"
                else:
                    ParamPPulseSt = "PPulseSt=TRUE"
                if PEnRstart.lower() == "false":
                    ParamPEnRstart = "PEnRstart=FALSE"
                    ParamPRstartFS = "PRstartFS=FALSE"
                elif (PEnRstart.lower() == "true only if full stop disappeared"):
                    ParamPEnRstart = "PEnRstart=TRUE"
                    ParamPRstartFS = "PRstartFS=FALSE"
                else:
                    ParamPEnRstart = "PEnRstart=TRUE"
                    ParamPRstartFS = "PRstartFS=TRUE"
                if ProcessOutputOff.strip() == "":
                    ParamProcessOutputOff = "POutOff=FALSE"
                else:
                    ParamProcessOutputOff = "POutOff=TRUE"
                if (LocalDrive.strip() != ""):
                    ParamPHLDrive = "PHLDrive=TRUE"
                else:
                    ParamPHLDrive = "PHLDrive=FALSE"
                if self.thePlugin.isString(WarningDelay) and WarningDelay <> "":
                    ParamWarningDelay = ",WARNING_DELAY_TIME=" + self.thePlugin.getLinkedExpertName(WarningDelay)
                else:
                    ParamWarningDelay = ""

                Parameters = ParamPFSPosOn + "," + ParamPHFOn + "," + ParamPHFOff + "," + ParamPPulseSt + "," + ParamPEnRstart + "," + ParamPRstartFS + "," + ParamProcessOutputOff + "," + ParamPHLDrive + ParamWarningDelay

                # 8. write the instance information in the database file
                self.thePlugin.writeInstanceInfo("CPC_$DeviceTypeName$;$deviceNumber$;$Alias$$DeviceLinkList$;$Description$;$Diagnostics$;$WWWLink$;$Synoptic$;$AccessControlDomain$$Domain$;$Privileges$$Nature$;$WidgetType$;$NormalPosition$;$addr_StsReg01$;$addr_StsReg02$;$addr_EvStsReg01$;$addr_EvStsReg02$;$addr_ManReg01$;$BooleanArchive$;$AnalogArchive$;$EventArchive$;$MaskEvent$;$LabelOn$;$LabelOff$;$Parameters$;$MasterDevice$;$parents$;$stringchildren$;$type$;$secondAlias$;")

    def end(self):
        self.thePlugin.writeInUABLog("end in Jython $self.theDeviceType$ template")

    def shutdown(self):
        self.thePlugin.writeInUABLog("shutdown in Jython $self.theDeviceType$ template")

    def getAddressSCADA(self, DPName, PLCType):
        if PLCType.lower() == "quantum":
            address = str(int(self.thePlugin.computeAddress(DPName)) - 1)
        else:
            address = str(self.thePlugin.computeAddress(DPName))

        return address
