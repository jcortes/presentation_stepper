# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
from research.ch.cern.unicos.templateshandling import IUnicosTemplate
from research.ch.cern.unicos.plugins.interfaces import APlugin
from time import strftime
from java.lang import System


class ApplicationGeneral_Template(IUnicosTemplate):
    thePlugin = 0
    isDataValid = 1

    def initialize(self):
        self.thePlugin = APlugin.getPluginInterface()
        self.thePlugin.writeInUABLog("Application Generation rules: initialize")

    def check(self):
        self.thePlugin.writeInUABLog("Application Generation rules: check")

    def begin(self):
        self.thePlugin.writeInUABLog("Application Generation rules: begin")

    def process(self, *params):
        self.thePlugin.writeInUABLog("Application Generation rules: processApplicationData")
        theXMLConfig = params[0]
        CRLF = System.getProperty("line.separator")

        theApplicationName = str(theXMLConfig.getApplicationParameter("GeneralData:ApplicationName"))

        # Get copy of instances, in order to be able to extract resource package version
        theOverallInstances = self.thePlugin.getUnicosProject()
        specVersion = theOverallInstances.getProjectDocumentation().getSpecsVersion()
        self.thePlugin.writeDBHeader("#$CRLF$# Spec version used for this generation: $specVersion$")
        self.thePlugin.writeDBHeader("#$CRLF$# Generated Objects:")

        thePlcDeclarations = theXMLConfig.getPLCDeclarations()

        for thePlcDeclaration in thePlcDeclarations:
            if (thePlcDeclaration is not None):
                thePlcName = str(thePlcDeclaration.getPLCName().getValue())
                if (thePlcName == ""):
                    thePlcName = str(thePlcDeclaration.getPLCName().getDefaultValue())

                thePlcType = str(thePlcDeclaration.getPLCType().getValue())

                # MODBUS Unit Address
                thePlcModBusAddress = str(theXMLConfig.getPLCParameter("SchneiderSpecificParameters:ModbusParameters:ModbusAddress"))

                # PLC IP Address
                thePlcIPAddress = str(theXMLConfig.getPLCParameter("SchneiderSpecificParameters:EthernetParameters:IpAddressPlc"))

                # PLC communication parameters
                theSendIPAddress = "70"
                theCounterAddress = str(theXMLConfig.getPLCParameter("SchneiderSpecificParameters:AddressesCalculation:TransmitBufferAddress"))
                theCommandInterfaceAddress = "0"

                # set -1 for quantum PLC
                if thePlcType.lower() == "quantum":
                    theCounterAddress = str(int(theCounterAddress) - 1)

                # Version parameters (fixed location)
                if thePlcType.lower() == "quantum":
                    theBaselineVersionAddress = "1055"
                    theApplicationVersionAddress = "1053"
                    theResourcePackageVersionMajorAddress = "1049"
                    theResourcePackageVersionMinorAddress = "1050"
                    theResourcePackageVersionSmallAddress = "1051"
                else:
                    theBaselineVersionAddress = "1056"
                    theApplicationVersionAddress = "1054"
                    theResourcePackageVersionMajorAddress = "1050"
                    theResourcePackageVersionMinorAddress = "1051"
                    theResourcePackageVersionSmallAddress = "1052"

                # Resource Package Version string
                theResourcePackageVersionString = self.thePlugin.getResourcesVersion()

                self.thePlugin.writeDeleteStatement("# If the user needs to delete the DB, the DELETE keyword can be used as follows:")
                self.thePlugin.writeDeleteStatement("# ")
                self.thePlugin.writeDeleteStatement("# uncomment the line below to delete all the devices and the front-end, $thePlcName$")
                self.thePlugin.writeDeleteStatement("#Delete;" + thePlcName + ";")
                self.thePlugin.writeDeleteStatement("# uncomment the line below to delete all the devices of the front-end, $thePlcName$, of front-end application, $theApplicationName$")
                self.thePlugin.writeDeleteStatement("#Delete;" + thePlcName + ";" + theApplicationName + ";")
                self.thePlugin.writeDeleteStatement("# uncomment the line below to delete all the devices of the front-end, $thePlcName$ of front-end application, $theApplicationName$, of type CPC_AnalogInput, for example.")
                self.thePlugin.writeDeleteStatement("#Delete;" + thePlcName + ";" + theApplicationName + ";CPC_AnalogInput;")
                self.thePlugin.writeDeleteStatement("# uncomment the line below to delete all the devices of the front-end, $thePlcName$ of front-end application, $theApplicationName$, of type CPC_AnalogInput and a given device number, with leading zeros, i.e. 00018 will delete device 18")
                self.thePlugin.writeDeleteStatement("#Delete;" + thePlcName + ";" + theApplicationName + ";CPC_AnalogInput;00018;")
                self.thePlugin.writeDeleteStatement("# ")
                self.thePlugin.writeDeleteStatement("# For more information, see documentation of DELETE here: https://edms.cern.ch/file/1176169/6.0.0/unicos-pvss-unCore-data-flow.pdf#page=25")
                self.thePlugin.writeDeleteStatement("# ")

                # const unsigned UN_CONFIG_UNPLC_LENGTH_CPC6 = 14;
                # const unsigned UN_CONFIG_PLC_TYPE = 1; // NEW: (PROTOCOL,PLCMODEL) where PROTOCOL=MODBUS or S7 and PLCMODEL=free text; OLD: PLC type = "PREMIUM","QUANTUM","S7-300","S7-400"
                # const unsigned UN_CONFIG_PLC_NAME = 2; // PLC name
                # const unsigned UN_CONFIG_PLC_SUBAPPLICATION = 3; // PLC subApplication
                # const unsigned UN_CONFIG_PLC_NUMBER = 4; // PLC number
                # const unsigned UN_CONFIG_UNPLC_CPC6_IP = 5; // PLC IP address
                # const unsigned UN_CONFIG_UNPLC_CPC6_ADDRESS_SEND_IP = 6; // Address in Plc for send IP
                # const unsigned UN_CONFIG_UNPLC_CPC6_ADDRESS_COUNTER = 7; // Address in Plc for counter
                # const unsigned UN_CONFIG_UNPLC_CPC6_ADDRESS_CMD = 8; // Address in Plc for the command interface
                # const unsigned UN_CONFIG_UNPLC_CPC6_PLC_BASELINE_ADDRESS = 9; // PLC baseline version
                # const unsigned UN_CONFIG_UNPLC_CPC6_PLC_APPLICATION_ADDRESS = 10; // PLC application version
                # const unsigned UN_CONFIG_UNPLC_CPC6_RESPACK_MAJOR_VERSION_ADDRESS = 11; // (TSPP address word) WORD, Addressing to read the major version of the Resource Package
                # const unsigned UN_CONFIG_UNPLC_CPC6_RESPACK_MINOR_VERSION_ADDRESS = 12; // (TSPP address word) WORD, Addressing to read the minor version of the Resource Package
                # const unsigned UN_CONFIG_UNPLC_CPC6_RESPACK_SMALL_VERSION_ADDRESS= 13; // (TSPP address word) WORD, Addressing to read the small version of the Resource Package
                # const unsigned UN_CONFIG_UNPLC_CPC6_RESPACK_VERSION = 14; // (String) : version of the Resource Package employed

                self.thePlugin.writeComment("#Config Line : PLCCONFIG;MODBUS,thePlcType;thePlcName;theApplicationName;thePlcModBusAddress;thePlcIPAddress;theSendIPAddress;theCounterAddress;theCommandInterfaceAddress;theBaselineVersionAddress;theApplicationVersionAddress;theResourcePackageVersionMajorAddress;theResourcePackageVersionMinorAddress;theResourcePackageVersionSmallAddress;theResourcePackageVersionString;")
                self.thePlugin.writePlcDeclaration("PLCCONFIG;MODBUS,$thePlcType$;$thePlcName$;$theApplicationName$;$thePlcModBusAddress$;$thePlcIPAddress$;$theSendIPAddress$;$theCounterAddress$;$theCommandInterfaceAddress$;$theBaselineVersionAddress$;$theApplicationVersionAddress$;$theResourcePackageVersionMajorAddress$;$theResourcePackageVersionMinorAddress$;$theResourcePackageVersionSmallAddress$;$theResourcePackageVersionString$;")

                # Recipes
                GenerateBuffers = theXMLConfig.getPLCParameter("RecipeParameters:GenerateBuffers").strip().lower()
                if GenerateBuffers == "true":
                    if thePlcType.lower() == "quantum":
                        addr_HeaderBuffer = str(int(self.thePlugin.computeAddress("HeaderBuffer")) - 1)
                        addr_StatusBuffer = str(int(self.thePlugin.computeAddress("StatusBuffer")) - 1)
                        addr_ManRegAddrBuffer = str(int(self.thePlugin.computeAddress("ManRegAddrBuffer")) - 1)
                        addr_ManRegValBuffer = str(int(self.thePlugin.computeAddress("ManRegValBuffer")) - 1)
                        addr_ReqAddrBuffer = str(int(self.thePlugin.computeAddress("ReqAddrBuffer")) - 1)
                        addr_ReqValBuffer = str(int(self.thePlugin.computeAddress("ReqValBuffer")) - 1)
                    else:
                        addr_HeaderBuffer = self.thePlugin.computeAddress("HeaderBuffer")
                        addr_StatusBuffer = self.thePlugin.computeAddress("StatusBuffer")
                        addr_ManRegAddrBuffer = self.thePlugin.computeAddress("ManRegAddrBuffer")
                        addr_ManRegValBuffer = self.thePlugin.computeAddress("ManRegValBuffer")
                        addr_ReqAddrBuffer = self.thePlugin.computeAddress("ReqAddrBuffer")
                        addr_ReqValBuffer = self.thePlugin.computeAddress("ReqValBuffer")

                    BufferSize = str(params[2])
                    activationTimeout = str(params[3])
                    activationFunction = ""

                    self.thePlugin.writePlcDeclaration('''
#
# Object: UnRcpBuffers
#
#Config Line : DeviceTypeName;deviceNumber;Alias;Description;Diagnostics;WWWLink;Synoptic;Domain;Nature;WidgetType;addr_HeaderBuffer;addr_StatusBuffer;BufferSize;addr_ManRegAddrBuffer;addr_ManRegValBuffer;addr_ReqAddrBuffer;addr_ReqValBuffer;activationFunction;activationTimeout;
#
CPC_RcpBuffers;1;_unPlc_$thePlcName$_CPC_RcpBuffers;$thePlcName$ Recipe Buffers;;;;;;RcpBuffersState;$addr_HeaderBuffer$;$addr_StatusBuffer$;$BufferSize$;$addr_ManRegAddrBuffer$;$addr_ManRegValBuffer$;$addr_ReqAddrBuffer$;$addr_ReqValBuffer$;$activationFunction$;$activationTimeout$;
''')

    def end(self):
        self.thePlugin.writeInUABLog("Application Generation rules: end")

    def shutdown(self):
        self.thePlugin.writeInUABLog("Application Generation rules: shutdown")
