# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for WordParameter Objects.

import WinCCFlex_Generic_Template
reload(WinCCFlex_Generic_Template)


class WordParameter_Template(WinCCFlex_Generic_Template.Generic_Template):
    theDeviceType = "WordParameter"

    def processInstance(self, instance, params):
        self.writeScriptTag(instance, 'visible', 'Bool')
        self.writeScriptTag(instance, 'name', 'String')
        self.writeScriptTag(instance, 'unit', 'String')
        self.writeScriptTag(instance, 'setValue', 'Bool')
        self.writeScriptTag(instance, 'ManReg01', self.integer)
        self.writeScriptTag(instance, 'PosSt', self.integer)
        self.writeScriptTag(instance, 'MPosR', self.integer)
        self.writeScriptTag(instance, 'MPosRSt', self.integer)

        self.writeSmartTagName(instance)
        self.writeSmartTagUnit(instance)
        self.writeTextListPattern(instance)
