# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for WordParameter Objects.

import TIAPortal_Generic_Template
reload(TIAPortal_Generic_Template)


class WordParameter_Template(TIAPortal_Generic_Template.Generic_Template):
    theDeviceType = "WordParameter"

    def processInstance(self, instance, params):
        self.writeScriptTag(instance, 'visible', 'Bool')
        self.writeScriptTag(instance, 'objName', 'WString')
        self.writeScriptTag(instance, 'unit', 'WString')
        self.writeScriptTag(instance, 'setValue', 'Bool')
        self.writeScriptTag(instance, 'ManReg01', self.integer)
        self.writeScriptTag(instance, 'PosSt', self.integer)
        self.writeScriptTag(instance, 'MPosR', self.integer)
        self.writeScriptTag(instance, 'MPosRSt', self.integer)

        self.writeSmartTagName(instance)
        self.writeSmartTagUnit(instance)
        self.writeTextListPattern(instance)
