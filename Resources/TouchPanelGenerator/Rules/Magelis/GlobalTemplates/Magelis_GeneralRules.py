# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
from research.ch.cern.unicos.templateshandling import IUnicosTemplate
from research.ch.cern.unicos.plugins.interfaces import APlugin
from time import strftime
from java.lang import System

import Magelis_GenericFunctions
reload(Magelis_GenericFunctions)
from Magelis_GenericFunctions import *


class ApplicationGeneral_Template(IUnicosTemplate, Magelis_GenericFunctions.GenericFunctions):
    thePlugin = 0
    isDataValid = 1

    def initialize(self):
        self.thePlugin = APlugin.getPluginInterface()
        self.thePlugin.writeInUABLog("Application Generation rules: initialize")

    def check(self):
        self.thePlugin.writeInUABLog("Application Generation rules: check")

    def begin(self):
        self.thePlugin.writeInUABLog("Application Generation rules: begin")
        self.dateAndTime = strftime("%Y-%m-%d %H:%M:%S")
        self.theUserName = System.getProperty("user.name")
        # Plugin Information
        self.thePluginId = self.thePlugin.getId()
        self.thePluginVersion = self.thePlugin.getVersionId()
        # Application information
        self.theUniqueId = self.thePlugin.getApplicationUniqueID()
        self.theApplicationVersion = self.thePlugin.getApplicationVersion()

    def process(self, *params):
        self.thePlugin.writeInUABLog("Application Generation rules: processApplicationData for Magelis")
        theXMLConfig = params[0]
        # write Header for the mapped variables generation file
        self.writeInstanceInfo(1, "<?xml version=\"1.0\" encoding=\"UTF-16\"?>")
        self.writeInstanceInfo(1, "<VariableData version=\"5.1\">")
        self.writeInstanceInfo(3, "</VariableData>")
        # TODO: set output as xml

    def end(self):
        self.thePlugin.writeInUABLog("Application Generation rules: end")

    def shutdown(self):
        self.thePlugin.writeInUABLog("Application Generation rules: shutdown")
