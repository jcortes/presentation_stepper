# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for WordStatus Objects.

import Magelis_Generic_Template
reload(Magelis_Generic_Template)


class WordStatus_Template(Magelis_Generic_Template.Generic_Template):
    theDeviceType = "WordStatus"

    def processInstance(self, instance, params):
        self.writeVariable(instance, "PosStWord", "UINT", {'DeviceAddress': self.computeAddress(instance, "PosSt")})
        self.writeVariable(instance, "Name", "STRING")
        self.writeVariable(instance, "Description", "STRING")
        self.processPattern(instance)

    def processPattern(self, instance):
        widgetType = instance.getAttributeData("SCADADeviceGraphics:Widget Type")
        if "Bit" in widgetType:
            self.writeWarning(instance, "skip text list generation for WSBit type.")
            return
        try:
            pattern = self.getWordStatusPattern(instance)

            self.writeRawVariable("nbmot", "UINT", {'Source': 'Internal', 'Sharing': 'Read/Write', 'InitialValue': str(len(pattern))})
            maxNum = max(map(lambda pair: int(pair[0]), pattern)) + 1 if len(pattern) > 0 else "16"

            self.writeInstanceInfo(2, '''<Variable name="WordList">
              <ArrayType elementType="STRING">
                <NoOfBytes>40</NoOfBytes>
                <ArrayDimension size="$maxNum$"/>
              </ArrayType>
              <Source>Internal</Source>
              ''')
            for key, value in pattern:
                self.writeInstanceInfo(2, '''<Variable name="[$key$]">
                <InitialValue>$value$</InitialValue>
                <Sharing>Read/Write</Sharing>
                </Variable>
                ''')
            self.writeInstanceInfo(2, "</Variable>")
        except Exception, e:
            self.writeError(instance, "Can't process pattern: " + str(e))
