# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# $LastChangedRevision$
# Jython source file for WordParameter Objects.

import Magelis_Generic_Template
reload(Magelis_Generic_Template)


class WordParameter_Template(Magelis_Generic_Template.Generic_Template):
    theDeviceType = "WordParameter"

    def processInstance(self, instance, params):
        self.writeVariable(instance, "ManReg01", "UINT")
        self.writeVariable(instance, "MPosR", "UINT")
        self.writeVariable(instance, "MPosRSt", "UINT")
        self.writeVariable(instance, "PosSt", "REAL")
        self.writeVariable(instance, "Name", "STRING")
        self.writeVariable(instance, "Unit", "STRING")
        self.writeVariable(instance, "Description", "STRING")
