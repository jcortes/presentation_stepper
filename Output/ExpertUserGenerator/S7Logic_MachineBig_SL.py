# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Encoding UTF-8 without BOM test line with accent: é
from java.util import Vector
from java.util import ArrayList
import S7Logic_DefaultAlarms_Template
# reload(S7Logic_DefaultAlarms_Template)


def SLLogic(thePlugin, theRawInstances, master, name, LparamVector):

    Lparam1, Lparam2, Lparam3, Lparam4, Lparam5, Lparam6, Lparam7, Lparam8, Lparam9, Lparam10 = S7Logic_DefaultAlarms_Template.getLparametersSplit(LparamVector)
	

    
    # Automatically generated variables
    ButtonB = "Button04"
    MotorA = "Motor03"
    PCOName = "MachineBig"
    MotorB = "Motor04"

# Step 1: Create the FUNCTION called PCOName_SectionName.
    thePlugin.writeSiemensLogic('''
FUNCTION $name$_SL : VOID
TITLE = '$name$_SL'
//
// Sequencer Logic of $name$
//
(*
 Lparam1: $Lparam1$
 Lparam2: $Lparam2$
 Lparam3: $Lparam3$
 Lparam4: $Lparam4$
 Lparam5: $Lparam5$
 Lparam6: $Lparam6$
 Lparam7: $Lparam7$
 Lparam8: $Lparam8$
 Lparam9: $Lparam9$
 Lparam10:  $Lparam10$
*)
//
AUTHOR: 'ICE/PLC'
NAME: 'Logic_SL'
FAMILY: 'SL'
VAR_TEMP
    ButtonA: BOOL;
    MotorB_PositionIn: BOOL;
    MotorB_PositionOut: BOOL;

END_VAR
BEGIN

''')
    thePlugin.writeSiemensLogic('''
// ----------------------------------------------------- USER code <begin>------------------------------------------------------------
''')

    thePlugin.writeSiemensLogic('''(* Automatic generated code from StepperSpecs.xlsx using the Siemens_Expert_Stepper_Template <begin> *)
''')

    thePlugin.writeSiemensLogic('''(* VARIABLES COMPUTATION *)

ButtonA := DB_DI_ALL.DI_SET.Button03.PosSt;
MotorB_PositionIn := DB_DI_ALL.DI_SET.Motor04_PositionIn.PosSt;
MotorB_PositionOut := DB_DI_ALL.DI_SET.Motor04_PositionOut.PosSt;

''')

    thePlugin.writeSiemensLogic('''Graph_Machine_Var.Graph_Big();

(* Automatic generated code from StepperSpecs.xlsx using the Siemens_Expert_Stepper_Template <end> *)''')

    thePlugin.writeSiemensLogic('''
// ----------------------------------------------------- USER code <end>------------------------------------------------------------
''')
