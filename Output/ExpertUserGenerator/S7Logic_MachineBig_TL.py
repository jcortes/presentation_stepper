# -*- coding: utf-8 -*-
# UNICOS
# (c) Copyright CERN 2013 all rights reserved
# Encoding UTF-8 without BOM test line with accent: é
from java.util import Vector
from java.util import ArrayList
import S7Logic_DefaultAlarms_Template
# reload(S7Logic_DefaultAlarms_Template)
import ucpc_library.shared_decorator
reload(ucpc_library.shared_decorator)


def TLLogic(thePlugin, theRawInstances, master, name, LparamVector):

    Lparam1, Lparam2, Lparam3, Lparam4, Lparam5, Lparam6, Lparam7, Lparam8, Lparam9, Lparam10 = S7Logic_DefaultAlarms_Template.getLparametersSplit(LparamVector)
	

    
    Decorator = ucpc_library.shared_decorator.ExpressionDecorator()
    # Automatically generated variables
    ButtonB = "Button04"
    MotorA = "Motor03"
    PCOName = "MachineBig"
    MotorB = "Motor04"

# Step 1: Create the FUNCTION called PCOName_SectionName.
    thePlugin.writeSiemensLogic('''
FUNCTION $name$_TL : VOID
TITLE = '$name$_TL'
//
// Transition Logic of $name$
//
(*
 Lparam1: $Lparam1$
 Lparam2: $Lparam2$
 Lparam3: $Lparam3$
 Lparam4: $Lparam4$
 Lparam5: $Lparam5$
 Lparam6: $Lparam6$
 Lparam7: $Lparam7$
 Lparam8: $Lparam8$
 Lparam9: $Lparam9$
 Lparam10:  $Lparam10$
*)
//
AUTHOR: 'ICE/PLC'
NAME: 'Logic_TL'
FAMILY: 'TL'
VAR_TEMP
    pco_transitions0 : WORD;
    pco_transitions0_bit AT pco_transitions0: ARRAY [0..15] OF BOOL;
    pco_transitions1 : WORD;
    pco_transitions1_bit AT pco_transitions1: ARRAY [0..15] OF BOOL;
    ButtonA: BOOL;
    MotorB_PositionIn: BOOL;
    MotorB_PositionOut: BOOL;

END_VAR
BEGIN

''')
    thePlugin.writeSiemensLogic('''
// ----------------------------------------------------- USER code <begin>------------------------------------------------------------
''')

    thePlugin.writeSiemensLogic('''

DB_ERROR_SIMU.$name$_TL_E := 0 ; // To complete
DB_ERROR_SIMU.$name$_TL_S := 0 ; // To complete

''')

    thePlugin.writeSiemensLogic('''(* Automatic generated code from StepperSpecs.xlsx using the Siemens_Expert_Stepper_Template <begin> *)
''')

    thePlugin.writeSiemensLogic('''(* VARIABLES COMPUTATION *)

ButtonA := DB_DI_ALL.DI_SET.Button03.PosSt;
MotorB_PositionIn := DB_DI_ALL.DI_SET.Motor04_PositionIn.PosSt;
MotorB_PositionOut := DB_DI_ALL.DI_SET.Motor04_PositionOut.PosSt;

''')

    thePlugin.writeSiemensLogic(Decorator.decorateExpression('''(* TRANSITIONS COMPUTATION *)

(* 1 TO 2 *) Graph_Big.SHUTDOWN_STOP := $PCOName$.EnRStartSt;
(* 2 TO 1 *) Graph_Big.STOP_SHUTDOWN := $PCOName$.FuStopISt OR $PCOName$.TStopISt;
(* 2 TO 3 *) Graph_Big.STOP_STANDBY := $PCOName$.RunOSt;
(* 3 TO 1 *) Graph_Big.STANDBY_SHUTDOWN := $PCOName$.FuStopISt OR $PCOName$.TStopISt;
(* 3 TO 2 *) Graph_Big.STANDBY_STOP := NOT $PCOName$.RunOSt;
(* 3 TO 4 *) Graph_Big.Start1 := ButtonA;
(* 3 TO 8 *) Graph_Big.STANDBY_MotorB_In := $ButtonB$;
(* 4 TO 1 *) Graph_Big.MotorA_In_SHUTDOWN := $PCOName$.FuStopISt OR $PCOName$.TStopISt;
(* 4 TO 2 *) Graph_Big.MotorA_In_STOP := NOT $PCOName$.RunOSt;
(* 4 TO 5 *) Graph_Big.IN_TO_STAY := $MotorA$_PositionIn;
(* 5 TO 1 *) Graph_Big.MotorA_Stay_SHUTDOWN := $PCOName$.FuStopISt OR $PCOName$.TStopISt;
(* 5 TO 2 *) Graph_Big.MotorA_Stay_STOP := NOT $PCOName$.RunOSt;
(* 5 TO 6 *) Graph_Big.STAY_TO_OUT := $MotorA$_TimerIn> TIME_TO_DINT(Graph_Big.MotorA_Stay.T)*1000.0;
(* 6 TO 1 *) Graph_Big.MotorA_Out_SHUTDOWN := $PCOName$.FuStopISt OR $PCOName$.TStopISt;
(* 6 TO 2 *) Graph_Big.MotorA_Out_STOP := NOT $PCOName$.RunOSt;
(* 6 TO 7 *) Graph_Big.OUT_TO_TIMEOUT := $MotorA$_PositionOut;
(* 7 TO 1 *) Graph_Big.MotorA_Timeout_SHUTDOWN := $PCOName$.FuStopISt OR $PCOName$.TStopISt;
(* 7 TO 2 *) Graph_Big.MotorA_Timeout_STOP := NOT $PCOName$.RunOSt;
(* 7 TO 8 *) Graph_Big.MotorA_Timeout_MotorB_In := $MotorA$_TimerIn> TIME_TO_DINT(Graph_Big.MotorA_Timeout.T)*1000.0;
(* 8 TO 1 *) Graph_Big.MotorB_In_SHUTDOWN := $PCOName$.FuStopISt OR $PCOName$.TStopISt;
(* 8 TO 2 *) Graph_Big.MotorB_In_STOP := NOT $PCOName$.RunOSt;
(* 8 TO 9 *) Graph_Big.MotorB_In_MotorB_Stay := MotorB_PositionIn;
(* 9 TO 1 *) Graph_Big.MotorB_Stay_SHUTDOWN := $PCOName$.FuStopISt OR $PCOName$.TStopISt;
(* 9 TO 2 *) Graph_Big.MotorB_Stay_STOP := NOT $PCOName$.RunOSt;
(* 9 TO 10 *) Graph_Big.MotorB_Stay_MotorB_Out := $MotorB$_TimerIn> TIME_TO_DINT(Graph_Big.MotorB_Stay.T)*1000.0;
(* 10 TO 1 *) Graph_Big.MotorB_Out_SHUTDOWN := $PCOName$.FuStopISt OR $PCOName$.TStopISt;
(* 10 TO 2 *) Graph_Big.MotorB_Out_STOP := NOT $PCOName$.RunOSt;
(* 11 TO 1 *) Graph_Big.MotorB_Timeout_SHUTDOWN := $PCOName$.FuStopISt OR $PCOName$.TStopISt;
(* 11 TO 2 *) Graph_Big.MotorB_Timeout_STOP := NOT $PCOName$.RunOSt;
(* 11 TO 3 *) Graph_Big.TIMEOUT_TO_STANDBY := $MotorB$_TimerOut> TIME_TO_DINT(Graph_Big.MotorB_Out.T)*1000.0;
'''))

    thePlugin.writeSiemensLogic('''
(* WORD STATUS FOR STEPS *)
DB_WS_ALL.WS_SET.MachineBigSt.AuPosR := INT_TO_WORD(Graph_Big.S_NO);
''')

    thePlugin.writeSiemensLogic('''
(* WORD STATUS FOR TRANSITIONS *)
pco_transitions0_bit[8]  := Graph_Big.SHUTDOWN_STOP;                                                         (* 0*)
pco_transitions0_bit[9]  := Graph_Big.STOP_SHUTDOWN;                                                         (* 1*)
pco_transitions0_bit[10] := Graph_Big.STOP_STANDBY;                                                          (* 2*)
pco_transitions0_bit[11] := Graph_Big.STANDBY_SHUTDOWN;                                                      (* 3*)
pco_transitions0_bit[12] := Graph_Big.STANDBY_STOP;                                                          (* 4*)
pco_transitions0_bit[13] := Graph_Big.Start1;                                                                (* 5*)
pco_transitions0_bit[14] := Graph_Big.STANDBY_MotorB_In;                                                     (* 6*)
pco_transitions0_bit[15] := Graph_Big.MotorA_In_SHUTDOWN;                                                    (* 7*)
pco_transitions0_bit[0]  := Graph_Big.MotorA_In_STOP;                                                        (* 8*)
pco_transitions0_bit[1]  := Graph_Big.IN_TO_STAY;                                                            (* 9*)
pco_transitions0_bit[2]  := Graph_Big.MotorA_Stay_SHUTDOWN;                                                  (*10*)
pco_transitions0_bit[3]  := Graph_Big.MotorA_Stay_STOP;                                                      (*11*)
pco_transitions0_bit[4]  := Graph_Big.STAY_TO_OUT;                                                           (*12*)
pco_transitions0_bit[5]  := Graph_Big.MotorA_Out_SHUTDOWN;                                                   (*13*)
pco_transitions0_bit[6]  := Graph_Big.MotorA_Out_STOP;                                                       (*14*)
pco_transitions0_bit[7]  := Graph_Big.OUT_TO_TIMEOUT;                                                        (*15*)
pco_transitions1_bit[8]  := Graph_Big.MotorA_Timeout_SHUTDOWN;                                               (* 0*)
pco_transitions1_bit[9]  := Graph_Big.MotorA_Timeout_STOP;                                                   (* 1*)
pco_transitions1_bit[10] := Graph_Big.MotorA_Timeout_MotorB_In;                                              (* 2*)
pco_transitions1_bit[11] := Graph_Big.MotorB_In_SHUTDOWN;                                                    (* 3*)
pco_transitions1_bit[12] := Graph_Big.MotorB_In_STOP;                                                        (* 4*)
pco_transitions1_bit[13] := Graph_Big.MotorB_In_MotorB_Stay;                                                 (* 5*)
pco_transitions1_bit[14] := Graph_Big.MotorB_Stay_SHUTDOWN;                                                  (* 6*)
pco_transitions1_bit[15] := Graph_Big.MotorB_Stay_STOP;                                                      (* 7*)
pco_transitions1_bit[0]  := Graph_Big.MotorB_Stay_MotorB_Out;                                                (* 8*)
pco_transitions1_bit[1]  := Graph_Big.MotorB_Out_SHUTDOWN;                                                   (* 9*)
pco_transitions1_bit[2]  := Graph_Big.MotorB_Out_STOP;                                                       (*10*)
pco_transitions1_bit[3]  := Graph_Big.MotorB_Timeout_SHUTDOWN;                                               (*11*)
pco_transitions1_bit[4]  := Graph_Big.MotorB_Timeout_STOP;                                                   (*12*)
pco_transitions1_bit[5]  := Graph_Big.TIMEOUT_TO_STANDBY;                                                    (*13*)
pco_transitions1_bit[6]  := false;                                                                           (*14*)
pco_transitions1_bit[7]  := false;                                                                           (*15*)


DB_WS_ALL.WS_SET.MachineBigTr.AuPosR := pco_transitions0;
DB_WS_ALL.WS_SET.MachineBigTr.AuPosR := pco_transitions1;

''')

    thePlugin.writeSiemensLogic('''
(* Automatic generated code from StepperSpecs.xlsx using the Siemens_Expert_Stepper_Template <end> *)''')

    thePlugin.writeSiemensLogic('''
// ----------------------------------------------------- USER code <end>------------------------------------------------------------
''')
